# InstaGrammar

InstaGrammar is a Javascript library that generates a random sentence according to a grammar.

A grammar is a set of syntactic rules written in a very easy to read and understand language.

## Installation

```
npm install instagrammar
```

## Usage

```
var instagrammar = require("instagrammar")

var grammar = "$start = Hello (World | Universe);"
var ig = new instagrammar.InstaGrammar(grammar);
var output = ig.generate();

// output is either "Hello World" or "Hello Universe".
// check home page for tutorial and syntax
```

## Manual 

Check http://instagrammar.surge.sh/ 