import { Expect, Test, TestCase, TestFixture, SetupFixture } from "alsatian";
import { InstaGrammarTemplate } from '../igt/engine';


@TestFixture("InstaGrammarTemplateTest Test")
export class InstaGrammarTemplateTest {


    @TestCase('', '', [''])
    @TestCase('a', '', ['a'])
    @TestCase('', '$start = a;', ['a'])
    @TestCase('Hi $a, how are you?', '$a = a;', ['Hi a, how are you?'])
    @TestCase('Hi $a and $a, how are you?', '$a <= a | b | c| d | e;', [
        'Hi a and a, how are you?',
        'Hi b and b, how are you?',
        'Hi c and c, how are you?',
        'Hi d and d, how are you?',
        'Hi e and e, how are you?'
    ])
    public runArray(template: string, grammar: string, expected: Array<string>) {
        let output = InstaGrammarTemplate.generate(template, grammar);
        Expect(expected).toContain(output);
    }


}
