import { Expect, Test, TestCase, TestFixture, SetupFixture } from "alsatian";
import { rules } from "../pp/rules";
import { lexer } from "../pp/lexer.js";
import { PreProcessor } from "../pp/engine";

@TestFixture("Preprocessor Test")
export class PPTest {
    preProcessor: PreProcessor;
    commentsPreProcessor: PreProcessor;

    @SetupFixture
    public setupFixture() {
        this.preProcessor = new PreProcessor(rules, lexer, 'START');
    }

    public run(input: string, expected: string) {
        Expect(this.preProcessor.process(input)).toBe(expected);
    }

    public runSingleDeclaration(input: string, expected: string) {
        let wrappedInput = `$start = ${input};`;
        let wrappedExpected = `$start = ${expected};`;
        this.run(wrappedInput, wrappedExpected);
    }

    @TestCase("(hello | a b)", '("hello" | "a b")')
    @TestCase("a | _ | b", '"a" | _ | "b"')
    public runOrTest(input: string, expected: string) {
        this.runSingleDeclaration(input, expected);
    }


    // TODO testare tutti i casi di escape
    @TestCase('a " b', '"a \\" b"')
    @TestCase('a \\; b', '"a ; b"')
    @TestCase('a \\> b', '"a > b"')
    @TestCase('a \\< b', '"a < b"')
    @TestCase('a \\! b', '"a ! b"')
    @TestCase('a \\_ b', '"a _ b"')
    @TestCase('a + b', '"a + b"')
    @TestCase('a +', '"a +"')
    @TestCase('a: b', '"a: b"')
    @TestCase('a? b', '"a? b"')
    @TestCase('+ b', '"+ b"')
    @TestCase('a \\{b\\}', '"a {b}"')
    public runEscapedTest(input: string, expected: string) {
        this.runSingleDeclaration(input, expected);
    }

    @TestCase('a^#mario', '"a"^#mario')
    @TestCase('a^0-4 a^2', '"a"^0-4 + "a"^2')
    public runApplyTest(input: string, expected: string) {
        this.runSingleDeclaration(input, expected);
    }

    @TestCase("hello", '"hello"')
    @TestCase("hello spank", '"hello spank"')
    @TestCase("à è ì ò ù", '"à è ì ò ù"')
    @TestCase("(hello)", '("hello")')
    @TestCase("a ([b])", '"a" + (["b"])')
    @TestCase("a (b (c d) e) f", '"a" + ("b" + ("c d") + "e") + "f"')
    @TestCase("a (b (c d) (d c) e) f", '"a" + ("b" + ("c d") + ("d c") + "e") + "f"')
    public runTextParenthesisTest(input: string, expected: string) {
        this.runSingleDeclaration(input, expected);
    }


    @TestCase("a ($b (c $d) ($d c) $e) f", '"a" + ($b + ("c" + $d) + ($d + "c") + $e) + "f"')
    @TestCase("$a a", '$a + "a"')

    @TestCase("a $a", '"a" + $a')
    @TestCase("$a $a", '$a + $a')
    @TestCase("$a^2 $a", '$a^2 + $a')
    @TestCase("$a <$a", '$a + <$a')
    public runVariablesTest(input: string, expected: string) {
        this.runSingleDeclaration(input, expected);
    }

    @TestCase('a (b) [<_] c', '"a" + ("b") + [<_] + "c"')
    public runMiscTest(input: string, expected: string) {
        this.runSingleDeclaration(input, expected);
    }

    @TestCase("$start = hello;\n $end = world;", '$start = "hello";\n $end = "world";')
    @TestCase("$start = a $a;\n $a = $start b;", '$start = "a" + $a;\n $a = $start + "b";')
    @TestCase("$start = $a $a; $a <= a | b;", '$start = $a + $a; $a <= "a" | "b";')
    @TestCase("$start = (a | b) => $a $a;", '$start = ("a" | "b") => $a + $a;')
    @TestCase("$start = a (a | b) => $a $a;", '$start = "a" + ("a" | "b") => $a + $a;')
    public runMultipleDeclaration(input: string, expected: string) {
        this.run(input, expected);
    }

}
