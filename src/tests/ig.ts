import { Expect, Test, TestCase, TestFixture, SetupFixture } from "alsatian";
import { InstaGrammar } from '../ig/engine';


@TestFixture("InstaGrammar Test")
export class InstaGrammarTest {

    public run(input: string, expected: string) {
        let output = InstaGrammar.generate(input);
        Expect(output).toBe(expected);
    }

    public runSingleSymbol(input: string, expected: string) {
        let wrappedInput = `$start = ${input};`;
        this.run(wrappedInput, expected);
    }

    @TestCase('a', 'a')
    @TestCase('(a)', 'a')
    @TestCase('(a b c d è)', 'a b c d è')
    @TestCase('a b c', 'a b c')
    @TestCase('a (b) c', 'a b c')
    @TestCase('(a (b)) c', 'a b c')
    @TestCase('a ((b) c d)', 'a b c d')
    public runAndBracket(input: string, expected: string) {
        this.runSingleSymbol(input, expected);
    }

    @TestCase('a <b', 'ab')
    @TestCase('a> b', 'ab')
    @TestCase('a> <b', 'ab')
    @TestCase('a> <b^2', 'abb')
    @TestCase('a\\> b', 'a> b')
    @TestCase('a\\>b', 'a>b')
    public runSnap(input: string, expected: string) {
        this.runSingleSymbol(input, expected);
    }

    @TestCase('a b^U', 'A b')
    @TestCase('a (b^U)', 'a B')
    @TestCase('(a b)^U', 'A b')
    public runUppercase(input: string, expected: string) {
        this.runSingleSymbol(input, expected);
    }

    public runArray(input: string, expected: Array<string>) {
        let output = InstaGrammar.generate(input);
        Expect(expected).toContain(output);
    }

    public runArraySingleSymbol(input: string, expected: Array<string>) {
        let wrappedInput = `$start = ${input};`;
        this.runArray(wrappedInput, expected);
    }

    @TestCase('[a]', ['a', ''])
    @TestCase('a^1-4', ['a', 'a a', 'a a a', 'a a a a'])
    public runTimes(input: string, expected: Array<string>) {
        this.runArraySingleSymbol(input, expected);
    }

    @TestCase('a | b', ['a', 'b'])
    @TestCase('a | b c', ['a', 'b c'])
    @TestCase('(a | b) c', ['a c', 'b c'])
    @TestCase('(a | b) [c]', ['a c', 'b c', 'a', 'b'])
    public runOr(input: string, expected: Array<string>) {
        this.runArraySingleSymbol(input, expected);
    }

    @TestCase('_', [''])
    @TestCase('a | _ | b | c', ['', 'a', 'b', 'c'])
    @TestCase('a (b) [<_] c', ['a bc', 'a b c'])
    public runEmpty(input: string, expected: Array<string>) {
        this.runArraySingleSymbol(input, expected);
    }

    @TestCase('       ', [''])
    @TestCase('', [''])
    public runEmptyGrammar(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }



    @TestCase('$start = $a; $a = b;', ['b'])
    @TestCase('$start = $a; $a = b | $a;', ['b'])
    @TestCase('$start = $a; $a = b | $a | $a b;', ['b', 'b b', 'b b b', 'b b b b', 'b b b b b', 'b b b b b b', 'b b b b b b b'])
    public runSymbol(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }

    @TestCase('$start = a;', ['a'])
    @TestCase('$start = $a $a; $a <= a | b | c | d | e | f;', ['a a', 'b b', 'c c', 'd d', 'e e', 'f f'])
    @TestCase('$start = (a | b) => $a $a;', ['a a', 'b b'])
    @TestCase('$start = a (a | b) => $a $a;', ['a a a', 'a b b'])
    @TestCase('$start = [a] => $a $a;', ['a a', ''])
    @TestCase('$start = ([a | b] c) => $a;', ['a c', 'b c', 'c'])
    @TestCase('$start = $a $a; $a <= a | b | c | d | e;', ['a a', 'b b', 'c c', 'd d', 'e e'])
    public runStrongBinding(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }

    @TestCase('$start = {a};', ['a'])
    @TestCase('$start = {a} {b};', ['a b', 'b a'])
    @TestCase('$start = {a}<, {b} e {c};', ['a, b e c', 'a, c e b', 'b, a e c', 'b, c e a', 'c, a e b', 'c, b e a'])
    public runShuffle(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }

    @TestCase('$start = #m #boss Enrico #f Giada #f Claudia;', ['Enrico Giada Claudia'])
    @TestCase('$start = (#m #boss Enrico #f Giada #f Claudia) ^#f;', ['Giada Claudia'])
    @TestCase('$start = (#m #boss Enrico #f Giada #f Claudia) ^#m;', ['Enrico'])
    @TestCase('$start = (#m #boss Enrico #f Giada #f Claudia) ^#boss;', ['Enrico'])
    @TestCase('$start = (#m Enrico (#f Giada | #f Claudia)) ^#f;',  ['Giada', 'Claudia'])
    @TestCase('$start = (#m Enrico (#f Giada #m Enrico)^##) ^#m;',  ['Enrico Giada Enrico'])
    @TestCase('$start = (#m Enrico (#f Giada #m Enrico)^##) ^#f;',  ['Giada Enrico'])
    @TestCase('$start = ($m | $f)^#m; $m = #m (Mario | Enrico); $f = #f (Claudia | Monica);', [
        'Mario', 'Enrico'
    ])
    /*@TestCase('$oggetto = #animale (#mammifero mucca) | #elettrodomestico forno;  $start = $oggetto^#animale;', [
        'mucca'
    ])*/
    public runTagsAndFilters(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }

    @TestCase('$start = a*10 | b;', ['a',  'b'])
    public runLocalWeight(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }

    @TestCase('$start = a | b (c | d);', ['1_1_1_1','1_1_1_1_1_1'])
    @TestCase('$start = a | b !(c | d);', ['1_1_1_1_2_2_2_1_1','1_1_1_1_2_2_2'])
    @TestCase('$start = a | b !(c | d) !(e | f);', [
        '1_1_1_1_2_2_1_1_2_2_4_1_1_1_1',
        '1_1_1_1_2_2_1_1_2_2_4'
    ])
    @TestCase('$start = a | b !(c | d !(e | f));', [
        '1_1_1_1_1_1_2_2_2_3_3_3',
        '1_1_1_1_1_1_2_2_2_3_3_3_1_1_1_1_2_2_2',
        '1_1_1_1_1_1_2_2_2_3_3_3_1_1_1_1_2_2_2_1_1'
    ])
    @TestCase('$start = a | !(b | c !(d | e | g | h) !(f | g));', [
        '1_1_1_1_1_1_1_4_4_1_1_2_2_8_9_9_1_1_1_1_1_1_4_4_1_1_2_2_8_1_1_1_1_1_1',
        '1_1_1_1_1_1_1_4_4_1_1_2_2_8_9_9_1_1_1_1_1_1_4_4_1_1_2_2_8',
        '1_1_1_1_1_1_1_4_4_1_1_2_2_8_9_9'
    ])
    @TestCase('$start = a | [b];', ['1_1'])
    @TestCase('$start = a | ![b];', ['1_1_2'])
    @TestCase('$start = a | b ![c];', ['1_1_1_2_2'])
    @TestCase('$start = a | b ![c | d];', [
        '1_1_1_1_2_3_3_1_1',
        '1_1_1_1_2_3_3'
    ])
    @TestCase('$start = a | b ![(c | d)];', [
        '1_1_1_1_2_2_3_3',
        '1_1_1_1_2_2_3_3_1_1'
    ])
    @TestCase('$start = !$a | b; $a = A | AA;', [
        '1_1_2_2_1',
        '1_1_2_2_1_1_1'
    ])
    @TestCase('$start = a | ({c} {d});', ['1_1'])
    @TestCase('$start = a | !({c} {d});', ['1_1_1_2_2'])
    @TestCase('$start = a | !({c} {d} [e]);', ['1_1_1_1_2_2'])
    @TestCase('$start = a | !({c} {d} ![e]);', ['1_1_1_1_2_4_4'])
    @TestCase('$start = a | !({c} {d} e^1-2);', ['1_1_1_1_2_2'])
    @TestCase('$start = a | !({c} {d} e^1-3);', ['1_1_1_1_2_2'])

    @TestCase('$start = a | !({c} {d} !(e | f));', [
        '1_1_1_1_1_2_2_4_4_1_1',
        '1_1_1_1_1_2_2_4_4'
    ])
    @TestCase('$start = a | mario !({c} {d} !(e | f));', [
        '1_1_1_1_1_1_2_2_4_4_4',
        '1_1_1_1_1_1_2_2_4_4_4_1_1'
    ])


    public runUnfoldingWeight(input: string, expected: Array<string>) {
        let ig = new InstaGrammar(input);
        let output = ig.generate();
        let weights = ig.env.prettyWeights();
        Expect(expected).toContain(weights);
    }

    @TestCase('$start = a | b (c | d);', ['a', 'b c', 'b d'])
    @TestCase('$start = a | b !(c | d);', ['a', 'b c', 'b d'])
    @TestCase('$start = a | b !(c | d) !(e | f);', ['a', 'b d e', 'b c e', 'b d f', 'b c f'])
    @TestCase('$start = a | b !(c | d !(e | f));', ['a', 'b c', 'b d e', 'b d f'])
    @TestCase('$start = a | !(b | c !(d | e | g | h) !(f | g));', ['a', 'b',
        'c d f', 'c e f', 'c g f', 'c h f',
        'c d g', 'c e g', 'c g g', 'c h g'
    ])
    @TestCase('$start = a | [b];', ['a', 'b', ''])
    @TestCase('$start = a | ![b];', ['a', 'b', ''])
    @TestCase('$start = a | b ![c];', ['a', 'b', 'b c'])
    @TestCase('$start = a | b ![c | d];', ['a', 'b', 'b c', 'b d'])
    @TestCase('$start = a | b ![(c | d)];', ['a', 'b', 'b c', 'b d'])
    @TestCase('$start = !$a | b; $a = A | AA;', ['b', 'A', 'AA'])

    @TestCase('$start = a | ({c} {d});', ['a', 'c d', 'd c'])
    @TestCase('$start = a | !({c} {d});', ['a', 'c d', 'd c'])
    @TestCase('$start = a | !({c} {d} [e]);', ['a', 'c d', 'd c', 'c d e', 'd c e'])
    @TestCase('$start = a | !({c} {d} ![e]);', ['a', 'c d', 'd c', 'c d e', 'd c e'])
    @TestCase('$start = a | !({c} {d} e^1-2);', ['a', 'c d e e', 'd c e e', 'c d e', 'd c e'])
    @TestCase('$start = a | !({c} {d} !(e | f));', ['a', 'c d e', 'd c e', 'c d f', 'd c f'])
    @TestCase('$start = a | m !({c} {d} !(e | f));', ['a', 'm c d e', 'm d c e', 'm c d f', 'm d c f'])
    public runUnfolding(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }


    @TestCase('// ciao sono un commento \n $start = a;', ['a'])
    @TestCase('$start = a // ciao \n;', ['a'])
    @TestCase('$a = b; // ciao sono un commento \n $start = a;', ['a'])
    @TestCase('/* comment \n multiline \n */  $start = a;', ['a'])
    @TestCase('/* comment \n multiline \n */  $start = a /* mario \n */;', ['a'])
    @TestCase('/* comment \n multiline \n */  $start = a; /* comment \n multiline \n */ ', ['a'])
    public runComment(input: string, expected: Array<string>) {
        this.runArray(input, expected);
    }



}
