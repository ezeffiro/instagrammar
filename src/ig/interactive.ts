import * as util from "util";
import * as chalk from "chalk";

import { InstaGrammar } from './engine';
import { ReadLine, createInterface } from "readline";

function processGrammar(input: string, verbose = true) {
    try {
        let ig = new InstaGrammar(input);
        if (verbose) {
            console.log('input');
            console.log(input.trim());

            console.log('preprocessed');
            console.log(ig.preProcessedInput.trim());

            console.log('ast');
            console.log(util.inspect(ig.ast, { colors: true, depth: null }));

            console.log('bindings');
            console.log(util.inspect(ig.bindings, { colors: true, depth: null }));
        }

        let ouput = ig.generate();

        if (verbose) {
            console.log('bindings after generation');
            console.log(util.inspect(ig.bindings, { colors: true, depth: null }));

            console.log('tokens');
            console.log(util.inspect(ig.env.tokens, { colors: true, depth: null }));

            console.log('weights');
            console.log(ig.env.prettyWeights());

            console.log('output');
        }
        console.log(ouput);
    }
    catch (e) {
        console.log(e.message);
    }
}

function help() {
    console.log(" - \\n\\n to process grammar");
    console.log(" - $ to clear buffer");
    console.log(" - ! to generate again");
    console.log(" - # to generate many");
    console.log(" - v toggle verbose mode ");
    console.log(" - h to show this help");
}

function main() {
    console.log("InstaGrammar Interactive - h for help");

    const readLine = createInterface({
        input: process.stdin,
        output: process.stdout
    });
    let lines: Array<string> = [];
    let firstReturnCaught = false;
    let latestGrammar = "";
    let verbose = true;

    readLine.on('line', (input) => {
        if (input == '!' && latestGrammar != '') {
            processGrammar(latestGrammar, verbose);
        }
        else if (input == 'h') {
            help();
        }
        else if (input == '$') {
            lines = [];
        }
        else if (input == 'v') {
            verbose = !verbose;
            console.log(`verbose mode: ${verbose}`);
        }
        else if (input == '#') {
            for (let i = 0; i < 10; i++) {
                processGrammar(latestGrammar, false);
            }
        }
        else if (input == '') {
            if (firstReturnCaught) {
                latestGrammar = lines.join('\n');
                processGrammar(latestGrammar, verbose);
                lines = [];
                firstReturnCaught = false;
            }
            else {
                firstReturnCaught = true;
            }
        }
        else {
            lines.push(input);
        }
        readLine.prompt();

    });

    readLine.prompt();
}


main();
