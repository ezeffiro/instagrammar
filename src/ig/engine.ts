/*
    InstaGrammar main class 
*/

import { PreProcessor } from '../pp/engine';
import { rules } from "../pp/rules";
import { lexer } from "../pp/lexer.js";
import { parse as instaGrammarParse } from "./parser.js";
import { Node } from "./node";
import { Environment } from "./env";


/*
    InstaGrammar is the main class.
*/
class InstaGrammar {
    private readonly preProcessor: PreProcessor;
    readonly input: string;
    readonly commentsPreProcessedInput: string;
    readonly preProcessedInput: string;
    readonly ast: any;
    readonly bindings: { [id: string] : Node; };
    env: Environment;

    /*
        The constructor is a lengthy operation, since it has to:
            - preprocess the grammar (string => string)
            - parse the preprocessed grammar (string => ast)
            - convert the ast in dictionary of named nodes (bindings) (ast => bindings)

        All these intermediate structures are kept in the object for debugging purposes.
        This method can throw a number of parsing/lexing exception, so it must be called in
        a try-catch block.
    */
    constructor(input: string) {
        this.preProcessor = new PreProcessor(rules, lexer, 'START');
        this.input = input;
        const regexp = /\/\*(.|\n)*?\*\/|\/\/.*$/gm;
        this.commentsPreProcessedInput = this.input.replace(regexp, '');
        this.preProcessedInput = this.preProcessor.process(this.commentsPreProcessedInput);
        this.ast = instaGrammarParse(this.preProcessedInput);
        let bindings: { [id: string] : Node; } = {};
        if (Object.keys(this.ast).length > 0) {
            for (let binding of this.ast) {
                bindings[binding.name] = Node.factory(binding);
            }
        }
        this.bindings = bindings;
    }

    /*
        generate is the method to call on a InstaGrammar object.
            - startSymbol is the grammar entruy point
            - keepStrongBindings is used when you are going to call generate multiple times on the
                same object and you want to re-use strongBindings (CFR => InstaGrammarTemplate)
    */
    generate(startSymbol = '$start', keepStrongBindings = false): string {
        let strongBindings = {};
        if (keepStrongBindings && (this.env != null)) {
            strongBindings = this.env.strongBindings;
        }
        this.env = new Environment(this.bindings, strongBindings, startSymbol);
        return this.env.generate();
    }

    /*
        generate is a static convenience method useful when you need a generation only once,
        or when you don't care about performances.
    */
    static generate(input: string, startSymbol = '$start'): string {
        return (new InstaGrammar(input)).generate(startSymbol);
    }
}


export { InstaGrammar };
