/*
    Node.ts contains the whole InstaGrammar logic.
*/

import { Environment, PruningEnvironment, Token, UppercaseToken, SnapToken, TextToken } from './env';


function randomIntBetween(a: number, b: number): number {
    var diff = b - a;
    return Math.floor ((Math.random() * (diff + 1)) + a);
}


enum NodeType {
    Text = 'text',
    Symbol = 'symbol',
    Bracket = 'bracket',
    And = 'and',
    Or = 'or',
    Empty = 'empty'
}


/*
    Node is a super type with some abstract method that MUST be implemented by subclasses.
    Hyerarchy:
        - Node
            - Text
            - Symbol
            - Bracket
            - Empty
            - Children
                - And
                - Or

    A node is created for each AST object, using class method Node.factory
    All members are readonly (as to prevent accidental modifications during use),
    except for unfolded and computedWeight which are only used for debugging and
    automated testing purposes
*/
abstract class Node {
    readonly minTimes?: number;
    readonly maxTimes?: number;
    readonly leftSnap?: boolean;
    readonly rightSnap?: boolean;
    readonly strongBinding?: string;
    readonly shuffle?: boolean;
    readonly uppercase?: boolean;
    readonly tags?: Array<string>;
    readonly filters?: Array<string>;
    readonly resetFilters?: boolean;
    readonly localWeight?: number;
    readonly unfold?: boolean;

    computedWeight?: number;
    unfolded?: boolean;

    constructor(nodeAst: any) {
        if (nodeAst.minTimes != null) { this.minTimes = nodeAst.minTimes; }
        if (nodeAst.maxTimes != null) { this.maxTimes = nodeAst.maxTimes; }
        if (nodeAst.leftSnap != null) { this.leftSnap = nodeAst.leftSnap; }
        if (nodeAst.rightSnap != null) { this.rightSnap = nodeAst.rightSnap; }
        if (nodeAst.strongBinding != null) { this.strongBinding = nodeAst.strongBinding; }
        if (nodeAst.shuffle != null) { this.shuffle = nodeAst.shuffle; }
        if (nodeAst.uppercase != null) { this.uppercase = nodeAst.uppercase; }
        if (nodeAst.tags != null) { this.tags = nodeAst.tags; }
        if (nodeAst.filters != null) { this.filters = nodeAst.filters; }
        if (nodeAst.resetFilters != null) { this.resetFilters = nodeAst.resetFilters; }
        if (nodeAst.localWeight != null) { this.localWeight = nodeAst.localWeight; }
        if (nodeAst.unfold != null) { this.unfold = nodeAst.unfold; }
    }

    /*
        Returns the number of time this node must be generated (es: N^2 => 2)
    */
    randomTimes(): number {
        if (this.minTimes != null && this.maxTimes != null) {
            return randomIntBetween(this.minTimes, this.maxTimes);
        }
        else {
            return 1;
        }
    }

    /*
        Returns node weight, calling abstract method "customWeight" on self.
        This method CANNOT be overriden. Instead, override customWeight.
        Quite a complicated logic has been implemented for unfolding.
    */
    weight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean = false, visits: Array<string>): number {
        let weight = 0;
        filters = this.updateFilters(filters);
        unfold = unfold || this.unfold;
        if (unfold) {
            this.unfolded = true;
        }
        if (!this.excludedByFilters(filters)) {
            weight = 1;
            weight = this.customWeight(pruningEnv, filters, unfold, visits, weight);
            if (unfold == true && this.minTimes != null && this.maxTimes != null && weight > 0) {
                // nel caso di una produzione opzionale unfoldata, es ![a | b | c],
                // il peso non è 3*2 ma 3+1 perché la produzione vuota è unica
                // indipendentemente dal peso dell'OR.
                // Invece nel caso di una produzione iterata tipo A^2-4
                // il peso è n*(4-2+1)
                if (this.minTimes == 0) {
                    weight = (weight * (this.maxTimes - this.minTimes)) + 1;
                }
                else {
                    weight = weight * ((this.maxTimes - this.minTimes) + 1);
                }
            }
        }
        pruningEnv.weights.push(weight);
        this.computedWeight = weight;
        return weight;
    }

    /*
        This method MUST be implmented by subclasses, and CANNOT be called directly.
        If a subclass need to know the weight of a child, it has to call weight.
        superWeight is the weight computed by method weight; customWeight SHOULD use it
    */
    abstract customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number;

    /*
        Returns TRUE is the node is excluded by current filters.
        Used during generation and while pruning node graph.
    */
    excludedByFilters(filters: Array<string>): boolean {
        if (this.tags != null) {
            // if node has tags, check all filters (in AND)
            //console.log(`node has tags ${this.tags}`);
            for (let filter of filters) {
                if (this.tags.indexOf(filter) == -1) {
                    //console.log(`filter not found ${filter}, return true`);
                    return true;
                }
            }
            //console.log(`all filters found, return false`);
            return false;
        }
        else {
            // if node has no tags, it won't be excluded
            return false;
        }
    }

    /*
        Returns a NEW array of filters updated with node filters
    */
    updateFilters(filters: Array<string>): Array<string> {
        if (this.resetFilters === true) { return []; }
        else if (this.filters == null) { return [...filters]; }
        else {
            let newFilters = [...filters];
            for (let filter of this.filters) {
                if (newFilters.indexOf(filter) == -1) {
                    newFilters.push(filter);
                }
            }
            return newFilters;
        }
    }

    /*
        Returns an array of token starting from self.
        This is the most important method:
            - it handles the commong things every node has
            - it uses the abstract method customGenerate on subclasses
            - it uses and progatate env (trasversal) and filters (vertical)

    */
    generate(env: Environment, filters: Array<string>): Array<Token> {
        let tokens: Array<Token> = [];
        filters = this.updateFilters(filters);

        if (!this.excludedByFilters(filters)) {
            for (let i = 0; i < this.randomTimes(); i++) {
                if (this.uppercase) { tokens.push(new UppercaseToken()); }
                if (this.leftSnap) { tokens.push(new SnapToken()); }
                tokens = tokens.concat (this.customGenerate(env, filters));
                if (this.rightSnap) { tokens.push(new SnapToken()); }
            }
            // saving strongBinding
            if (this.strongBinding != null) {
                env.strongBindings[this.strongBinding] = tokens;
            }
        }
        return tokens;
    }

    /*
        Returns and array of tokens.
        This method MUST be implemented by subclasses, and is where the custom logic is written
    */
    abstract customGenerate(env: Environment, filters: Array<string>): Array<Token>;

    /*
        Static method to create a Node given an object from the parse.
        See grammar.jison
    */
    static factory(nodeAst: any): Node {
        let node: Node;
        switch (nodeAst.type) {
            case NodeType.Text:
                node = new TextNode(nodeAst);
                break;
            case NodeType.Symbol:
                node = new SymbolNode(nodeAst);
                break;
            case NodeType.Bracket:
                node = new BracketNode(nodeAst);
                break;
            case NodeType.And:
                node = new AndNode(nodeAst);
                break;
            case NodeType.Or:
                node = new OrNode(nodeAst);
                break;
            case NodeType.Empty:
                node = new EmptyNode(nodeAst);
                break;
            default:
                throw new Error(`Cannot create node for type ${nodeAst.type}`);
        }
        return node;
    }
}


/*
    Leaf node
*/
class TextNode extends Node {
    readonly contents: string;

    constructor(nodeAst: any) {
        super(nodeAst);
        this.contents = nodeAst.contents;
    }

    customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number {
        return superWeight;
    }

    customGenerate(env: Environment) {
        return[new TextToken(this.contents)];
    }
}


/*
    A Symbol node has a target which is the name of a binding
*/
class SymbolNode extends Node {
    readonly target: string;

    constructor(nodeAst: any) {
        super(nodeAst);
        this.target = nodeAst.target;
    }

    customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number {
        let childWeight = 1;
        // node already visited. Let's get back
        if (visits.indexOf(this.target) > -1) {
            // let's avoid recursion
            // TODO: are we sure it's a good thing? Uhm...
            return 0;
        }
        visits.push(this.target);
        /*
            child/children weight is only interesting if unfold, or if there are filters, because
            we want to deep-prune the graph
        */
        if (unfold || filters.length > 0) {
            let childNode = pruningEnv.lookupBinding(this.target);
            childWeight = childNode.weight(pruningEnv, filters, unfold, visits);
            superWeight *= childWeight;
        }

        return superWeight;
    }

    customGenerate(env: Environment, filters: Array<string>): Array<Token> {
        // tokens already generated and saved in a strongbinding
        if (env.strongBindings[this.target]) {
            return env.strongBindings[this.target];
        }
        let node = env.lookupBinding(this.target);
        let tokens = node.generate(env, filters);
        return tokens;
    }
}

/*
    BracketNode has exactly one child.
    Notice: it is used for any kind of bracket!
        - (a | b)
        - {a | b}
        - [a | b]
*/
class BracketNode extends Node {
    readonly child: Node;

    constructor(nodeAst: any) {
        super(nodeAst);
        this.child = Node.factory(nodeAst.child);
    }

    customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number {
        /*
            child/children weight is only interesting if unfold, or if there are filters, because
            we want to deep-prune the graph
        */
        if (unfold || filters.length > 0) {
            let childWeight = this.child.weight(pruningEnv, filters, unfold, visits);
            superWeight *= childWeight;
        }
        return superWeight;
    }

    customGenerate(env: Environment, filters: Array<string>): Array<Token> {
        return (this.child.generate(env, filters));
    }
}


/*
    Children has an array of children node.
    This class is never used directly, instead use subclasses And and Or
*/
abstract class ChildrenNode extends Node {
    readonly children: Array<Node>;

    constructor(nodeAst: any) {
        super(nodeAst);
        let children: Array<Node> = new Array();
        for (let childNodeAst of nodeAst.children) {
            children.push(Node.factory(childNodeAst));
        }
        this.children = children;
    }
}


/*
    AndNode. During generation, all of the children are generated
*/
class AndNode extends ChildrenNode {
    customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number {
        let childrenWeight = 0;
        let shuffleNodesNumber = 0; // all node can have weight = 0
        for (let child of this.children) {
            /*
             !(a {b} {c} [c])  =>
               1  1   1   1  => (1 * 1) *   ((n * (n -1)) * 1 * 1)
             !(a {b | g} {c} (a | b))
               1    1   1     1  => (1 * 1) * ((n * (n -1) * 1 * 1))
             !(a !{b | g} {c} !(a | b | c))
               1   1   1   1    1   1   1
               1     2     1        3
              1 * 3  * ((n * (n -1)) * 2  * 1
            */

            let childWeight = child.weight(pruningEnv, filters, false, visits);
            //console.log(`child weight is ${childWeight}`);
            if (childWeight > 0) {
                // in case all children weight is 0
                if (childrenWeight == 0) {
                    childrenWeight = 1;
                }
                childrenWeight *= childWeight;
                if (unfold && child.shuffle) {
                    shuffleNodesNumber++;
                }
            }
        }

        // with more than 1 shuffle node, number of permutations is n * (n - 1)
        if (shuffleNodesNumber > 1) {
            childrenWeight *= (shuffleNodesNumber * (shuffleNodesNumber - 1));
        }

        superWeight *= childrenWeight;
        return superWeight;
    }

    customGenerate(env: Environment, filters: Array<string>): Array<Token> {
        let tokens: Array<Token> = [];
        let shuffleNodes: Array<Node> = this.children.filter(function(node) {
            return (node.shuffle == true);
        });
        for (let node of this.children) {
            if (node.shuffle) {
                let choosen = randomIntBetween(0, shuffleNodes.length - 1);
                let choosenNode = shuffleNodes.splice(choosen, 1)[0];
                tokens = tokens.concat(choosenNode.generate(env, filters));
            } else {
                tokens = tokens.concat(node.generate(env, filters));
            }
        }
        return tokens;
    }
}


/*
    OrNode. During generation, ONE of the children is choosen and generated
*/
class OrNode extends ChildrenNode {
    customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number {
        let childrenWeight = 0;
        /*
            child/children weight is only interesting if unfold, or if there are filters, because
            we want to deep-prune the graph
        */
        if (unfold || filters.length > 0) {
            for (let child of this.children) {
                childrenWeight += child.weight(pruningEnv, filters, false, visits);
            }
            superWeight *= childrenWeight;
        }
        return superWeight;
    }

    customGenerate(env: Environment, filters: Array<string>): Array<Token> {
        let pruningEnv = new PruningEnvironment(env.bindings);
        let weights = new Array();
        /*
            This is a bit tricky. We need to consider relative node weight.
            Eg: $a = a | b !(c | d | e);
            weights:
                - a: 1
                - b !(c | d | e): 3
            so the weights array is:
                [0; 1; 1; 1]
        */
        for (let childIndex = 0; childIndex < this.children.length; childIndex++) {
            let w = this.children[childIndex].weight(pruningEnv, filters, false, []);
            if (this.children[childIndex].localWeight != null) {
                w *= this.children[childIndex].localWeight;
            }
            for (let j = 0; j < w; j++) {
                weights.push(childIndex);
            }
        }

        // saving all weights for testing purposes
        env.weights = env.weights.concat(pruningEnv.weights);

        if (weights.length > 0) {
            let randomIndex = randomIntBetween(0, weights.length - 1);
            let choosenNode = this.children[weights[randomIndex]];
            return (choosenNode.generate(env, filters));
        }
        else {
            // TODO: this should never happen thanks to deep pruning;
            // if it happens there is a bug somewhere, so better to check it
            // EDIT: ehm it happens a lot. :)
            // Must rethink the way tags&filters are managed
            throw new Error('Cannot choose an or node. It looks like the tags&filters in the grammar excluded completely a node');
        }
    }
}


/*
    Empty Node. Used with literal "_"
*/
class EmptyNode extends Node {
    customWeight(pruningEnv: PruningEnvironment, filters: Array<string>, unfold: boolean, visits: Array<string>, superWeight: number): number {
        return superWeight;
    }

    customGenerate(env: Environment, filters: Array<string>): Array<Token> {
        return [];
    }
}

export { Node, EmptyNode };
