%lex
%%

\s+                   /* skip whitespace */
\"(?:'\\'[\\"bfnrt\/]|'\\u'[a-fA-F0-9]{4}|[^\\\0-\x09\x0a-\x1f"]|\n)*\"    yytext = yytext.substr(1,yyleng-2); return 'TEXT'
\$[a-zA-Z0-9]+        return 'SYMBOL'
[0-9]+				        return 'NUMBER'
\#[a-zA-Z0-9]+        return 'TAG'
\^\#[a-zA-Z0-9]+      yytext = yytext.substr(1,yyleng-1); return 'FILTER'
"<="                  return 'STRONGBIND'
"=>"                  return 'INNERSTRONGBIND'
"="                   return 'BIND'
"("                   return 'BRA'
")"                   return 'KET'
"["                   return 'SQUAREBRA'
"]"                   return 'SQUAREKET'
"{"                   return 'CURLYBRA'
"}"                   return 'CURLYKET'
"^U"                  return 'UPPERCASE'
"^##"                 return 'RESETFILTERS'
"^"                   return 'TIMES'
"<"                   return 'LEFTSNAP'
">"                   return 'RIGHTSNAP'
"|"                   return 'OR'
"_"                   return 'EMPTY'
";"                   return 'SEMICOLON'
"+"                   return 'AND'
"-"                   return 'MINUS'
"!"                   return 'UNFOLD'
"*"                   return 'LOCALWEIGHT'

.                     return 'INVALID'
<<EOF>>               return 'EOF'

/lex



%left 'BIND'
%left 'STRONGBIND'
%left 'OR'
%left 'AND'
%right 'LEFTSNAP' 'UNFOLD' 'TAG'
%left 'RIGHTSNAP' 'LOCALWEIGHT' 'TIMES' 'INNERSTRONGBIND' 'UPPERCASE' 'FILTER'


%start START

%% /* language grammar */

START
  : BINDINGS EOF { $$ = $1; return($$); }
  | EOF { return {}; }
  ;

BINDINGS
  : BINDING             { $$ = [$1]; }
  | BINDINGS BINDING    { $$ = $1; $$.push($2); }
  ;

BINDING
  : SYMBOL BIND NODE SEMICOLON            { $3['name'] = $1; $$ = $3 }
  | SYMBOL STRONGBIND NODE SEMICOLON
    {
        $3['name'] = $3['strongBinding'] = $1;
        $$ = $3
    }
  ;

NODE
  : TAG NODE
    {
      if (!$2['tags']) { $2['tags'] = []; }
      $2['tags'].push($1);
      $$ = $2;
    }
  | NODE AND NODE
    {
      var l = new Array();
      ($1.type == "and") ? l = l.concat($1.children) : l.push($1);
      ($3.type == "and") ? l = l.concat($3.children) : l.push($3);
      $$ = { type: "and", "children": l };
    }
  | NODE OR NODE
    {
      var l = new Array();
      ($1.type == "or") ? l = l.concat($1.children) : l.push($1);
      ($3.type == "or") ? l = l.concat($3.children) : l.push($3);
      $$ = { type: "or", "children": l };
    }
  | BRA NODE KET              { $$ = { type: "bracket", child: $2 } }
  | SQUAREBRA NODE SQUAREKET
    {
      $$ = { type: "bracket", child: $2, minTimes: 0, maxTimes: 1 }
    }
  | CURLYBRA NODE CURLYKET
    {
      $$ = { type: "bracket", child: $2, shuffle: true }
    }
  | NODE LOCALWEIGHT NUMBER
    {
      var w = Number($3);
      if (w > 1) {
        $1['localWeight'] = w;
      }
      $$ = $1;
    }
  | LEFTSNAP NODE       { $2['leftSnap'] = true; $$ = $2 }
  | NODE RIGHTSNAP      { $1['rightSnap'] = true; $$ = $1 }
  | NODE UPPERCASE      { $1['uppercase'] = true; $$ = $1 }
  | UNFOLD NODE         { $2['unfold'] = true; $$ = $2 }
  | NODE INNERSTRONGBIND SYMBOL
    {
      $1['strongBinding'] = $3;
      $$ = $1;
    }
  | NODE TIMES NUMBER
    {
      $1['minTimes'] = Number($3);
      $1['maxTimes'] = Number($3);
      $$ = $1;
    }
  | NODE TIMES NUMBER MINUS NUMBER
    {
      $1['minTimes'] = Number($3);
      $1['maxTimes'] = Number($5);
      $$ = $1;
    }
  | TEXT                { $$ = { type: "text", contents: $1 } }
  | SYMBOL              { $$ = { type: "symbol", target: $1 } }
  | EMPTY               { $$ = { type: "empty" } }
  | NODE FILTER
    {
      if (!$1['filters']) { $1['filters'] = []; }
      $1['filters'].push($2);
      $$ = $1;
    }
  | NODE RESETFILTERS        { $1['resetFilters'] = true; $$ = $1 }
  ;
