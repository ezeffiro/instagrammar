/*
    The ENV objects are created fresh for each InstaGrammar generation
*/

import { Node, EmptyNode } from './node';

/*
    During generation, all output is managed by tokens
    In this way, "function" tokens (e.g. SNAP) can be folded
*/
abstract class Token {
    abstract readonly kind: string;
    readonly contents?: string;
}


class TextToken extends Token {
    kind = 'TextToken';
    readonly contents: string;
    constructor(contents: string) {
        super();
        this.contents = contents;
    }
}


class SnapToken extends Token {
    kind = 'SnapToken';
}


class UppercaseToken extends Token {
    kind = 'Uppercase';
}


/*
    Environment is the companion object for each InstaGrammar generation.
    Its main purposes are:
        - collects output tokens
        - collect strongBindings
        - record symbol visits (in order to avoid recursion, which is caught at runtime)
        - track node weight for automated testing purposes
*/

class Environment {
    static kRecursionThreshold: number = 100;
    tokens: Array<Token>;
    bindings: { [id: string] : Node; };
    strongBindings: { [id: string] : Array<Token>; };
    startSymbol: string;
    output: string;
    visits: { [id: string] : number; };
    weights: Array<number>; // used for automated testing

    prettyWeights(): string {
        return this.weights.join('_');
    }

    constructor(bindings: { [id: string] : Node; }, strongBindings: { [id: string] : Array<Token>; } = {}, startSymbol: string) {
        this.tokens = new Array();
        this.bindings = bindings;
        this.strongBindings = strongBindings;
        this.startSymbol = startSymbol;
        this.output = "";
        this.visits = {};
        this.weights = [];
    }

    /*
        lookups a binding given its name.
        It raises an exception if node is node found
    */
    lookupBinding(name: string): Node {

        // Empty grammar, we use the EmptyNode to have a working grammar anyway
        if (Object.keys(this.bindings).length == 0) {
            return new EmptyNode({});
        }

        if (this.bindings[name]) {
            if (this.visits[name]) {
                this.visits[name] += 1;
                if (this.visits[name] == Environment.kRecursionThreshold) {
                    throw new Error(`Possibile recursion found for binding ${name}`);
                }
            }
            else {
                this.visits[name] = 1;
            }
            return this.bindings[name];
        }
        else {
            throw new Error(`Binding not found: ${name}`);
        }
    }

    /*
        converts an array of tokens into a string
    */
    prettyTokens(): string {
        let output = "";
        let snap = true;
        let uppercase = false;
        for (let token of this.tokens){
            switch (token.kind) {
                case 'TextToken': {
                    if (snap) {
                        snap = false;
                    }
                    else {
                        output += " ";
                    }
                    if (uppercase) {
                        output += token.contents.charAt(0).toUpperCase();
                        output += token.contents.slice(1);
                        uppercase = false;
                    } else {
                        output += token.contents;
                    }
                    break;
                }
                case 'SnapToken': {
                    snap = true;
                    break;
                }
                case 'Uppercase': {
                    uppercase = true;
                    break;
                }
            }
        }
        return output;
    }

    /*
        Generate is the main method.
    */
    generate(): string {
        // strongBinding found: let's re-use the previously generate tokens
        if (this.strongBindings[this.startSymbol] != null) {
            this.tokens = this.strongBindings[this.startSymbol];
        }
        else {
            let startNode = this.lookupBinding(this.startSymbol);
            this.tokens = startNode.generate(this, []);
        }
        this.output = this.prettyTokens();
        return this.output;
    }
}


/*
    PruningEnvironment is used when pruning graph.
    Its main purposes are:
        - lookup binding without taking care of recursion detection
        - collect weight for testing purposes
*/

class PruningEnvironment {
    weights: Array<number>;
    bindings: { [id: string] : Node; };

    constructor(bindings: { [id: string] : Node; }) {
        this.weights = [];
        this.bindings = bindings;
    }

    lookupBinding(name: string): Node {
        if (this.bindings[name]) {
            return this.bindings[name];
        }
        else {
            throw new Error(`Binding not found: ${name}`);
        }
    }
}

export { Environment, PruningEnvironment, Token, UppercaseToken, SnapToken, TextToken };
