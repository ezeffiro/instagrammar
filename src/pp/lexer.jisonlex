%lex
%%

\s+                     return 'WHITESPACE'
\\[;!<>_#^${}()\[\]\*]  yytext = yytext.substr(1,yyleng-1); return 'TEXT'
'"'                     yytext = '\\"'; return 'TEXT'
";"                     return 'RESET'
"["                     return 'LEFT_KEYWORD'
"]"                     return 'RIGHT_KEYWORD'
"("                     return 'LEFT_KEYWORD'
")"                     return 'RIGHT_KEYWORD'
"{"                     return 'LEFT_KEYWORD'
"}"                     return 'RIGHT_KEYWORD'
"|"                     return 'NODE_SEPARATOR'
"_"                     return 'NODE'
"<"                     return 'LEFT_KEYWORD'
">"                     return 'RIGHT_KEYWORD'
"!"                     return 'LEFT_KEYWORD'
\*[0-9]+				return 'RIGHT_KEYWORD'
(\=\>\s*\$[a-zA-Z0-9]+)
                        return 'RIGHT_KEYWORD'
"<="                    return 'BIND_OR_STRONGBIND'
"="                     return 'BIND_OR_STRONGBIND'
\^\#[a-zA-Z0-9]+        return 'RIGHT_KEYWORD'
\^\#\#                  return 'RIGHT_KEYWORD'
\#[a-zA-Z0-9]+          return 'LEFT_KEYWORD'
\^[0-9]+[\-][0-9]+      return 'RIGHT_KEYWORD'
\^[0-9]+                return 'RIGHT_KEYWORD'
\^\#[a-zA-Z0-9]+        return 'RIGHT_KEYWORD'
"^U"                    return 'RIGHT_KEYWORD'
\$[a-zA-Z0-9]+          return 'NODE'
[^;\[\]\(\)\|\\"\{\}=#<>!\s\n\t\r\^\*]+
                        return 'TEXT'
<<EOF>>                 return 'EOF'
