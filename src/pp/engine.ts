/*
    PreProcessor engine

    Given a lexer and a set of rules, it converts a string into a preprocessed string.
    This module is bundled into InstaGrammar, but it could be standalone.


*/

import { parse as rulesParse } from "./rulesParser.js";

class Rule {
    fromState: string;
    tokens: Array<string>;
    toState?: string;
    text?: string;
    consume?: number;

    constructor(fromState: string, rule:any) {
        this.fromState = fromState;
        this.tokens = rule.tokens;
         if (rule.then) {
            if (rule.then.text) {
                this.text = rule.then.text;
            }
            if (rule.then.consume) {
                this.consume = rule.then.consume;
            }
            if (rule.then.state) {
                this.toState = rule.then.state;
            }
        }
    }

    /*
        Returns true if tokens matches the head of the stack
    */
    matchesStack(stack: Array<Token>): Boolean {
        if (stack.length >= this.tokens.length) {
            for (let i in this.tokens) {
                if (this.tokens[i] != stack[i].name) {
                    // the token "*" matches with anything
                    if (this.tokens[i] != '*') {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
}

class Token {
    readonly name: string;
    readonly text: string;

    constructor(name:string, text:string) {
        this.name = name;
        this.text = text;
    }
}

class PreProcessor {
    readonly lexer: any;
    readonly rules: Array<Rule>;
    readonly startState: string;

    constructor(rules:string, lexer: any, startState:string) {
        this.rules = this.parseRules(rules);
        this.lexer = lexer;
        this.startState = startState;
    }

    /*
        Converts the parser AST into a flatten array of PPRules
    */
    parseRules(rules: string): Array<Rule> {
        let localRules = new Array();
        let rulesets = null;
        try {
            rulesets = rulesParse(rules);
        }
        catch (e) {
            throw new Error(e.message);
        }
        for (let ruleset of rulesets) {
            for (let fromState of ruleset.states) {
                for (let rule of ruleset.rules) {
                    localRules.push(new Rule(fromState, rule));
                }
            }
        }
        return localRules;
    }


    /*
        Coverts a stack and a template string into a string
        Eg.: - T2 T3 T4 T1 T4 => "ok $1 $2 $3"

    */
    private processText(tokens:Array<Token>, template:string): string {
        let output = template;
        for (let i in tokens) {
            output = output.replace("$"+i, tokens[i].text);
        }
        return output;
    }

    /*
        Converts the input string into an array of tokens
    */
    private parseTokens(input:string): Array<Token> {
        let tokens = new Array();
        this.lexer.setInput(input);
        let name = "";
        while((name = this.lexer.lex()) != 1) {
            tokens.push(new Token(name, this.lexer.yytext));
        }
        return tokens;
    }

    /*
        Preprocess an input string
    */
    process(input:string): string {
        let tokens = this.parseTokens(input);
        let stack = [...tokens];
        let currentState = this.startState;
        let output = "";
        while (stack.length > 0) {
            // filtered rules according to state
            let filteredRules = this.rules.filter(function(rule) {
                return (rule.fromState == currentState) || (rule.fromState == '*');
            });
            let foundRule = false;

            for (let filteredRule of filteredRules) {
                if (filteredRule.matchesStack(stack)) {
                    // process rule
                    let oldState = currentState;

                    // text need to be processed
                    if (filteredRule.text) {
                        output += this.processText(stack.slice(0, filteredRule.tokens.length), filteredRule.text);
                    }

                    // moving FSM to new state
                    if (filteredRule.toState) {
                        currentState = filteredRule.toState;
                    }

                    // consuming input tokens
                    let tokensToConsume = filteredRule.tokens.length;
                    if (filteredRule.consume && filteredRule.consume < tokensToConsume) {
                        tokensToConsume = filteredRule.consume;
                    }

                    // popping consumed tokens out of stack
                    stack = stack.slice(tokensToConsume, stack.length);
                    foundRule = true;

                    break;
                }
            }
            if (!foundRule) {
                let prettyStack = "";
                let prettyStackSize = stack.length < 5 ? stack.length : 5;
                for (let i = 0; i<prettyStackSize; i++) {
                    prettyStack += `${stack[i].name} `;
                }
                let s = `Rule not found. State: ${currentState}, stack: ${prettyStack}`;
                throw new Error(s);
            }
        }
        return output;
    }
}

export { PreProcessor };
