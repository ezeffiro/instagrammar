%lex
%%

\s+                           /* skip whitespace */
\'(.)*\'                      yytext = yytext.substr(1,yyleng-2); return 'TEXT'
[a-zA-Z][a-zA-Z0-9\-_]*|\*    return 'SYMBOL'
":"                           return 'COLON'
","                           return 'COMMA'
";"                           return 'SEMICOLON'
[0-9]+                        return 'NUMBER'
"-"                           return 'MINUS'
"=>"                          return 'IMPLIES'
"*"                           return 'ANY'
.                             return 'INVALID'
<<EOF>>                       return 'EOF'

/lex



%start START

%% /* language grammar */

START
  : RULESETS EOF                   { $$ = $1; return($$) }
  ;

RULESETS
  : RULESET                       { $$ = [$1]; }
  | RULESETS RULESET              { $$ = $1; $$.push($2); }
  ;

RULESET
  : STATES COLON RULES SEMICOLON  { $$ = { states: $1, rules: $3 }  }
  ;

RULES
  : RULE                          { $$ = [$1]; }
  | RULES RULE                    { $$ = $1; $$.push($2); }
  ;

RULE
  : MINUS TOKENS IMPLIES THEN     { $$ = { tokens: $2, then: $4 }; }
  | MINUS TOKENS                  { $$ = { tokens: $2 }; }
  ;

THEN
  : THEN TEXT                     { $$ = $1; $$['text'] = $2; }
  | THEN SYMBOL                   { $$ = $1; $$['state'] = $2; }
  | THEN NUMBER                   { $$ = $1; $$['consume'] = $2; }
  | TEXT                          { $$ = { text: $1}; }
  | SYMBOL                        { $$ = { state: $1}; }
  | NUMBER                        { $$ = { consume: $1}; }
;

TOKENS
  : SYMBOL                        { $$ = [$1]; }
  | TOKENS  SYMBOL                { $$ = $1; $$.push($2); }
  ;

STATES
  : SYMBOL                        { $$ = [$1]; }
  | STATES COMMA SYMBOL           { $$ = $1; $$.push($3); }
  ;
