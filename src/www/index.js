var payoffApp = new Vue({
    el: '#payoffApp',
    data: {
        grammar: `
$start = InstaGrammar is a $library which
            {$generate a $sentence}
            {[ $according ]};
$library = [tiny | small] Javascript
            (library | framework | class);
$generate = generates | produces | calculates;
$sentence = [[pseudo] random] (sentence | text);
$according = \\(> according to a grammar
            ![definition | file | source] <\\);
`,
        output: "",
        ig: null,
        showSource: false
    },
    methods: {
        generate: function () {
            this.output = this.ig.generate();
        },
        source: function () {
            this.showSource = !this.showSource;
        }
    },
    beforeMount() {
        this.ig = new InstaGrammar.InstaGrammar(this.grammar);
        this.generate();
    }
});



var examples = {
    'hello':  {
        'title': 'Hello World',
        'text': `<p>
                    Let's start with a basic example: Hello World!
                </p>
                <p>
                    And now some technicalities:
                    <ul>
                        <li>A <em>grammar</em> is a collection of variable declarations called <em>bindings</em>.</li>
                        <li>A variable name must start with a <code>$</code>.</li>
                        <li>In every grammar there should be a starting variable called <code>$start</code>.</li>
                        <li>A binding must end with the keyword <code>;</code>. If you want to use the character
                            ';' in your text, you have to quote it: <code>\\;</code>.</li>
                    </ul>
                </p>`,
        'grammar': `$start = Hello World;`
    },
    'choices':  {
        'title': 'Choices',
        'text': `<p>
                    Let's go on with something more interesting: choices.
                </p>
                <p>
                    The keyword <code>|</code> tells the interpreter to choose one of the available choices,
                    in a random manner.
                </p>
                <p>
                    You can indent the grammar as you prefer. A good habit is to put a choice on a line of its own,
                    as in the example.
                </p>
                <p>
                    Technicalities:
                    <ul>
                        <li>The <code>|</code> has the lowest precedence among the operators.</li>
                        <li>If you want to use the character
                            '|' in your text, you have to quote it: <code>\\|</code>.
                        </li>
                    </ul>
                </p>`,
        'grammar': `$start = Hello World
       | Hello Earth
       | Hello Universe;`
    },
    'parenthesis':  {
       'title': 'Parenthesis',
       'text': `<p>
                   No one wants to repeat itself, keep it dry!
               </p>
               <p>
                   The parenthesis <code>(</code> and <code>)</code> will group text
                   (and more complicated structures) together.
               </p>
               <p>
                   Here is the above example rewritten with parenthesis.
               </p>`,
       'grammar': `$start = Hello (World | Earth | Universe);`
    },
    'variables':  {
       'title': 'Variables',
       'text': `<p>
                   Once your grammar gets complicated, you need to refactor it, as in every
                   other computer language.
               </p>
               <p>
                   You can create as many variables as you want, and you can use them in your
                   text. The order of declaration is not important: you can use a variable which
                   is defined later in the code.
               </p>
               <p>
                   Here is the above example rewritten with a couple of new variable.
               </p>`,
       'grammar': `$start = $hello $world;
$hello = Hello | Good (morning | evening);
$world = World | Earth | Universe;`
    },
    'recursion':  {
       'title': 'Recursion',
       'text': `<p>
                   A varible can appear also inside its definition.
               </p>
               <p>
                   To avoid infinite recursion issues, InstaGrammar implements a simple recursion detection algorithm:
                   while traversing the <em>generation tree</em>, InstaGrammar counts the number of times it finds a
                   variable: when the number reaches a threshold, it halts and issues a warning.
               </p>
               <p>
                   In the example, InstaGrammar generates a random length string with 0 and 1.
               </p>`,
       'grammar': `$start = binary string: $binary;
$binary = 0 | 1 | $binary $binary;`
    },
    'optional':  {
       'title': 'Optional',
       'text': `<p>
                   You can wrap a text in square brakets (<code>[</code> and <code>]</code>) to make it optional.
               </p>
               <p>
                   In the example, the optional text is nested twice.
               </p>`,
       'grammar': `$start = Hello [[my [beautiful]] World];`
    },
    'shuffle':  {
       'title': 'Shuffle',
       'text': `<p>
                   If you wrap two or more text in curly brackets (<code>{</code> and <code>}</code>), they will be shuffled.
               </p>
               <p>
                   The shuffle works for all shuffled <em>nodes</em> at the same level.
               </p>
               <p>
                    Notice two more things:
                    <ul>
                        <li>the <em>snap</em> keyword <code>&lt;</code> used to avoid spaces before comma;</li>
                        <li>the <em>uppercase</em> keyword <code>^U</code> used to make the first letter uppercase.</li>
                    </ul>
               <p>`,
        'grammar': `$start = ({me}<, {myself} and {Irene}<.)^U;`
    },
    'snap':  {
       'title': 'Snap',
       'text': `<p>
                   InstaGrammar uses the 'space' character to join the text tokens.
               </p>
               <p>
                   You can use <code>&lt;</code> before a text or <code>&gt;</code> after a text to snap!
               </p>`,
        'grammar': `$start = $language is the most $adjective language ever;
$language = (Type | Java)> Script;
$adjective = use <(less | ful);`
    },
    'empty':  {
       'title': 'Empty',
       'text': `<p>
                   Sometimes you need to generate... nothing!
               </p>
               <p>
                   In this case you can use the keyword <code>_</code>.
               </p>
               <p>
                   In the example, InstaGrammar will sometime generates just "Hello".
               </p>`,
       'grammar': `$start = Hello $world;
$world = World | Earth | Universe | _ ;`
    },
    'times':  {
       'title': 'Times',
       'text': `<p>
                   If you want to iterate the generation multiple times, you have to choices:
                   <ul>
                        <li>Iterate exactly N times: use this suffix <code>^N</code></li>
                        <li>Iterate between N and M times: use this suffix <code>^N-M</code></li>
                    </ul>
               </p>`,
       'grammar': `$start = I (really)^1-10 like^1 Instagrammar;`
    },
    'strongbinding':  {
       'title': 'Strong Binding',
       'text': `<p>
                    Every time the generator encounters a variable, it generates a fresh text.
               </p>
               <p>
                    Use the <code>&lt;=</code> operator to <em>save</em> the generate text for that symbol.
               </p>`,
       'grammar': `$start = $man loves $woman but $woman doesn't;
$man = Enrico | Matteo | Fortunato;
$woman <= Monica | Claudia | Giada | Mirta;`
    },
    'uppercase':  {
       'title': 'Uppercase',
       'text': `<p>
                    You can use the suffix <code>^U</code> after a text to render it uppercase (just the first letter).
               </p>`,
       'grammar': `$start = ([today is | it's] a nice day.)^U;`
   },
   'tagsfilters':  {
      'title': 'Tags and Filters',
      'text': `<p>
                    You can prefix one or more <code>#tag</code> before a text.
              </p>
              <p>
                    You can apply a tag by suffixing <code>^#tag</code>.
              </p>
              <p>
                    Notice that Andrea and Jean are both masculine and feminine: in this case you can specify all the tags or none.
              </p>`,
      'grammar': `$start = $name^#m married $name^#f
        and had a child named $name;
$name = #m Enrico
      | #m Matteo
      | #f #m Andrea
      | Jean
      | #f Claudia
      | #f Monica;`
   },
   'filtersreset':  {
      'title': 'Filters reset',
      'text': `<p>
                    The suffix <code>^##</code> will reset the filters, down the generation tree.
              </p>
              <p>
                    The example generates an even two-digits number, and the first number can be, of course, any number.
            </p>`,
      'grammar': `$start = $twoDigits^#even;
$twoDigits = $number^## <$number;
$number = #even (0 | 2 | 4 | 6 | 8)
        | #odd (1 | 3 | 5 | 7 | 9);
`
   },
   'localweight':  {
    'title': 'Local Weight',
    'text': `<p>
                  The suffix <code>*N</code> will change the probability of the preceding node. It works only inside a choice.
            </p>
            <p>
                  In the following example, the probability to see <em>TypeScript</em> is 20 times higher than <em>JavaScript</em>.
          </p>`,
    'grammar': `$start = I love (Type*20 | Java) <Script;`
    }
    ,
   'unfolding-choice':  {
    'title': 'Unfolding a choice',
    'text': `<p>
                With <em>unfolding</em> you can flatten the probability of a nested node, in case of a choice.             
            </p>
            <p>
                Consider this example: <br/> <code>$s = a | (b | c);</code>. <br/>
                Without unfolding, <code>a</code> has 50% probability such as <code>(b | c)</code>. 
                In turn, <code>b</code> has 50% probability versus <code>c</code>. 
                Overall, <code>a</code> has 50%, <code>b</code> 25% and <code>c</code> 25%. 
          </p>
          <p>
            Let's rewrite it with unfolding. <br/>
            <code>$s = a | !(b | c);</code>. <br/>    
            Now all letters have the same 33% probability. 
          </p>
          <p>
                  Folding, at the moment, doesn't take <em>local weight</em> into account.
          </p>`,
    'grammar': `$start = letters: (!(a | e | i | o | u) | !(b | etcetera));`
    },
    'unfolding-variable':  {
        'title': 'Unfolding a variable',
        'text': `<p>
                    Unfolding applies also to variable. 
                </p>
                <p>
                    The previous example has been rewritten, and it's now much more readable.
              </p>`,
        'grammar': `$start = letters: (!$vowel | !$consonant);
$vowel = a | e | i | o | u;
$consonant = b | etcetera;`
    },
    'unfolding-optional':  {
        'title': 'Unfolding an optional node',
        'text': `<p>
                    Unfolding applies also to an optional noode, 
                    that is node surrounded by <code>[</code> and <code>]</code>. 
                </p>
                <p>
                    Consider this example: <br/> <code>$start = I like (hiking | [TypeScript] programming);</code>. <br/>
                    Without unfolding, <code>hiking</code> has 50% probability such as the whole node
                    <code>[TypeScript] programming</code>. 
                    In turn, <code>programming</code> has 50% probability versus <code>TypeScript programming</code>. <br/>
                    Overall, <code>hiking</code> has 50%, <code>programming</code> 25% and <code>TypeScript programming</code> 25%. 
            </p>
            <p>
                Let's rewrite it with unfolding. <br/>
                <code>$start = I like (hiking | ![TypeScript] programming);</code>. <br/>    
                Now all three possibilities have the same 33% probability. 
            </p>
            `,
        'grammar': `$start = I like (
    hiking 
  | ![TypeScript] programming ![during weekends]
);`
    },
    'unfolding-shuffle':  {
        'title': 'Unfolding shuffle',
        'text': `<p> 
                    Unfolding applies also to shuffled nodes.                      
                </p>
                <p>
                    Consider this example: <br/>
                    <code>$start = Italy is popular for (the Dolomites | {pizza}<, {spaghetti} and {opera});</code>. <br/>
                    Without unfolding, <code>the Dolomiti</code> has 50% probability such as the whole node
                    <code>{pizza}<, {spaghetti} and {opera}</code>. <br/>
                    The second node has 6 possible outcome, each with a 16% probability, 
                    which becomes 8% considering its initial 50%. 
            </p>
            <p>
                Let's rewrite it with unfolding. <br/>
                <code>$start = Italy is popular for (
    the Dolomites 
  | !({pizza}<, {spaghetti} and {opera}));</code>. <br/>    
                Now all 7 possibilities have the same 100/7 % probability. 
            </p>
            `,
        'grammar': `$start = Italy is popular for (
    the Dolomites 
  | !({pizza}<, {spaghetti} and {opera}));`
    },
    'comments':  {
        'title': 'Comments',
        'text': `<p> 
                    A commented grammar is a good grammar!
                </p>
                <p>
                    InstaGrammar supports both single line comments: <br/> 
                    <code>// this is a comment </code><br/>
                    and multi-line comments: <br/>
                    <pre><code>$start = a | /* no b 
this time.
 */ | c;</code></pre>
            </p>`,
        'grammar': `// Single line comment

$start = Hello (
    World 
 /* |  Universe 
 */
    | Mario);`
    }
}

var alternateManualBackgroundCounter = 0;

Vue.component('ig-example', {
    props: ['source'],
    template: `<section v-bind:class="background">
        <a class="anchor" v-bind:name="source"></a>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-12">
                    <h2>{{ title }}</h2>
                    <hr />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div v-html="text"></div>
                </div>
                <div class="col-md-6">
                    <div class="ig-example-container">
                        <div class="ig-example-grammar">
                            <pre><code>{{ grammar }}</code></pre>
                        </div>
                        <div class="ig-example-output">
                            <button type="button" class="btn btn-sm btn-success ig-example-btn" v-on:click="generate">try</button>
                            <p>{{ output }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>`,
    data: function () {
        alternateManualBackgroundCounter++;
        return {
            text: examples[this.source].text,
            grammar: examples[this.source].grammar,
            title: examples[this.source].title,
            background: (alternateManualBackgroundCounter % 2 == 0) ? 'ig-light-gray' : 'ig-dark-gray',
            output: ''
        }
    },
    methods: {
        generate: function () {
            this.output = this.ig.generate();
        }
    },
    beforeMount() {
        this.ig = new InstaGrammar.InstaGrammar(this.grammar);
        this.generate();
    }
});


var manualApp = new Vue({
    el: '#manualApp'
});


var tryApp = new Vue({
    el: '#tryApp',
    data: {
        grammar: `$start = Hello (World | Universe)>, edit me;`,
        result: {},
        error: "",
        ig: null,
        showDebug: false
    },
    methods: {
        generate: function () {
            try {
                this.result = {};
                ig = new InstaGrammar.InstaGrammar(this.grammar);
                this.result.input = this.grammar;
                this.result.preProcessedInput = ig.preProcessedInput;
                this.result.ast = ig.ast;
                this.result.bindings = JSON.parse(JSON.stringify(ig.bindings));
                this.result.output = ig.generate();
                this.result.bindingsAfterGenerate = ig.bindings;                
                this.result.tokens = ig.env.tokens;
                this.result.weights = ig.env.prettyWeights();

                this.error = "";
            }
            catch (e) {
                console.log(e.message);
                this.output = {};
                this.error = e.message;
            }
        },
        debug: function() {
            this.showDebug = !this.showDebug;
        }
    },
    beforeMount() {
        this.ig = new InstaGrammar.InstaGrammar(this.grammar);
        this.generate();
    }
});



