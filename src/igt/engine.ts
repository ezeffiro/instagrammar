/*
    InstaGrammarTemplate
*/

import { InstaGrammar } from '../ig/engine';

/*
    InstaGrammarTemplate is a convenience class to be used when you have a general purposes
    text (template) with occasional $variables inside.
*/
class InstaGrammarTemplate {
    readonly template: string;
    readonly grammar: string;
    readonly instaGrammar: InstaGrammar;
    output: string;

    /*
        contructor wants a template and a grammar.
        this method can throw all the exection of InstaGrammar contructor
    */
    constructor(template: string, grammar: string) {
        this.grammar = grammar;
        this.template = template;
        this.instaGrammar = new InstaGrammar(grammar);
        this.output = "";
    }

    /*
        generate is the method to call on a InstaGrammarTemplate object.
        It uses simple RegExp to find $variables in the template, and converting them
        in a generate text, reusing the strongBindings.
    */
    generate(): string {
        if (this.template == '') {
            this.output = this.instaGrammar.generate();
        }
        else if (this.grammar == '') {
            this.output = this.template;
        }
        else {
            // resetting strongBindings before everything else
            if (this.instaGrammar.env && this.instaGrammar.env.strongBindings) {
                this.instaGrammar.env.strongBindings = {};
            }
            let instaGrammar = this.instaGrammar;
            let generateSymbol = function(name: string) {
                // true = keepStrongBindings
                return instaGrammar.generate(name, true);
            }
            let symbols = new RegExp('\\$[a-zA-Z0-9]+', 'g');
            this.output = this.template.replace(symbols, generateSymbol);
        }

        return this.output;
    }

    /*
        Convenience method
    */
    static generate(template: string, grammar: string): string {
        return new InstaGrammarTemplate(template, grammar).generate();
    }
}

export { InstaGrammarTemplate };
