import { InstaGrammar } from './ig/engine';
import { InstaGrammarTemplate } from './igt/engine';

export { InstaGrammar, InstaGrammarTemplate };
