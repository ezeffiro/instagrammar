
/*
    InstaGrammar GULP file
*/

var gulp  = require('gulp'),
    del = require('del'),
    command = require('child-process-promise').exec
    ts = require("gulp-typescript"),
    tsProject = ts.createProject("tsconfig.json"),
    inject = require('gulp-inject-string'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    tsify = require("tsify"),
    exec = require('child_process').exec,
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    buffer = require('vinyl-buffer'),
    rename = require("gulp-rename"),
    merge = require('merge-stream'),
    surge = require('gulp-surge');


var paths = {
    assets: [
        'src/www/*.css',
        'src/www/*.js',
        'src/www/*.html'
    ],
    buildPP: 'build/pp/',
    buildIG: 'build/ig/',
    dist: 'dist/',
    build: 'build/',
    www: 'www/',
    sourceWww: 'src/www/',
    rulesParser: 'src/pp/rulesParser.js',
    ppLexer: 'src/pp/lexer.js',
    igParser: 'src/ig/parser.js'

};


gulp.task('preprocessor-rules-parser', function() {
    return(command('yarn run jison src/pp/rulesGrammar.jison -o ' + paths.rulesParser)
        .then(function(result) {
            gulp.src(paths.rulesParser)
            .pipe(gulp.dest(paths.buildPP));
        }));
});


gulp.task('preprocessor-lexer', function() {
    return(command('yarn run jison-lex src/pp/lexer.jisonlex --module-type commonjs -o ' + paths.ppLexer)
        .then(function (result) {
            gulp.src(paths.ppLexer)
            .pipe(inject.append("\nexports.lexer = lexer;"))
            .pipe(gulp.dest('./src/pp/'))
            .pipe(gulp.dest(paths.buildPP));
    }));
});


gulp.task('instagrammar-parser', function() {
    return(command('yarn run jison src/ig/grammar.jison -o ' + paths.igParser)
        .then(function(result) {
            gulp.src(paths.igParser)
            .pipe(gulp.dest(paths.buildIG));
        }));
});

var tsFunction = function() {
    return(tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest(paths.build)));
}
gulp.task('ts', gulp.series(gulp.parallel('preprocessor-rules-parser', 'preprocessor-lexer', 'instagrammar-parser'), tsFunction));

var sourcemapsFunction = function() {
    return(tsProject
        .src()
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write('.', {includeContent: false, sourceRoot: '../src'}))
        .pipe(gulp.dest(paths.build)));
}
gulp.task('sourcemaps', gulp.series('ts', sourcemapsFunction));


var browserifyFunction = function() {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/exports.ts'],
        cache: {},
        packageCache: {},
        standalone: 'InstaGrammar'
    })
    .plugin(tsify)
    .transform('babelify', {
        presets: ['env'],
        extensions: ['.ts']
    })
    .bundle()
    .pipe(source('bundle.js'))

    // non minified JS
    .pipe(rename("instagrammar.js"))
    .pipe(gulp.dest(paths.dist))

    // minified JS
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.dist));
}
gulp.task('browserify', gulp.series('sourcemaps', browserifyFunction));


gulp.task("assets", function () {
    return gulp.src(paths.assets)
        .pipe(gulp.dest(paths.www));
});


var wwwFunction = function() {
    return(gulp.src(paths.dist + 'instagrammar.js')
        .pipe(gulp.dest(paths.www)));
}
gulp.task("www", gulp.series('browserify', 'assets', wwwFunction));


gulp.task('build', gulp.series('www'));


gulp.task('test', gulp.series('build', function (cb) {
    var cmd = 'yarn run alsatian build/tests/pp.js build/tests/ig.js build/tests/igt.js';
    console.log(cmd);
    exec(cmd, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
}));


gulp.task('clean', function() {
    return(del([paths.build, paths.dist, paths.www]));
});





gulp.task('default', gulp.series('build'));


gulp.task('deploy', gulp.series('build'), function() {
    return surge({
        project: './www/',                       // Path to dist directory
        domain: 'http://instagrammar.surge.sh/'  // Domain to deploy
    });
});