(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.InstaGrammar = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){

},{}],2:[function(require,module,exports){
(function (process){
// .dirname, .basename, and .extname methods are extracted from Node.js v8.11.1,
// backported and transplited with Babel, with backwards-compat fixes

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function (path) {
  if (typeof path !== 'string') path = path + '';
  if (path.length === 0) return '.';
  var code = path.charCodeAt(0);
  var hasRoot = code === 47 /*/*/;
  var end = -1;
  var matchedSlash = true;
  for (var i = path.length - 1; i >= 1; --i) {
    code = path.charCodeAt(i);
    if (code === 47 /*/*/) {
        if (!matchedSlash) {
          end = i;
          break;
        }
      } else {
      // We saw the first non-path separator
      matchedSlash = false;
    }
  }

  if (end === -1) return hasRoot ? '/' : '.';
  if (hasRoot && end === 1) {
    // return '//';
    // Backwards-compat fix:
    return '/';
  }
  return path.slice(0, end);
};

function basename(path) {
  if (typeof path !== 'string') path = path + '';

  var start = 0;
  var end = -1;
  var matchedSlash = true;
  var i;

  for (i = path.length - 1; i >= 0; --i) {
    if (path.charCodeAt(i) === 47 /*/*/) {
        // If we reached a path separator that was not part of a set of path
        // separators at the end of the string, stop now
        if (!matchedSlash) {
          start = i + 1;
          break;
        }
      } else if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // path component
      matchedSlash = false;
      end = i + 1;
    }
  }

  if (end === -1) return '';
  return path.slice(start, end);
}

// Uses a mixed approach for backwards-compatibility, as ext behavior changed
// in new Node.js versions, so only basename() above is backported here
exports.basename = function (path, ext) {
  var f = basename(path);
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};

exports.extname = function (path) {
  if (typeof path !== 'string') path = path + '';
  var startDot = -1;
  var startPart = 0;
  var end = -1;
  var matchedSlash = true;
  // Track the state of characters (if any) we see before our first dot and
  // after any path separator we find
  var preDotState = 0;
  for (var i = path.length - 1; i >= 0; --i) {
    var code = path.charCodeAt(i);
    if (code === 47 /*/*/) {
        // If we reached a path separator that was not part of a set of path
        // separators at the end of the string, stop now
        if (!matchedSlash) {
          startPart = i + 1;
          break;
        }
        continue;
      }
    if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // extension
      matchedSlash = false;
      end = i + 1;
    }
    if (code === 46 /*.*/) {
        // If this is our first dot, mark it as the start of our extension
        if (startDot === -1)
          startDot = i;
        else if (preDotState !== 1)
          preDotState = 1;
    } else if (startDot !== -1) {
      // We saw a non-dot and non-path separator before our dot, so we should
      // have a good chance at having a non-empty extension
      preDotState = -1;
    }
  }

  if (startDot === -1 || end === -1 ||
      // We saw a non-dot character immediately before the dot
      preDotState === 0 ||
      // The (right-most) trimmed path component is exactly '..'
      preDotState === 1 && startDot === end - 1 && startDot === startPart + 1) {
    return '';
  }
  return path.slice(startDot, end);
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

}).call(this,require('_process'))

},{"_process":3}],3:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var engine_1 = require("./ig/engine");
exports.InstaGrammar = engine_1.InstaGrammar;
var engine_2 = require("./igt/engine");
exports.InstaGrammarTemplate = engine_2.InstaGrammarTemplate;

},{"./ig/engine":5,"./igt/engine":9}],5:[function(require,module,exports){
"use strict";
/*
    InstaGrammar main class
*/

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var engine_1 = require("../pp/engine");
var rules_1 = require("../pp/rules");
var lexer_js_1 = require("../pp/lexer.js");
var parser_js_1 = require("./parser.js");
var node_1 = require("./node");
var env_1 = require("./env");
/*
    InstaGrammar is the main class.
*/

var InstaGrammar = function () {
    /*
        The constructor is a lengthy operation, since it has to:
            - preprocess the grammar (string => string)
            - parse the preprocessed grammar (string => ast)
            - convert the ast in dictionary of named nodes (bindings) (ast => bindings)
         All these intermediate structures are kept in the object for debugging purposes.
        This method can throw a number of parsing/lexing exception, so it must be called in
        a try-catch block.
    */
    function InstaGrammar(input) {
        _classCallCheck(this, InstaGrammar);

        this.preProcessor = new engine_1.PreProcessor(rules_1.rules, lexer_js_1.lexer, 'START');
        this.input = input;
        var regexp = /\/\*(.|\n)*?\*\/|\/\/.*$/gm;
        this.commentsPreProcessedInput = this.input.replace(regexp, '');
        this.preProcessedInput = this.preProcessor.process(this.commentsPreProcessedInput);
        this.ast = parser_js_1.parse(this.preProcessedInput);
        var bindings = {};
        if (Object.keys(this.ast).length > 0) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.ast[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var binding = _step.value;

                    bindings[binding.name] = node_1.Node.factory(binding);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
        this.bindings = bindings;
    }
    /*
        generate is the method to call on a InstaGrammar object.
            - startSymbol is the grammar entruy point
            - keepStrongBindings is used when you are going to call generate multiple times on the
                same object and you want to re-use strongBindings (CFR => InstaGrammarTemplate)
    */


    _createClass(InstaGrammar, [{
        key: "generate",
        value: function generate() {
            var startSymbol = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '$start';
            var keepStrongBindings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var strongBindings = {};
            if (keepStrongBindings && this.env != null) {
                strongBindings = this.env.strongBindings;
            }
            this.env = new env_1.Environment(this.bindings, strongBindings, startSymbol);
            return this.env.generate();
        }
        /*
            generate is a static convenience method useful when you need a generation only once,
            or when you don't care about performances.
        */

    }], [{
        key: "generate",
        value: function generate(input) {
            var startSymbol = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '$start';

            return new InstaGrammar(input).generate(startSymbol);
        }
    }]);

    return InstaGrammar;
}();

exports.InstaGrammar = InstaGrammar;

},{"../pp/engine":10,"../pp/lexer.js":11,"../pp/rules":12,"./env":6,"./node":7,"./parser.js":8}],6:[function(require,module,exports){
"use strict";
/*
    The ENV objects are created fresh for each InstaGrammar generation
*/

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var node_1 = require("./node");
/*
    During generation, all output is managed by tokens
    In this way, "function" tokens (e.g. SNAP) can be folded
*/

var Token = function Token() {
    _classCallCheck(this, Token);
};

exports.Token = Token;

var TextToken = function (_Token) {
    _inherits(TextToken, _Token);

    function TextToken(contents) {
        _classCallCheck(this, TextToken);

        var _this = _possibleConstructorReturn(this, (TextToken.__proto__ || Object.getPrototypeOf(TextToken)).call(this));

        _this.kind = 'TextToken';
        _this.contents = contents;
        return _this;
    }

    return TextToken;
}(Token);

exports.TextToken = TextToken;

var SnapToken = function (_Token2) {
    _inherits(SnapToken, _Token2);

    function SnapToken() {
        _classCallCheck(this, SnapToken);

        var _this2 = _possibleConstructorReturn(this, (SnapToken.__proto__ || Object.getPrototypeOf(SnapToken)).apply(this, arguments));

        _this2.kind = 'SnapToken';
        return _this2;
    }

    return SnapToken;
}(Token);

exports.SnapToken = SnapToken;

var UppercaseToken = function (_Token3) {
    _inherits(UppercaseToken, _Token3);

    function UppercaseToken() {
        _classCallCheck(this, UppercaseToken);

        var _this3 = _possibleConstructorReturn(this, (UppercaseToken.__proto__ || Object.getPrototypeOf(UppercaseToken)).apply(this, arguments));

        _this3.kind = 'Uppercase';
        return _this3;
    }

    return UppercaseToken;
}(Token);

exports.UppercaseToken = UppercaseToken;
/*
    Environment is the companion object for each InstaGrammar generation.
    Its main purposes are:
        - collects output tokens
        - collect strongBindings
        - record symbol visits (in order to avoid recursion, which is caught at runtime)
        - track node weight for automated testing purposes
*/

var Environment = function () {
    function Environment(bindings) {
        var strongBindings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var startSymbol = arguments[2];

        _classCallCheck(this, Environment);

        this.tokens = new Array();
        this.bindings = bindings;
        this.strongBindings = strongBindings;
        this.startSymbol = startSymbol;
        this.output = "";
        this.visits = {};
        this.weights = [];
    }

    _createClass(Environment, [{
        key: "prettyWeights",
        value: function prettyWeights() {
            return this.weights.join('_');
        }
        /*
            lookups a binding given its name.
            It raises an exception if node is node found
        */

    }, {
        key: "lookupBinding",
        value: function lookupBinding(name) {
            // Empty grammar, we use the EmptyNode to have a working grammar anyway
            if (Object.keys(this.bindings).length == 0) {
                return new node_1.EmptyNode({});
            }
            if (this.bindings[name]) {
                if (this.visits[name]) {
                    this.visits[name] += 1;
                    if (this.visits[name] == Environment.kRecursionThreshold) {
                        throw new Error("Possibile recursion found for binding " + name);
                    }
                } else {
                    this.visits[name] = 1;
                }
                return this.bindings[name];
            } else {
                throw new Error("Binding not found: " + name);
            }
        }
        /*
            converts an array of tokens into a string
        */

    }, {
        key: "prettyTokens",
        value: function prettyTokens() {
            var output = "";
            var snap = true;
            var uppercase = false;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.tokens[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var token = _step.value;

                    switch (token.kind) {
                        case 'TextToken':
                            {
                                if (snap) {
                                    snap = false;
                                } else {
                                    output += " ";
                                }
                                if (uppercase) {
                                    output += token.contents.charAt(0).toUpperCase();
                                    output += token.contents.slice(1);
                                    uppercase = false;
                                } else {
                                    output += token.contents;
                                }
                                break;
                            }
                        case 'SnapToken':
                            {
                                snap = true;
                                break;
                            }
                        case 'Uppercase':
                            {
                                uppercase = true;
                                break;
                            }
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return output;
        }
        /*
            Generate is the main method.
        */

    }, {
        key: "generate",
        value: function generate() {
            // strongBinding found: let's re-use the previously generate tokens
            if (this.strongBindings[this.startSymbol] != null) {
                this.tokens = this.strongBindings[this.startSymbol];
            } else {
                var startNode = this.lookupBinding(this.startSymbol);
                this.tokens = startNode.generate(this, []);
            }
            this.output = this.prettyTokens();
            return this.output;
        }
    }]);

    return Environment;
}();

Environment.kRecursionThreshold = 100;
exports.Environment = Environment;
/*
    PruningEnvironment is used when pruning graph.
    Its main purposes are:
        - lookup binding without taking care of recursion detection
        - collect weight for testing purposes
*/

var PruningEnvironment = function () {
    function PruningEnvironment(bindings) {
        _classCallCheck(this, PruningEnvironment);

        this.weights = [];
        this.bindings = bindings;
    }

    _createClass(PruningEnvironment, [{
        key: "lookupBinding",
        value: function lookupBinding(name) {
            if (this.bindings[name]) {
                return this.bindings[name];
            } else {
                throw new Error("Binding not found: " + name);
            }
        }
    }]);

    return PruningEnvironment;
}();

exports.PruningEnvironment = PruningEnvironment;

},{"./node":7}],7:[function(require,module,exports){
"use strict";
/*
    Node.ts contains the whole InstaGrammar logic.
*/

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var env_1 = require("./env");
function randomIntBetween(a, b) {
    var diff = b - a;
    return Math.floor(Math.random() * (diff + 1) + a);
}
var NodeType;
(function (NodeType) {
    NodeType["Text"] = "text";
    NodeType["Symbol"] = "symbol";
    NodeType["Bracket"] = "bracket";
    NodeType["And"] = "and";
    NodeType["Or"] = "or";
    NodeType["Empty"] = "empty";
})(NodeType || (NodeType = {}));
/*
    Node is a super type with some abstract method that MUST be implemented by subclasses.
    Hyerarchy:
        - Node
            - Text
            - Symbol
            - Bracket
            - Empty
            - Children
                - And
                - Or

    A node is created for each AST object, using class method Node.factory
    All members are readonly (as to prevent accidental modifications during use),
    except for unfolded and computedWeight which are only used for debugging and
    automated testing purposes
*/

var Node = function () {
    function Node(nodeAst) {
        _classCallCheck(this, Node);

        if (nodeAst.minTimes != null) {
            this.minTimes = nodeAst.minTimes;
        }
        if (nodeAst.maxTimes != null) {
            this.maxTimes = nodeAst.maxTimes;
        }
        if (nodeAst.leftSnap != null) {
            this.leftSnap = nodeAst.leftSnap;
        }
        if (nodeAst.rightSnap != null) {
            this.rightSnap = nodeAst.rightSnap;
        }
        if (nodeAst.strongBinding != null) {
            this.strongBinding = nodeAst.strongBinding;
        }
        if (nodeAst.shuffle != null) {
            this.shuffle = nodeAst.shuffle;
        }
        if (nodeAst.uppercase != null) {
            this.uppercase = nodeAst.uppercase;
        }
        if (nodeAst.tags != null) {
            this.tags = nodeAst.tags;
        }
        if (nodeAst.filters != null) {
            this.filters = nodeAst.filters;
        }
        if (nodeAst.resetFilters != null) {
            this.resetFilters = nodeAst.resetFilters;
        }
        if (nodeAst.localWeight != null) {
            this.localWeight = nodeAst.localWeight;
        }
        if (nodeAst.unfold != null) {
            this.unfold = nodeAst.unfold;
        }
    }
    /*
        Returns the number of time this node must be generated (es: N^2 => 2)
    */


    _createClass(Node, [{
        key: "randomTimes",
        value: function randomTimes() {
            if (this.minTimes != null && this.maxTimes != null) {
                return randomIntBetween(this.minTimes, this.maxTimes);
            } else {
                return 1;
            }
        }
        /*
            Returns node weight, calling abstract method "customWeight" on self.
            This method CANNOT be overriden. Instead, override customWeight.
            Quite a complicated logic has been implemented for unfolding.
        */

    }, {
        key: "weight",
        value: function weight(pruningEnv, filters) {
            var unfold = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            var visits = arguments[3];

            var weight = 0;
            filters = this.updateFilters(filters);
            unfold = unfold || this.unfold;
            if (unfold) {
                this.unfolded = true;
            }
            if (!this.excludedByFilters(filters)) {
                weight = 1;
                weight = this.customWeight(pruningEnv, filters, unfold, visits, weight);
                if (unfold == true && this.minTimes != null && this.maxTimes != null && weight > 0) {
                    // nel caso di una produzione opzionale unfoldata, es ![a | b | c],
                    // il peso non è 3*2 ma 3+1 perché la produzione vuota è unica
                    // indipendentemente dal peso dell'OR.
                    // Invece nel caso di una produzione iterata tipo A^2-4
                    // il peso è n*(4-2+1)
                    if (this.minTimes == 0) {
                        weight = weight * (this.maxTimes - this.minTimes) + 1;
                    } else {
                        weight = weight * (this.maxTimes - this.minTimes + 1);
                    }
                }
            }
            pruningEnv.weights.push(weight);
            this.computedWeight = weight;
            return weight;
        }
        /*
            Returns TRUE is the node is excluded by current filters.
            Used during generation and while pruning node graph.
        */

    }, {
        key: "excludedByFilters",
        value: function excludedByFilters(filters) {
            if (this.tags != null) {
                // if node has tags, check all filters (in AND)
                //console.log(`node has tags ${this.tags}`);
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = filters[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var filter = _step.value;

                        if (this.tags.indexOf(filter) == -1) {
                            //console.log(`filter not found ${filter}, return true`);
                            return true;
                        }
                    }
                    //console.log(`all filters found, return false`);
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                return false;
            } else {
                // if node has no tags, it won't be excluded
                return false;
            }
        }
        /*
            Returns a NEW array of filters updated with node filters
        */

    }, {
        key: "updateFilters",
        value: function updateFilters(filters) {
            if (this.resetFilters === true) {
                return [];
            } else if (this.filters == null) {
                return [].concat(_toConsumableArray(filters));
            } else {
                var newFilters = [].concat(_toConsumableArray(filters));
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = this.filters[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var filter = _step2.value;

                        if (newFilters.indexOf(filter) == -1) {
                            newFilters.push(filter);
                        }
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }

                return newFilters;
            }
        }
        /*
            Returns an array of token starting from self.
            This is the most important method:
                - it handles the commong things every node has
                - it uses the abstract method customGenerate on subclasses
                - it uses and progatate env (trasversal) and filters (vertical)
         */

    }, {
        key: "generate",
        value: function generate(env, filters) {
            var tokens = [];
            filters = this.updateFilters(filters);
            if (!this.excludedByFilters(filters)) {
                for (var i = 0; i < this.randomTimes(); i++) {
                    if (this.uppercase) {
                        tokens.push(new env_1.UppercaseToken());
                    }
                    if (this.leftSnap) {
                        tokens.push(new env_1.SnapToken());
                    }
                    tokens = tokens.concat(this.customGenerate(env, filters));
                    if (this.rightSnap) {
                        tokens.push(new env_1.SnapToken());
                    }
                }
                // saving strongBinding
                if (this.strongBinding != null) {
                    env.strongBindings[this.strongBinding] = tokens;
                }
            }
            return tokens;
        }
        /*
            Static method to create a Node given an object from the parse.
            See grammar.jison
        */

    }], [{
        key: "factory",
        value: function factory(nodeAst) {
            var node = void 0;
            switch (nodeAst.type) {
                case NodeType.Text:
                    node = new TextNode(nodeAst);
                    break;
                case NodeType.Symbol:
                    node = new SymbolNode(nodeAst);
                    break;
                case NodeType.Bracket:
                    node = new BracketNode(nodeAst);
                    break;
                case NodeType.And:
                    node = new AndNode(nodeAst);
                    break;
                case NodeType.Or:
                    node = new OrNode(nodeAst);
                    break;
                case NodeType.Empty:
                    node = new EmptyNode(nodeAst);
                    break;
                default:
                    throw new Error("Cannot create node for type " + nodeAst.type);
            }
            return node;
        }
    }]);

    return Node;
}();

exports.Node = Node;
/*
    Leaf node
*/

var TextNode = function (_Node) {
    _inherits(TextNode, _Node);

    function TextNode(nodeAst) {
        _classCallCheck(this, TextNode);

        var _this = _possibleConstructorReturn(this, (TextNode.__proto__ || Object.getPrototypeOf(TextNode)).call(this, nodeAst));

        _this.contents = nodeAst.contents;
        return _this;
    }

    _createClass(TextNode, [{
        key: "customWeight",
        value: function customWeight(pruningEnv, filters, unfold, visits, superWeight) {
            return superWeight;
        }
    }, {
        key: "customGenerate",
        value: function customGenerate(env) {
            return [new env_1.TextToken(this.contents)];
        }
    }]);

    return TextNode;
}(Node);
/*
    A Symbol node has a target which is the name of a binding
*/


var SymbolNode = function (_Node2) {
    _inherits(SymbolNode, _Node2);

    function SymbolNode(nodeAst) {
        _classCallCheck(this, SymbolNode);

        var _this2 = _possibleConstructorReturn(this, (SymbolNode.__proto__ || Object.getPrototypeOf(SymbolNode)).call(this, nodeAst));

        _this2.target = nodeAst.target;
        return _this2;
    }

    _createClass(SymbolNode, [{
        key: "customWeight",
        value: function customWeight(pruningEnv, filters, unfold, visits, superWeight) {
            var childWeight = 1;
            // node already visited. Let's get back
            if (visits.indexOf(this.target) > -1) {
                // let's avoid recursion
                // TODO: are we sure it's a good thing? Uhm...
                return 0;
            }
            visits.push(this.target);
            /*
                child/children weight is only interesting if unfold, or if there are filters, because
                we want to deep-prune the graph
            */
            if (unfold || filters.length > 0) {
                var childNode = pruningEnv.lookupBinding(this.target);
                childWeight = childNode.weight(pruningEnv, filters, unfold, visits);
                superWeight *= childWeight;
            }
            return superWeight;
        }
    }, {
        key: "customGenerate",
        value: function customGenerate(env, filters) {
            // tokens already generated and saved in a strongbinding
            if (env.strongBindings[this.target]) {
                return env.strongBindings[this.target];
            }
            var node = env.lookupBinding(this.target);
            var tokens = node.generate(env, filters);
            return tokens;
        }
    }]);

    return SymbolNode;
}(Node);
/*
    BracketNode has exactly one child.
    Notice: it is used for any kind of bracket!
        - (a | b)
        - {a | b}
        - [a | b]
*/


var BracketNode = function (_Node3) {
    _inherits(BracketNode, _Node3);

    function BracketNode(nodeAst) {
        _classCallCheck(this, BracketNode);

        var _this3 = _possibleConstructorReturn(this, (BracketNode.__proto__ || Object.getPrototypeOf(BracketNode)).call(this, nodeAst));

        _this3.child = Node.factory(nodeAst.child);
        return _this3;
    }

    _createClass(BracketNode, [{
        key: "customWeight",
        value: function customWeight(pruningEnv, filters, unfold, visits, superWeight) {
            /*
                child/children weight is only interesting if unfold, or if there are filters, because
                we want to deep-prune the graph
            */
            if (unfold || filters.length > 0) {
                var childWeight = this.child.weight(pruningEnv, filters, unfold, visits);
                superWeight *= childWeight;
            }
            return superWeight;
        }
    }, {
        key: "customGenerate",
        value: function customGenerate(env, filters) {
            return this.child.generate(env, filters);
        }
    }]);

    return BracketNode;
}(Node);
/*
    Children has an array of children node.
    This class is never used directly, instead use subclasses And and Or
*/


var ChildrenNode = function (_Node4) {
    _inherits(ChildrenNode, _Node4);

    function ChildrenNode(nodeAst) {
        _classCallCheck(this, ChildrenNode);

        var _this4 = _possibleConstructorReturn(this, (ChildrenNode.__proto__ || Object.getPrototypeOf(ChildrenNode)).call(this, nodeAst));

        var children = new Array();
        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            for (var _iterator3 = nodeAst.children[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var childNodeAst = _step3.value;

                children.push(Node.factory(childNodeAst));
            }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }

        _this4.children = children;
        return _this4;
    }

    return ChildrenNode;
}(Node);
/*
    AndNode. During generation, all of the children are generated
*/


var AndNode = function (_ChildrenNode) {
    _inherits(AndNode, _ChildrenNode);

    function AndNode() {
        _classCallCheck(this, AndNode);

        return _possibleConstructorReturn(this, (AndNode.__proto__ || Object.getPrototypeOf(AndNode)).apply(this, arguments));
    }

    _createClass(AndNode, [{
        key: "customWeight",
        value: function customWeight(pruningEnv, filters, unfold, visits, superWeight) {
            var childrenWeight = 0;
            var shuffleNodesNumber = 0; // all node can have weight = 0
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
                for (var _iterator4 = this.children[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                    var child = _step4.value;

                    /*
                     !(a {b} {c} [c])  =>
                       1  1   1   1  => (1 * 1) *   ((n * (n -1)) * 1 * 1)
                     !(a {b | g} {c} (a | b))
                       1    1   1     1  => (1 * 1) * ((n * (n -1) * 1 * 1))
                     !(a !{b | g} {c} !(a | b | c))
                       1   1   1   1    1   1   1
                       1     2     1        3
                      1 * 3  * ((n * (n -1)) * 2  * 1
                    */
                    var childWeight = child.weight(pruningEnv, filters, false, visits);
                    //console.log(`child weight is ${childWeight}`);
                    if (childWeight > 0) {
                        // in case all children weight is 0
                        if (childrenWeight == 0) {
                            childrenWeight = 1;
                        }
                        childrenWeight *= childWeight;
                        if (unfold && child.shuffle) {
                            shuffleNodesNumber++;
                        }
                    }
                }
                // with more than 1 shuffle node, number of permutations is n * (n - 1)
            } catch (err) {
                _didIteratorError4 = true;
                _iteratorError4 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion4 && _iterator4.return) {
                        _iterator4.return();
                    }
                } finally {
                    if (_didIteratorError4) {
                        throw _iteratorError4;
                    }
                }
            }

            if (shuffleNodesNumber > 1) {
                childrenWeight *= shuffleNodesNumber * (shuffleNodesNumber - 1);
            }
            superWeight *= childrenWeight;
            return superWeight;
        }
    }, {
        key: "customGenerate",
        value: function customGenerate(env, filters) {
            var tokens = [];
            var shuffleNodes = this.children.filter(function (node) {
                return node.shuffle == true;
            });
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
                for (var _iterator5 = this.children[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                    var node = _step5.value;

                    if (node.shuffle) {
                        var choosen = randomIntBetween(0, shuffleNodes.length - 1);
                        var choosenNode = shuffleNodes.splice(choosen, 1)[0];
                        tokens = tokens.concat(choosenNode.generate(env, filters));
                    } else {
                        tokens = tokens.concat(node.generate(env, filters));
                    }
                }
            } catch (err) {
                _didIteratorError5 = true;
                _iteratorError5 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion5 && _iterator5.return) {
                        _iterator5.return();
                    }
                } finally {
                    if (_didIteratorError5) {
                        throw _iteratorError5;
                    }
                }
            }

            return tokens;
        }
    }]);

    return AndNode;
}(ChildrenNode);
/*
    OrNode. During generation, ONE of the children is choosen and generated
*/


var OrNode = function (_ChildrenNode2) {
    _inherits(OrNode, _ChildrenNode2);

    function OrNode() {
        _classCallCheck(this, OrNode);

        return _possibleConstructorReturn(this, (OrNode.__proto__ || Object.getPrototypeOf(OrNode)).apply(this, arguments));
    }

    _createClass(OrNode, [{
        key: "customWeight",
        value: function customWeight(pruningEnv, filters, unfold, visits, superWeight) {
            var childrenWeight = 0;
            /*
                child/children weight is only interesting if unfold, or if there are filters, because
                we want to deep-prune the graph
            */
            if (unfold || filters.length > 0) {
                var _iteratorNormalCompletion6 = true;
                var _didIteratorError6 = false;
                var _iteratorError6 = undefined;

                try {
                    for (var _iterator6 = this.children[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                        var child = _step6.value;

                        childrenWeight += child.weight(pruningEnv, filters, false, visits);
                    }
                } catch (err) {
                    _didIteratorError6 = true;
                    _iteratorError6 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion6 && _iterator6.return) {
                            _iterator6.return();
                        }
                    } finally {
                        if (_didIteratorError6) {
                            throw _iteratorError6;
                        }
                    }
                }

                superWeight *= childrenWeight;
            }
            return superWeight;
        }
    }, {
        key: "customGenerate",
        value: function customGenerate(env, filters) {
            var pruningEnv = new env_1.PruningEnvironment(env.bindings);
            var weights = new Array();
            /*
                This is a bit tricky. We need to consider relative node weight.
                Eg: $a = a | b (c | d | e);
                weights:
                    - a: 1
                    - b (c | d | e): 3
                so the weights array is:
                    [0; 1; 1; 1]
            */
            for (var childIndex = 0; childIndex < this.children.length; childIndex++) {
                var w = this.children[childIndex].weight(pruningEnv, filters, false, []);
                if (this.children[childIndex].localWeight != null) {
                    w *= this.children[childIndex].localWeight;
                }
                for (var j = 0; j < w; j++) {
                    weights.push(childIndex);
                }
            }
            // saving all weights for testing purposes
            env.weights = env.weights.concat(pruningEnv.weights);
            if (weights.length > 0) {
                var randomIndex = randomIntBetween(0, weights.length - 1);
                var choosenNode = this.children[weights[randomIndex]];
                return choosenNode.generate(env, filters);
            } else {
                // TODO: this should never happen thanks to deep pruning;
                // if it happens there is a bug somewhere, so better to check it
                // EDIT: ehm it happens a lot. :)
                // Must rethink the way tags&filters are managed
                throw new Error('Cannot choose an or node. It looks like the tags&filters in the grammar excluded completely a node');
            }
        }
    }]);

    return OrNode;
}(ChildrenNode);
/*
    Empty Node. Used with literal "_"
*/


var EmptyNode = function (_Node5) {
    _inherits(EmptyNode, _Node5);

    function EmptyNode() {
        _classCallCheck(this, EmptyNode);

        return _possibleConstructorReturn(this, (EmptyNode.__proto__ || Object.getPrototypeOf(EmptyNode)).apply(this, arguments));
    }

    _createClass(EmptyNode, [{
        key: "customWeight",
        value: function customWeight(pruningEnv, filters, unfold, visits, superWeight) {
            return superWeight;
        }
    }, {
        key: "customGenerate",
        value: function customGenerate(env, filters) {
            return [];
        }
    }]);

    return EmptyNode;
}(Node);

exports.EmptyNode = EmptyNode;

},{"./env":6}],8:[function(require,module,exports){
(function (process){
/* parser generated by jison 0.4.18 */
/*
  Returns a Parser object of the following structure:

  Parser: {
    yy: {}
  }

  Parser.prototype: {
    yy: {},
    trace: function(),
    symbols_: {associative list: name ==> number},
    terminals_: {associative list: number ==> name},
    productions_: [...],
    performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate, $$, _$),
    table: [...],
    defaultActions: {...},
    parseError: function(str, hash),
    parse: function(input),

    lexer: {
        EOF: 1,
        parseError: function(str, hash),
        setInput: function(input),
        input: function(),
        unput: function(str),
        more: function(),
        less: function(n),
        pastInput: function(),
        upcomingInput: function(),
        showPosition: function(),
        test_match: function(regex_match_array, rule_index),
        next: function(),
        lex: function(),
        begin: function(condition),
        popState: function(),
        _currentRules: function(),
        topState: function(),
        pushState: function(condition),

        options: {
            ranges: boolean           (optional: true ==> token location info will include a .range[] member)
            flex: boolean             (optional: true ==> flex-like lexing behaviour where the rules are tested exhaustively to find the longest match)
            backtrack_lexer: boolean  (optional: true ==> lexer regexes are tested in order and for each matching regex the action code is invoked; the lexer terminates the scan when a token is returned by the action code)
        },

        performAction: function(yy, yy_, $avoiding_name_collisions, YY_START),
        rules: [...],
        conditions: {associative list: name ==> set},
    }
  }


  token location info (@$, _$, etc.): {
    first_line: n,
    last_line: n,
    first_column: n,
    last_column: n,
    range: [start_number, end_number]       (where the numbers are indexes into the input string, regular zero-based)
  }


  the parseError function receives a 'hash' object with these members for lexer and parser errors: {
    text:        (matched text)
    token:       (the produced terminal token, if any)
    line:        (yylineno)
  }
  while parser (grammar) errors will also provide these members, i.e. parser errors deliver a superset of attributes: {
    loc:         (yylloc)
    expected:    (string describing the set of expected tokens)
    recoverable: (boolean: TRUE when the parser has a error recovery rule available for this particular error)
  }
*/
var parser = (function () {
    var o = function (k, v, o, l) { for (o = o || {}, l = k.length; l--; o[k[l]] = v)
        ; return o; }, $V0 = [1, 5], $V1 = [5, 7], $V2 = [1, 18], $V3 = [1, 11], $V4 = [1, 12], $V5 = [1, 13], $V6 = [1, 14], $V7 = [1, 15], $V8 = [1, 16], $V9 = [1, 17], $Va = [1, 19], $Vb = [1, 22], $Vc = [1, 23], $Vd = [1, 24], $Ve = [1, 25], $Vf = [1, 26], $Vg = [1, 27], $Vh = [1, 28], $Vi = [1, 29], $Vj = [1, 30], $Vk = [10, 13, 14, 16, 18, 20, 21, 24, 25, 27, 28, 32, 33], $Vl = [10, 13, 14, 16, 18, 20];
    var parser = { trace: function trace() { },
        yy: {},
        symbols_: { "error": 2, "START": 3, "BINDINGS": 4, "EOF": 5, "BINDING": 6, "SYMBOL": 7, "BIND": 8, "NODE": 9, "SEMICOLON": 10, "STRONGBIND": 11, "TAG": 12, "AND": 13, "OR": 14, "BRA": 15, "KET": 16, "SQUAREBRA": 17, "SQUAREKET": 18, "CURLYBRA": 19, "CURLYKET": 20, "LOCALWEIGHT": 21, "NUMBER": 22, "LEFTSNAP": 23, "RIGHTSNAP": 24, "UPPERCASE": 25, "UNFOLD": 26, "INNERSTRONGBIND": 27, "TIMES": 28, "MINUS": 29, "TEXT": 30, "EMPTY": 31, "FILTER": 32, "RESETFILTERS": 33, "$accept": 0, "$end": 1 },
        terminals_: { 2: "error", 5: "EOF", 7: "SYMBOL", 8: "BIND", 10: "SEMICOLON", 11: "STRONGBIND", 12: "TAG", 13: "AND", 14: "OR", 15: "BRA", 16: "KET", 17: "SQUAREBRA", 18: "SQUAREKET", 19: "CURLYBRA", 20: "CURLYKET", 21: "LOCALWEIGHT", 22: "NUMBER", 23: "LEFTSNAP", 24: "RIGHTSNAP", 25: "UPPERCASE", 26: "UNFOLD", 27: "INNERSTRONGBIND", 28: "TIMES", 29: "MINUS", 30: "TEXT", 31: "EMPTY", 32: "FILTER", 33: "RESETFILTERS" },
        productions_: [0, [3, 2], [3, 1], [4, 1], [4, 2], [6, 4], [6, 4], [9, 2], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 2], [9, 2], [9, 2], [9, 2], [9, 3], [9, 3], [9, 5], [9, 1], [9, 1], [9, 1], [9, 2], [9, 2]],
        performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate /* action[1] */, $$ /* vstack */, _$ /* lstack */) {
            /* this == yyval */
            var $0 = $$.length - 1;
            switch (yystate) {
                case 1:
                    this.$ = $$[$0 - 1];
                    return (this.$);
                    break;
                case 2:
                    return {};
                    break;
                case 3:
                    this.$ = [$$[$0]];
                    break;
                case 4:
                    this.$ = $$[$0 - 1];
                    this.$.push($$[$0]);
                    break;
                case 5:
                    $$[$0 - 1]['name'] = $$[$0 - 3];
                    this.$ = $$[$0 - 1];
                    break;
                case 6:
                    $$[$0 - 1]['name'] = $$[$0 - 1]['strongBinding'] = $$[$0 - 3];
                    this.$ = $$[$0 - 1];
                    break;
                case 7:
                    if (!$$[$0]['tags']) {
                        $$[$0]['tags'] = [];
                    }
                    $$[$0]['tags'].push($$[$0 - 1]);
                    this.$ = $$[$0];
                    break;
                case 8:
                    var l = new Array();
                    ($$[$0 - 2].type == "and") ? l = l.concat($$[$0 - 2].children) : l.push($$[$0 - 2]);
                    ($$[$0].type == "and") ? l = l.concat($$[$0].children) : l.push($$[$0]);
                    this.$ = { type: "and", "children": l };
                    break;
                case 9:
                    var l = new Array();
                    ($$[$0 - 2].type == "or") ? l = l.concat($$[$0 - 2].children) : l.push($$[$0 - 2]);
                    ($$[$0].type == "or") ? l = l.concat($$[$0].children) : l.push($$[$0]);
                    this.$ = { type: "or", "children": l };
                    break;
                case 10:
                    this.$ = { type: "bracket", child: $$[$0 - 1] };
                    break;
                case 11:
                    this.$ = { type: "bracket", child: $$[$0 - 1], minTimes: 0, maxTimes: 1 };
                    break;
                case 12:
                    this.$ = { type: "bracket", child: $$[$0 - 1], shuffle: true };
                    break;
                case 13:
                    var w = Number($$[$0]);
                    if (w > 1) {
                        $$[$0 - 2]['localWeight'] = w;
                    }
                    this.$ = $$[$0 - 2];
                    break;
                case 14:
                    $$[$0]['leftSnap'] = true;
                    this.$ = $$[$0];
                    break;
                case 15:
                    $$[$0 - 1]['rightSnap'] = true;
                    this.$ = $$[$0 - 1];
                    break;
                case 16:
                    $$[$0 - 1]['uppercase'] = true;
                    this.$ = $$[$0 - 1];
                    break;
                case 17:
                    $$[$0]['unfold'] = true;
                    this.$ = $$[$0];
                    break;
                case 18:
                    $$[$0 - 2]['strongBinding'] = $$[$0];
                    this.$ = $$[$0 - 2];
                    break;
                case 19:
                    $$[$0 - 2]['minTimes'] = Number($$[$0]);
                    $$[$0 - 2]['maxTimes'] = Number($$[$0]);
                    this.$ = $$[$0 - 2];
                    break;
                case 20:
                    $$[$0 - 4]['minTimes'] = Number($$[$0 - 2]);
                    $$[$0 - 4]['maxTimes'] = Number($$[$0]);
                    this.$ = $$[$0 - 4];
                    break;
                case 21:
                    this.$ = { type: "text", contents: $$[$0] };
                    break;
                case 22:
                    this.$ = { type: "symbol", target: $$[$0] };
                    break;
                case 23:
                    this.$ = { type: "empty" };
                    break;
                case 24:
                    if (!$$[$0 - 1]['filters']) {
                        $$[$0 - 1]['filters'] = [];
                    }
                    $$[$0 - 1]['filters'].push($$[$0]);
                    this.$ = $$[$0 - 1];
                    break;
                case 25:
                    $$[$0 - 1]['resetFilters'] = true;
                    this.$ = $$[$0 - 1];
                    break;
            }
        },
        table: [{ 3: 1, 4: 2, 5: [1, 3], 6: 4, 7: $V0 }, { 1: [3] }, { 5: [1, 6], 6: 7, 7: $V0 }, { 1: [2, 2] }, o($V1, [2, 3]), { 8: [1, 8], 11: [1, 9] }, { 1: [2, 1] }, o($V1, [2, 4]), { 7: $V2, 9: 10, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 20, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 10: [1, 21], 13: $Vb, 14: $Vc, 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }, { 7: $V2, 9: 31, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 32, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 33, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 34, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 35, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 36, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, o($Vk, [2, 21]), o($Vk, [2, 22]), o($Vk, [2, 23]), { 10: [1, 37], 13: $Vb, 14: $Vc, 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }, o($V1, [2, 5]), { 7: $V2, 9: 38, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 7: $V2, 9: 39, 12: $V3, 15: $V4, 17: $V5, 19: $V6, 23: $V7, 26: $V8, 30: $V9, 31: $Va }, { 22: [1, 40] }, o($Vk, [2, 15]), o($Vk, [2, 16]), { 7: [1, 41] }, { 22: [1, 42] }, o($Vk, [2, 24]), o($Vk, [2, 25]), o($Vl, [2, 7], { 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }), { 13: $Vb, 14: $Vc, 16: [1, 43], 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }, { 13: $Vb, 14: $Vc, 18: [1, 44], 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }, { 13: $Vb, 14: $Vc, 20: [1, 45], 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }, o($Vl, [2, 14], { 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }), o($Vl, [2, 17], { 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }), o($V1, [2, 6]), o($Vl, [2, 8], { 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }), o([10, 14, 16, 18, 20], [2, 9], { 13: $Vb, 21: $Vd, 24: $Ve, 25: $Vf, 27: $Vg, 28: $Vh, 32: $Vi, 33: $Vj }), o($Vk, [2, 13]), o($Vk, [2, 18]), o($Vk, [2, 19], { 29: [1, 46] }), o($Vk, [2, 10]), o($Vk, [2, 11]), o($Vk, [2, 12]), { 22: [1, 47] }, o($Vk, [2, 20])],
        defaultActions: { 3: [2, 2], 6: [2, 1] },
        parseError: function parseError(str, hash) {
            if (hash.recoverable) {
                this.trace(str);
            }
            else {
                var error = new Error(str);
                error.hash = hash;
                throw error;
            }
        },
        parse: function parse(input) {
            var self = this, stack = [0], tstack = [], vstack = [null], lstack = [], table = this.table, yytext = '', yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
            var args = lstack.slice.call(arguments, 1);
            var lexer = Object.create(this.lexer);
            var sharedState = { yy: {} };
            for (var k in this.yy) {
                if (Object.prototype.hasOwnProperty.call(this.yy, k)) {
                    sharedState.yy[k] = this.yy[k];
                }
            }
            lexer.setInput(input, sharedState.yy);
            sharedState.yy.lexer = lexer;
            sharedState.yy.parser = this;
            if (typeof lexer.yylloc == 'undefined') {
                lexer.yylloc = {};
            }
            var yyloc = lexer.yylloc;
            lstack.push(yyloc);
            var ranges = lexer.options && lexer.options.ranges;
            if (typeof sharedState.yy.parseError === 'function') {
                this.parseError = sharedState.yy.parseError;
            }
            else {
                this.parseError = Object.getPrototypeOf(this).parseError;
            }
            function popStack(n) {
                stack.length = stack.length - 2 * n;
                vstack.length = vstack.length - n;
                lstack.length = lstack.length - n;
            }
            _token_stack: var lex = function () {
                var token;
                token = lexer.lex() || EOF;
                if (typeof token !== 'number') {
                    token = self.symbols_[token] || token;
                }
                return token;
            };
            var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
            while (true) {
                state = stack[stack.length - 1];
                if (this.defaultActions[state]) {
                    action = this.defaultActions[state];
                }
                else {
                    if (symbol === null || typeof symbol == 'undefined') {
                        symbol = lex();
                    }
                    action = table[state] && table[state][symbol];
                }
                if (typeof action === 'undefined' || !action.length || !action[0]) {
                    var errStr = '';
                    expected = [];
                    for (p in table[state]) {
                        if (this.terminals_[p] && p > TERROR) {
                            expected.push('\'' + this.terminals_[p] + '\'');
                        }
                    }
                    if (lexer.showPosition) {
                        errStr = 'Parse error on line ' + (yylineno + 1) + ':\n' + lexer.showPosition() + '\nExpecting ' + expected.join(', ') + ', got \'' + (this.terminals_[symbol] || symbol) + '\'';
                    }
                    else {
                        errStr = 'Parse error on line ' + (yylineno + 1) + ': Unexpected ' + (symbol == EOF ? 'end of input' : '\'' + (this.terminals_[symbol] || symbol) + '\'');
                    }
                    this.parseError(errStr, {
                        text: lexer.match,
                        token: this.terminals_[symbol] || symbol,
                        line: lexer.yylineno,
                        loc: yyloc,
                        expected: expected
                    });
                }
                if (action[0] instanceof Array && action.length > 1) {
                    throw new Error('Parse Error: multiple actions possible at state: ' + state + ', token: ' + symbol);
                }
                switch (action[0]) {
                    case 1:
                        stack.push(symbol);
                        vstack.push(lexer.yytext);
                        lstack.push(lexer.yylloc);
                        stack.push(action[1]);
                        symbol = null;
                        if (!preErrorSymbol) {
                            yyleng = lexer.yyleng;
                            yytext = lexer.yytext;
                            yylineno = lexer.yylineno;
                            yyloc = lexer.yylloc;
                            if (recovering > 0) {
                                recovering--;
                            }
                        }
                        else {
                            symbol = preErrorSymbol;
                            preErrorSymbol = null;
                        }
                        break;
                    case 2:
                        len = this.productions_[action[1]][1];
                        yyval.$ = vstack[vstack.length - len];
                        yyval._$ = {
                            first_line: lstack[lstack.length - (len || 1)].first_line,
                            last_line: lstack[lstack.length - 1].last_line,
                            first_column: lstack[lstack.length - (len || 1)].first_column,
                            last_column: lstack[lstack.length - 1].last_column
                        };
                        if (ranges) {
                            yyval._$.range = [
                                lstack[lstack.length - (len || 1)].range[0],
                                lstack[lstack.length - 1].range[1]
                            ];
                        }
                        r = this.performAction.apply(yyval, [
                            yytext,
                            yyleng,
                            yylineno,
                            sharedState.yy,
                            action[1],
                            vstack,
                            lstack
                        ].concat(args));
                        if (typeof r !== 'undefined') {
                            return r;
                        }
                        if (len) {
                            stack = stack.slice(0, -1 * len * 2);
                            vstack = vstack.slice(0, -1 * len);
                            lstack = lstack.slice(0, -1 * len);
                        }
                        stack.push(this.productions_[action[1]][0]);
                        vstack.push(yyval.$);
                        lstack.push(yyval._$);
                        newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
                        stack.push(newState);
                        break;
                    case 3:
                        return true;
                }
            }
            return true;
        } };
    /* generated by jison-lex 0.3.4 */
    var lexer = (function () {
        var lexer = ({
            EOF: 1,
            parseError: function parseError(str, hash) {
                if (this.yy.parser) {
                    this.yy.parser.parseError(str, hash);
                }
                else {
                    throw new Error(str);
                }
            },
            // resets the lexer, sets new input
            setInput: function (input, yy) {
                this.yy = yy || this.yy || {};
                this._input = input;
                this._more = this._backtrack = this.done = false;
                this.yylineno = this.yyleng = 0;
                this.yytext = this.matched = this.match = '';
                this.conditionStack = ['INITIAL'];
                this.yylloc = {
                    first_line: 1,
                    first_column: 0,
                    last_line: 1,
                    last_column: 0
                };
                if (this.options.ranges) {
                    this.yylloc.range = [0, 0];
                }
                this.offset = 0;
                return this;
            },
            // consumes and returns one char from the input
            input: function () {
                var ch = this._input[0];
                this.yytext += ch;
                this.yyleng++;
                this.offset++;
                this.match += ch;
                this.matched += ch;
                var lines = ch.match(/(?:\r\n?|\n).*/g);
                if (lines) {
                    this.yylineno++;
                    this.yylloc.last_line++;
                }
                else {
                    this.yylloc.last_column++;
                }
                if (this.options.ranges) {
                    this.yylloc.range[1]++;
                }
                this._input = this._input.slice(1);
                return ch;
            },
            // unshifts one char (or a string) into the input
            unput: function (ch) {
                var len = ch.length;
                var lines = ch.split(/(?:\r\n?|\n)/g);
                this._input = ch + this._input;
                this.yytext = this.yytext.substr(0, this.yytext.length - len);
                //this.yyleng -= len;
                this.offset -= len;
                var oldLines = this.match.split(/(?:\r\n?|\n)/g);
                this.match = this.match.substr(0, this.match.length - 1);
                this.matched = this.matched.substr(0, this.matched.length - 1);
                if (lines.length - 1) {
                    this.yylineno -= lines.length - 1;
                }
                var r = this.yylloc.range;
                this.yylloc = {
                    first_line: this.yylloc.first_line,
                    last_line: this.yylineno + 1,
                    first_column: this.yylloc.first_column,
                    last_column: lines ?
                        (lines.length === oldLines.length ? this.yylloc.first_column : 0)
                            + oldLines[oldLines.length - lines.length].length - lines[0].length :
                        this.yylloc.first_column - len
                };
                if (this.options.ranges) {
                    this.yylloc.range = [r[0], r[0] + this.yyleng - len];
                }
                this.yyleng = this.yytext.length;
                return this;
            },
            // When called from action, caches matched text and appends it on next action
            more: function () {
                this._more = true;
                return this;
            },
            // When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
            reject: function () {
                if (this.options.backtrack_lexer) {
                    this._backtrack = true;
                }
                else {
                    return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n' + this.showPosition(), {
                        text: "",
                        token: null,
                        line: this.yylineno
                    });
                }
                return this;
            },
            // retain first n characters of the match
            less: function (n) {
                this.unput(this.match.slice(n));
            },
            // displays already matched input, i.e. for error messages
            pastInput: function () {
                var past = this.matched.substr(0, this.matched.length - this.match.length);
                return (past.length > 20 ? '...' : '') + past.substr(-20).replace(/\n/g, "");
            },
            // displays upcoming input, i.e. for error messages
            upcomingInput: function () {
                var next = this.match;
                if (next.length < 20) {
                    next += this._input.substr(0, 20 - next.length);
                }
                return (next.substr(0, 20) + (next.length > 20 ? '...' : '')).replace(/\n/g, "");
            },
            // displays the character position where the lexing error occurred, i.e. for error messages
            showPosition: function () {
                var pre = this.pastInput();
                var c = new Array(pre.length + 1).join("-");
                return pre + this.upcomingInput() + "\n" + c + "^";
            },
            // test the lexed token: return FALSE when not a match, otherwise return token
            test_match: function (match, indexed_rule) {
                var token, lines, backup;
                if (this.options.backtrack_lexer) {
                    // save context
                    backup = {
                        yylineno: this.yylineno,
                        yylloc: {
                            first_line: this.yylloc.first_line,
                            last_line: this.last_line,
                            first_column: this.yylloc.first_column,
                            last_column: this.yylloc.last_column
                        },
                        yytext: this.yytext,
                        match: this.match,
                        matches: this.matches,
                        matched: this.matched,
                        yyleng: this.yyleng,
                        offset: this.offset,
                        _more: this._more,
                        _input: this._input,
                        yy: this.yy,
                        conditionStack: this.conditionStack.slice(0),
                        done: this.done
                    };
                    if (this.options.ranges) {
                        backup.yylloc.range = this.yylloc.range.slice(0);
                    }
                }
                lines = match[0].match(/(?:\r\n?|\n).*/g);
                if (lines) {
                    this.yylineno += lines.length;
                }
                this.yylloc = {
                    first_line: this.yylloc.last_line,
                    last_line: this.yylineno + 1,
                    first_column: this.yylloc.last_column,
                    last_column: lines ?
                        lines[lines.length - 1].length - lines[lines.length - 1].match(/\r?\n?/)[0].length :
                        this.yylloc.last_column + match[0].length
                };
                this.yytext += match[0];
                this.match += match[0];
                this.matches = match;
                this.yyleng = this.yytext.length;
                if (this.options.ranges) {
                    this.yylloc.range = [this.offset, this.offset += this.yyleng];
                }
                this._more = false;
                this._backtrack = false;
                this._input = this._input.slice(match[0].length);
                this.matched += match[0];
                token = this.performAction.call(this, this.yy, this, indexed_rule, this.conditionStack[this.conditionStack.length - 1]);
                if (this.done && this._input) {
                    this.done = false;
                }
                if (token) {
                    return token;
                }
                else if (this._backtrack) {
                    // recover context
                    for (var k in backup) {
                        this[k] = backup[k];
                    }
                    return false; // rule action called reject() implying the next rule should be tested instead.
                }
                return false;
            },
            // return next match in input
            next: function () {
                if (this.done) {
                    return this.EOF;
                }
                if (!this._input) {
                    this.done = true;
                }
                var token, match, tempMatch, index;
                if (!this._more) {
                    this.yytext = '';
                    this.match = '';
                }
                var rules = this._currentRules();
                for (var i = 0; i < rules.length; i++) {
                    tempMatch = this._input.match(this.rules[rules[i]]);
                    if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                        match = tempMatch;
                        index = i;
                        if (this.options.backtrack_lexer) {
                            token = this.test_match(tempMatch, rules[i]);
                            if (token !== false) {
                                return token;
                            }
                            else if (this._backtrack) {
                                match = false;
                                continue; // rule action called reject() implying a rule MISmatch.
                            }
                            else {
                                // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                                return false;
                            }
                        }
                        else if (!this.options.flex) {
                            break;
                        }
                    }
                }
                if (match) {
                    token = this.test_match(match, rules[index]);
                    if (token !== false) {
                        return token;
                    }
                    // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                    return false;
                }
                if (this._input === "") {
                    return this.EOF;
                }
                else {
                    return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), {
                        text: "",
                        token: null,
                        line: this.yylineno
                    });
                }
            },
            // return next match that has a token
            lex: function lex() {
                var r = this.next();
                if (r) {
                    return r;
                }
                else {
                    return this.lex();
                }
            },
            // activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
            begin: function begin(condition) {
                this.conditionStack.push(condition);
            },
            // pop the previously active lexer condition state off the condition stack
            popState: function popState() {
                var n = this.conditionStack.length - 1;
                if (n > 0) {
                    return this.conditionStack.pop();
                }
                else {
                    return this.conditionStack[0];
                }
            },
            // produce the lexer rule set which is active for the currently active lexer condition state
            _currentRules: function _currentRules() {
                if (this.conditionStack.length && this.conditionStack[this.conditionStack.length - 1]) {
                    return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules;
                }
                else {
                    return this.conditions["INITIAL"].rules;
                }
            },
            // return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
            topState: function topState(n) {
                n = this.conditionStack.length - 1 - Math.abs(n || 0);
                if (n >= 0) {
                    return this.conditionStack[n];
                }
                else {
                    return "INITIAL";
                }
            },
            // alias for begin(condition)
            pushState: function pushState(condition) {
                this.begin(condition);
            },
            // return the number of states currently on the stack
            stateStackSize: function stateStackSize() {
                return this.conditionStack.length;
            },
            options: {},
            performAction: function anonymous(yy, yy_, $avoiding_name_collisions, YY_START) {
                var YYSTATE = YY_START;
                switch ($avoiding_name_collisions) {
                    case 0: /* skip whitespace */
                        break;
                    case 1:
                        yy_.yytext = yy_.yytext.substr(1, yy_.yyleng - 2);
                        return 30;
                        break;
                    case 2:
                        return 7;
                        break;
                    case 3:
                        return 22;
                        break;
                    case 4:
                        return 12;
                        break;
                    case 5:
                        yy_.yytext = yy_.yytext.substr(1, yy_.yyleng - 1);
                        return 32;
                        break;
                    case 6:
                        return 11;
                        break;
                    case 7:
                        return 27;
                        break;
                    case 8:
                        return 8;
                        break;
                    case 9:
                        return 15;
                        break;
                    case 10:
                        return 16;
                        break;
                    case 11:
                        return 17;
                        break;
                    case 12:
                        return 18;
                        break;
                    case 13:
                        return 19;
                        break;
                    case 14:
                        return 20;
                        break;
                    case 15:
                        return 25;
                        break;
                    case 16:
                        return 33;
                        break;
                    case 17:
                        return 28;
                        break;
                    case 18:
                        return 23;
                        break;
                    case 19:
                        return 24;
                        break;
                    case 20:
                        return 14;
                        break;
                    case 21:
                        return 31;
                        break;
                    case 22:
                        return 10;
                        break;
                    case 23:
                        return 13;
                        break;
                    case 24:
                        return 29;
                        break;
                    case 25:
                        return 26;
                        break;
                    case 26:
                        return 21;
                        break;
                    case 27:
                        return 'INVALID';
                        break;
                    case 28:
                        return 5;
                        break;
                }
            },
            rules: [/^(?:\s+)/, /^(?:"(?:\\[\\"bfnrt\/]|\\u[a-fA-F0-9]{4}|[^\\\0-\x09\x0a-\x1f"]|\n)*")/, /^(?:\$[a-zA-Z0-9]+)/, /^(?:[0-9]+)/, /^(?:#[a-zA-Z0-9]+)/, /^(?:\^#[a-zA-Z0-9]+)/, /^(?:<=)/, /^(?:=>)/, /^(?:=)/, /^(?:\()/, /^(?:\))/, /^(?:\[)/, /^(?:\])/, /^(?:\{)/, /^(?:\})/, /^(?:\^U\b)/, /^(?:\^##)/, /^(?:\^)/, /^(?:<)/, /^(?:>)/, /^(?:\|)/, /^(?:_\b)/, /^(?:;)/, /^(?:\+)/, /^(?:-)/, /^(?:!)/, /^(?:\*)/, /^(?:.)/, /^(?:$)/],
            conditions: { "INITIAL": { "rules": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28], "inclusive": true } }
        });
        return lexer;
    })();
    parser.lexer = lexer;
    function Parser() {
        this.yy = {};
    }
    Parser.prototype = parser;
    parser.Parser = Parser;
    return new Parser;
})();
if (typeof require !== 'undefined' && typeof exports !== 'undefined') {
    exports.parser = parser;
    exports.Parser = parser.Parser;
    exports.parse = function () { return parser.parse.apply(parser, arguments); };
    exports.main = function commonjsMain(args) {
        if (!args[1]) {
            console.log('Usage: ' + args[0] + ' FILE');
            process.exit(1);
        }
        var source = require('fs').readFileSync(require('path').normalize(args[1]), "utf8");
        return exports.parser.parse(source);
    };
    if (typeof module !== 'undefined' && require.main === module) {
        exports.main(process.argv.slice(1));
    }
}

}).call(this,require('_process'))

},{"_process":3,"fs":1,"path":2}],9:[function(require,module,exports){
"use strict";
/*
    InstaGrammarTemplate
*/

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var engine_1 = require("../ig/engine");
/*
    InstaGrammarTemplate is a convenience class to be used when you have a general purposes
    text (template) with occasional $variables inside.
*/

var InstaGrammarTemplate = function () {
    /*
        contructor wants a template and a grammar.
        this method can throw all the exection of InstaGrammar contructor
    */
    function InstaGrammarTemplate(template, grammar) {
        _classCallCheck(this, InstaGrammarTemplate);

        this.grammar = grammar;
        this.template = template;
        this.instaGrammar = new engine_1.InstaGrammar(grammar);
        this.output = "";
    }
    /*
        generate is the method to call on a InstaGrammarTemplate object.
        It uses simple RegExp to find $variables in the template, and converting them
        in a generate text, reusing the strongBindings.
    */


    _createClass(InstaGrammarTemplate, [{
        key: "generate",
        value: function generate() {
            if (this.template == '') {
                this.output = this.instaGrammar.generate();
            } else if (this.grammar == '') {
                this.output = this.template;
            } else {
                // resetting strongBindings before everything else
                if (this.instaGrammar.env && this.instaGrammar.env.strongBindings) {
                    this.instaGrammar.env.strongBindings = {};
                }
                var instaGrammar = this.instaGrammar;
                var generateSymbol = function generateSymbol(name) {
                    // true = keepStrongBindings
                    return instaGrammar.generate(name, true);
                };
                var symbols = new RegExp('\\$[a-zA-Z0-9]+', 'g');
                this.output = this.template.replace(symbols, generateSymbol);
            }
            return this.output;
        }
        /*
            Convenience method
        */

    }], [{
        key: "generate",
        value: function generate(template, grammar) {
            return new InstaGrammarTemplate(template, grammar).generate();
        }
    }]);

    return InstaGrammarTemplate;
}();

exports.InstaGrammarTemplate = InstaGrammarTemplate;

},{"../ig/engine":5}],10:[function(require,module,exports){
"use strict";
/*
    PreProcessor engine

    Given a lexer and a set of rules, it converts a string into a preprocessed string.
    This module is bundled into InstaGrammar, but it could be standalone.


*/

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
var rulesParser_js_1 = require("./rulesParser.js");

var Rule = function () {
    function Rule(fromState, rule) {
        _classCallCheck(this, Rule);

        this.fromState = fromState;
        this.tokens = rule.tokens;
        if (rule.then) {
            if (rule.then.text) {
                this.text = rule.then.text;
            }
            if (rule.then.consume) {
                this.consume = rule.then.consume;
            }
            if (rule.then.state) {
                this.toState = rule.then.state;
            }
        }
    }
    /*
        Returns true if tokens matches the head of the stack
    */


    _createClass(Rule, [{
        key: "matchesStack",
        value: function matchesStack(stack) {
            if (stack.length >= this.tokens.length) {
                for (var i in this.tokens) {
                    if (this.tokens[i] != stack[i].name) {
                        // the token "*" matches with anything
                        if (this.tokens[i] != '*') {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        }
    }]);

    return Rule;
}();

var Token = function Token(name, text) {
    _classCallCheck(this, Token);

    this.name = name;
    this.text = text;
};

var PreProcessor = function () {
    function PreProcessor(rules, lexer, startState) {
        _classCallCheck(this, PreProcessor);

        this.rules = this.parseRules(rules);
        this.lexer = lexer;
        this.startState = startState;
    }
    /*
        Converts the parser AST into a flatten array of PPRules
    */


    _createClass(PreProcessor, [{
        key: "parseRules",
        value: function parseRules(rules) {
            var localRules = new Array();
            var rulesets = null;
            try {
                rulesets = rulesParser_js_1.parse(rules);
            } catch (e) {
                throw new Error(e.message);
            }
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = rulesets[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var ruleset = _step.value;
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;

                    try {
                        for (var _iterator2 = ruleset.states[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            var fromState = _step2.value;
                            var _iteratorNormalCompletion3 = true;
                            var _didIteratorError3 = false;
                            var _iteratorError3 = undefined;

                            try {
                                for (var _iterator3 = ruleset.rules[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                    var rule = _step3.value;

                                    localRules.push(new Rule(fromState, rule));
                                }
                            } catch (err) {
                                _didIteratorError3 = true;
                                _iteratorError3 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                        _iterator3.return();
                                    }
                                } finally {
                                    if (_didIteratorError3) {
                                        throw _iteratorError3;
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        } finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return localRules;
        }
        /*
            Coverts a stack and a template string into a string
            Eg.: - T2 T3 T4 T1 T4 => "ok $1 $2 $3"
         */

    }, {
        key: "processText",
        value: function processText(tokens, template) {
            var output = template;
            for (var i in tokens) {
                output = output.replace("$" + i, tokens[i].text);
            }
            return output;
        }
        /*
            Converts the input string into an array of tokens
        */

    }, {
        key: "parseTokens",
        value: function parseTokens(input) {
            var tokens = new Array();
            this.lexer.setInput(input);
            var name = "";
            while ((name = this.lexer.lex()) != 1) {
                tokens.push(new Token(name, this.lexer.yytext));
            }
            return tokens;
        }
        /*
            Preprocess an input string
        */

    }, {
        key: "process",
        value: function process(input) {
            var tokens = this.parseTokens(input);
            var stack = [].concat(_toConsumableArray(tokens));
            var currentState = this.startState;
            var output = "";
            while (stack.length > 0) {
                // filtered rules according to state
                var filteredRules = this.rules.filter(function (rule) {
                    return rule.fromState == currentState || rule.fromState == '*';
                });
                var foundRule = false;
                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = filteredRules[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var filteredRule = _step4.value;

                        if (filteredRule.matchesStack(stack)) {
                            // process rule
                            var oldState = currentState;
                            // text need to be processed
                            if (filteredRule.text) {
                                output += this.processText(stack.slice(0, filteredRule.tokens.length), filteredRule.text);
                            }
                            // moving FSM to new state
                            if (filteredRule.toState) {
                                currentState = filteredRule.toState;
                            }
                            // consuming input tokens
                            var tokensToConsume = filteredRule.tokens.length;
                            if (filteredRule.consume && filteredRule.consume < tokensToConsume) {
                                tokensToConsume = filteredRule.consume;
                            }
                            // popping consumed tokens out of stack
                            stack = stack.slice(tokensToConsume, stack.length);
                            foundRule = true;
                            break;
                        }
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }

                if (!foundRule) {
                    var prettyStack = "";
                    var prettyStackSize = stack.length < 5 ? stack.length : 5;
                    for (var i = 0; i < prettyStackSize; i++) {
                        prettyStack += stack[i].name + " ";
                    }
                    var s = "Rule not found. State: " + currentState + ", stack: " + prettyStack;
                    throw new Error(s);
                }
            }
            return output;
        }
    }]);

    return PreProcessor;
}();

exports.PreProcessor = PreProcessor;

},{"./rulesParser.js":13}],11:[function(require,module,exports){
/* generated by jison-lex 0.3.4 */
var lexer = (function () {
    var lexer = ({
        EOF: 1,
        parseError: function parseError(str, hash) {
            if (this.yy.parser) {
                this.yy.parser.parseError(str, hash);
            }
            else {
                throw new Error(str);
            }
        },
        // resets the lexer, sets new input
        setInput: function (input, yy) {
            this.yy = yy || this.yy || {};
            this._input = input;
            this._more = this._backtrack = this.done = false;
            this.yylineno = this.yyleng = 0;
            this.yytext = this.matched = this.match = '';
            this.conditionStack = ['INITIAL'];
            this.yylloc = {
                first_line: 1,
                first_column: 0,
                last_line: 1,
                last_column: 0
            };
            if (this.options.ranges) {
                this.yylloc.range = [0, 0];
            }
            this.offset = 0;
            return this;
        },
        // consumes and returns one char from the input
        input: function () {
            var ch = this._input[0];
            this.yytext += ch;
            this.yyleng++;
            this.offset++;
            this.match += ch;
            this.matched += ch;
            var lines = ch.match(/(?:\r\n?|\n).*/g);
            if (lines) {
                this.yylineno++;
                this.yylloc.last_line++;
            }
            else {
                this.yylloc.last_column++;
            }
            if (this.options.ranges) {
                this.yylloc.range[1]++;
            }
            this._input = this._input.slice(1);
            return ch;
        },
        // unshifts one char (or a string) into the input
        unput: function (ch) {
            var len = ch.length;
            var lines = ch.split(/(?:\r\n?|\n)/g);
            this._input = ch + this._input;
            this.yytext = this.yytext.substr(0, this.yytext.length - len);
            //this.yyleng -= len;
            this.offset -= len;
            var oldLines = this.match.split(/(?:\r\n?|\n)/g);
            this.match = this.match.substr(0, this.match.length - 1);
            this.matched = this.matched.substr(0, this.matched.length - 1);
            if (lines.length - 1) {
                this.yylineno -= lines.length - 1;
            }
            var r = this.yylloc.range;
            this.yylloc = {
                first_line: this.yylloc.first_line,
                last_line: this.yylineno + 1,
                first_column: this.yylloc.first_column,
                last_column: lines ?
                    (lines.length === oldLines.length ? this.yylloc.first_column : 0)
                        + oldLines[oldLines.length - lines.length].length - lines[0].length :
                    this.yylloc.first_column - len
            };
            if (this.options.ranges) {
                this.yylloc.range = [r[0], r[0] + this.yyleng - len];
            }
            this.yyleng = this.yytext.length;
            return this;
        },
        // When called from action, caches matched text and appends it on next action
        more: function () {
            this._more = true;
            return this;
        },
        // When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
        reject: function () {
            if (this.options.backtrack_lexer) {
                this._backtrack = true;
            }
            else {
                return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n' + this.showPosition(), {
                    text: "",
                    token: null,
                    line: this.yylineno
                });
            }
            return this;
        },
        // retain first n characters of the match
        less: function (n) {
            this.unput(this.match.slice(n));
        },
        // displays already matched input, i.e. for error messages
        pastInput: function () {
            var past = this.matched.substr(0, this.matched.length - this.match.length);
            return (past.length > 20 ? '...' : '') + past.substr(-20).replace(/\n/g, "");
        },
        // displays upcoming input, i.e. for error messages
        upcomingInput: function () {
            var next = this.match;
            if (next.length < 20) {
                next += this._input.substr(0, 20 - next.length);
            }
            return (next.substr(0, 20) + (next.length > 20 ? '...' : '')).replace(/\n/g, "");
        },
        // displays the character position where the lexing error occurred, i.e. for error messages
        showPosition: function () {
            var pre = this.pastInput();
            var c = new Array(pre.length + 1).join("-");
            return pre + this.upcomingInput() + "\n" + c + "^";
        },
        // test the lexed token: return FALSE when not a match, otherwise return token
        test_match: function (match, indexed_rule) {
            var token, lines, backup;
            if (this.options.backtrack_lexer) {
                // save context
                backup = {
                    yylineno: this.yylineno,
                    yylloc: {
                        first_line: this.yylloc.first_line,
                        last_line: this.last_line,
                        first_column: this.yylloc.first_column,
                        last_column: this.yylloc.last_column
                    },
                    yytext: this.yytext,
                    match: this.match,
                    matches: this.matches,
                    matched: this.matched,
                    yyleng: this.yyleng,
                    offset: this.offset,
                    _more: this._more,
                    _input: this._input,
                    yy: this.yy,
                    conditionStack: this.conditionStack.slice(0),
                    done: this.done
                };
                if (this.options.ranges) {
                    backup.yylloc.range = this.yylloc.range.slice(0);
                }
            }
            lines = match[0].match(/(?:\r\n?|\n).*/g);
            if (lines) {
                this.yylineno += lines.length;
            }
            this.yylloc = {
                first_line: this.yylloc.last_line,
                last_line: this.yylineno + 1,
                first_column: this.yylloc.last_column,
                last_column: lines ?
                    lines[lines.length - 1].length - lines[lines.length - 1].match(/\r?\n?/)[0].length :
                    this.yylloc.last_column + match[0].length
            };
            this.yytext += match[0];
            this.match += match[0];
            this.matches = match;
            this.yyleng = this.yytext.length;
            if (this.options.ranges) {
                this.yylloc.range = [this.offset, this.offset += this.yyleng];
            }
            this._more = false;
            this._backtrack = false;
            this._input = this._input.slice(match[0].length);
            this.matched += match[0];
            token = this.performAction.call(this, this.yy, this, indexed_rule, this.conditionStack[this.conditionStack.length - 1]);
            if (this.done && this._input) {
                this.done = false;
            }
            if (token) {
                return token;
            }
            else if (this._backtrack) {
                // recover context
                for (var k in backup) {
                    this[k] = backup[k];
                }
                return false; // rule action called reject() implying the next rule should be tested instead.
            }
            return false;
        },
        // return next match in input
        next: function () {
            if (this.done) {
                return this.EOF;
            }
            if (!this._input) {
                this.done = true;
            }
            var token, match, tempMatch, index;
            if (!this._more) {
                this.yytext = '';
                this.match = '';
            }
            var rules = this._currentRules();
            for (var i = 0; i < rules.length; i++) {
                tempMatch = this._input.match(this.rules[rules[i]]);
                if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                    match = tempMatch;
                    index = i;
                    if (this.options.backtrack_lexer) {
                        token = this.test_match(tempMatch, rules[i]);
                        if (token !== false) {
                            return token;
                        }
                        else if (this._backtrack) {
                            match = false;
                            continue; // rule action called reject() implying a rule MISmatch.
                        }
                        else {
                            // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                            return false;
                        }
                    }
                    else if (!this.options.flex) {
                        break;
                    }
                }
            }
            if (match) {
                token = this.test_match(match, rules[index]);
                if (token !== false) {
                    return token;
                }
                // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                return false;
            }
            if (this._input === "") {
                return this.EOF;
            }
            else {
                return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), {
                    text: "",
                    token: null,
                    line: this.yylineno
                });
            }
        },
        // return next match that has a token
        lex: function lex() {
            var r = this.next();
            if (r) {
                return r;
            }
            else {
                return this.lex();
            }
        },
        // activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
        begin: function begin(condition) {
            this.conditionStack.push(condition);
        },
        // pop the previously active lexer condition state off the condition stack
        popState: function popState() {
            var n = this.conditionStack.length - 1;
            if (n > 0) {
                return this.conditionStack.pop();
            }
            else {
                return this.conditionStack[0];
            }
        },
        // produce the lexer rule set which is active for the currently active lexer condition state
        _currentRules: function _currentRules() {
            if (this.conditionStack.length && this.conditionStack[this.conditionStack.length - 1]) {
                return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules;
            }
            else {
                return this.conditions["INITIAL"].rules;
            }
        },
        // return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
        topState: function topState(n) {
            n = this.conditionStack.length - 1 - Math.abs(n || 0);
            if (n >= 0) {
                return this.conditionStack[n];
            }
            else {
                return "INITIAL";
            }
        },
        // alias for begin(condition)
        pushState: function pushState(condition) {
            this.begin(condition);
        },
        // return the number of states currently on the stack
        stateStackSize: function stateStackSize() {
            return this.conditionStack.length;
        },
        options: { "moduleName": "lexer" },
        performAction: function anonymous(yy, yy_, $avoiding_name_collisions, YY_START) {
            var YYSTATE = YY_START;
            switch ($avoiding_name_collisions) {
                case 0:
                    return 'WHITESPACE';
                    break;
                case 1:
                    yy_.yytext = yy_.yytext.substr(1, yy_.yyleng - 1);
                    return 'TEXT';
                    break;
                case 2:
                    yy_.yytext = '\\"';
                    return 'TEXT';
                    break;
                case 3:
                    return 'RESET';
                    break;
                case 4:
                    return 'LEFT_KEYWORD';
                    break;
                case 5:
                    return 'RIGHT_KEYWORD';
                    break;
                case 6:
                    return 'LEFT_KEYWORD';
                    break;
                case 7:
                    return 'RIGHT_KEYWORD';
                    break;
                case 8:
                    return 'LEFT_KEYWORD';
                    break;
                case 9:
                    return 'RIGHT_KEYWORD';
                    break;
                case 10:
                    return 'NODE_SEPARATOR';
                    break;
                case 11:
                    return 'NODE';
                    break;
                case 12:
                    return 'LEFT_KEYWORD';
                    break;
                case 13:
                    return 'RIGHT_KEYWORD';
                    break;
                case 14:
                    return 'LEFT_KEYWORD';
                    break;
                case 15:
                    return 'RIGHT_KEYWORD';
                    break;
                case 16:
                    return 'RIGHT_KEYWORD';
                    break;
                case 17:
                    return 'BIND_OR_STRONGBIND';
                    break;
                case 18:
                    return 'BIND_OR_STRONGBIND';
                    break;
                case 19:
                    return 'RIGHT_KEYWORD';
                    break;
                case 20:
                    return 'RIGHT_KEYWORD';
                    break;
                case 21:
                    return 'LEFT_KEYWORD';
                    break;
                case 22:
                    return 'RIGHT_KEYWORD';
                    break;
                case 23:
                    return 'RIGHT_KEYWORD';
                    break;
                case 24:
                    return 'RIGHT_KEYWORD';
                    break;
                case 25:
                    return 'RIGHT_KEYWORD';
                    break;
                case 26:
                    return 'NODE';
                    break;
                case 27:
                    return 'TEXT';
                    break;
                case 28:
                    return 'EOF';
                    break;
            }
        },
        rules: [/^(?:\s+)/, /^(?:\\[;!<>_#^${}()\[\]\*])/, /^(?:")/, /^(?:;)/, /^(?:\[)/, /^(?:\])/, /^(?:\()/, /^(?:\))/, /^(?:\{)/, /^(?:\})/, /^(?:\|)/, /^(?:_\b)/, /^(?:<)/, /^(?:>)/, /^(?:!)/, /^(?:\*[0-9]+)/, /^(?:(=>\s*\$[a-zA-Z0-9]+))/, /^(?:<=)/, /^(?:=)/, /^(?:\^#[a-zA-Z0-9]+)/, /^(?:\^##)/, /^(?:#[a-zA-Z0-9]+)/, /^(?:\^[0-9]+[\-][0-9]+)/, /^(?:\^[0-9]+)/, /^(?:\^#[a-zA-Z0-9]+)/, /^(?:\^U\b)/, /^(?:\$[a-zA-Z0-9]+)/, /^(?:[^;\[\]\(\)\|\\"\{\}=#<>!\s\n\t\r\^\*]+)/, /^(?:$)/],
        conditions: { "INITIAL": { "rules": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28], "inclusive": true } }
    });
    return lexer;
})();
exports.lexer = lexer;

},{}],12:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var rules = "\n\nSTART:\n- * => LEFT_BINDING 0\n;\n\nLEFT_BINDING:\n- BIND_OR_STRONGBIND => '$0' WAITING_FOR_NODE\n;\n\nWAITING_FOR_NODE:\n- TEXT => '\"' TEXT 0\n- LEFT_KEYWORD => LEFT_KEYWORD 0\n- NODE => '$0' RIGHT_KEYWORD\n;\n\nTEXT:\n- NODE_SEPARATOR => '\" ' WAITING_FOR_NODE 0\n- WHITESPACE NODE_SEPARATOR => '\" ' WAITING_FOR_NODE 1\n- LEFT_KEYWORD => '\" + ' LEFT_KEYWORD 0\n- WHITESPACE LEFT_KEYWORD => '\" + ' LEFT_KEYWORD 1\n- RIGHT_KEYWORD => '\"' RIGHT_KEYWORD 0\n- WHITESPACE RIGHT_KEYWORD => '\" ' RIGHT_KEYWORD 1\n- NODE => '\" + $0' RIGHT_KEYWORD\n- WHITESPACE NODE => '\" + $1' RIGHT_KEYWORD\n- RESET => '\"$0' START\n- WHITESPACE RESET => '\"$1' START\n;\n\nLEFT_KEYWORD:\n- TEXT => '\"' TEXT 0\n- NODE => '$0' RIGHT_KEYWORD\n;\n\nRIGHT_KEYWORD:\n- TEXT => '+ \"' TEXT 0\n- LEFT_KEYWORD => '+ ' LEFT_KEYWORD 0\n- NODE => '+ $0'\n- NODE_SEPARATOR => WAITING_FOR_NODE 0\n;\n\n*:\n- RESET => '$0' START\n- * => '$0'\n;\n\n";
exports.rules = rules;

},{}],13:[function(require,module,exports){
(function (process){
/* parser generated by jison 0.4.18 */
/*
  Returns a Parser object of the following structure:

  Parser: {
    yy: {}
  }

  Parser.prototype: {
    yy: {},
    trace: function(),
    symbols_: {associative list: name ==> number},
    terminals_: {associative list: number ==> name},
    productions_: [...],
    performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate, $$, _$),
    table: [...],
    defaultActions: {...},
    parseError: function(str, hash),
    parse: function(input),

    lexer: {
        EOF: 1,
        parseError: function(str, hash),
        setInput: function(input),
        input: function(),
        unput: function(str),
        more: function(),
        less: function(n),
        pastInput: function(),
        upcomingInput: function(),
        showPosition: function(),
        test_match: function(regex_match_array, rule_index),
        next: function(),
        lex: function(),
        begin: function(condition),
        popState: function(),
        _currentRules: function(),
        topState: function(),
        pushState: function(condition),

        options: {
            ranges: boolean           (optional: true ==> token location info will include a .range[] member)
            flex: boolean             (optional: true ==> flex-like lexing behaviour where the rules are tested exhaustively to find the longest match)
            backtrack_lexer: boolean  (optional: true ==> lexer regexes are tested in order and for each matching regex the action code is invoked; the lexer terminates the scan when a token is returned by the action code)
        },

        performAction: function(yy, yy_, $avoiding_name_collisions, YY_START),
        rules: [...],
        conditions: {associative list: name ==> set},
    }
  }


  token location info (@$, _$, etc.): {
    first_line: n,
    last_line: n,
    first_column: n,
    last_column: n,
    range: [start_number, end_number]       (where the numbers are indexes into the input string, regular zero-based)
  }


  the parseError function receives a 'hash' object with these members for lexer and parser errors: {
    text:        (matched text)
    token:       (the produced terminal token, if any)
    line:        (yylineno)
  }
  while parser (grammar) errors will also provide these members, i.e. parser errors deliver a superset of attributes: {
    loc:         (yylloc)
    expected:    (string describing the set of expected tokens)
    recoverable: (boolean: TRUE when the parser has a error recovery rule available for this particular error)
  }
*/
var rulesParser = (function () {
    var o = function (k, v, o, l) { for (o = o || {}, l = k.length; l--; o[k[l]] = v)
        ; return o; }, $V0 = [1, 5], $V1 = [5, 17], $V2 = [8, 19], $V3 = [1, 12], $V4 = [10, 12], $V5 = [10, 12, 14, 17], $V6 = [10, 12, 16, 17, 18];
    var parser = { trace: function trace() { },
        yy: {},
        symbols_: { "error": 2, "START": 3, "RULESETS": 4, "EOF": 5, "RULESET": 6, "STATES": 7, "COLON": 8, "RULES": 9, "SEMICOLON": 10, "RULE": 11, "MINUS": 12, "TOKENS": 13, "IMPLIES": 14, "THEN": 15, "TEXT": 16, "SYMBOL": 17, "NUMBER": 18, "COMMA": 19, "$accept": 0, "$end": 1 },
        terminals_: { 2: "error", 5: "EOF", 8: "COLON", 10: "SEMICOLON", 12: "MINUS", 14: "IMPLIES", 16: "TEXT", 17: "SYMBOL", 18: "NUMBER", 19: "COMMA" },
        productions_: [0, [3, 2], [4, 1], [4, 2], [6, 4], [9, 1], [9, 2], [11, 4], [11, 2], [15, 2], [15, 2], [15, 2], [15, 1], [15, 1], [15, 1], [13, 1], [13, 2], [7, 1], [7, 3]],
        performAction: function anonymous(yytext, yyleng, yylineno, yy, yystate /* action[1] */, $$ /* vstack */, _$ /* lstack */) {
            /* this == yyval */
            var $0 = $$.length - 1;
            switch (yystate) {
                case 1:
                    this.$ = $$[$0 - 1];
                    return (this.$);
                    break;
                case 2:
                case 5:
                case 15:
                case 17:
                    this.$ = [$$[$0]];
                    break;
                case 3:
                case 6:
                case 16:
                    this.$ = $$[$0 - 1];
                    this.$.push($$[$0]);
                    break;
                case 4:
                    this.$ = { states: $$[$0 - 3], rules: $$[$0 - 1] };
                    break;
                case 7:
                    this.$ = { tokens: $$[$0 - 2], then: $$[$0] };
                    break;
                case 8:
                    this.$ = { tokens: $$[$0] };
                    break;
                case 9:
                    this.$ = $$[$0 - 1];
                    this.$['text'] = $$[$0];
                    break;
                case 10:
                    this.$ = $$[$0 - 1];
                    this.$['state'] = $$[$0];
                    break;
                case 11:
                    this.$ = $$[$0 - 1];
                    this.$['consume'] = $$[$0];
                    break;
                case 12:
                    this.$ = { text: $$[$0] };
                    break;
                case 13:
                    this.$ = { state: $$[$0] };
                    break;
                case 14:
                    this.$ = { consume: $$[$0] };
                    break;
                case 18:
                    this.$ = $$[$0 - 2];
                    this.$.push($$[$0]);
                    break;
            }
        },
        table: [{ 3: 1, 4: 2, 6: 3, 7: 4, 17: $V0 }, { 1: [3] }, { 5: [1, 6], 6: 7, 7: 4, 17: $V0 }, o($V1, [2, 2]), { 8: [1, 8], 19: [1, 9] }, o($V2, [2, 17]), { 1: [2, 1] }, o($V1, [2, 3]), { 9: 10, 11: 11, 12: $V3 }, { 17: [1, 13] }, { 10: [1, 14], 11: 15, 12: $V3 }, o($V4, [2, 5]), { 13: 16, 17: [1, 17] }, o($V2, [2, 18]), o($V1, [2, 4]), o($V4, [2, 6]), o($V4, [2, 8], { 14: [1, 18], 17: [1, 19] }), o($V5, [2, 15]), { 15: 20, 16: [1, 21], 17: [1, 22], 18: [1, 23] }, o($V5, [2, 16]), o($V4, [2, 7], { 16: [1, 24], 17: [1, 25], 18: [1, 26] }), o($V6, [2, 12]), o($V6, [2, 13]), o($V6, [2, 14]), o($V6, [2, 9]), o($V6, [2, 10]), o($V6, [2, 11])],
        defaultActions: { 6: [2, 1] },
        parseError: function parseError(str, hash) {
            if (hash.recoverable) {
                this.trace(str);
            }
            else {
                var error = new Error(str);
                error.hash = hash;
                throw error;
            }
        },
        parse: function parse(input) {
            var self = this, stack = [0], tstack = [], vstack = [null], lstack = [], table = this.table, yytext = '', yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
            var args = lstack.slice.call(arguments, 1);
            var lexer = Object.create(this.lexer);
            var sharedState = { yy: {} };
            for (var k in this.yy) {
                if (Object.prototype.hasOwnProperty.call(this.yy, k)) {
                    sharedState.yy[k] = this.yy[k];
                }
            }
            lexer.setInput(input, sharedState.yy);
            sharedState.yy.lexer = lexer;
            sharedState.yy.parser = this;
            if (typeof lexer.yylloc == 'undefined') {
                lexer.yylloc = {};
            }
            var yyloc = lexer.yylloc;
            lstack.push(yyloc);
            var ranges = lexer.options && lexer.options.ranges;
            if (typeof sharedState.yy.parseError === 'function') {
                this.parseError = sharedState.yy.parseError;
            }
            else {
                this.parseError = Object.getPrototypeOf(this).parseError;
            }
            function popStack(n) {
                stack.length = stack.length - 2 * n;
                vstack.length = vstack.length - n;
                lstack.length = lstack.length - n;
            }
            _token_stack: var lex = function () {
                var token;
                token = lexer.lex() || EOF;
                if (typeof token !== 'number') {
                    token = self.symbols_[token] || token;
                }
                return token;
            };
            var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
            while (true) {
                state = stack[stack.length - 1];
                if (this.defaultActions[state]) {
                    action = this.defaultActions[state];
                }
                else {
                    if (symbol === null || typeof symbol == 'undefined') {
                        symbol = lex();
                    }
                    action = table[state] && table[state][symbol];
                }
                if (typeof action === 'undefined' || !action.length || !action[0]) {
                    var errStr = '';
                    expected = [];
                    for (p in table[state]) {
                        if (this.terminals_[p] && p > TERROR) {
                            expected.push('\'' + this.terminals_[p] + '\'');
                        }
                    }
                    if (lexer.showPosition) {
                        errStr = 'Parse error on line ' + (yylineno + 1) + ':\n' + lexer.showPosition() + '\nExpecting ' + expected.join(', ') + ', got \'' + (this.terminals_[symbol] || symbol) + '\'';
                    }
                    else {
                        errStr = 'Parse error on line ' + (yylineno + 1) + ': Unexpected ' + (symbol == EOF ? 'end of input' : '\'' + (this.terminals_[symbol] || symbol) + '\'');
                    }
                    this.parseError(errStr, {
                        text: lexer.match,
                        token: this.terminals_[symbol] || symbol,
                        line: lexer.yylineno,
                        loc: yyloc,
                        expected: expected
                    });
                }
                if (action[0] instanceof Array && action.length > 1) {
                    throw new Error('Parse Error: multiple actions possible at state: ' + state + ', token: ' + symbol);
                }
                switch (action[0]) {
                    case 1:
                        stack.push(symbol);
                        vstack.push(lexer.yytext);
                        lstack.push(lexer.yylloc);
                        stack.push(action[1]);
                        symbol = null;
                        if (!preErrorSymbol) {
                            yyleng = lexer.yyleng;
                            yytext = lexer.yytext;
                            yylineno = lexer.yylineno;
                            yyloc = lexer.yylloc;
                            if (recovering > 0) {
                                recovering--;
                            }
                        }
                        else {
                            symbol = preErrorSymbol;
                            preErrorSymbol = null;
                        }
                        break;
                    case 2:
                        len = this.productions_[action[1]][1];
                        yyval.$ = vstack[vstack.length - len];
                        yyval._$ = {
                            first_line: lstack[lstack.length - (len || 1)].first_line,
                            last_line: lstack[lstack.length - 1].last_line,
                            first_column: lstack[lstack.length - (len || 1)].first_column,
                            last_column: lstack[lstack.length - 1].last_column
                        };
                        if (ranges) {
                            yyval._$.range = [
                                lstack[lstack.length - (len || 1)].range[0],
                                lstack[lstack.length - 1].range[1]
                            ];
                        }
                        r = this.performAction.apply(yyval, [
                            yytext,
                            yyleng,
                            yylineno,
                            sharedState.yy,
                            action[1],
                            vstack,
                            lstack
                        ].concat(args));
                        if (typeof r !== 'undefined') {
                            return r;
                        }
                        if (len) {
                            stack = stack.slice(0, -1 * len * 2);
                            vstack = vstack.slice(0, -1 * len);
                            lstack = lstack.slice(0, -1 * len);
                        }
                        stack.push(this.productions_[action[1]][0]);
                        vstack.push(yyval.$);
                        lstack.push(yyval._$);
                        newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
                        stack.push(newState);
                        break;
                    case 3:
                        return true;
                }
            }
            return true;
        } };
    /* generated by jison-lex 0.3.4 */
    var lexer = (function () {
        var lexer = ({
            EOF: 1,
            parseError: function parseError(str, hash) {
                if (this.yy.parser) {
                    this.yy.parser.parseError(str, hash);
                }
                else {
                    throw new Error(str);
                }
            },
            // resets the lexer, sets new input
            setInput: function (input, yy) {
                this.yy = yy || this.yy || {};
                this._input = input;
                this._more = this._backtrack = this.done = false;
                this.yylineno = this.yyleng = 0;
                this.yytext = this.matched = this.match = '';
                this.conditionStack = ['INITIAL'];
                this.yylloc = {
                    first_line: 1,
                    first_column: 0,
                    last_line: 1,
                    last_column: 0
                };
                if (this.options.ranges) {
                    this.yylloc.range = [0, 0];
                }
                this.offset = 0;
                return this;
            },
            // consumes and returns one char from the input
            input: function () {
                var ch = this._input[0];
                this.yytext += ch;
                this.yyleng++;
                this.offset++;
                this.match += ch;
                this.matched += ch;
                var lines = ch.match(/(?:\r\n?|\n).*/g);
                if (lines) {
                    this.yylineno++;
                    this.yylloc.last_line++;
                }
                else {
                    this.yylloc.last_column++;
                }
                if (this.options.ranges) {
                    this.yylloc.range[1]++;
                }
                this._input = this._input.slice(1);
                return ch;
            },
            // unshifts one char (or a string) into the input
            unput: function (ch) {
                var len = ch.length;
                var lines = ch.split(/(?:\r\n?|\n)/g);
                this._input = ch + this._input;
                this.yytext = this.yytext.substr(0, this.yytext.length - len);
                //this.yyleng -= len;
                this.offset -= len;
                var oldLines = this.match.split(/(?:\r\n?|\n)/g);
                this.match = this.match.substr(0, this.match.length - 1);
                this.matched = this.matched.substr(0, this.matched.length - 1);
                if (lines.length - 1) {
                    this.yylineno -= lines.length - 1;
                }
                var r = this.yylloc.range;
                this.yylloc = {
                    first_line: this.yylloc.first_line,
                    last_line: this.yylineno + 1,
                    first_column: this.yylloc.first_column,
                    last_column: lines ?
                        (lines.length === oldLines.length ? this.yylloc.first_column : 0)
                            + oldLines[oldLines.length - lines.length].length - lines[0].length :
                        this.yylloc.first_column - len
                };
                if (this.options.ranges) {
                    this.yylloc.range = [r[0], r[0] + this.yyleng - len];
                }
                this.yyleng = this.yytext.length;
                return this;
            },
            // When called from action, caches matched text and appends it on next action
            more: function () {
                this._more = true;
                return this;
            },
            // When called from action, signals the lexer that this rule fails to match the input, so the next matching rule (regex) should be tested instead.
            reject: function () {
                if (this.options.backtrack_lexer) {
                    this._backtrack = true;
                }
                else {
                    return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. You can only invoke reject() in the lexer when the lexer is of the backtracking persuasion (options.backtrack_lexer = true).\n' + this.showPosition(), {
                        text: "",
                        token: null,
                        line: this.yylineno
                    });
                }
                return this;
            },
            // retain first n characters of the match
            less: function (n) {
                this.unput(this.match.slice(n));
            },
            // displays already matched input, i.e. for error messages
            pastInput: function () {
                var past = this.matched.substr(0, this.matched.length - this.match.length);
                return (past.length > 20 ? '...' : '') + past.substr(-20).replace(/\n/g, "");
            },
            // displays upcoming input, i.e. for error messages
            upcomingInput: function () {
                var next = this.match;
                if (next.length < 20) {
                    next += this._input.substr(0, 20 - next.length);
                }
                return (next.substr(0, 20) + (next.length > 20 ? '...' : '')).replace(/\n/g, "");
            },
            // displays the character position where the lexing error occurred, i.e. for error messages
            showPosition: function () {
                var pre = this.pastInput();
                var c = new Array(pre.length + 1).join("-");
                return pre + this.upcomingInput() + "\n" + c + "^";
            },
            // test the lexed token: return FALSE when not a match, otherwise return token
            test_match: function (match, indexed_rule) {
                var token, lines, backup;
                if (this.options.backtrack_lexer) {
                    // save context
                    backup = {
                        yylineno: this.yylineno,
                        yylloc: {
                            first_line: this.yylloc.first_line,
                            last_line: this.last_line,
                            first_column: this.yylloc.first_column,
                            last_column: this.yylloc.last_column
                        },
                        yytext: this.yytext,
                        match: this.match,
                        matches: this.matches,
                        matched: this.matched,
                        yyleng: this.yyleng,
                        offset: this.offset,
                        _more: this._more,
                        _input: this._input,
                        yy: this.yy,
                        conditionStack: this.conditionStack.slice(0),
                        done: this.done
                    };
                    if (this.options.ranges) {
                        backup.yylloc.range = this.yylloc.range.slice(0);
                    }
                }
                lines = match[0].match(/(?:\r\n?|\n).*/g);
                if (lines) {
                    this.yylineno += lines.length;
                }
                this.yylloc = {
                    first_line: this.yylloc.last_line,
                    last_line: this.yylineno + 1,
                    first_column: this.yylloc.last_column,
                    last_column: lines ?
                        lines[lines.length - 1].length - lines[lines.length - 1].match(/\r?\n?/)[0].length :
                        this.yylloc.last_column + match[0].length
                };
                this.yytext += match[0];
                this.match += match[0];
                this.matches = match;
                this.yyleng = this.yytext.length;
                if (this.options.ranges) {
                    this.yylloc.range = [this.offset, this.offset += this.yyleng];
                }
                this._more = false;
                this._backtrack = false;
                this._input = this._input.slice(match[0].length);
                this.matched += match[0];
                token = this.performAction.call(this, this.yy, this, indexed_rule, this.conditionStack[this.conditionStack.length - 1]);
                if (this.done && this._input) {
                    this.done = false;
                }
                if (token) {
                    return token;
                }
                else if (this._backtrack) {
                    // recover context
                    for (var k in backup) {
                        this[k] = backup[k];
                    }
                    return false; // rule action called reject() implying the next rule should be tested instead.
                }
                return false;
            },
            // return next match in input
            next: function () {
                if (this.done) {
                    return this.EOF;
                }
                if (!this._input) {
                    this.done = true;
                }
                var token, match, tempMatch, index;
                if (!this._more) {
                    this.yytext = '';
                    this.match = '';
                }
                var rules = this._currentRules();
                for (var i = 0; i < rules.length; i++) {
                    tempMatch = this._input.match(this.rules[rules[i]]);
                    if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                        match = tempMatch;
                        index = i;
                        if (this.options.backtrack_lexer) {
                            token = this.test_match(tempMatch, rules[i]);
                            if (token !== false) {
                                return token;
                            }
                            else if (this._backtrack) {
                                match = false;
                                continue; // rule action called reject() implying a rule MISmatch.
                            }
                            else {
                                // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                                return false;
                            }
                        }
                        else if (!this.options.flex) {
                            break;
                        }
                    }
                }
                if (match) {
                    token = this.test_match(match, rules[index]);
                    if (token !== false) {
                        return token;
                    }
                    // else: this is a lexer rule which consumes input without producing a token (e.g. whitespace)
                    return false;
                }
                if (this._input === "") {
                    return this.EOF;
                }
                else {
                    return this.parseError('Lexical error on line ' + (this.yylineno + 1) + '. Unrecognized text.\n' + this.showPosition(), {
                        text: "",
                        token: null,
                        line: this.yylineno
                    });
                }
            },
            // return next match that has a token
            lex: function lex() {
                var r = this.next();
                if (r) {
                    return r;
                }
                else {
                    return this.lex();
                }
            },
            // activates a new lexer condition state (pushes the new lexer condition state onto the condition stack)
            begin: function begin(condition) {
                this.conditionStack.push(condition);
            },
            // pop the previously active lexer condition state off the condition stack
            popState: function popState() {
                var n = this.conditionStack.length - 1;
                if (n > 0) {
                    return this.conditionStack.pop();
                }
                else {
                    return this.conditionStack[0];
                }
            },
            // produce the lexer rule set which is active for the currently active lexer condition state
            _currentRules: function _currentRules() {
                if (this.conditionStack.length && this.conditionStack[this.conditionStack.length - 1]) {
                    return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules;
                }
                else {
                    return this.conditions["INITIAL"].rules;
                }
            },
            // return the currently active lexer condition state; when an index argument is provided it produces the N-th previous condition state, if available
            topState: function topState(n) {
                n = this.conditionStack.length - 1 - Math.abs(n || 0);
                if (n >= 0) {
                    return this.conditionStack[n];
                }
                else {
                    return "INITIAL";
                }
            },
            // alias for begin(condition)
            pushState: function pushState(condition) {
                this.begin(condition);
            },
            // return the number of states currently on the stack
            stateStackSize: function stateStackSize() {
                return this.conditionStack.length;
            },
            options: {},
            performAction: function anonymous(yy, yy_, $avoiding_name_collisions, YY_START) {
                var YYSTATE = YY_START;
                switch ($avoiding_name_collisions) {
                    case 0: /* skip whitespace */
                        break;
                    case 1:
                        yy_.yytext = yy_.yytext.substr(1, yy_.yyleng - 2);
                        return 16;
                        break;
                    case 2:
                        return 17;
                        break;
                    case 3:
                        return 8;
                        break;
                    case 4:
                        return 19;
                        break;
                    case 5:
                        return 10;
                        break;
                    case 6:
                        return 18;
                        break;
                    case 7:
                        return 12;
                        break;
                    case 8:
                        return 14;
                        break;
                    case 9:
                        return 'ANY';
                        break;
                    case 10:
                        return 'INVALID';
                        break;
                    case 11:
                        return 5;
                        break;
                }
            },
            rules: [/^(?:\s+)/, /^(?:'(.)*')/, /^(?:[a-zA-Z][a-zA-Z0-9\-_]*|\*)/, /^(?::)/, /^(?:,)/, /^(?:;)/, /^(?:[0-9]+)/, /^(?:-)/, /^(?:=>)/, /^(?:\*)/, /^(?:.)/, /^(?:$)/],
            conditions: { "INITIAL": { "rules": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], "inclusive": true } }
        });
        return lexer;
    })();
    parser.lexer = lexer;
    function Parser() {
        this.yy = {};
    }
    Parser.prototype = parser;
    parser.Parser = Parser;
    return new Parser;
})();
if (typeof require !== 'undefined' && typeof exports !== 'undefined') {
    exports.parser = rulesParser;
    exports.Parser = rulesParser.Parser;
    exports.parse = function () { return rulesParser.parse.apply(rulesParser, arguments); };
    exports.main = function commonjsMain(args) {
        if (!args[1]) {
            console.log('Usage: ' + args[0] + ' FILE');
            process.exit(1);
        }
        var source = require('fs').readFileSync(require('path').normalize(args[1]), "utf8");
        return exports.parser.parse(source);
    };
    if (typeof module !== 'undefined' && require.main === module) {
        exports.main(process.argv.slice(1));
    }
}

}).call(this,require('_process'))

},{"_process":3,"fs":1,"path":2}]},{},[4])(4)
});

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9saWIvX2VtcHR5LmpzIiwibm9kZV9tb2R1bGVzL3BhdGgtYnJvd3NlcmlmeS9pbmRleC5qcyIsIm5vZGVfbW9kdWxlcy9wcm9jZXNzL2Jyb3dzZXIuanMiLCJzcmMvZXhwb3J0cy50cyIsInNyYy9pZy9lbmdpbmUudHMiLCJzcmMvaWcvZW52LnRzIiwic3JjL2lnL25vZGUudHMiLCJzcmMvaWcvcGFyc2VyLmpzIiwic3JjL2lndC9lbmdpbmUudHMiLCJzcmMvcHAvZW5naW5lLnRzIiwic3JjL3BwL2xleGVyLmpzIiwic3JjL3BwL3J1bGVzLnRzIiwic3JjL3BwL3J1bGVzUGFyc2VyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQzlTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQ3hMQSxJQUFBLFdBQUEsUUFBQSxhQUFBLENBQUE7QUFHUyxRQUFBLFlBQUEsR0FIQSxTQUFBLFlBR0E7QUFGVCxJQUFBLFdBQUEsUUFBQSxjQUFBLENBQUE7QUFFdUIsUUFBQSxvQkFBQSxHQUZkLFNBQUEsb0JBRWM7Ozs7QUNIdkI7Ozs7Ozs7OztBQUlBLElBQUEsV0FBQSxRQUFBLGNBQUEsQ0FBQTtBQUNBLElBQUEsVUFBQSxRQUFBLGFBQUEsQ0FBQTtBQUNBLElBQUEsYUFBQSxRQUFBLGdCQUFBLENBQUE7QUFDQSxJQUFBLGNBQUEsUUFBQSxhQUFBLENBQUE7QUFDQSxJQUFBLFNBQUEsUUFBQSxRQUFBLENBQUE7QUFDQSxJQUFBLFFBQUEsUUFBQSxPQUFBLENBQUE7QUFHQTs7OztJQUdNLFk7QUFTRjs7Ozs7Ozs7O0FBVUEsMEJBQVksS0FBWixFQUF5QjtBQUFBOztBQUNyQixhQUFLLFlBQUwsR0FBb0IsSUFBSSxTQUFBLFlBQUosQ0FBaUIsUUFBQSxLQUFqQixFQUF3QixXQUFBLEtBQXhCLEVBQStCLE9BQS9CLENBQXBCO0FBQ0EsYUFBSyxLQUFMLEdBQWEsS0FBYjtBQUNBLFlBQU0sU0FBUyw0QkFBZjtBQUNBLGFBQUsseUJBQUwsR0FBaUMsS0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQixNQUFuQixFQUEyQixFQUEzQixDQUFqQztBQUNBLGFBQUssaUJBQUwsR0FBeUIsS0FBSyxZQUFMLENBQWtCLE9BQWxCLENBQTBCLEtBQUsseUJBQS9CLENBQXpCO0FBQ0EsYUFBSyxHQUFMLEdBQVcsWUFBQSxLQUFBLENBQWtCLEtBQUssaUJBQXZCLENBQVg7QUFDQSxZQUFJLFdBQXFDLEVBQXpDO0FBQ0EsWUFBSSxPQUFPLElBQVAsQ0FBWSxLQUFLLEdBQWpCLEVBQXNCLE1BQXRCLEdBQStCLENBQW5DLEVBQXNDO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQ2xDLHFDQUFvQixLQUFLLEdBQXpCLDhIQUE4QjtBQUFBLHdCQUFyQixPQUFxQjs7QUFDMUIsNkJBQVMsUUFBUSxJQUFqQixJQUF5QixPQUFBLElBQUEsQ0FBSyxPQUFMLENBQWEsT0FBYixDQUF6QjtBQUNIO0FBSGlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJckM7QUFDRCxhQUFLLFFBQUwsR0FBZ0IsUUFBaEI7QUFDSDtBQUVEOzs7Ozs7Ozs7O21DQU0yRDtBQUFBLGdCQUFsRCxXQUFrRCx1RUFBcEMsUUFBb0M7QUFBQSxnQkFBMUIsa0JBQTBCLHVFQUFMLEtBQUs7O0FBQ3ZELGdCQUFJLGlCQUFpQixFQUFyQjtBQUNBLGdCQUFJLHNCQUF1QixLQUFLLEdBQUwsSUFBWSxJQUF2QyxFQUE4QztBQUMxQyxpQ0FBaUIsS0FBSyxHQUFMLENBQVMsY0FBMUI7QUFDSDtBQUNELGlCQUFLLEdBQUwsR0FBVyxJQUFJLE1BQUEsV0FBSixDQUFnQixLQUFLLFFBQXJCLEVBQStCLGNBQS9CLEVBQStDLFdBQS9DLENBQVg7QUFDQSxtQkFBTyxLQUFLLEdBQUwsQ0FBUyxRQUFULEVBQVA7QUFDSDtBQUVEOzs7Ozs7O2lDQUlnQixLLEVBQXFDO0FBQUEsZ0JBQXRCLFdBQXNCLHVFQUFSLFFBQVE7O0FBQ2pELG1CQUFRLElBQUksWUFBSixDQUFpQixLQUFqQixDQUFELENBQTBCLFFBQTFCLENBQW1DLFdBQW5DLENBQVA7QUFDSDs7Ozs7O0FBSUksUUFBQSxZQUFBLEdBQUEsWUFBQTs7OztBQzNFVDs7Ozs7Ozs7Ozs7OztBQUlBLElBQUEsU0FBQSxRQUFBLFFBQUEsQ0FBQTtBQUVBOzs7OztJQUllLEs7Ozs7QUF5SzJCLFFBQUEsS0FBQSxHQUFBLEtBQUE7O0lBbktwQyxTOzs7QUFHRix1QkFBWSxRQUFaLEVBQTRCO0FBQUE7O0FBQUE7O0FBRjVCLGNBQUEsSUFBQSxHQUFPLFdBQVA7QUFJSSxjQUFLLFFBQUwsR0FBZ0IsUUFBaEI7QUFGd0I7QUFHM0I7OztFQU5tQixLOztBQW1Lb0QsUUFBQSxTQUFBLEdBQUEsU0FBQTs7SUF6SnRFLFM7OztBQUFOLHlCQUFBO0FBQUE7O0FBQUEsNEgsU0FBQTs7QUFDSSxlQUFBLElBQUEsR0FBTyxXQUFQO0FBREo7QUFFQzs7O0VBRnVCLEs7O0FBeUp5QyxRQUFBLFNBQUEsR0FBQSxTQUFBOztJQXBKM0QsYzs7O0FBQU4sOEJBQUE7QUFBQTs7QUFBQSxzSSxTQUFBOztBQUNJLGVBQUEsSUFBQSxHQUFPLFdBQVA7QUFESjtBQUVDOzs7RUFGNEIsSzs7QUFvSm9CLFFBQUEsY0FBQSxHQUFBLGNBQUE7QUEvSWpEOzs7Ozs7Ozs7SUFTTSxXO0FBY0YseUJBQVksUUFBWixFQUEwSDtBQUFBLFlBQTFFLGNBQTBFLHVFQUF2QixFQUF1QjtBQUFBLFlBQW5CLFdBQW1COztBQUFBOztBQUN0SCxhQUFLLE1BQUwsR0FBYyxJQUFJLEtBQUosRUFBZDtBQUNBLGFBQUssUUFBTCxHQUFnQixRQUFoQjtBQUNBLGFBQUssY0FBTCxHQUFzQixjQUF0QjtBQUNBLGFBQUssV0FBTCxHQUFtQixXQUFuQjtBQUNBLGFBQUssTUFBTCxHQUFjLEVBQWQ7QUFDQSxhQUFLLE1BQUwsR0FBYyxFQUFkO0FBQ0EsYUFBSyxPQUFMLEdBQWUsRUFBZjtBQUNIOzs7O3dDQVpZO0FBQ1QsbUJBQU8sS0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixHQUFsQixDQUFQO0FBQ0g7QUFZRDs7Ozs7OztzQ0FJYyxJLEVBQVk7QUFFdEI7QUFDQSxnQkFBSSxPQUFPLElBQVAsQ0FBWSxLQUFLLFFBQWpCLEVBQTJCLE1BQTNCLElBQXFDLENBQXpDLEVBQTRDO0FBQ3hDLHVCQUFPLElBQUksT0FBQSxTQUFKLENBQWMsRUFBZCxDQUFQO0FBQ0g7QUFFRCxnQkFBSSxLQUFLLFFBQUwsQ0FBYyxJQUFkLENBQUosRUFBeUI7QUFDckIsb0JBQUksS0FBSyxNQUFMLENBQVksSUFBWixDQUFKLEVBQXVCO0FBQ25CLHlCQUFLLE1BQUwsQ0FBWSxJQUFaLEtBQXFCLENBQXJCO0FBQ0Esd0JBQUksS0FBSyxNQUFMLENBQVksSUFBWixLQUFxQixZQUFZLG1CQUFyQyxFQUEwRDtBQUN0RCw4QkFBTSxJQUFJLEtBQUosNENBQW1ELElBQW5ELENBQU47QUFDSDtBQUNKLGlCQUxELE1BTUs7QUFDRCx5QkFBSyxNQUFMLENBQVksSUFBWixJQUFvQixDQUFwQjtBQUNIO0FBQ0QsdUJBQU8sS0FBSyxRQUFMLENBQWMsSUFBZCxDQUFQO0FBQ0gsYUFYRCxNQVlLO0FBQ0Qsc0JBQU0sSUFBSSxLQUFKLHlCQUFnQyxJQUFoQyxDQUFOO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7dUNBR1k7QUFDUixnQkFBSSxTQUFTLEVBQWI7QUFDQSxnQkFBSSxPQUFPLElBQVg7QUFDQSxnQkFBSSxZQUFZLEtBQWhCO0FBSFE7QUFBQTtBQUFBOztBQUFBO0FBSVIscUNBQWtCLEtBQUssTUFBdkIsOEhBQThCO0FBQUEsd0JBQXJCLEtBQXFCOztBQUMxQiw0QkFBUSxNQUFNLElBQWQ7QUFDSSw2QkFBSyxXQUFMO0FBQWtCO0FBQ2Qsb0NBQUksSUFBSixFQUFVO0FBQ04sMkNBQU8sS0FBUDtBQUNILGlDQUZELE1BR0s7QUFDRCw4Q0FBVSxHQUFWO0FBQ0g7QUFDRCxvQ0FBSSxTQUFKLEVBQWU7QUFDWCw4Q0FBVSxNQUFNLFFBQU4sQ0FBZSxNQUFmLENBQXNCLENBQXRCLEVBQXlCLFdBQXpCLEVBQVY7QUFDQSw4Q0FBVSxNQUFNLFFBQU4sQ0FBZSxLQUFmLENBQXFCLENBQXJCLENBQVY7QUFDQSxnREFBWSxLQUFaO0FBQ0gsaUNBSkQsTUFJTztBQUNILDhDQUFVLE1BQU0sUUFBaEI7QUFDSDtBQUNEO0FBQ0g7QUFDRCw2QkFBSyxXQUFMO0FBQWtCO0FBQ2QsdUNBQU8sSUFBUDtBQUNBO0FBQ0g7QUFDRCw2QkFBSyxXQUFMO0FBQWtCO0FBQ2QsNENBQVksSUFBWjtBQUNBO0FBQ0g7QUF4Qkw7QUEwQkg7QUEvQk87QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFnQ1IsbUJBQU8sTUFBUDtBQUNIO0FBRUQ7Ozs7OzttQ0FHUTtBQUNKO0FBQ0EsZ0JBQUksS0FBSyxjQUFMLENBQW9CLEtBQUssV0FBekIsS0FBeUMsSUFBN0MsRUFBbUQ7QUFDL0MscUJBQUssTUFBTCxHQUFjLEtBQUssY0FBTCxDQUFvQixLQUFLLFdBQXpCLENBQWQ7QUFDSCxhQUZELE1BR0s7QUFDRCxvQkFBSSxZQUFZLEtBQUssYUFBTCxDQUFtQixLQUFLLFdBQXhCLENBQWhCO0FBQ0EscUJBQUssTUFBTCxHQUFjLFVBQVUsUUFBVixDQUFtQixJQUFuQixFQUF5QixFQUF6QixDQUFkO0FBQ0g7QUFDRCxpQkFBSyxNQUFMLEdBQWMsS0FBSyxZQUFMLEVBQWQ7QUFDQSxtQkFBTyxLQUFLLE1BQVo7QUFDSDs7Ozs7O0FBdkdNLFlBQUEsbUJBQUEsR0FBOEIsR0FBOUI7QUFxSUYsUUFBQSxXQUFBLEdBQUEsV0FBQTtBQTFCVDs7Ozs7OztJQU9NLGtCO0FBSUYsZ0NBQVksUUFBWixFQUE4QztBQUFBOztBQUMxQyxhQUFLLE9BQUwsR0FBZSxFQUFmO0FBQ0EsYUFBSyxRQUFMLEdBQWdCLFFBQWhCO0FBQ0g7Ozs7c0NBRWEsSSxFQUFZO0FBQ3RCLGdCQUFJLEtBQUssUUFBTCxDQUFjLElBQWQsQ0FBSixFQUF5QjtBQUNyQix1QkFBTyxLQUFLLFFBQUwsQ0FBYyxJQUFkLENBQVA7QUFDSCxhQUZELE1BR0s7QUFDRCxzQkFBTSxJQUFJLEtBQUoseUJBQWdDLElBQWhDLENBQU47QUFDSDtBQUNKOzs7Ozs7QUFHaUIsUUFBQSxrQkFBQSxHQUFBLGtCQUFBOzs7O0FDbkx0Qjs7Ozs7Ozs7Ozs7Ozs7O0FBSUEsSUFBQSxRQUFBLFFBQUEsT0FBQSxDQUFBO0FBR0EsU0FBUyxnQkFBVCxDQUEwQixDQUExQixFQUFxQyxDQUFyQyxFQUE4QztBQUMxQyxRQUFJLE9BQU8sSUFBSSxDQUFmO0FBQ0EsV0FBTyxLQUFLLEtBQUwsQ0FBYSxLQUFLLE1BQUwsTUFBaUIsT0FBTyxDQUF4QixDQUFELEdBQStCLENBQTNDLENBQVA7QUFDSDtBQUdELElBQUssUUFBTDtBQUFBLENBQUEsVUFBSyxRQUFMLEVBQWE7QUFDVCxhQUFBLE1BQUEsSUFBQSxNQUFBO0FBQ0EsYUFBQSxRQUFBLElBQUEsUUFBQTtBQUNBLGFBQUEsU0FBQSxJQUFBLFNBQUE7QUFDQSxhQUFBLEtBQUEsSUFBQSxLQUFBO0FBQ0EsYUFBQSxJQUFBLElBQUEsSUFBQTtBQUNBLGFBQUEsT0FBQSxJQUFBLE9BQUE7QUFDSCxDQVBELEVBQUssYUFBQSxXQUFRLEVBQVIsQ0FBTDtBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFpQmUsSTtBQWlCWCxrQkFBWSxPQUFaLEVBQXdCO0FBQUE7O0FBQ3BCLFlBQUksUUFBUSxRQUFSLElBQW9CLElBQXhCLEVBQThCO0FBQUUsaUJBQUssUUFBTCxHQUFnQixRQUFRLFFBQXhCO0FBQW1DO0FBQ25FLFlBQUksUUFBUSxRQUFSLElBQW9CLElBQXhCLEVBQThCO0FBQUUsaUJBQUssUUFBTCxHQUFnQixRQUFRLFFBQXhCO0FBQW1DO0FBQ25FLFlBQUksUUFBUSxRQUFSLElBQW9CLElBQXhCLEVBQThCO0FBQUUsaUJBQUssUUFBTCxHQUFnQixRQUFRLFFBQXhCO0FBQW1DO0FBQ25FLFlBQUksUUFBUSxTQUFSLElBQXFCLElBQXpCLEVBQStCO0FBQUUsaUJBQUssU0FBTCxHQUFpQixRQUFRLFNBQXpCO0FBQXFDO0FBQ3RFLFlBQUksUUFBUSxhQUFSLElBQXlCLElBQTdCLEVBQW1DO0FBQUUsaUJBQUssYUFBTCxHQUFxQixRQUFRLGFBQTdCO0FBQTZDO0FBQ2xGLFlBQUksUUFBUSxPQUFSLElBQW1CLElBQXZCLEVBQTZCO0FBQUUsaUJBQUssT0FBTCxHQUFlLFFBQVEsT0FBdkI7QUFBaUM7QUFDaEUsWUFBSSxRQUFRLFNBQVIsSUFBcUIsSUFBekIsRUFBK0I7QUFBRSxpQkFBSyxTQUFMLEdBQWlCLFFBQVEsU0FBekI7QUFBcUM7QUFDdEUsWUFBSSxRQUFRLElBQVIsSUFBZ0IsSUFBcEIsRUFBMEI7QUFBRSxpQkFBSyxJQUFMLEdBQVksUUFBUSxJQUFwQjtBQUEyQjtBQUN2RCxZQUFJLFFBQVEsT0FBUixJQUFtQixJQUF2QixFQUE2QjtBQUFFLGlCQUFLLE9BQUwsR0FBZSxRQUFRLE9BQXZCO0FBQWlDO0FBQ2hFLFlBQUksUUFBUSxZQUFSLElBQXdCLElBQTVCLEVBQWtDO0FBQUUsaUJBQUssWUFBTCxHQUFvQixRQUFRLFlBQTVCO0FBQTJDO0FBQy9FLFlBQUksUUFBUSxXQUFSLElBQXVCLElBQTNCLEVBQWlDO0FBQUUsaUJBQUssV0FBTCxHQUFtQixRQUFRLFdBQTNCO0FBQXlDO0FBQzVFLFlBQUksUUFBUSxNQUFSLElBQWtCLElBQXRCLEVBQTRCO0FBQUUsaUJBQUssTUFBTCxHQUFjLFFBQVEsTUFBdEI7QUFBK0I7QUFDaEU7QUFFRDs7Ozs7OztzQ0FHVztBQUNQLGdCQUFJLEtBQUssUUFBTCxJQUFpQixJQUFqQixJQUF5QixLQUFLLFFBQUwsSUFBaUIsSUFBOUMsRUFBb0Q7QUFDaEQsdUJBQU8saUJBQWlCLEtBQUssUUFBdEIsRUFBZ0MsS0FBSyxRQUFyQyxDQUFQO0FBQ0gsYUFGRCxNQUdLO0FBQ0QsdUJBQU8sQ0FBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozs7K0JBS08sVSxFQUFnQyxPLEVBQXNFO0FBQUEsZ0JBQTlDLE1BQThDLHVFQUE1QixLQUE0QjtBQUFBLGdCQUFyQixNQUFxQjs7QUFDekcsZ0JBQUksU0FBUyxDQUFiO0FBQ0Esc0JBQVUsS0FBSyxhQUFMLENBQW1CLE9BQW5CLENBQVY7QUFDQSxxQkFBUyxVQUFVLEtBQUssTUFBeEI7QUFDQSxnQkFBSSxNQUFKLEVBQVk7QUFDUixxQkFBSyxRQUFMLEdBQWdCLElBQWhCO0FBQ0g7QUFDRCxnQkFBSSxDQUFDLEtBQUssaUJBQUwsQ0FBdUIsT0FBdkIsQ0FBTCxFQUFzQztBQUNsQyx5QkFBUyxDQUFUO0FBQ0EseUJBQVMsS0FBSyxZQUFMLENBQWtCLFVBQWxCLEVBQThCLE9BQTlCLEVBQXVDLE1BQXZDLEVBQStDLE1BQS9DLEVBQXVELE1BQXZELENBQVQ7QUFDQSxvQkFBSSxVQUFVLElBQVYsSUFBa0IsS0FBSyxRQUFMLElBQWlCLElBQW5DLElBQTJDLEtBQUssUUFBTCxJQUFpQixJQUE1RCxJQUFvRSxTQUFTLENBQWpGLEVBQW9GO0FBQ2hGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBSSxLQUFLLFFBQUwsSUFBaUIsQ0FBckIsRUFBd0I7QUFDcEIsaUNBQVUsVUFBVSxLQUFLLFFBQUwsR0FBZ0IsS0FBSyxRQUEvQixDQUFELEdBQTZDLENBQXREO0FBQ0gscUJBRkQsTUFHSztBQUNELGlDQUFTLFVBQVcsS0FBSyxRQUFMLEdBQWdCLEtBQUssUUFBdEIsR0FBa0MsQ0FBNUMsQ0FBVDtBQUNIO0FBQ0o7QUFDSjtBQUNELHVCQUFXLE9BQVgsQ0FBbUIsSUFBbkIsQ0FBd0IsTUFBeEI7QUFDQSxpQkFBSyxjQUFMLEdBQXNCLE1BQXRCO0FBQ0EsbUJBQU8sTUFBUDtBQUNIO0FBU0Q7Ozs7Ozs7MENBSWtCLE8sRUFBc0I7QUFDcEMsZ0JBQUksS0FBSyxJQUFMLElBQWEsSUFBakIsRUFBdUI7QUFDbkI7QUFDQTtBQUZtQjtBQUFBO0FBQUE7O0FBQUE7QUFHbkIseUNBQW1CLE9BQW5CLDhIQUE0QjtBQUFBLDRCQUFuQixNQUFtQjs7QUFDeEIsNEJBQUksS0FBSyxJQUFMLENBQVUsT0FBVixDQUFrQixNQUFsQixLQUE2QixDQUFDLENBQWxDLEVBQXFDO0FBQ2pDO0FBQ0EsbUNBQU8sSUFBUDtBQUNIO0FBQ0o7QUFDRDtBQVRtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQVVuQix1QkFBTyxLQUFQO0FBQ0gsYUFYRCxNQVlLO0FBQ0Q7QUFDQSx1QkFBTyxLQUFQO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7c0NBR2MsTyxFQUFzQjtBQUNoQyxnQkFBSSxLQUFLLFlBQUwsS0FBc0IsSUFBMUIsRUFBZ0M7QUFBRSx1QkFBTyxFQUFQO0FBQVksYUFBOUMsTUFDSyxJQUFJLEtBQUssT0FBTCxJQUFnQixJQUFwQixFQUEwQjtBQUFFLG9EQUFXLE9BQVg7QUFBc0IsYUFBbEQsTUFDQTtBQUNELG9CQUFJLDBDQUFpQixPQUFqQixFQUFKO0FBREM7QUFBQTtBQUFBOztBQUFBO0FBRUQsMENBQW1CLEtBQUssT0FBeEIsbUlBQWlDO0FBQUEsNEJBQXhCLE1BQXdCOztBQUM3Qiw0QkFBSSxXQUFXLE9BQVgsQ0FBbUIsTUFBbkIsS0FBOEIsQ0FBQyxDQUFuQyxFQUFzQztBQUNsQyx1Q0FBVyxJQUFYLENBQWdCLE1BQWhCO0FBQ0g7QUFDSjtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBT0QsdUJBQU8sVUFBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7Ozs7OztpQ0FRUyxHLEVBQWtCLE8sRUFBc0I7QUFDN0MsZ0JBQUksU0FBdUIsRUFBM0I7QUFDQSxzQkFBVSxLQUFLLGFBQUwsQ0FBbUIsT0FBbkIsQ0FBVjtBQUVBLGdCQUFJLENBQUMsS0FBSyxpQkFBTCxDQUF1QixPQUF2QixDQUFMLEVBQXNDO0FBQ2xDLHFCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksS0FBSyxXQUFMLEVBQXBCLEVBQXdDLEdBQXhDLEVBQTZDO0FBQ3pDLHdCQUFJLEtBQUssU0FBVCxFQUFvQjtBQUFFLCtCQUFPLElBQVAsQ0FBWSxJQUFJLE1BQUEsY0FBSixFQUFaO0FBQW9DO0FBQzFELHdCQUFJLEtBQUssUUFBVCxFQUFtQjtBQUFFLCtCQUFPLElBQVAsQ0FBWSxJQUFJLE1BQUEsU0FBSixFQUFaO0FBQStCO0FBQ3BELDZCQUFTLE9BQU8sTUFBUCxDQUFlLEtBQUssY0FBTCxDQUFvQixHQUFwQixFQUF5QixPQUF6QixDQUFmLENBQVQ7QUFDQSx3QkFBSSxLQUFLLFNBQVQsRUFBb0I7QUFBRSwrQkFBTyxJQUFQLENBQVksSUFBSSxNQUFBLFNBQUosRUFBWjtBQUErQjtBQUN4RDtBQUNEO0FBQ0Esb0JBQUksS0FBSyxhQUFMLElBQXNCLElBQTFCLEVBQWdDO0FBQzVCLHdCQUFJLGNBQUosQ0FBbUIsS0FBSyxhQUF4QixJQUF5QyxNQUF6QztBQUNIO0FBQ0o7QUFDRCxtQkFBTyxNQUFQO0FBQ0g7QUFRRDs7Ozs7OztnQ0FJZSxPLEVBQVk7QUFDdkIsZ0JBQUksYUFBSjtBQUNBLG9CQUFRLFFBQVEsSUFBaEI7QUFDSSxxQkFBSyxTQUFTLElBQWQ7QUFDSSwyQkFBTyxJQUFJLFFBQUosQ0FBYSxPQUFiLENBQVA7QUFDQTtBQUNKLHFCQUFLLFNBQVMsTUFBZDtBQUNJLDJCQUFPLElBQUksVUFBSixDQUFlLE9BQWYsQ0FBUDtBQUNBO0FBQ0oscUJBQUssU0FBUyxPQUFkO0FBQ0ksMkJBQU8sSUFBSSxXQUFKLENBQWdCLE9BQWhCLENBQVA7QUFDQTtBQUNKLHFCQUFLLFNBQVMsR0FBZDtBQUNJLDJCQUFPLElBQUksT0FBSixDQUFZLE9BQVosQ0FBUDtBQUNBO0FBQ0oscUJBQUssU0FBUyxFQUFkO0FBQ0ksMkJBQU8sSUFBSSxNQUFKLENBQVcsT0FBWCxDQUFQO0FBQ0E7QUFDSixxQkFBSyxTQUFTLEtBQWQ7QUFDSSwyQkFBTyxJQUFJLFNBQUosQ0FBYyxPQUFkLENBQVA7QUFDQTtBQUNKO0FBQ0ksMEJBQU0sSUFBSSxLQUFKLGtDQUF5QyxRQUFRLElBQWpELENBQU47QUFwQlI7QUFzQkEsbUJBQU8sSUFBUDtBQUNIOzs7Ozs7QUE4UEksUUFBQSxJQUFBLEdBQUEsSUFBQTtBQTFQVDs7OztJQUdNLFE7OztBQUdGLHNCQUFZLE9BQVosRUFBd0I7QUFBQTs7QUFBQSx3SEFDZCxPQURjOztBQUVwQixjQUFLLFFBQUwsR0FBZ0IsUUFBUSxRQUF4QjtBQUZvQjtBQUd2Qjs7OztxQ0FFWSxVLEVBQWdDLE8sRUFBd0IsTSxFQUFpQixNLEVBQXVCLFcsRUFBbUI7QUFDNUgsbUJBQU8sV0FBUDtBQUNIOzs7dUNBRWMsRyxFQUFnQjtBQUMzQixtQkFBTSxDQUFDLElBQUksTUFBQSxTQUFKLENBQWMsS0FBSyxRQUFuQixDQUFELENBQU47QUFDSDs7OztFQWRrQixJO0FBa0J2Qjs7Ozs7SUFHTSxVOzs7QUFHRix3QkFBWSxPQUFaLEVBQXdCO0FBQUE7O0FBQUEsNkhBQ2QsT0FEYzs7QUFFcEIsZUFBSyxNQUFMLEdBQWMsUUFBUSxNQUF0QjtBQUZvQjtBQUd2Qjs7OztxQ0FFWSxVLEVBQWdDLE8sRUFBd0IsTSxFQUFpQixNLEVBQXVCLFcsRUFBbUI7QUFDNUgsZ0JBQUksY0FBYyxDQUFsQjtBQUNBO0FBQ0EsZ0JBQUksT0FBTyxPQUFQLENBQWUsS0FBSyxNQUFwQixJQUE4QixDQUFDLENBQW5DLEVBQXNDO0FBQ2xDO0FBQ0E7QUFDQSx1QkFBTyxDQUFQO0FBQ0g7QUFDRCxtQkFBTyxJQUFQLENBQVksS0FBSyxNQUFqQjtBQUNBOzs7O0FBSUEsZ0JBQUksVUFBVSxRQUFRLE1BQVIsR0FBaUIsQ0FBL0IsRUFBa0M7QUFDOUIsb0JBQUksWUFBWSxXQUFXLGFBQVgsQ0FBeUIsS0FBSyxNQUE5QixDQUFoQjtBQUNBLDhCQUFjLFVBQVUsTUFBVixDQUFpQixVQUFqQixFQUE2QixPQUE3QixFQUFzQyxNQUF0QyxFQUE4QyxNQUE5QyxDQUFkO0FBQ0EsK0JBQWUsV0FBZjtBQUNIO0FBRUQsbUJBQU8sV0FBUDtBQUNIOzs7dUNBRWMsRyxFQUFrQixPLEVBQXNCO0FBQ25EO0FBQ0EsZ0JBQUksSUFBSSxjQUFKLENBQW1CLEtBQUssTUFBeEIsQ0FBSixFQUFxQztBQUNqQyx1QkFBTyxJQUFJLGNBQUosQ0FBbUIsS0FBSyxNQUF4QixDQUFQO0FBQ0g7QUFDRCxnQkFBSSxPQUFPLElBQUksYUFBSixDQUFrQixLQUFLLE1BQXZCLENBQVg7QUFDQSxnQkFBSSxTQUFTLEtBQUssUUFBTCxDQUFjLEdBQWQsRUFBbUIsT0FBbkIsQ0FBYjtBQUNBLG1CQUFPLE1BQVA7QUFDSDs7OztFQXRDb0IsSTtBQXlDekI7Ozs7Ozs7OztJQU9NLFc7OztBQUdGLHlCQUFZLE9BQVosRUFBd0I7QUFBQTs7QUFBQSwrSEFDZCxPQURjOztBQUVwQixlQUFLLEtBQUwsR0FBYSxLQUFLLE9BQUwsQ0FBYSxRQUFRLEtBQXJCLENBQWI7QUFGb0I7QUFHdkI7Ozs7cUNBRVksVSxFQUFnQyxPLEVBQXdCLE0sRUFBaUIsTSxFQUF1QixXLEVBQW1CO0FBQzVIOzs7O0FBSUEsZ0JBQUksVUFBVSxRQUFRLE1BQVIsR0FBaUIsQ0FBL0IsRUFBa0M7QUFDOUIsb0JBQUksY0FBYyxLQUFLLEtBQUwsQ0FBVyxNQUFYLENBQWtCLFVBQWxCLEVBQThCLE9BQTlCLEVBQXVDLE1BQXZDLEVBQStDLE1BQS9DLENBQWxCO0FBQ0EsK0JBQWUsV0FBZjtBQUNIO0FBQ0QsbUJBQU8sV0FBUDtBQUNIOzs7dUNBRWMsRyxFQUFrQixPLEVBQXNCO0FBQ25ELG1CQUFRLEtBQUssS0FBTCxDQUFXLFFBQVgsQ0FBb0IsR0FBcEIsRUFBeUIsT0FBekIsQ0FBUjtBQUNIOzs7O0VBdEJxQixJO0FBMEIxQjs7Ozs7O0lBSWUsWTs7O0FBR1gsMEJBQVksT0FBWixFQUF3QjtBQUFBOztBQUFBLGlJQUNkLE9BRGM7O0FBRXBCLFlBQUksV0FBd0IsSUFBSSxLQUFKLEVBQTVCO0FBRm9CO0FBQUE7QUFBQTs7QUFBQTtBQUdwQixrQ0FBeUIsUUFBUSxRQUFqQyxtSUFBMkM7QUFBQSxvQkFBbEMsWUFBa0M7O0FBQ3ZDLHlCQUFTLElBQVQsQ0FBYyxLQUFLLE9BQUwsQ0FBYSxZQUFiLENBQWQ7QUFDSDtBQUxtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQU1wQixlQUFLLFFBQUwsR0FBZ0IsUUFBaEI7QUFOb0I7QUFPdkI7OztFQVYrQixJO0FBY3BDOzs7OztJQUdNLE87Ozs7Ozs7Ozs7O3FDQUNXLFUsRUFBZ0MsTyxFQUF3QixNLEVBQWlCLE0sRUFBdUIsVyxFQUFtQjtBQUM1SCxnQkFBSSxpQkFBaUIsQ0FBckI7QUFDQSxnQkFBSSxxQkFBcUIsQ0FBekIsQ0FGNEgsQ0FFaEc7QUFGZ0c7QUFBQTtBQUFBOztBQUFBO0FBRzVILHNDQUFrQixLQUFLLFFBQXZCLG1JQUFpQztBQUFBLHdCQUF4QixLQUF3Qjs7QUFDN0I7Ozs7Ozs7Ozs7QUFXQSx3QkFBSSxjQUFjLE1BQU0sTUFBTixDQUFhLFVBQWIsRUFBeUIsT0FBekIsRUFBa0MsS0FBbEMsRUFBeUMsTUFBekMsQ0FBbEI7QUFDQTtBQUNBLHdCQUFJLGNBQWMsQ0FBbEIsRUFBcUI7QUFDakI7QUFDQSw0QkFBSSxrQkFBa0IsQ0FBdEIsRUFBeUI7QUFDckIsNkNBQWlCLENBQWpCO0FBQ0g7QUFDRCwwQ0FBa0IsV0FBbEI7QUFDQSw0QkFBSSxVQUFVLE1BQU0sT0FBcEIsRUFBNkI7QUFDekI7QUFDSDtBQUNKO0FBQ0o7QUFFRDtBQTdCNEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUE4QjVILGdCQUFJLHFCQUFxQixDQUF6QixFQUE0QjtBQUN4QixrQ0FBbUIsc0JBQXNCLHFCQUFxQixDQUEzQyxDQUFuQjtBQUNIO0FBRUQsMkJBQWUsY0FBZjtBQUNBLG1CQUFPLFdBQVA7QUFDSDs7O3VDQUVjLEcsRUFBa0IsTyxFQUFzQjtBQUNuRCxnQkFBSSxTQUF1QixFQUEzQjtBQUNBLGdCQUFJLGVBQTRCLEtBQUssUUFBTCxDQUFjLE1BQWQsQ0FBcUIsVUFBUyxJQUFULEVBQWE7QUFDOUQsdUJBQVEsS0FBSyxPQUFMLElBQWdCLElBQXhCO0FBQ0gsYUFGK0IsQ0FBaEM7QUFGbUQ7QUFBQTtBQUFBOztBQUFBO0FBS25ELHNDQUFpQixLQUFLLFFBQXRCLG1JQUFnQztBQUFBLHdCQUF2QixJQUF1Qjs7QUFDNUIsd0JBQUksS0FBSyxPQUFULEVBQWtCO0FBQ2QsNEJBQUksVUFBVSxpQkFBaUIsQ0FBakIsRUFBb0IsYUFBYSxNQUFiLEdBQXNCLENBQTFDLENBQWQ7QUFDQSw0QkFBSSxjQUFjLGFBQWEsTUFBYixDQUFvQixPQUFwQixFQUE2QixDQUE3QixFQUFnQyxDQUFoQyxDQUFsQjtBQUNBLGlDQUFTLE9BQU8sTUFBUCxDQUFjLFlBQVksUUFBWixDQUFxQixHQUFyQixFQUEwQixPQUExQixDQUFkLENBQVQ7QUFDSCxxQkFKRCxNQUlPO0FBQ0gsaUNBQVMsT0FBTyxNQUFQLENBQWMsS0FBSyxRQUFMLENBQWMsR0FBZCxFQUFtQixPQUFuQixDQUFkLENBQVQ7QUFDSDtBQUNKO0FBYmtEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBY25ELG1CQUFPLE1BQVA7QUFDSDs7OztFQXREaUIsWTtBQTBEdEI7Ozs7O0lBR00sTTs7Ozs7Ozs7Ozs7cUNBQ1csVSxFQUFnQyxPLEVBQXdCLE0sRUFBaUIsTSxFQUF1QixXLEVBQW1CO0FBQzVILGdCQUFJLGlCQUFpQixDQUFyQjtBQUNBOzs7O0FBSUEsZ0JBQUksVUFBVSxRQUFRLE1BQVIsR0FBaUIsQ0FBL0IsRUFBa0M7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFDOUIsMENBQWtCLEtBQUssUUFBdkIsbUlBQWlDO0FBQUEsNEJBQXhCLEtBQXdCOztBQUM3QiwwQ0FBa0IsTUFBTSxNQUFOLENBQWEsVUFBYixFQUF5QixPQUF6QixFQUFrQyxLQUFsQyxFQUF5QyxNQUF6QyxDQUFsQjtBQUNIO0FBSDZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBSTlCLCtCQUFlLGNBQWY7QUFDSDtBQUNELG1CQUFPLFdBQVA7QUFDSDs7O3VDQUVjLEcsRUFBa0IsTyxFQUFzQjtBQUNuRCxnQkFBSSxhQUFhLElBQUksTUFBQSxrQkFBSixDQUF1QixJQUFJLFFBQTNCLENBQWpCO0FBQ0EsZ0JBQUksVUFBVSxJQUFJLEtBQUosRUFBZDtBQUNBOzs7Ozs7Ozs7QUFTQSxpQkFBSyxJQUFJLGFBQWEsQ0FBdEIsRUFBeUIsYUFBYSxLQUFLLFFBQUwsQ0FBYyxNQUFwRCxFQUE0RCxZQUE1RCxFQUEwRTtBQUN0RSxvQkFBSSxJQUFJLEtBQUssUUFBTCxDQUFjLFVBQWQsRUFBMEIsTUFBMUIsQ0FBaUMsVUFBakMsRUFBNkMsT0FBN0MsRUFBc0QsS0FBdEQsRUFBNkQsRUFBN0QsQ0FBUjtBQUNBLG9CQUFJLEtBQUssUUFBTCxDQUFjLFVBQWQsRUFBMEIsV0FBMUIsSUFBeUMsSUFBN0MsRUFBbUQ7QUFDL0MseUJBQUssS0FBSyxRQUFMLENBQWMsVUFBZCxFQUEwQixXQUEvQjtBQUNIO0FBQ0QscUJBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxDQUFwQixFQUF1QixHQUF2QixFQUE0QjtBQUN4Qiw0QkFBUSxJQUFSLENBQWEsVUFBYjtBQUNIO0FBQ0o7QUFFRDtBQUNBLGdCQUFJLE9BQUosR0FBYyxJQUFJLE9BQUosQ0FBWSxNQUFaLENBQW1CLFdBQVcsT0FBOUIsQ0FBZDtBQUVBLGdCQUFJLFFBQVEsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUNwQixvQkFBSSxjQUFjLGlCQUFpQixDQUFqQixFQUFvQixRQUFRLE1BQVIsR0FBaUIsQ0FBckMsQ0FBbEI7QUFDQSxvQkFBSSxjQUFjLEtBQUssUUFBTCxDQUFjLFFBQVEsV0FBUixDQUFkLENBQWxCO0FBQ0EsdUJBQVEsWUFBWSxRQUFaLENBQXFCLEdBQXJCLEVBQTBCLE9BQTFCLENBQVI7QUFDSCxhQUpELE1BS0s7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFNLElBQUksS0FBSixDQUFVLG9HQUFWLENBQU47QUFDSDtBQUNKOzs7O0VBckRnQixZO0FBeURyQjs7Ozs7SUFHTSxTOzs7Ozs7Ozs7OztxQ0FDVyxVLEVBQWdDLE8sRUFBd0IsTSxFQUFpQixNLEVBQXVCLFcsRUFBbUI7QUFDNUgsbUJBQU8sV0FBUDtBQUNIOzs7dUNBRWMsRyxFQUFrQixPLEVBQXNCO0FBQ25ELG1CQUFPLEVBQVA7QUFDSDs7OztFQVBtQixJOztBQVVULFFBQUEsU0FBQSxHQUFBLFNBQUE7Ozs7QUNqZWYsc0NBQXNDO0FBQ3RDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQXVFRTtBQUNGLElBQUksTUFBTSxHQUFHLENBQUM7SUFDZCxJQUFJLENBQUMsR0FBQyxVQUFTLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsSUFBRSxLQUFJLENBQUMsR0FBQyxDQUFDLElBQUUsRUFBRSxFQUFDLENBQUMsR0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDO1FBQUMsQ0FBQyxDQUFBLE9BQU8sQ0FBQyxDQUFBLENBQUEsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsQ0FBQztJQUN2VyxJQUFJLE1BQU0sR0FBRyxFQUFDLEtBQUssRUFBRSxTQUFTLEtBQUssS0FBTSxDQUFDO1FBQzFDLEVBQUUsRUFBRSxFQUFFO1FBQ04sUUFBUSxFQUFFLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLFVBQVUsRUFBQyxDQUFDLEVBQUMsS0FBSyxFQUFDLENBQUMsRUFBQyxTQUFTLEVBQUMsQ0FBQyxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsQ0FBQyxFQUFDLFdBQVcsRUFBQyxFQUFFLEVBQUMsWUFBWSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxFQUFFLEVBQUMsSUFBSSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxFQUFFLEVBQUMsV0FBVyxFQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDLFVBQVUsRUFBQyxFQUFFLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxhQUFhLEVBQUMsRUFBRSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDLFdBQVcsRUFBQyxFQUFFLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxpQkFBaUIsRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQyxFQUFFLEVBQUMsU0FBUyxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsQ0FBQyxFQUFDO1FBQzFhLFVBQVUsRUFBRSxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLFdBQVcsRUFBQyxFQUFFLEVBQUMsWUFBWSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxFQUFFLEVBQUMsSUFBSSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxFQUFFLEVBQUMsV0FBVyxFQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDLFVBQVUsRUFBQyxFQUFFLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxhQUFhLEVBQUMsRUFBRSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDLFdBQVcsRUFBQyxFQUFFLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxpQkFBaUIsRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLGNBQWMsRUFBQztRQUMzVyxZQUFZLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkssYUFBYSxFQUFFLFNBQVMsU0FBUyxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLFlBQVk7WUFDekgsbUJBQW1CO1lBRW5CLElBQUksRUFBRSxHQUFHLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLFFBQVEsT0FBTyxFQUFFO2dCQUNqQixLQUFLLENBQUM7b0JBQ0wsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDO29CQUFDLE9BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ25DLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUNMLE9BQU8sRUFBRSxDQUFDO29CQUNYLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUNMLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDbkIsTUFBTTtnQkFDTixLQUFLLENBQUM7b0JBQ0wsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN4QyxNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFDTCxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFBO29CQUMvQyxNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFFRSxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFBO29CQUV6QixNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFFQSxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUM7cUJBQUU7b0JBQzdDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFFdEIsTUFBTTtnQkFDTixLQUFLLENBQUM7b0JBRUEsSUFBSSxDQUFDLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztvQkFDcEIsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN4RSxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBRTlDLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUVBLElBQUksQ0FBQyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7b0JBQ3BCLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM3RSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDdkUsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUU3QyxNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFDTixJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO29CQUM5QyxNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFFRCxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUUsQ0FBQTtvQkFFN0UsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBRUQsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFBO29CQUVsRSxNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFFRCxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDVCxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDN0I7b0JBQ0QsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDO29CQUV4QixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFDTixFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFBO29CQUMzQyxNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFDTixFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUE7b0JBQ2hELE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUNOLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFDaEQsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQ04sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQTtvQkFDekMsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBRUQsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ25DLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFeEIsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBRUQsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN0QyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRXhCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUVELEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEMsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFeEIsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQ04sSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFBO29CQUM1QyxNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFDTixJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUE7b0JBQzVDLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUNOLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLENBQUE7b0JBQzNCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUVELElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUFFLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDO3FCQUFFO29CQUN2RCxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDO29CQUV4QixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFDTixFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUE7b0JBQ25ELE1BQU07YUFDTDtRQUNELENBQUM7UUFDRCxLQUFLLEVBQUUsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLEdBQUcsRUFBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFDLEVBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFDLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3p4RCxjQUFjLEVBQUUsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDO1FBQ2pDLFVBQVUsRUFBRSxTQUFTLFVBQVUsQ0FBRSxHQUFHLEVBQUUsSUFBSTtZQUN0QyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbkI7aUJBQU07Z0JBQ0gsSUFBSSxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNsQixNQUFNLEtBQUssQ0FBQzthQUNmO1FBQ0wsQ0FBQztRQUNELEtBQUssRUFBRSxTQUFTLEtBQUssQ0FBQyxLQUFLO1lBQ3ZCLElBQUksSUFBSSxHQUFHLElBQUksRUFBRSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEdBQUcsRUFBRSxFQUFFLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sR0FBRyxFQUFFLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxHQUFHLEVBQUUsRUFBRSxRQUFRLEdBQUcsQ0FBQyxFQUFFLE1BQU0sR0FBRyxDQUFDLEVBQUUsVUFBVSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDeEssSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLElBQUksV0FBVyxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQzdCLEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTtnQkFDbkIsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRTtvQkFDbEQsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQzthQUNKO1lBQ0QsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3RDLFdBQVcsQ0FBQyxFQUFFLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUM3QixXQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDN0IsSUFBSSxPQUFPLEtBQUssQ0FBQyxNQUFNLElBQUksV0FBVyxFQUFFO2dCQUNwQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzthQUNyQjtZQUNELElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQ25ELElBQUksT0FBTyxXQUFXLENBQUMsRUFBRSxDQUFDLFVBQVUsS0FBSyxVQUFVLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUM7YUFDL0M7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQzthQUM1RDtZQUNELFNBQVMsUUFBUSxDQUFDLENBQUM7Z0JBQ2YsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDdEMsQ0FBQztZQUNELFlBQVksRUFDUixJQUFJLEdBQUcsR0FBRztnQkFDTixJQUFJLEtBQUssQ0FBQztnQkFDVixLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsRUFBRSxJQUFJLEdBQUcsQ0FBQztnQkFDM0IsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBQzNCLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQztpQkFDekM7Z0JBQ0QsT0FBTyxLQUFLLENBQUM7WUFDakIsQ0FBQyxDQUFDO1lBQ04sSUFBSSxNQUFNLEVBQUUsY0FBYyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQztZQUN4RixPQUFPLElBQUksRUFBRTtnQkFDVCxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDNUIsTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3ZDO3FCQUFNO29CQUNILElBQUksTUFBTSxLQUFLLElBQUksSUFBSSxPQUFPLE1BQU0sSUFBSSxXQUFXLEVBQUU7d0JBQ2pELE1BQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQztxQkFDbEI7b0JBQ0QsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ2pEO2dCQUNXLElBQUksT0FBTyxNQUFNLEtBQUssV0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDdkUsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO29CQUNoQixRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNkLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDcEIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLEVBQUU7NEJBQ2xDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7eUJBQ25EO3FCQUNKO29CQUNELElBQUksS0FBSyxDQUFDLFlBQVksRUFBRTt3QkFDcEIsTUFBTSxHQUFHLHNCQUFzQixHQUFHLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUcsY0FBYyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ3BMO3lCQUFNO3dCQUNILE1BQU0sR0FBRyxzQkFBc0IsR0FBRyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsR0FBRyxlQUFlLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7cUJBQzdKO29CQUNELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO3dCQUNwQixJQUFJLEVBQUUsS0FBSyxDQUFDLEtBQUs7d0JBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLE1BQU07d0JBQ3hDLElBQUksRUFBRSxLQUFLLENBQUMsUUFBUTt3QkFDcEIsR0FBRyxFQUFFLEtBQUs7d0JBQ1YsUUFBUSxFQUFFLFFBQVE7cUJBQ3JCLENBQUMsQ0FBQztpQkFDTjtnQkFDTCxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsWUFBWSxLQUFLLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2pELE1BQU0sSUFBSSxLQUFLLENBQUMsbURBQW1ELEdBQUcsS0FBSyxHQUFHLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQztpQkFDdkc7Z0JBQ0QsUUFBUSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ25CLEtBQUssQ0FBQzt3QkFDRixLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQzFCLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ2QsSUFBSSxDQUFDLGNBQWMsRUFBRTs0QkFDakIsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7NEJBQ3RCLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDOzRCQUN0QixRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQzs0QkFDMUIsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7NEJBQ3JCLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRTtnQ0FDaEIsVUFBVSxFQUFFLENBQUM7NkJBQ2hCO3lCQUNKOzZCQUFNOzRCQUNILE1BQU0sR0FBRyxjQUFjLENBQUM7NEJBQ3hCLGNBQWMsR0FBRyxJQUFJLENBQUM7eUJBQ3pCO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxDQUFDO3dCQUNGLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN0QyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO3dCQUN0QyxLQUFLLENBQUMsRUFBRSxHQUFHOzRCQUNQLFVBQVUsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVU7NEJBQ3pELFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTOzRCQUM5QyxZQUFZLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZOzRCQUM3RCxXQUFXLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVzt5QkFDckQsQ0FBQzt3QkFDRixJQUFJLE1BQU0sRUFBRTs0QkFDUixLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssR0FBRztnQ0FDYixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0NBQzNDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7NkJBQ3JDLENBQUM7eUJBQ0w7d0JBQ0QsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTs0QkFDaEMsTUFBTTs0QkFDTixNQUFNOzRCQUNOLFFBQVE7NEJBQ1IsV0FBVyxDQUFDLEVBQUU7NEJBQ2QsTUFBTSxDQUFDLENBQUMsQ0FBQzs0QkFDVCxNQUFNOzRCQUNOLE1BQU07eUJBQ1QsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDaEIsSUFBSSxPQUFPLENBQUMsS0FBSyxXQUFXLEVBQUU7NEJBQzFCLE9BQU8sQ0FBQyxDQUFDO3lCQUNaO3dCQUNELElBQUksR0FBRyxFQUFFOzRCQUNMLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQ3JDLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQzs0QkFDbkMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO3lCQUN0Qzt3QkFDRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDNUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUN0QixRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDckIsTUFBTTtvQkFDVixLQUFLLENBQUM7d0JBQ0YsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7YUFDSjtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUMsRUFBQyxDQUFDO0lBQ0gsa0NBQWtDO0lBQ2xDLElBQUksS0FBSyxHQUFHLENBQUM7UUFDYixJQUFJLEtBQUssR0FBRyxDQUFDO1lBRWIsR0FBRyxFQUFDLENBQUM7WUFFTCxVQUFVLEVBQUMsU0FBUyxVQUFVLENBQUMsR0FBRyxFQUFFLElBQUk7Z0JBQ2hDLElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ3hDO3FCQUFNO29CQUNILE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3hCO1lBQ0wsQ0FBQztZQUVMLG1DQUFtQztZQUNuQyxRQUFRLEVBQUMsVUFBVSxLQUFLLEVBQUUsRUFBRTtnQkFDcEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztnQkFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsTUFBTSxHQUFHO29CQUNWLFVBQVUsRUFBRSxDQUFDO29CQUNiLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxDQUFDO29CQUNaLFdBQVcsRUFBRSxDQUFDO2lCQUNqQixDQUFDO2dCQUNGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDaEIsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUVMLCtDQUErQztZQUMvQyxLQUFLLEVBQUM7Z0JBQ0UsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUM7Z0JBQ2xCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDZCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ2QsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO2dCQUNuQixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3hDLElBQUksS0FBSyxFQUFFO29CQUNQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztpQkFDM0I7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDN0I7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtvQkFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDMUI7Z0JBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkMsT0FBTyxFQUFFLENBQUM7WUFDZCxDQUFDO1lBRUwsaURBQWlEO1lBQ2pELEtBQUssRUFBQyxVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQztnQkFDcEIsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFFdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQzlELHFCQUFxQjtnQkFDckIsSUFBSSxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUM7Z0JBQ25CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBRS9ELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7aUJBQ3JDO2dCQUNELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUUxQixJQUFJLENBQUMsTUFBTSxHQUFHO29CQUNWLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVU7b0JBQ2xDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUM7b0JBQzVCLFlBQVksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7b0JBQ3RDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFDaEIsQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7OEJBQzlELFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN4RSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxHQUFHO2lCQUNuQyxDQUFDO2dCQUVGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUN4RDtnQkFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO2dCQUNqQyxPQUFPLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUwsNkVBQTZFO1lBQzdFLElBQUksRUFBQztnQkFDRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztnQkFDbEIsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUVMLGtKQUFrSjtZQUNsSixNQUFNLEVBQUM7Z0JBQ0MsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRTtvQkFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7aUJBQzFCO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEdBQUcsa0lBQWtJLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFO3dCQUM5TixJQUFJLEVBQUUsRUFBRTt3QkFDUixLQUFLLEVBQUUsSUFBSTt3QkFDWCxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVE7cUJBQ3RCLENBQUMsQ0FBQztpQkFFTjtnQkFDRCxPQUFPLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUwseUNBQXlDO1lBQ3pDLElBQUksRUFBQyxVQUFVLENBQUM7Z0JBQ1IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLENBQUM7WUFFTCwwREFBMEQ7WUFDMUQsU0FBUyxFQUFDO2dCQUNGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMzRSxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQSxDQUFDLENBQUEsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDL0UsQ0FBQztZQUVMLG1EQUFtRDtZQUNuRCxhQUFhLEVBQUM7Z0JBQ04sSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRTtvQkFDbEIsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNqRDtnQkFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDcEYsQ0FBQztZQUVMLDJGQUEyRjtZQUMzRixZQUFZLEVBQUM7Z0JBQ0wsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUMsT0FBTyxHQUFHLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLElBQUksR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQ3ZELENBQUM7WUFFTCw4RUFBOEU7WUFDOUUsVUFBVSxFQUFDLFVBQVMsS0FBSyxFQUFFLFlBQVk7Z0JBQy9CLElBQUksS0FBSyxFQUNMLEtBQUssRUFDTCxNQUFNLENBQUM7Z0JBRVgsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRTtvQkFDOUIsZUFBZTtvQkFDZixNQUFNLEdBQUc7d0JBQ0wsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVTs0QkFDbEMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTOzRCQUN6QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZOzRCQUN0QyxXQUFXLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXO3lCQUN2Qzt3QkFDRCxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07d0JBQ25CLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzt3QkFDakIsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPO3dCQUNyQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87d0JBQ3JCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTt3QkFDbkIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO3dCQUNuQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7d0JBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTt3QkFDbkIsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFO3dCQUNYLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQzVDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtxQkFDbEIsQ0FBQztvQkFDRixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO3dCQUNyQixNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ3BEO2lCQUNKO2dCQUVELEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQzFDLElBQUksS0FBSyxFQUFFO29CQUNQLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQztpQkFDakM7Z0JBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRztvQkFDVixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO29CQUNqQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDO29CQUM1QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXO29CQUNyQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7d0JBQ1AsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDcEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU07aUJBQ3pELENBQUM7Z0JBQ0YsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFDakMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtvQkFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNqRTtnQkFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekIsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4SCxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7aUJBQ3JCO2dCQUNELElBQUksS0FBSyxFQUFFO29CQUNQLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ3hCLGtCQUFrQjtvQkFDbEIsS0FBSyxJQUFJLENBQUMsSUFBSSxNQUFNLEVBQUU7d0JBQ2xCLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ3ZCO29CQUNELE9BQU8sS0FBSyxDQUFDLENBQUMsK0VBQStFO2lCQUNoRztnQkFDRCxPQUFPLEtBQUssQ0FBQztZQUNqQixDQUFDO1lBRUwsNkJBQTZCO1lBQzdCLElBQUksRUFBQztnQkFDRyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ1gsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO2lCQUNuQjtnQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDZCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztpQkFDcEI7Z0JBRUQsSUFBSSxLQUFLLEVBQ0wsS0FBSyxFQUNMLFNBQVMsRUFDVCxLQUFLLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7b0JBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2lCQUNuQjtnQkFDRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNuQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwRCxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNoRSxLQUFLLEdBQUcsU0FBUyxDQUFDO3dCQUNsQixLQUFLLEdBQUcsQ0FBQyxDQUFDO3dCQUNWLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7NEJBQzlCLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDN0MsSUFBSSxLQUFLLEtBQUssS0FBSyxFQUFFO2dDQUNqQixPQUFPLEtBQUssQ0FBQzs2QkFDaEI7aUNBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dDQUN4QixLQUFLLEdBQUcsS0FBSyxDQUFDO2dDQUNkLFNBQVMsQ0FBQyx3REFBd0Q7NkJBQ3JFO2lDQUFNO2dDQUNILDhGQUE4RjtnQ0FDOUYsT0FBTyxLQUFLLENBQUM7NkJBQ2hCO3lCQUNKOzZCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRTs0QkFDM0IsTUFBTTt5QkFDVDtxQkFDSjtpQkFDSjtnQkFDRCxJQUFJLEtBQUssRUFBRTtvQkFDUCxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQzdDLElBQUksS0FBSyxLQUFLLEtBQUssRUFBRTt3QkFDakIsT0FBTyxLQUFLLENBQUM7cUJBQ2hCO29CQUNELDhGQUE4RjtvQkFDOUYsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUNELElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7b0JBQ3BCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztpQkFDbkI7cUJBQU07b0JBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLHdCQUF3QixHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsR0FBRyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUU7d0JBQ3BILElBQUksRUFBRSxFQUFFO3dCQUNSLEtBQUssRUFBRSxJQUFJO3dCQUNYLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUTtxQkFDdEIsQ0FBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQztZQUVMLHFDQUFxQztZQUNyQyxHQUFHLEVBQUMsU0FBUyxHQUFHO2dCQUNSLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDcEIsSUFBSSxDQUFDLEVBQUU7b0JBQ0gsT0FBTyxDQUFDLENBQUM7aUJBQ1o7cUJBQU07b0JBQ0gsT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3JCO1lBQ0wsQ0FBQztZQUVMLHdHQUF3RztZQUN4RyxLQUFLLEVBQUMsU0FBUyxLQUFLLENBQUUsU0FBUztnQkFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDeEMsQ0FBQztZQUVMLDBFQUEwRTtZQUMxRSxRQUFRLEVBQUMsU0FBUyxRQUFRO2dCQUNsQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDUCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3BDO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDakM7WUFDTCxDQUFDO1lBRUwsNEZBQTRGO1lBQzVGLGFBQWEsRUFBQyxTQUFTLGFBQWE7Z0JBQzVCLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDbkYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQ3JGO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQzNDO1lBQ0wsQ0FBQztZQUVMLG9KQUFvSjtZQUNwSixRQUFRLEVBQUMsU0FBUyxRQUFRLENBQUUsQ0FBQztnQkFDckIsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNSLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDakM7cUJBQU07b0JBQ0gsT0FBTyxTQUFTLENBQUM7aUJBQ3BCO1lBQ0wsQ0FBQztZQUVMLDZCQUE2QjtZQUM3QixTQUFTLEVBQUMsU0FBUyxTQUFTLENBQUUsU0FBUztnQkFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMxQixDQUFDO1lBRUwscURBQXFEO1lBQ3JELGNBQWMsRUFBQyxTQUFTLGNBQWM7Z0JBQzlCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDdEMsQ0FBQztZQUNMLE9BQU8sRUFBRSxFQUFFO1lBQ1gsYUFBYSxFQUFFLFNBQVMsU0FBUyxDQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMseUJBQXlCLEVBQUMsUUFBUTtnQkFDM0UsSUFBSSxPQUFPLEdBQUMsUUFBUSxDQUFDO2dCQUNyQixRQUFPLHlCQUF5QixFQUFFO29CQUNsQyxLQUFLLENBQUMsRUFBQyxxQkFBcUI7d0JBQzVCLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2hFLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLE9BQU8sQ0FBQyxDQUFBO3dCQUNmLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNoQixNQUFNO29CQUNOLEtBQUssQ0FBQzt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDaEIsTUFBTTtvQkFDTixLQUFLLENBQUM7d0JBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsR0FBRyxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDaEUsTUFBTTtvQkFDTixLQUFLLENBQUM7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2hCLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNoQixNQUFNO29CQUNOLEtBQUssQ0FBQzt3QkFBQyxPQUFPLENBQUMsQ0FBQTt3QkFDZixNQUFNO29CQUNOLEtBQUssQ0FBQzt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDaEIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2pCLE1BQU07b0JBQ04sS0FBSyxFQUFFO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNqQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDakIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2pCLE1BQU07b0JBQ04sS0FBSyxFQUFFO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNqQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDakIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2pCLE1BQU07b0JBQ04sS0FBSyxFQUFFO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNqQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDakIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2pCLE1BQU07b0JBQ04sS0FBSyxFQUFFO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNqQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDakIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2pCLE1BQU07b0JBQ04sS0FBSyxFQUFFO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNqQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDakIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2pCLE1BQU07b0JBQ04sS0FBSyxFQUFFO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNqQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLFNBQVMsQ0FBQTt3QkFDeEIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxDQUFDLENBQUE7d0JBQ2hCLE1BQU07aUJBQ0w7WUFDRCxDQUFDO1lBQ0QsS0FBSyxFQUFFLENBQUMsVUFBVSxFQUFDLHdFQUF3RSxFQUFDLHFCQUFxQixFQUFDLGFBQWEsRUFBQyxvQkFBb0IsRUFBQyxzQkFBc0IsRUFBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLFFBQVEsRUFBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxZQUFZLEVBQUMsV0FBVyxFQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLFNBQVMsRUFBQyxVQUFVLEVBQUMsUUFBUSxFQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsUUFBUSxDQUFDO1lBQ2haLFVBQVUsRUFBRSxFQUFDLFNBQVMsRUFBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsRUFBQyxXQUFXLEVBQUMsSUFBSSxFQUFDLEVBQUM7U0FDaEksQ0FBQyxDQUFDO1FBQ0gsT0FBTyxLQUFLLENBQUM7SUFDYixDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ0wsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDckIsU0FBUyxNQUFNO1FBQ2IsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7SUFDZixDQUFDO0lBQ0QsTUFBTSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7SUFBQSxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUNqRCxPQUFPLElBQUksTUFBTSxDQUFDO0FBQ2xCLENBQUMsQ0FBQyxFQUFFLENBQUM7QUFHTCxJQUFJLE9BQU8sT0FBTyxLQUFLLFdBQVcsSUFBSSxPQUFPLE9BQU8sS0FBSyxXQUFXLEVBQUU7SUFDdEUsT0FBTyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDeEIsT0FBTyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQy9CLE9BQU8sQ0FBQyxLQUFLLEdBQUcsY0FBYyxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM5RSxPQUFPLENBQUMsSUFBSSxHQUFHLFNBQVMsWUFBWSxDQUFFLElBQUk7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2QyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25CO1FBQ0QsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3BGLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEMsQ0FBQyxDQUFDO0lBQ0YsSUFBSSxPQUFPLE1BQU0sS0FBSyxXQUFXLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7UUFDNUQsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ3JDO0NBQ0E7Ozs7OztBQ2p3QkQ7Ozs7Ozs7OztBQUlBLElBQUEsV0FBQSxRQUFBLGNBQUEsQ0FBQTtBQUVBOzs7OztJQUlNLG9CO0FBTUY7Ozs7QUFJQSxrQ0FBWSxRQUFaLEVBQThCLE9BQTlCLEVBQTZDO0FBQUE7O0FBQ3pDLGFBQUssT0FBTCxHQUFlLE9BQWY7QUFDQSxhQUFLLFFBQUwsR0FBZ0IsUUFBaEI7QUFDQSxhQUFLLFlBQUwsR0FBb0IsSUFBSSxTQUFBLFlBQUosQ0FBaUIsT0FBakIsQ0FBcEI7QUFDQSxhQUFLLE1BQUwsR0FBYyxFQUFkO0FBQ0g7QUFFRDs7Ozs7Ozs7O21DQUtRO0FBQ0osZ0JBQUksS0FBSyxRQUFMLElBQWlCLEVBQXJCLEVBQXlCO0FBQ3JCLHFCQUFLLE1BQUwsR0FBYyxLQUFLLFlBQUwsQ0FBa0IsUUFBbEIsRUFBZDtBQUNILGFBRkQsTUFHSyxJQUFJLEtBQUssT0FBTCxJQUFnQixFQUFwQixFQUF3QjtBQUN6QixxQkFBSyxNQUFMLEdBQWMsS0FBSyxRQUFuQjtBQUNILGFBRkksTUFHQTtBQUNEO0FBQ0Esb0JBQUksS0FBSyxZQUFMLENBQWtCLEdBQWxCLElBQXlCLEtBQUssWUFBTCxDQUFrQixHQUFsQixDQUFzQixjQUFuRCxFQUFtRTtBQUMvRCx5QkFBSyxZQUFMLENBQWtCLEdBQWxCLENBQXNCLGNBQXRCLEdBQXVDLEVBQXZDO0FBQ0g7QUFDRCxvQkFBSSxlQUFlLEtBQUssWUFBeEI7QUFDQSxvQkFBSSxpQkFBaUIsU0FBakIsY0FBaUIsQ0FBUyxJQUFULEVBQXFCO0FBQ3RDO0FBQ0EsMkJBQU8sYUFBYSxRQUFiLENBQXNCLElBQXRCLEVBQTRCLElBQTVCLENBQVA7QUFDSCxpQkFIRDtBQUlBLG9CQUFJLFVBQVUsSUFBSSxNQUFKLENBQVcsaUJBQVgsRUFBOEIsR0FBOUIsQ0FBZDtBQUNBLHFCQUFLLE1BQUwsR0FBYyxLQUFLLFFBQUwsQ0FBYyxPQUFkLENBQXNCLE9BQXRCLEVBQStCLGNBQS9CLENBQWQ7QUFDSDtBQUVELG1CQUFPLEtBQUssTUFBWjtBQUNIO0FBRUQ7Ozs7OztpQ0FHZ0IsUSxFQUFrQixPLEVBQWU7QUFDN0MsbUJBQU8sSUFBSSxvQkFBSixDQUF5QixRQUF6QixFQUFtQyxPQUFuQyxFQUE0QyxRQUE1QyxFQUFQO0FBQ0g7Ozs7OztBQUdJLFFBQUEsb0JBQUEsR0FBQSxvQkFBQTs7OztBQ2hFVDs7Ozs7Ozs7Ozs7Ozs7OztBQVNBLElBQUEsbUJBQUEsUUFBQSxrQkFBQSxDQUFBOztJQUVNLEk7QUFPRixrQkFBWSxTQUFaLEVBQStCLElBQS9CLEVBQXVDO0FBQUE7O0FBQ25DLGFBQUssU0FBTCxHQUFpQixTQUFqQjtBQUNBLGFBQUssTUFBTCxHQUFjLEtBQUssTUFBbkI7QUFDQyxZQUFJLEtBQUssSUFBVCxFQUFlO0FBQ1osZ0JBQUksS0FBSyxJQUFMLENBQVUsSUFBZCxFQUFvQjtBQUNoQixxQkFBSyxJQUFMLEdBQVksS0FBSyxJQUFMLENBQVUsSUFBdEI7QUFDSDtBQUNELGdCQUFJLEtBQUssSUFBTCxDQUFVLE9BQWQsRUFBdUI7QUFDbkIscUJBQUssT0FBTCxHQUFlLEtBQUssSUFBTCxDQUFVLE9BQXpCO0FBQ0g7QUFDRCxnQkFBSSxLQUFLLElBQUwsQ0FBVSxLQUFkLEVBQXFCO0FBQ2pCLHFCQUFLLE9BQUwsR0FBZSxLQUFLLElBQUwsQ0FBVSxLQUF6QjtBQUNIO0FBQ0o7QUFDSjtBQUVEOzs7Ozs7O3FDQUdhLEssRUFBbUI7QUFDNUIsZ0JBQUksTUFBTSxNQUFOLElBQWdCLEtBQUssTUFBTCxDQUFZLE1BQWhDLEVBQXdDO0FBQ3BDLHFCQUFLLElBQUksQ0FBVCxJQUFjLEtBQUssTUFBbkIsRUFBMkI7QUFDdkIsd0JBQUksS0FBSyxNQUFMLENBQVksQ0FBWixLQUFrQixNQUFNLENBQU4sRUFBUyxJQUEvQixFQUFxQztBQUNqQztBQUNBLDRCQUFJLEtBQUssTUFBTCxDQUFZLENBQVosS0FBa0IsR0FBdEIsRUFBMkI7QUFDdkIsbUNBQU8sS0FBUDtBQUNIO0FBQ0o7QUFDSjtBQUNELHVCQUFPLElBQVA7QUFDSDtBQUNELG1CQUFPLEtBQVA7QUFDSDs7Ozs7O0lBR0MsSyxHQUlGLGVBQVksSUFBWixFQUF5QixJQUF6QixFQUFvQztBQUFBOztBQUNoQyxTQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0EsU0FBSyxJQUFMLEdBQVksSUFBWjtBQUNILEM7O0lBR0MsWTtBQUtGLDBCQUFZLEtBQVosRUFBMEIsS0FBMUIsRUFBc0MsVUFBdEMsRUFBdUQ7QUFBQTs7QUFDbkQsYUFBSyxLQUFMLEdBQWEsS0FBSyxVQUFMLENBQWdCLEtBQWhCLENBQWI7QUFDQSxhQUFLLEtBQUwsR0FBYSxLQUFiO0FBQ0EsYUFBSyxVQUFMLEdBQWtCLFVBQWxCO0FBQ0g7QUFFRDs7Ozs7OzttQ0FHVyxLLEVBQWE7QUFDcEIsZ0JBQUksYUFBYSxJQUFJLEtBQUosRUFBakI7QUFDQSxnQkFBSSxXQUFXLElBQWY7QUFDQSxnQkFBSTtBQUNBLDJCQUFXLGlCQUFBLEtBQUEsQ0FBVyxLQUFYLENBQVg7QUFDSCxhQUZELENBR0EsT0FBTyxDQUFQLEVBQVU7QUFDTixzQkFBTSxJQUFJLEtBQUosQ0FBVSxFQUFFLE9BQVosQ0FBTjtBQUNIO0FBUm1CO0FBQUE7QUFBQTs7QUFBQTtBQVNwQixxQ0FBb0IsUUFBcEIsOEhBQThCO0FBQUEsd0JBQXJCLE9BQXFCO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQzFCLDhDQUFzQixRQUFRLE1BQTlCLG1JQUFzQztBQUFBLGdDQUE3QixTQUE2QjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUNsQyxzREFBaUIsUUFBUSxLQUF6QixtSUFBZ0M7QUFBQSx3Q0FBdkIsSUFBdUI7O0FBQzVCLCtDQUFXLElBQVgsQ0FBZ0IsSUFBSSxJQUFKLENBQVMsU0FBVCxFQUFvQixJQUFwQixDQUFoQjtBQUNIO0FBSGlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJckM7QUFMeUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU03QjtBQWZtQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWdCcEIsbUJBQU8sVUFBUDtBQUNIO0FBR0Q7Ozs7Ozs7b0NBS29CLE0sRUFBcUIsUSxFQUFlO0FBQ3BELGdCQUFJLFNBQVMsUUFBYjtBQUNBLGlCQUFLLElBQUksQ0FBVCxJQUFjLE1BQWQsRUFBc0I7QUFDbEIseUJBQVMsT0FBTyxPQUFQLENBQWUsTUFBSSxDQUFuQixFQUFzQixPQUFPLENBQVAsRUFBVSxJQUFoQyxDQUFUO0FBQ0g7QUFDRCxtQkFBTyxNQUFQO0FBQ0g7QUFFRDs7Ozs7O29DQUdvQixLLEVBQVk7QUFDNUIsZ0JBQUksU0FBUyxJQUFJLEtBQUosRUFBYjtBQUNBLGlCQUFLLEtBQUwsQ0FBVyxRQUFYLENBQW9CLEtBQXBCO0FBQ0EsZ0JBQUksT0FBTyxFQUFYO0FBQ0EsbUJBQU0sQ0FBQyxPQUFPLEtBQUssS0FBTCxDQUFXLEdBQVgsRUFBUixLQUE2QixDQUFuQyxFQUFzQztBQUNsQyx1QkFBTyxJQUFQLENBQVksSUFBSSxLQUFKLENBQVUsSUFBVixFQUFnQixLQUFLLEtBQUwsQ0FBVyxNQUEzQixDQUFaO0FBQ0g7QUFDRCxtQkFBTyxNQUFQO0FBQ0g7QUFFRDs7Ozs7O2dDQUdRLEssRUFBWTtBQUNoQixnQkFBSSxTQUFTLEtBQUssV0FBTCxDQUFpQixLQUFqQixDQUFiO0FBQ0EsZ0JBQUkscUNBQVksTUFBWixFQUFKO0FBQ0EsZ0JBQUksZUFBZSxLQUFLLFVBQXhCO0FBQ0EsZ0JBQUksU0FBUyxFQUFiO0FBQ0EsbUJBQU8sTUFBTSxNQUFOLEdBQWUsQ0FBdEIsRUFBeUI7QUFDckI7QUFDQSxvQkFBSSxnQkFBZ0IsS0FBSyxLQUFMLENBQVcsTUFBWCxDQUFrQixVQUFTLElBQVQsRUFBYTtBQUMvQywyQkFBUSxLQUFLLFNBQUwsSUFBa0IsWUFBbkIsSUFBcUMsS0FBSyxTQUFMLElBQWtCLEdBQTlEO0FBQ0gsaUJBRm1CLENBQXBCO0FBR0Esb0JBQUksWUFBWSxLQUFoQjtBQUxxQjtBQUFBO0FBQUE7O0FBQUE7QUFPckIsMENBQXlCLGFBQXpCLG1JQUF3QztBQUFBLDRCQUEvQixZQUErQjs7QUFDcEMsNEJBQUksYUFBYSxZQUFiLENBQTBCLEtBQTFCLENBQUosRUFBc0M7QUFDbEM7QUFDQSxnQ0FBSSxXQUFXLFlBQWY7QUFFQTtBQUNBLGdDQUFJLGFBQWEsSUFBakIsRUFBdUI7QUFDbkIsMENBQVUsS0FBSyxXQUFMLENBQWlCLE1BQU0sS0FBTixDQUFZLENBQVosRUFBZSxhQUFhLE1BQWIsQ0FBb0IsTUFBbkMsQ0FBakIsRUFBNkQsYUFBYSxJQUExRSxDQUFWO0FBQ0g7QUFFRDtBQUNBLGdDQUFJLGFBQWEsT0FBakIsRUFBMEI7QUFDdEIsK0NBQWUsYUFBYSxPQUE1QjtBQUNIO0FBRUQ7QUFDQSxnQ0FBSSxrQkFBa0IsYUFBYSxNQUFiLENBQW9CLE1BQTFDO0FBQ0EsZ0NBQUksYUFBYSxPQUFiLElBQXdCLGFBQWEsT0FBYixHQUF1QixlQUFuRCxFQUFvRTtBQUNoRSxrREFBa0IsYUFBYSxPQUEvQjtBQUNIO0FBRUQ7QUFDQSxvQ0FBUSxNQUFNLEtBQU4sQ0FBWSxlQUFaLEVBQTZCLE1BQU0sTUFBbkMsQ0FBUjtBQUNBLHdDQUFZLElBQVo7QUFFQTtBQUNIO0FBQ0o7QUFsQ29CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBbUNyQixvQkFBSSxDQUFDLFNBQUwsRUFBZ0I7QUFDWix3QkFBSSxjQUFjLEVBQWxCO0FBQ0Esd0JBQUksa0JBQWtCLE1BQU0sTUFBTixHQUFlLENBQWYsR0FBbUIsTUFBTSxNQUF6QixHQUFrQyxDQUF4RDtBQUNBLHlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUUsZUFBbEIsRUFBbUMsR0FBbkMsRUFBd0M7QUFDcEMsdUNBQWtCLE1BQU0sQ0FBTixFQUFTLElBQTNCO0FBQ0g7QUFDRCx3QkFBSSxnQ0FBOEIsWUFBOUIsaUJBQXNELFdBQTFEO0FBQ0EsMEJBQU0sSUFBSSxLQUFKLENBQVUsQ0FBVixDQUFOO0FBQ0g7QUFDSjtBQUNELG1CQUFPLE1BQVA7QUFDSDs7Ozs7O0FBR0ksUUFBQSxZQUFBLEdBQUEsWUFBQTs7O0FDcExULGtDQUFrQztBQUNsQyxJQUFJLEtBQUssR0FBRyxDQUFDO0lBQ2IsSUFBSSxLQUFLLEdBQUcsQ0FBQztRQUViLEdBQUcsRUFBQyxDQUFDO1FBRUwsVUFBVSxFQUFDLFNBQVMsVUFBVSxDQUFDLEdBQUcsRUFBRSxJQUFJO1lBQ2hDLElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0gsTUFBTSxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN4QjtRQUNMLENBQUM7UUFFTCxtQ0FBbUM7UUFDbkMsUUFBUSxFQUFDLFVBQVUsS0FBSyxFQUFFLEVBQUU7WUFDcEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsTUFBTSxHQUFHO2dCQUNWLFVBQVUsRUFBRSxDQUFDO2dCQUNiLFlBQVksRUFBRSxDQUFDO2dCQUNmLFNBQVMsRUFBRSxDQUFDO2dCQUNaLFdBQVcsRUFBRSxDQUFDO2FBQ2pCLENBQUM7WUFDRixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQzthQUM3QjtZQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ2hCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFTCwrQ0FBK0M7UUFDL0MsS0FBSyxFQUFDO1lBQ0UsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsTUFBTSxJQUFJLEVBQUUsQ0FBQztZQUNsQixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDZCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDZCxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztZQUNqQixJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztZQUNuQixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDeEMsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQzNCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDN0I7WUFDRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQzFCO1lBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyxPQUFPLEVBQUUsQ0FBQztRQUNkLENBQUM7UUFFTCxpREFBaUQ7UUFDakQsS0FBSyxFQUFDLFVBQVUsRUFBRTtZQUNWLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUM7WUFDcEIsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUV0QyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQzlELHFCQUFxQjtZQUNyQixJQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQztZQUNuQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUUvRCxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2FBQ3JDO1lBQ0QsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFFMUIsSUFBSSxDQUFDLE1BQU0sR0FBRztnQkFDVixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVO2dCQUNsQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDO2dCQUM1QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZO2dCQUN0QyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQ2hCLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzBCQUM5RCxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDeEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsR0FBRzthQUNuQyxDQUFDO1lBRUYsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtnQkFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7YUFDeEQ7WUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ2pDLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFTCw2RUFBNkU7UUFDN0UsSUFBSSxFQUFDO1lBQ0csSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDbEIsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVMLGtKQUFrSjtRQUNsSixNQUFNLEVBQUM7WUFDQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFO2dCQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUMxQjtpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLGtJQUFrSSxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRTtvQkFDOU4sSUFBSSxFQUFFLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLElBQUk7b0JBQ1gsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRO2lCQUN0QixDQUFDLENBQUM7YUFFTjtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFFTCx5Q0FBeUM7UUFDekMsSUFBSSxFQUFDLFVBQVUsQ0FBQztZQUNSLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBRUwsMERBQTBEO1FBQzFELFNBQVMsRUFBQztZQUNGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzNFLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQSxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMvRSxDQUFDO1FBRUwsbURBQW1EO1FBQ25ELGFBQWEsRUFBQztZQUNOLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRTtnQkFDbEIsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2pEO1lBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BGLENBQUM7UUFFTCwyRkFBMkY7UUFDM0YsWUFBWSxFQUFDO1lBQ0wsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxHQUFHLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQztRQUN2RCxDQUFDO1FBRUwsOEVBQThFO1FBQzlFLFVBQVUsRUFBQyxVQUFTLEtBQUssRUFBRSxZQUFZO1lBQy9CLElBQUksS0FBSyxFQUNMLEtBQUssRUFDTCxNQUFNLENBQUM7WUFFWCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFO2dCQUM5QixlQUFlO2dCQUNmLE1BQU0sR0FBRztvQkFDTCxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3ZCLE1BQU0sRUFBRTt3QkFDSixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVO3dCQUNsQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7d0JBQ3pCLFlBQVksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7d0JBQ3RDLFdBQVcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVc7cUJBQ3ZDO29CQUNELE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtvQkFDbkIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO29CQUNqQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87b0JBQ3JCLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztvQkFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO29CQUNuQixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07b0JBQ25CLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztvQkFDakIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO29CQUNuQixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ1gsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2lCQUNsQixDQUFDO2dCQUNGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7b0JBQ3JCLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDcEQ7YUFDSjtZQUVELEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDMUMsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDO2FBQ2pDO1lBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRztnQkFDVixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO2dCQUNqQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDO2dCQUM1QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXO2dCQUNyQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQ1AsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDcEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU07YUFDekQsQ0FBQztZQUNGLElBQUksQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDakMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtnQkFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2pFO1lBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekIsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hILElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUMxQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQzthQUNyQjtZQUNELElBQUksS0FBSyxFQUFFO2dCQUNQLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO2lCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDeEIsa0JBQWtCO2dCQUNsQixLQUFLLElBQUksQ0FBQyxJQUFJLE1BQU0sRUFBRTtvQkFDbEIsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdkI7Z0JBQ0QsT0FBTyxLQUFLLENBQUMsQ0FBQywrRUFBK0U7YUFDaEc7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUwsNkJBQTZCO1FBQzdCLElBQUksRUFBQztZQUNHLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDWCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7YUFDbkI7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzthQUNwQjtZQUVELElBQUksS0FBSyxFQUNMLEtBQUssRUFDTCxTQUFTLEVBQ1QsS0FBSyxDQUFDO1lBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2FBQ25CO1lBQ0QsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUNoRSxLQUFLLEdBQUcsU0FBUyxDQUFDO29CQUNsQixLQUFLLEdBQUcsQ0FBQyxDQUFDO29CQUNWLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7d0JBQzlCLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDN0MsSUFBSSxLQUFLLEtBQUssS0FBSyxFQUFFOzRCQUNqQixPQUFPLEtBQUssQ0FBQzt5QkFDaEI7NkJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOzRCQUN4QixLQUFLLEdBQUcsS0FBSyxDQUFDOzRCQUNkLFNBQVMsQ0FBQyx3REFBd0Q7eUJBQ3JFOzZCQUFNOzRCQUNILDhGQUE4Rjs0QkFDOUYsT0FBTyxLQUFLLENBQUM7eUJBQ2hCO3FCQUNKO3lCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRTt3QkFDM0IsTUFBTTtxQkFDVDtpQkFDSjthQUNKO1lBQ0QsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxJQUFJLEtBQUssS0FBSyxLQUFLLEVBQUU7b0JBQ2pCLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjtnQkFDRCw4RkFBOEY7Z0JBQzlGLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEVBQUUsRUFBRTtnQkFDcEIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO2FBQ25CO2lCQUFNO2dCQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEdBQUcsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFO29CQUNwSCxJQUFJLEVBQUUsRUFBRTtvQkFDUixLQUFLLEVBQUUsSUFBSTtvQkFDWCxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVE7aUJBQ3RCLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQztRQUVMLHFDQUFxQztRQUNyQyxHQUFHLEVBQUMsU0FBUyxHQUFHO1lBQ1IsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxFQUFFO2dCQUNILE9BQU8sQ0FBQyxDQUFDO2FBQ1o7aUJBQU07Z0JBQ0gsT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDckI7UUFDTCxDQUFDO1FBRUwsd0dBQXdHO1FBQ3hHLEtBQUssRUFBQyxTQUFTLEtBQUssQ0FBRSxTQUFTO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFFTCwwRUFBMEU7UUFDMUUsUUFBUSxFQUFDLFNBQVMsUUFBUTtZQUNsQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNQLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUNwQztpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakM7UUFDTCxDQUFDO1FBRUwsNEZBQTRGO1FBQzVGLGFBQWEsRUFBQyxTQUFTLGFBQWE7WUFDNUIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNuRixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUNyRjtpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQzNDO1FBQ0wsQ0FBQztRQUVMLG9KQUFvSjtRQUNwSixRQUFRLEVBQUMsU0FBUyxRQUFRLENBQUUsQ0FBQztZQUNyQixDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDUixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakM7aUJBQU07Z0JBQ0gsT0FBTyxTQUFTLENBQUM7YUFDcEI7UUFDTCxDQUFDO1FBRUwsNkJBQTZCO1FBQzdCLFNBQVMsRUFBQyxTQUFTLFNBQVMsQ0FBRSxTQUFTO1lBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUIsQ0FBQztRQUVMLHFEQUFxRDtRQUNyRCxjQUFjLEVBQUMsU0FBUyxjQUFjO1lBQzlCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7UUFDdEMsQ0FBQztRQUNMLE9BQU8sRUFBRSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUM7UUFDL0IsYUFBYSxFQUFFLFNBQVMsU0FBUyxDQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMseUJBQXlCLEVBQUMsUUFBUTtZQUMzRSxJQUFJLE9BQU8sR0FBQyxRQUFRLENBQUM7WUFDckIsUUFBTyx5QkFBeUIsRUFBRTtnQkFDbEMsS0FBSyxDQUFDO29CQUFDLE9BQU8sWUFBWSxDQUFBO29CQUMxQixNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxHQUFHLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFDO29CQUFDLE9BQU8sTUFBTSxDQUFBO29CQUNwRSxNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFBQyxPQUFPLE1BQU0sQ0FBQTtvQkFDeEMsTUFBTTtnQkFDTixLQUFLLENBQUM7b0JBQUMsT0FBTyxPQUFPLENBQUE7b0JBQ3JCLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUFDLE9BQU8sY0FBYyxDQUFBO29CQUM1QixNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFBQyxPQUFPLGVBQWUsQ0FBQTtvQkFDN0IsTUFBTTtnQkFDTixLQUFLLENBQUM7b0JBQUMsT0FBTyxjQUFjLENBQUE7b0JBQzVCLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUFDLE9BQU8sZUFBZSxDQUFBO29CQUM3QixNQUFNO2dCQUNOLEtBQUssQ0FBQztvQkFBQyxPQUFPLGNBQWMsQ0FBQTtvQkFDNUIsTUFBTTtnQkFDTixLQUFLLENBQUM7b0JBQUMsT0FBTyxlQUFlLENBQUE7b0JBQzdCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sZ0JBQWdCLENBQUE7b0JBQy9CLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sTUFBTSxDQUFBO29CQUNyQixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFBQyxPQUFPLGNBQWMsQ0FBQTtvQkFDN0IsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQUMsT0FBTyxlQUFlLENBQUE7b0JBQzlCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sY0FBYyxDQUFBO29CQUM3QixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFBQyxPQUFPLGVBQWUsQ0FBQTtvQkFDOUIsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQUMsT0FBTyxlQUFlLENBQUE7b0JBQzlCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sb0JBQW9CLENBQUE7b0JBQ25DLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sb0JBQW9CLENBQUE7b0JBQ25DLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sZUFBZSxDQUFBO29CQUM5QixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFBQyxPQUFPLGVBQWUsQ0FBQTtvQkFDOUIsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQUMsT0FBTyxjQUFjLENBQUE7b0JBQzdCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sZUFBZSxDQUFBO29CQUM5QixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFBQyxPQUFPLGVBQWUsQ0FBQTtvQkFDOUIsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQUMsT0FBTyxlQUFlLENBQUE7b0JBQzlCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sZUFBZSxDQUFBO29CQUM5QixNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFBQyxPQUFPLE1BQU0sQ0FBQTtvQkFDckIsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQUMsT0FBTyxNQUFNLENBQUE7b0JBQ3JCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUFDLE9BQU8sS0FBSyxDQUFBO29CQUNwQixNQUFNO2FBQ0w7UUFDRCxDQUFDO1FBQ0QsS0FBSyxFQUFFLENBQUMsVUFBVSxFQUFDLDZCQUE2QixFQUFDLFFBQVEsRUFBQyxRQUFRLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsU0FBUyxFQUFDLFVBQVUsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLFFBQVEsRUFBQyxlQUFlLEVBQUMsNEJBQTRCLEVBQUMsU0FBUyxFQUFDLFFBQVEsRUFBQyxzQkFBc0IsRUFBQyxXQUFXLEVBQUMsb0JBQW9CLEVBQUMseUJBQXlCLEVBQUMsZUFBZSxFQUFDLHNCQUFzQixFQUFDLFlBQVksRUFBQyxxQkFBcUIsRUFBQyw4Q0FBOEMsRUFBQyxRQUFRLENBQUM7UUFDbmMsVUFBVSxFQUFFLEVBQUMsU0FBUyxFQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsQ0FBQyxFQUFDLFdBQVcsRUFBQyxJQUFJLEVBQUMsRUFBQztLQUNoSSxDQUFDLENBQUM7SUFDSCxPQUFPLEtBQUssQ0FBQztBQUNiLENBQUMsQ0FBQyxFQUFFLENBQUM7QUFDTCxPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzs7Ozs7O0FDell0QixJQUFJLHU2QkFBSjtBQWdEUyxRQUFBLEtBQUEsR0FBQSxLQUFBOzs7O0FDaERULHNDQUFzQztBQUN0Qzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUF1RUU7QUFDRixJQUFJLFdBQVcsR0FBRyxDQUFDO0lBQ25CLElBQUksQ0FBQyxHQUFDLFVBQVMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxJQUFFLEtBQUksQ0FBQyxHQUFDLENBQUMsSUFBRSxFQUFFLEVBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFDLENBQUM7UUFBQyxDQUFDLENBQUEsT0FBTyxDQUFDLENBQUEsQ0FBQSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsR0FBRyxHQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEdBQUcsR0FBQyxDQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsRUFBQyxHQUFHLEdBQUMsQ0FBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxDQUFDLENBQUM7SUFDdEssSUFBSSxNQUFNLEdBQUcsRUFBQyxLQUFLLEVBQUUsU0FBUyxLQUFLLEtBQU0sQ0FBQztRQUMxQyxFQUFFLEVBQUUsRUFBRTtRQUNOLFFBQVEsRUFBRSxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsQ0FBQyxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsU0FBUyxFQUFDLENBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDLE1BQU0sRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLFNBQVMsRUFBQyxFQUFFLEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLFNBQVMsRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLENBQUMsRUFBQztRQUN4TyxVQUFVLEVBQUUsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxLQUFLLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsV0FBVyxFQUFDLEVBQUUsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLFNBQVMsRUFBQyxFQUFFLEVBQUMsTUFBTSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDO1FBQzdILFlBQVksRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQztRQUN2SSxhQUFhLEVBQUUsU0FBUyxTQUFTLENBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLE9BQU8sQ0FBQyxlQUFlLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsWUFBWTtZQUN6SCxtQkFBbUI7WUFFbkIsSUFBSSxFQUFFLEdBQUcsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDdkIsUUFBUSxPQUFPLEVBQUU7Z0JBQ2pCLEtBQUssQ0FBQztvQkFDTCxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQUMsT0FBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFDbEMsTUFBTTtnQkFDTixLQUFLLENBQUMsQ0FBQztnQkFBQyxLQUFLLENBQUMsQ0FBQztnQkFBQyxLQUFLLEVBQUUsQ0FBQztnQkFBQyxLQUFLLEVBQUU7b0JBQy9CLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDbkIsTUFBTTtnQkFDTixLQUFLLENBQUMsQ0FBQztnQkFBQyxLQUFLLENBQUMsQ0FBQztnQkFBQyxLQUFLLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDeEMsTUFBTTtnQkFDTixLQUFLLENBQUM7b0JBQ0wsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7b0JBQy9DLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUNMLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQzdDLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUNMLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQzdCLE1BQU07Z0JBQ04sS0FBSyxDQUFDO29CQUNMLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDNUMsTUFBTTtnQkFDTixLQUFLLEVBQUU7b0JBQ04sSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUM3QyxNQUFNO2dCQUNOLEtBQUssRUFBRTtvQkFDTixJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQy9DLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUNOLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFDLENBQUM7b0JBQzFCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUNOLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFDLENBQUM7b0JBQzNCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUNOLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFDLENBQUM7b0JBQzdCLE1BQU07Z0JBQ04sS0FBSyxFQUFFO29CQUNOLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBQyxDQUFDLENBQUMsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDeEMsTUFBTTthQUNMO1FBQ0QsQ0FBQztRQUNELEtBQUssRUFBRSxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxFQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsRUFBQyxFQUFDLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDNWUsY0FBYyxFQUFFLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDO1FBQ3pCLFVBQVUsRUFBRSxTQUFTLFVBQVUsQ0FBRSxHQUFHLEVBQUUsSUFBSTtZQUN0QyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbkI7aUJBQU07Z0JBQ0gsSUFBSSxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNsQixNQUFNLEtBQUssQ0FBQzthQUNmO1FBQ0wsQ0FBQztRQUNELEtBQUssRUFBRSxTQUFTLEtBQUssQ0FBQyxLQUFLO1lBQ3ZCLElBQUksSUFBSSxHQUFHLElBQUksRUFBRSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEdBQUcsRUFBRSxFQUFFLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sR0FBRyxFQUFFLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxHQUFHLEVBQUUsRUFBRSxRQUFRLEdBQUcsQ0FBQyxFQUFFLE1BQU0sR0FBRyxDQUFDLEVBQUUsVUFBVSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDeEssSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLElBQUksV0FBVyxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQzdCLEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTtnQkFDbkIsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRTtvQkFDbEQsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQzthQUNKO1lBQ0QsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3RDLFdBQVcsQ0FBQyxFQUFFLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUM3QixXQUFXLENBQUMsRUFBRSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDN0IsSUFBSSxPQUFPLEtBQUssQ0FBQyxNQUFNLElBQUksV0FBVyxFQUFFO2dCQUNwQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzthQUNyQjtZQUNELElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQ25ELElBQUksT0FBTyxXQUFXLENBQUMsRUFBRSxDQUFDLFVBQVUsS0FBSyxVQUFVLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUM7YUFDL0M7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQzthQUM1RDtZQUNELFNBQVMsUUFBUSxDQUFDLENBQUM7Z0JBQ2YsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDdEMsQ0FBQztZQUNELFlBQVksRUFDUixJQUFJLEdBQUcsR0FBRztnQkFDTixJQUFJLEtBQUssQ0FBQztnQkFDVixLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsRUFBRSxJQUFJLEdBQUcsQ0FBQztnQkFDM0IsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBQzNCLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQztpQkFDekM7Z0JBQ0QsT0FBTyxLQUFLLENBQUM7WUFDakIsQ0FBQyxDQUFDO1lBQ04sSUFBSSxNQUFNLEVBQUUsY0FBYyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQztZQUN4RixPQUFPLElBQUksRUFBRTtnQkFDVCxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDNUIsTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3ZDO3FCQUFNO29CQUNILElBQUksTUFBTSxLQUFLLElBQUksSUFBSSxPQUFPLE1BQU0sSUFBSSxXQUFXLEVBQUU7d0JBQ2pELE1BQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQztxQkFDbEI7b0JBQ0QsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ2pEO2dCQUNXLElBQUksT0FBTyxNQUFNLEtBQUssV0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDdkUsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO29CQUNoQixRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNkLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDcEIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLEVBQUU7NEJBQ2xDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7eUJBQ25EO3FCQUNKO29CQUNELElBQUksS0FBSyxDQUFDLFlBQVksRUFBRTt3QkFDcEIsTUFBTSxHQUFHLHNCQUFzQixHQUFHLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLEdBQUcsY0FBYyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ3BMO3lCQUFNO3dCQUNILE1BQU0sR0FBRyxzQkFBc0IsR0FBRyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsR0FBRyxlQUFlLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7cUJBQzdKO29CQUNELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO3dCQUNwQixJQUFJLEVBQUUsS0FBSyxDQUFDLEtBQUs7d0JBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLE1BQU07d0JBQ3hDLElBQUksRUFBRSxLQUFLLENBQUMsUUFBUTt3QkFDcEIsR0FBRyxFQUFFLEtBQUs7d0JBQ1YsUUFBUSxFQUFFLFFBQVE7cUJBQ3JCLENBQUMsQ0FBQztpQkFDTjtnQkFDTCxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsWUFBWSxLQUFLLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2pELE1BQU0sSUFBSSxLQUFLLENBQUMsbURBQW1ELEdBQUcsS0FBSyxHQUFHLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQztpQkFDdkc7Z0JBQ0QsUUFBUSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ25CLEtBQUssQ0FBQzt3QkFDRixLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQzFCLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ2QsSUFBSSxDQUFDLGNBQWMsRUFBRTs0QkFDakIsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7NEJBQ3RCLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDOzRCQUN0QixRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQzs0QkFDMUIsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7NEJBQ3JCLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRTtnQ0FDaEIsVUFBVSxFQUFFLENBQUM7NkJBQ2hCO3lCQUNKOzZCQUFNOzRCQUNILE1BQU0sR0FBRyxjQUFjLENBQUM7NEJBQ3hCLGNBQWMsR0FBRyxJQUFJLENBQUM7eUJBQ3pCO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxDQUFDO3dCQUNGLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN0QyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO3dCQUN0QyxLQUFLLENBQUMsRUFBRSxHQUFHOzRCQUNQLFVBQVUsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVU7NEJBQ3pELFNBQVMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTOzRCQUM5QyxZQUFZLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZOzRCQUM3RCxXQUFXLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVzt5QkFDckQsQ0FBQzt3QkFDRixJQUFJLE1BQU0sRUFBRTs0QkFDUixLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssR0FBRztnQ0FDYixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0NBQzNDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7NkJBQ3JDLENBQUM7eUJBQ0w7d0JBQ0QsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTs0QkFDaEMsTUFBTTs0QkFDTixNQUFNOzRCQUNOLFFBQVE7NEJBQ1IsV0FBVyxDQUFDLEVBQUU7NEJBQ2QsTUFBTSxDQUFDLENBQUMsQ0FBQzs0QkFDVCxNQUFNOzRCQUNOLE1BQU07eUJBQ1QsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDaEIsSUFBSSxPQUFPLENBQUMsS0FBSyxXQUFXLEVBQUU7NEJBQzFCLE9BQU8sQ0FBQyxDQUFDO3lCQUNaO3dCQUNELElBQUksR0FBRyxFQUFFOzRCQUNMLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQ3JDLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQzs0QkFDbkMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO3lCQUN0Qzt3QkFDRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDNUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUN0QixRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDckIsTUFBTTtvQkFDVixLQUFLLENBQUM7d0JBQ0YsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7YUFDSjtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUMsRUFBQyxDQUFDO0lBQ0gsa0NBQWtDO0lBQ2xDLElBQUksS0FBSyxHQUFHLENBQUM7UUFDYixJQUFJLEtBQUssR0FBRyxDQUFDO1lBRWIsR0FBRyxFQUFDLENBQUM7WUFFTCxVQUFVLEVBQUMsU0FBUyxVQUFVLENBQUMsR0FBRyxFQUFFLElBQUk7Z0JBQ2hDLElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ3hDO3FCQUFNO29CQUNILE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3hCO1lBQ0wsQ0FBQztZQUVMLG1DQUFtQztZQUNuQyxRQUFRLEVBQUMsVUFBVSxLQUFLLEVBQUUsRUFBRTtnQkFDcEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztnQkFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsTUFBTSxHQUFHO29CQUNWLFVBQVUsRUFBRSxDQUFDO29CQUNiLFlBQVksRUFBRSxDQUFDO29CQUNmLFNBQVMsRUFBRSxDQUFDO29CQUNaLFdBQVcsRUFBRSxDQUFDO2lCQUNqQixDQUFDO2dCQUNGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDaEIsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUVMLCtDQUErQztZQUMvQyxLQUFLLEVBQUM7Z0JBQ0UsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUM7Z0JBQ2xCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDZCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ2QsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO2dCQUNuQixJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3hDLElBQUksS0FBSyxFQUFFO29CQUNQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztpQkFDM0I7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDN0I7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtvQkFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDMUI7Z0JBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkMsT0FBTyxFQUFFLENBQUM7WUFDZCxDQUFDO1lBRUwsaURBQWlEO1lBQ2pELEtBQUssRUFBQyxVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQztnQkFDcEIsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFFdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQzlELHFCQUFxQjtnQkFDckIsSUFBSSxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUM7Z0JBQ25CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBRS9ELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7aUJBQ3JDO2dCQUNELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUUxQixJQUFJLENBQUMsTUFBTSxHQUFHO29CQUNWLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVU7b0JBQ2xDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUM7b0JBQzVCLFlBQVksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVk7b0JBQ3RDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFDaEIsQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7OEJBQzlELFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN4RSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxHQUFHO2lCQUNuQyxDQUFDO2dCQUVGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUN4RDtnQkFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO2dCQUNqQyxPQUFPLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUwsNkVBQTZFO1lBQzdFLElBQUksRUFBQztnQkFDRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztnQkFDbEIsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQztZQUVMLGtKQUFrSjtZQUNsSixNQUFNLEVBQUM7Z0JBQ0MsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRTtvQkFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7aUJBQzFCO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEdBQUcsa0lBQWtJLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFO3dCQUM5TixJQUFJLEVBQUUsRUFBRTt3QkFDUixLQUFLLEVBQUUsSUFBSTt3QkFDWCxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVE7cUJBQ3RCLENBQUMsQ0FBQztpQkFFTjtnQkFDRCxPQUFPLElBQUksQ0FBQztZQUNoQixDQUFDO1lBRUwseUNBQXlDO1lBQ3pDLElBQUksRUFBQyxVQUFVLENBQUM7Z0JBQ1IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLENBQUM7WUFFTCwwREFBMEQ7WUFDMUQsU0FBUyxFQUFDO2dCQUNGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMzRSxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQSxDQUFDLENBQUEsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDL0UsQ0FBQztZQUVMLG1EQUFtRDtZQUNuRCxhQUFhLEVBQUM7Z0JBQ04sSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRTtvQkFDbEIsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNqRDtnQkFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDcEYsQ0FBQztZQUVMLDJGQUEyRjtZQUMzRixZQUFZLEVBQUM7Z0JBQ0wsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUMsT0FBTyxHQUFHLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLElBQUksR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQ3ZELENBQUM7WUFFTCw4RUFBOEU7WUFDOUUsVUFBVSxFQUFDLFVBQVMsS0FBSyxFQUFFLFlBQVk7Z0JBQy9CLElBQUksS0FBSyxFQUNMLEtBQUssRUFDTCxNQUFNLENBQUM7Z0JBRVgsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRTtvQkFDOUIsZUFBZTtvQkFDZixNQUFNLEdBQUc7d0JBQ0wsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVTs0QkFDbEMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTOzRCQUN6QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZOzRCQUN0QyxXQUFXLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXO3lCQUN2Qzt3QkFDRCxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07d0JBQ25CLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzt3QkFDakIsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPO3dCQUNyQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87d0JBQ3JCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTt3QkFDbkIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO3dCQUNuQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7d0JBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTt3QkFDbkIsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFO3dCQUNYLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQzVDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtxQkFDbEIsQ0FBQztvQkFDRixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO3dCQUNyQixNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ3BEO2lCQUNKO2dCQUVELEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQzFDLElBQUksS0FBSyxFQUFFO29CQUNQLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQztpQkFDakM7Z0JBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRztvQkFDVixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO29CQUNqQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDO29CQUM1QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXO29CQUNyQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7d0JBQ1AsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDcEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU07aUJBQ3pELENBQUM7Z0JBQ0YsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFDakMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtvQkFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNqRTtnQkFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekIsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4SCxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7aUJBQ3JCO2dCQUNELElBQUksS0FBSyxFQUFFO29CQUNQLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjtxQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ3hCLGtCQUFrQjtvQkFDbEIsS0FBSyxJQUFJLENBQUMsSUFBSSxNQUFNLEVBQUU7d0JBQ2xCLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ3ZCO29CQUNELE9BQU8sS0FBSyxDQUFDLENBQUMsK0VBQStFO2lCQUNoRztnQkFDRCxPQUFPLEtBQUssQ0FBQztZQUNqQixDQUFDO1lBRUwsNkJBQTZCO1lBQzdCLElBQUksRUFBQztnQkFDRyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ1gsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO2lCQUNuQjtnQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDZCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztpQkFDcEI7Z0JBRUQsSUFBSSxLQUFLLEVBQ0wsS0FBSyxFQUNMLFNBQVMsRUFDVCxLQUFLLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7b0JBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2lCQUNuQjtnQkFDRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNuQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwRCxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNoRSxLQUFLLEdBQUcsU0FBUyxDQUFDO3dCQUNsQixLQUFLLEdBQUcsQ0FBQyxDQUFDO3dCQUNWLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7NEJBQzlCLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDN0MsSUFBSSxLQUFLLEtBQUssS0FBSyxFQUFFO2dDQUNqQixPQUFPLEtBQUssQ0FBQzs2QkFDaEI7aUNBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dDQUN4QixLQUFLLEdBQUcsS0FBSyxDQUFDO2dDQUNkLFNBQVMsQ0FBQyx3REFBd0Q7NkJBQ3JFO2lDQUFNO2dDQUNILDhGQUE4RjtnQ0FDOUYsT0FBTyxLQUFLLENBQUM7NkJBQ2hCO3lCQUNKOzZCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRTs0QkFDM0IsTUFBTTt5QkFDVDtxQkFDSjtpQkFDSjtnQkFDRCxJQUFJLEtBQUssRUFBRTtvQkFDUCxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQzdDLElBQUksS0FBSyxLQUFLLEtBQUssRUFBRTt3QkFDakIsT0FBTyxLQUFLLENBQUM7cUJBQ2hCO29CQUNELDhGQUE4RjtvQkFDOUYsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUNELElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7b0JBQ3BCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztpQkFDbkI7cUJBQU07b0JBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLHdCQUF3QixHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsR0FBRyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUU7d0JBQ3BILElBQUksRUFBRSxFQUFFO3dCQUNSLEtBQUssRUFBRSxJQUFJO3dCQUNYLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUTtxQkFDdEIsQ0FBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQztZQUVMLHFDQUFxQztZQUNyQyxHQUFHLEVBQUMsU0FBUyxHQUFHO2dCQUNSLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDcEIsSUFBSSxDQUFDLEVBQUU7b0JBQ0gsT0FBTyxDQUFDLENBQUM7aUJBQ1o7cUJBQU07b0JBQ0gsT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3JCO1lBQ0wsQ0FBQztZQUVMLHdHQUF3RztZQUN4RyxLQUFLLEVBQUMsU0FBUyxLQUFLLENBQUUsU0FBUztnQkFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDeEMsQ0FBQztZQUVMLDBFQUEwRTtZQUMxRSxRQUFRLEVBQUMsU0FBUyxRQUFRO2dCQUNsQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDUCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3BDO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDakM7WUFDTCxDQUFDO1lBRUwsNEZBQTRGO1lBQzVGLGFBQWEsRUFBQyxTQUFTLGFBQWE7Z0JBQzVCLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDbkYsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQ3JGO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQzNDO1lBQ0wsQ0FBQztZQUVMLG9KQUFvSjtZQUNwSixRQUFRLEVBQUMsU0FBUyxRQUFRLENBQUUsQ0FBQztnQkFDckIsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNSLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDakM7cUJBQU07b0JBQ0gsT0FBTyxTQUFTLENBQUM7aUJBQ3BCO1lBQ0wsQ0FBQztZQUVMLDZCQUE2QjtZQUM3QixTQUFTLEVBQUMsU0FBUyxTQUFTLENBQUUsU0FBUztnQkFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMxQixDQUFDO1lBRUwscURBQXFEO1lBQ3JELGNBQWMsRUFBQyxTQUFTLGNBQWM7Z0JBQzlCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDdEMsQ0FBQztZQUNMLE9BQU8sRUFBRSxFQUFFO1lBQ1gsYUFBYSxFQUFFLFNBQVMsU0FBUyxDQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMseUJBQXlCLEVBQUMsUUFBUTtnQkFDM0UsSUFBSSxPQUFPLEdBQUMsUUFBUSxDQUFDO2dCQUNyQixRQUFPLHlCQUF5QixFQUFFO29CQUNsQyxLQUFLLENBQUMsRUFBQyxxQkFBcUI7d0JBQzVCLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2hFLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNoQixNQUFNO29CQUNOLEtBQUssQ0FBQzt3QkFBQyxPQUFPLENBQUMsQ0FBQTt3QkFDZixNQUFNO29CQUNOLEtBQUssQ0FBQzt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDaEIsTUFBTTtvQkFDTixLQUFLLENBQUM7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2hCLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLE9BQU8sRUFBRSxDQUFBO3dCQUNoQixNQUFNO29CQUNOLEtBQUssQ0FBQzt3QkFBQyxPQUFPLEVBQUUsQ0FBQTt3QkFDaEIsTUFBTTtvQkFDTixLQUFLLENBQUM7d0JBQUMsT0FBTyxFQUFFLENBQUE7d0JBQ2hCLE1BQU07b0JBQ04sS0FBSyxDQUFDO3dCQUFDLE9BQU8sS0FBSyxDQUFBO3dCQUNuQixNQUFNO29CQUNOLEtBQUssRUFBRTt3QkFBQyxPQUFPLFNBQVMsQ0FBQTt3QkFDeEIsTUFBTTtvQkFDTixLQUFLLEVBQUU7d0JBQUMsT0FBTyxDQUFDLENBQUE7d0JBQ2hCLE1BQU07aUJBQ0w7WUFDRCxDQUFDO1lBQ0QsS0FBSyxFQUFFLENBQUMsVUFBVSxFQUFDLGFBQWEsRUFBQyxpQ0FBaUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLFFBQVEsRUFBQyxhQUFhLEVBQUMsUUFBUSxFQUFDLFNBQVMsRUFBQyxTQUFTLEVBQUMsUUFBUSxFQUFDLFFBQVEsQ0FBQztZQUMzSixVQUFVLEVBQUUsRUFBQyxTQUFTLEVBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsRUFBRSxFQUFDLEVBQUUsQ0FBQyxFQUFDLFdBQVcsRUFBQyxJQUFJLEVBQUMsRUFBQztTQUM3RSxDQUFDLENBQUM7UUFDSCxPQUFPLEtBQUssQ0FBQztJQUNiLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDTCxNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNyQixTQUFTLE1BQU07UUFDYixJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFDRCxNQUFNLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQztJQUFBLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ2pELE9BQU8sSUFBSSxNQUFNLENBQUM7QUFDbEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztBQUdMLElBQUksT0FBTyxPQUFPLEtBQUssV0FBVyxJQUFJLE9BQU8sT0FBTyxLQUFLLFdBQVcsRUFBRTtJQUN0RSxPQUFPLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQztJQUM3QixPQUFPLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7SUFDcEMsT0FBTyxDQUFDLEtBQUssR0FBRyxjQUFjLE9BQU8sV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3hGLE9BQU8sQ0FBQyxJQUFJLEdBQUcsU0FBUyxZQUFZLENBQUUsSUFBSTtRQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkI7UUFDRCxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDcEYsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN4QyxDQUFDLENBQUM7SUFDRixJQUFJLE9BQU8sTUFBTSxLQUFLLFdBQVcsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtRQUM1RCxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7S0FDckM7Q0FDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIiIsIi8vIC5kaXJuYW1lLCAuYmFzZW5hbWUsIGFuZCAuZXh0bmFtZSBtZXRob2RzIGFyZSBleHRyYWN0ZWQgZnJvbSBOb2RlLmpzIHY4LjExLjEsXG4vLyBiYWNrcG9ydGVkIGFuZCB0cmFuc3BsaXRlZCB3aXRoIEJhYmVsLCB3aXRoIGJhY2t3YXJkcy1jb21wYXQgZml4ZXNcblxuLy8gQ29weXJpZ2h0IEpveWVudCwgSW5jLiBhbmQgb3RoZXIgTm9kZSBjb250cmlidXRvcnMuXG4vL1xuLy8gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGFcbi8vIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbi8vIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZ1xuLy8gd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHMgdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLFxuLy8gZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdFxuLy8gcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpcyBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlXG4vLyBmb2xsb3dpbmcgY29uZGl0aW9uczpcbi8vXG4vLyBUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZFxuLy8gaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4vL1xuLy8gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTU1xuLy8gT1IgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRlxuLy8gTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTlxuLy8gTk8gRVZFTlQgU0hBTEwgVEhFIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sXG4vLyBEQU1BR0VTIE9SIE9USEVSIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1Jcbi8vIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEVcbi8vIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG5cbi8vIHJlc29sdmVzIC4gYW5kIC4uIGVsZW1lbnRzIGluIGEgcGF0aCBhcnJheSB3aXRoIGRpcmVjdG9yeSBuYW1lcyB0aGVyZVxuLy8gbXVzdCBiZSBubyBzbGFzaGVzLCBlbXB0eSBlbGVtZW50cywgb3IgZGV2aWNlIG5hbWVzIChjOlxcKSBpbiB0aGUgYXJyYXlcbi8vIChzbyBhbHNvIG5vIGxlYWRpbmcgYW5kIHRyYWlsaW5nIHNsYXNoZXMgLSBpdCBkb2VzIG5vdCBkaXN0aW5ndWlzaFxuLy8gcmVsYXRpdmUgYW5kIGFic29sdXRlIHBhdGhzKVxuZnVuY3Rpb24gbm9ybWFsaXplQXJyYXkocGFydHMsIGFsbG93QWJvdmVSb290KSB7XG4gIC8vIGlmIHRoZSBwYXRoIHRyaWVzIHRvIGdvIGFib3ZlIHRoZSByb290LCBgdXBgIGVuZHMgdXAgPiAwXG4gIHZhciB1cCA9IDA7XG4gIGZvciAodmFyIGkgPSBwYXJ0cy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgIHZhciBsYXN0ID0gcGFydHNbaV07XG4gICAgaWYgKGxhc3QgPT09ICcuJykge1xuICAgICAgcGFydHMuc3BsaWNlKGksIDEpO1xuICAgIH0gZWxzZSBpZiAobGFzdCA9PT0gJy4uJykge1xuICAgICAgcGFydHMuc3BsaWNlKGksIDEpO1xuICAgICAgdXArKztcbiAgICB9IGVsc2UgaWYgKHVwKSB7XG4gICAgICBwYXJ0cy5zcGxpY2UoaSwgMSk7XG4gICAgICB1cC0tO1xuICAgIH1cbiAgfVxuXG4gIC8vIGlmIHRoZSBwYXRoIGlzIGFsbG93ZWQgdG8gZ28gYWJvdmUgdGhlIHJvb3QsIHJlc3RvcmUgbGVhZGluZyAuLnNcbiAgaWYgKGFsbG93QWJvdmVSb290KSB7XG4gICAgZm9yICg7IHVwLS07IHVwKSB7XG4gICAgICBwYXJ0cy51bnNoaWZ0KCcuLicpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBwYXJ0cztcbn1cblxuLy8gcGF0aC5yZXNvbHZlKFtmcm9tIC4uLl0sIHRvKVxuLy8gcG9zaXggdmVyc2lvblxuZXhwb3J0cy5yZXNvbHZlID0gZnVuY3Rpb24oKSB7XG4gIHZhciByZXNvbHZlZFBhdGggPSAnJyxcbiAgICAgIHJlc29sdmVkQWJzb2x1dGUgPSBmYWxzZTtcblxuICBmb3IgKHZhciBpID0gYXJndW1lbnRzLmxlbmd0aCAtIDE7IGkgPj0gLTEgJiYgIXJlc29sdmVkQWJzb2x1dGU7IGktLSkge1xuICAgIHZhciBwYXRoID0gKGkgPj0gMCkgPyBhcmd1bWVudHNbaV0gOiBwcm9jZXNzLmN3ZCgpO1xuXG4gICAgLy8gU2tpcCBlbXB0eSBhbmQgaW52YWxpZCBlbnRyaWVzXG4gICAgaWYgKHR5cGVvZiBwYXRoICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignQXJndW1lbnRzIHRvIHBhdGgucmVzb2x2ZSBtdXN0IGJlIHN0cmluZ3MnKTtcbiAgICB9IGVsc2UgaWYgKCFwYXRoKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICByZXNvbHZlZFBhdGggPSBwYXRoICsgJy8nICsgcmVzb2x2ZWRQYXRoO1xuICAgIHJlc29sdmVkQWJzb2x1dGUgPSBwYXRoLmNoYXJBdCgwKSA9PT0gJy8nO1xuICB9XG5cbiAgLy8gQXQgdGhpcyBwb2ludCB0aGUgcGF0aCBzaG91bGQgYmUgcmVzb2x2ZWQgdG8gYSBmdWxsIGFic29sdXRlIHBhdGgsIGJ1dFxuICAvLyBoYW5kbGUgcmVsYXRpdmUgcGF0aHMgdG8gYmUgc2FmZSAobWlnaHQgaGFwcGVuIHdoZW4gcHJvY2Vzcy5jd2QoKSBmYWlscylcblxuICAvLyBOb3JtYWxpemUgdGhlIHBhdGhcbiAgcmVzb2x2ZWRQYXRoID0gbm9ybWFsaXplQXJyYXkoZmlsdGVyKHJlc29sdmVkUGF0aC5zcGxpdCgnLycpLCBmdW5jdGlvbihwKSB7XG4gICAgcmV0dXJuICEhcDtcbiAgfSksICFyZXNvbHZlZEFic29sdXRlKS5qb2luKCcvJyk7XG5cbiAgcmV0dXJuICgocmVzb2x2ZWRBYnNvbHV0ZSA/ICcvJyA6ICcnKSArIHJlc29sdmVkUGF0aCkgfHwgJy4nO1xufTtcblxuLy8gcGF0aC5ub3JtYWxpemUocGF0aClcbi8vIHBvc2l4IHZlcnNpb25cbmV4cG9ydHMubm9ybWFsaXplID0gZnVuY3Rpb24ocGF0aCkge1xuICB2YXIgaXNBYnNvbHV0ZSA9IGV4cG9ydHMuaXNBYnNvbHV0ZShwYXRoKSxcbiAgICAgIHRyYWlsaW5nU2xhc2ggPSBzdWJzdHIocGF0aCwgLTEpID09PSAnLyc7XG5cbiAgLy8gTm9ybWFsaXplIHRoZSBwYXRoXG4gIHBhdGggPSBub3JtYWxpemVBcnJheShmaWx0ZXIocGF0aC5zcGxpdCgnLycpLCBmdW5jdGlvbihwKSB7XG4gICAgcmV0dXJuICEhcDtcbiAgfSksICFpc0Fic29sdXRlKS5qb2luKCcvJyk7XG5cbiAgaWYgKCFwYXRoICYmICFpc0Fic29sdXRlKSB7XG4gICAgcGF0aCA9ICcuJztcbiAgfVxuICBpZiAocGF0aCAmJiB0cmFpbGluZ1NsYXNoKSB7XG4gICAgcGF0aCArPSAnLyc7XG4gIH1cblxuICByZXR1cm4gKGlzQWJzb2x1dGUgPyAnLycgOiAnJykgKyBwYXRoO1xufTtcblxuLy8gcG9zaXggdmVyc2lvblxuZXhwb3J0cy5pc0Fic29sdXRlID0gZnVuY3Rpb24ocGF0aCkge1xuICByZXR1cm4gcGF0aC5jaGFyQXQoMCkgPT09ICcvJztcbn07XG5cbi8vIHBvc2l4IHZlcnNpb25cbmV4cG9ydHMuam9pbiA9IGZ1bmN0aW9uKCkge1xuICB2YXIgcGF0aHMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDApO1xuICByZXR1cm4gZXhwb3J0cy5ub3JtYWxpemUoZmlsdGVyKHBhdGhzLCBmdW5jdGlvbihwLCBpbmRleCkge1xuICAgIGlmICh0eXBlb2YgcCAhPT0gJ3N0cmluZycpIHtcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0FyZ3VtZW50cyB0byBwYXRoLmpvaW4gbXVzdCBiZSBzdHJpbmdzJyk7XG4gICAgfVxuICAgIHJldHVybiBwO1xuICB9KS5qb2luKCcvJykpO1xufTtcblxuXG4vLyBwYXRoLnJlbGF0aXZlKGZyb20sIHRvKVxuLy8gcG9zaXggdmVyc2lvblxuZXhwb3J0cy5yZWxhdGl2ZSA9IGZ1bmN0aW9uKGZyb20sIHRvKSB7XG4gIGZyb20gPSBleHBvcnRzLnJlc29sdmUoZnJvbSkuc3Vic3RyKDEpO1xuICB0byA9IGV4cG9ydHMucmVzb2x2ZSh0bykuc3Vic3RyKDEpO1xuXG4gIGZ1bmN0aW9uIHRyaW0oYXJyKSB7XG4gICAgdmFyIHN0YXJ0ID0gMDtcbiAgICBmb3IgKDsgc3RhcnQgPCBhcnIubGVuZ3RoOyBzdGFydCsrKSB7XG4gICAgICBpZiAoYXJyW3N0YXJ0XSAhPT0gJycpIGJyZWFrO1xuICAgIH1cblxuICAgIHZhciBlbmQgPSBhcnIubGVuZ3RoIC0gMTtcbiAgICBmb3IgKDsgZW5kID49IDA7IGVuZC0tKSB7XG4gICAgICBpZiAoYXJyW2VuZF0gIT09ICcnKSBicmVhaztcbiAgICB9XG5cbiAgICBpZiAoc3RhcnQgPiBlbmQpIHJldHVybiBbXTtcbiAgICByZXR1cm4gYXJyLnNsaWNlKHN0YXJ0LCBlbmQgLSBzdGFydCArIDEpO1xuICB9XG5cbiAgdmFyIGZyb21QYXJ0cyA9IHRyaW0oZnJvbS5zcGxpdCgnLycpKTtcbiAgdmFyIHRvUGFydHMgPSB0cmltKHRvLnNwbGl0KCcvJykpO1xuXG4gIHZhciBsZW5ndGggPSBNYXRoLm1pbihmcm9tUGFydHMubGVuZ3RoLCB0b1BhcnRzLmxlbmd0aCk7XG4gIHZhciBzYW1lUGFydHNMZW5ndGggPSBsZW5ndGg7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoZnJvbVBhcnRzW2ldICE9PSB0b1BhcnRzW2ldKSB7XG4gICAgICBzYW1lUGFydHNMZW5ndGggPSBpO1xuICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgdmFyIG91dHB1dFBhcnRzID0gW107XG4gIGZvciAodmFyIGkgPSBzYW1lUGFydHNMZW5ndGg7IGkgPCBmcm9tUGFydHMubGVuZ3RoOyBpKyspIHtcbiAgICBvdXRwdXRQYXJ0cy5wdXNoKCcuLicpO1xuICB9XG5cbiAgb3V0cHV0UGFydHMgPSBvdXRwdXRQYXJ0cy5jb25jYXQodG9QYXJ0cy5zbGljZShzYW1lUGFydHNMZW5ndGgpKTtcblxuICByZXR1cm4gb3V0cHV0UGFydHMuam9pbignLycpO1xufTtcblxuZXhwb3J0cy5zZXAgPSAnLyc7XG5leHBvcnRzLmRlbGltaXRlciA9ICc6JztcblxuZXhwb3J0cy5kaXJuYW1lID0gZnVuY3Rpb24gKHBhdGgpIHtcbiAgaWYgKHR5cGVvZiBwYXRoICE9PSAnc3RyaW5nJykgcGF0aCA9IHBhdGggKyAnJztcbiAgaWYgKHBhdGgubGVuZ3RoID09PSAwKSByZXR1cm4gJy4nO1xuICB2YXIgY29kZSA9IHBhdGguY2hhckNvZGVBdCgwKTtcbiAgdmFyIGhhc1Jvb3QgPSBjb2RlID09PSA0NyAvKi8qLztcbiAgdmFyIGVuZCA9IC0xO1xuICB2YXIgbWF0Y2hlZFNsYXNoID0gdHJ1ZTtcbiAgZm9yICh2YXIgaSA9IHBhdGgubGVuZ3RoIC0gMTsgaSA+PSAxOyAtLWkpIHtcbiAgICBjb2RlID0gcGF0aC5jaGFyQ29kZUF0KGkpO1xuICAgIGlmIChjb2RlID09PSA0NyAvKi8qLykge1xuICAgICAgICBpZiAoIW1hdGNoZWRTbGFzaCkge1xuICAgICAgICAgIGVuZCA9IGk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAvLyBXZSBzYXcgdGhlIGZpcnN0IG5vbi1wYXRoIHNlcGFyYXRvclxuICAgICAgbWF0Y2hlZFNsYXNoID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgaWYgKGVuZCA9PT0gLTEpIHJldHVybiBoYXNSb290ID8gJy8nIDogJy4nO1xuICBpZiAoaGFzUm9vdCAmJiBlbmQgPT09IDEpIHtcbiAgICAvLyByZXR1cm4gJy8vJztcbiAgICAvLyBCYWNrd2FyZHMtY29tcGF0IGZpeDpcbiAgICByZXR1cm4gJy8nO1xuICB9XG4gIHJldHVybiBwYXRoLnNsaWNlKDAsIGVuZCk7XG59O1xuXG5mdW5jdGlvbiBiYXNlbmFtZShwYXRoKSB7XG4gIGlmICh0eXBlb2YgcGF0aCAhPT0gJ3N0cmluZycpIHBhdGggPSBwYXRoICsgJyc7XG5cbiAgdmFyIHN0YXJ0ID0gMDtcbiAgdmFyIGVuZCA9IC0xO1xuICB2YXIgbWF0Y2hlZFNsYXNoID0gdHJ1ZTtcbiAgdmFyIGk7XG5cbiAgZm9yIChpID0gcGF0aC5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgIGlmIChwYXRoLmNoYXJDb2RlQXQoaSkgPT09IDQ3IC8qLyovKSB7XG4gICAgICAgIC8vIElmIHdlIHJlYWNoZWQgYSBwYXRoIHNlcGFyYXRvciB0aGF0IHdhcyBub3QgcGFydCBvZiBhIHNldCBvZiBwYXRoXG4gICAgICAgIC8vIHNlcGFyYXRvcnMgYXQgdGhlIGVuZCBvZiB0aGUgc3RyaW5nLCBzdG9wIG5vd1xuICAgICAgICBpZiAoIW1hdGNoZWRTbGFzaCkge1xuICAgICAgICAgIHN0YXJ0ID0gaSArIDE7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoZW5kID09PSAtMSkge1xuICAgICAgLy8gV2Ugc2F3IHRoZSBmaXJzdCBub24tcGF0aCBzZXBhcmF0b3IsIG1hcmsgdGhpcyBhcyB0aGUgZW5kIG9mIG91clxuICAgICAgLy8gcGF0aCBjb21wb25lbnRcbiAgICAgIG1hdGNoZWRTbGFzaCA9IGZhbHNlO1xuICAgICAgZW5kID0gaSArIDE7XG4gICAgfVxuICB9XG5cbiAgaWYgKGVuZCA9PT0gLTEpIHJldHVybiAnJztcbiAgcmV0dXJuIHBhdGguc2xpY2Uoc3RhcnQsIGVuZCk7XG59XG5cbi8vIFVzZXMgYSBtaXhlZCBhcHByb2FjaCBmb3IgYmFja3dhcmRzLWNvbXBhdGliaWxpdHksIGFzIGV4dCBiZWhhdmlvciBjaGFuZ2VkXG4vLyBpbiBuZXcgTm9kZS5qcyB2ZXJzaW9ucywgc28gb25seSBiYXNlbmFtZSgpIGFib3ZlIGlzIGJhY2twb3J0ZWQgaGVyZVxuZXhwb3J0cy5iYXNlbmFtZSA9IGZ1bmN0aW9uIChwYXRoLCBleHQpIHtcbiAgdmFyIGYgPSBiYXNlbmFtZShwYXRoKTtcbiAgaWYgKGV4dCAmJiBmLnN1YnN0cigtMSAqIGV4dC5sZW5ndGgpID09PSBleHQpIHtcbiAgICBmID0gZi5zdWJzdHIoMCwgZi5sZW5ndGggLSBleHQubGVuZ3RoKTtcbiAgfVxuICByZXR1cm4gZjtcbn07XG5cbmV4cG9ydHMuZXh0bmFtZSA9IGZ1bmN0aW9uIChwYXRoKSB7XG4gIGlmICh0eXBlb2YgcGF0aCAhPT0gJ3N0cmluZycpIHBhdGggPSBwYXRoICsgJyc7XG4gIHZhciBzdGFydERvdCA9IC0xO1xuICB2YXIgc3RhcnRQYXJ0ID0gMDtcbiAgdmFyIGVuZCA9IC0xO1xuICB2YXIgbWF0Y2hlZFNsYXNoID0gdHJ1ZTtcbiAgLy8gVHJhY2sgdGhlIHN0YXRlIG9mIGNoYXJhY3RlcnMgKGlmIGFueSkgd2Ugc2VlIGJlZm9yZSBvdXIgZmlyc3QgZG90IGFuZFxuICAvLyBhZnRlciBhbnkgcGF0aCBzZXBhcmF0b3Igd2UgZmluZFxuICB2YXIgcHJlRG90U3RhdGUgPSAwO1xuICBmb3IgKHZhciBpID0gcGF0aC5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgIHZhciBjb2RlID0gcGF0aC5jaGFyQ29kZUF0KGkpO1xuICAgIGlmIChjb2RlID09PSA0NyAvKi8qLykge1xuICAgICAgICAvLyBJZiB3ZSByZWFjaGVkIGEgcGF0aCBzZXBhcmF0b3IgdGhhdCB3YXMgbm90IHBhcnQgb2YgYSBzZXQgb2YgcGF0aFxuICAgICAgICAvLyBzZXBhcmF0b3JzIGF0IHRoZSBlbmQgb2YgdGhlIHN0cmluZywgc3RvcCBub3dcbiAgICAgICAgaWYgKCFtYXRjaGVkU2xhc2gpIHtcbiAgICAgICAgICBzdGFydFBhcnQgPSBpICsgMTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cbiAgICBpZiAoZW5kID09PSAtMSkge1xuICAgICAgLy8gV2Ugc2F3IHRoZSBmaXJzdCBub24tcGF0aCBzZXBhcmF0b3IsIG1hcmsgdGhpcyBhcyB0aGUgZW5kIG9mIG91clxuICAgICAgLy8gZXh0ZW5zaW9uXG4gICAgICBtYXRjaGVkU2xhc2ggPSBmYWxzZTtcbiAgICAgIGVuZCA9IGkgKyAxO1xuICAgIH1cbiAgICBpZiAoY29kZSA9PT0gNDYgLyouKi8pIHtcbiAgICAgICAgLy8gSWYgdGhpcyBpcyBvdXIgZmlyc3QgZG90LCBtYXJrIGl0IGFzIHRoZSBzdGFydCBvZiBvdXIgZXh0ZW5zaW9uXG4gICAgICAgIGlmIChzdGFydERvdCA9PT0gLTEpXG4gICAgICAgICAgc3RhcnREb3QgPSBpO1xuICAgICAgICBlbHNlIGlmIChwcmVEb3RTdGF0ZSAhPT0gMSlcbiAgICAgICAgICBwcmVEb3RTdGF0ZSA9IDE7XG4gICAgfSBlbHNlIGlmIChzdGFydERvdCAhPT0gLTEpIHtcbiAgICAgIC8vIFdlIHNhdyBhIG5vbi1kb3QgYW5kIG5vbi1wYXRoIHNlcGFyYXRvciBiZWZvcmUgb3VyIGRvdCwgc28gd2Ugc2hvdWxkXG4gICAgICAvLyBoYXZlIGEgZ29vZCBjaGFuY2UgYXQgaGF2aW5nIGEgbm9uLWVtcHR5IGV4dGVuc2lvblxuICAgICAgcHJlRG90U3RhdGUgPSAtMTtcbiAgICB9XG4gIH1cblxuICBpZiAoc3RhcnREb3QgPT09IC0xIHx8IGVuZCA9PT0gLTEgfHxcbiAgICAgIC8vIFdlIHNhdyBhIG5vbi1kb3QgY2hhcmFjdGVyIGltbWVkaWF0ZWx5IGJlZm9yZSB0aGUgZG90XG4gICAgICBwcmVEb3RTdGF0ZSA9PT0gMCB8fFxuICAgICAgLy8gVGhlIChyaWdodC1tb3N0KSB0cmltbWVkIHBhdGggY29tcG9uZW50IGlzIGV4YWN0bHkgJy4uJ1xuICAgICAgcHJlRG90U3RhdGUgPT09IDEgJiYgc3RhcnREb3QgPT09IGVuZCAtIDEgJiYgc3RhcnREb3QgPT09IHN0YXJ0UGFydCArIDEpIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cbiAgcmV0dXJuIHBhdGguc2xpY2Uoc3RhcnREb3QsIGVuZCk7XG59O1xuXG5mdW5jdGlvbiBmaWx0ZXIgKHhzLCBmKSB7XG4gICAgaWYgKHhzLmZpbHRlcikgcmV0dXJuIHhzLmZpbHRlcihmKTtcbiAgICB2YXIgcmVzID0gW107XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB4cy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAoZih4c1tpXSwgaSwgeHMpKSByZXMucHVzaCh4c1tpXSk7XG4gICAgfVxuICAgIHJldHVybiByZXM7XG59XG5cbi8vIFN0cmluZy5wcm90b3R5cGUuc3Vic3RyIC0gbmVnYXRpdmUgaW5kZXggZG9uJ3Qgd29yayBpbiBJRThcbnZhciBzdWJzdHIgPSAnYWInLnN1YnN0cigtMSkgPT09ICdiJ1xuICAgID8gZnVuY3Rpb24gKHN0ciwgc3RhcnQsIGxlbikgeyByZXR1cm4gc3RyLnN1YnN0cihzdGFydCwgbGVuKSB9XG4gICAgOiBmdW5jdGlvbiAoc3RyLCBzdGFydCwgbGVuKSB7XG4gICAgICAgIGlmIChzdGFydCA8IDApIHN0YXJ0ID0gc3RyLmxlbmd0aCArIHN0YXJ0O1xuICAgICAgICByZXR1cm4gc3RyLnN1YnN0cihzdGFydCwgbGVuKTtcbiAgICB9XG47XG4iLCIvLyBzaGltIGZvciB1c2luZyBwcm9jZXNzIGluIGJyb3dzZXJcbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxuLy8gY2FjaGVkIGZyb20gd2hhdGV2ZXIgZ2xvYmFsIGlzIHByZXNlbnQgc28gdGhhdCB0ZXN0IHJ1bm5lcnMgdGhhdCBzdHViIGl0XG4vLyBkb24ndCBicmVhayB0aGluZ3MuICBCdXQgd2UgbmVlZCB0byB3cmFwIGl0IGluIGEgdHJ5IGNhdGNoIGluIGNhc2UgaXQgaXNcbi8vIHdyYXBwZWQgaW4gc3RyaWN0IG1vZGUgY29kZSB3aGljaCBkb2Vzbid0IGRlZmluZSBhbnkgZ2xvYmFscy4gIEl0J3MgaW5zaWRlIGFcbi8vIGZ1bmN0aW9uIGJlY2F1c2UgdHJ5L2NhdGNoZXMgZGVvcHRpbWl6ZSBpbiBjZXJ0YWluIGVuZ2luZXMuXG5cbnZhciBjYWNoZWRTZXRUaW1lb3V0O1xudmFyIGNhY2hlZENsZWFyVGltZW91dDtcblxuZnVuY3Rpb24gZGVmYXVsdFNldFRpbW91dCgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3NldFRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbmZ1bmN0aW9uIGRlZmF1bHRDbGVhclRpbWVvdXQgKCkge1xuICAgIHRocm93IG5ldyBFcnJvcignY2xlYXJUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG4oZnVuY3Rpb24gKCkge1xuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2Ygc2V0VGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2YgY2xlYXJUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgIH1cbn0gKCkpXG5mdW5jdGlvbiBydW5UaW1lb3V0KGZ1bikge1xuICAgIGlmIChjYWNoZWRTZXRUaW1lb3V0ID09PSBzZXRUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICAvLyBpZiBzZXRUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkU2V0VGltZW91dCA9PT0gZGVmYXVsdFNldFRpbW91dCB8fCAhY2FjaGVkU2V0VGltZW91dCkgJiYgc2V0VGltZW91dCkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sIDApO1xuICAgIH0gY2F0Y2goZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwobnVsbCwgZnVuLCAwKTtcbiAgICAgICAgfSBjYXRjaChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yXG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKHRoaXMsIGZ1biwgMCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxufVxuZnVuY3Rpb24gcnVuQ2xlYXJUaW1lb3V0KG1hcmtlcikge1xuICAgIGlmIChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGNsZWFyVGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICAvLyBpZiBjbGVhclRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGRlZmF1bHRDbGVhclRpbWVvdXQgfHwgIWNhY2hlZENsZWFyVGltZW91dCkgJiYgY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCAgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbChudWxsLCBtYXJrZXIpO1xuICAgICAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yLlxuICAgICAgICAgICAgLy8gU29tZSB2ZXJzaW9ucyBvZiBJLkUuIGhhdmUgZGlmZmVyZW50IHJ1bGVzIGZvciBjbGVhclRpbWVvdXQgdnMgc2V0VGltZW91dFxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKHRoaXMsIG1hcmtlcik7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG59XG52YXIgcXVldWUgPSBbXTtcbnZhciBkcmFpbmluZyA9IGZhbHNlO1xudmFyIGN1cnJlbnRRdWV1ZTtcbnZhciBxdWV1ZUluZGV4ID0gLTE7XG5cbmZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcbiAgICBpZiAoIWRyYWluaW5nIHx8ICFjdXJyZW50UXVldWUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGlmIChjdXJyZW50UXVldWUubGVuZ3RoKSB7XG4gICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgIH1cbiAgICBpZiAocXVldWUubGVuZ3RoKSB7XG4gICAgICAgIGRyYWluUXVldWUoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XG4gICAgaWYgKGRyYWluaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRpbWVvdXQgPSBydW5UaW1lb3V0KGNsZWFuVXBOZXh0VGljayk7XG4gICAgZHJhaW5pbmcgPSB0cnVlO1xuXG4gICAgdmFyIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB3aGlsZShsZW4pIHtcbiAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XG4gICAgICAgIHF1ZXVlID0gW107XG4gICAgICAgIHdoaWxlICgrK3F1ZXVlSW5kZXggPCBsZW4pIHtcbiAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UXVldWVbcXVldWVJbmRleF0ucnVuKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgICAgICBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgfVxuICAgIGN1cnJlbnRRdWV1ZSA9IG51bGw7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBydW5DbGVhclRpbWVvdXQodGltZW91dCk7XG59XG5cbnByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XG4gICAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCAtIDEpO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcXVldWUucHVzaChuZXcgSXRlbShmdW4sIGFyZ3MpKTtcbiAgICBpZiAocXVldWUubGVuZ3RoID09PSAxICYmICFkcmFpbmluZykge1xuICAgICAgICBydW5UaW1lb3V0KGRyYWluUXVldWUpO1xuICAgIH1cbn07XG5cbi8vIHY4IGxpa2VzIHByZWRpY3RpYmxlIG9iamVjdHNcbmZ1bmN0aW9uIEl0ZW0oZnVuLCBhcnJheSkge1xuICAgIHRoaXMuZnVuID0gZnVuO1xuICAgIHRoaXMuYXJyYXkgPSBhcnJheTtcbn1cbkl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmZ1bi5hcHBseShudWxsLCB0aGlzLmFycmF5KTtcbn07XG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcbnByb2Nlc3MudmVyc2lvbiA9ICcnOyAvLyBlbXB0eSBzdHJpbmcgdG8gYXZvaWQgcmVnZXhwIGlzc3Vlc1xucHJvY2Vzcy52ZXJzaW9ucyA9IHt9O1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRPbmNlTGlzdGVuZXIgPSBub29wO1xuXG5wcm9jZXNzLmxpc3RlbmVycyA9IGZ1bmN0aW9uIChuYW1lKSB7IHJldHVybiBbXSB9XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcblxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5wcm9jZXNzLnVtYXNrID0gZnVuY3Rpb24oKSB7IHJldHVybiAwOyB9O1xuIiwiaW1wb3J0IHsgSW5zdGFHcmFtbWFyIH0gZnJvbSAnLi9pZy9lbmdpbmUnO1xuaW1wb3J0IHsgSW5zdGFHcmFtbWFyVGVtcGxhdGUgfSBmcm9tICcuL2lndC9lbmdpbmUnO1xuXG5leHBvcnQgeyBJbnN0YUdyYW1tYXIsIEluc3RhR3JhbW1hclRlbXBsYXRlIH07XG4iLCIvKlxuICAgIEluc3RhR3JhbW1hciBtYWluIGNsYXNzIFxuKi9cblxuaW1wb3J0IHsgUHJlUHJvY2Vzc29yIH0gZnJvbSAnLi4vcHAvZW5naW5lJztcbmltcG9ydCB7IHJ1bGVzIH0gZnJvbSBcIi4uL3BwL3J1bGVzXCI7XG5pbXBvcnQgeyBsZXhlciB9IGZyb20gXCIuLi9wcC9sZXhlci5qc1wiO1xuaW1wb3J0IHsgcGFyc2UgYXMgaW5zdGFHcmFtbWFyUGFyc2UgfSBmcm9tIFwiLi9wYXJzZXIuanNcIjtcbmltcG9ydCB7IE5vZGUgfSBmcm9tIFwiLi9ub2RlXCI7XG5pbXBvcnQgeyBFbnZpcm9ubWVudCB9IGZyb20gXCIuL2VudlwiO1xuXG5cbi8qXG4gICAgSW5zdGFHcmFtbWFyIGlzIHRoZSBtYWluIGNsYXNzLlxuKi9cbmNsYXNzIEluc3RhR3JhbW1hciB7XG4gICAgcHJpdmF0ZSByZWFkb25seSBwcmVQcm9jZXNzb3I6IFByZVByb2Nlc3NvcjtcbiAgICByZWFkb25seSBpbnB1dDogc3RyaW5nO1xuICAgIHJlYWRvbmx5IGNvbW1lbnRzUHJlUHJvY2Vzc2VkSW5wdXQ6IHN0cmluZztcbiAgICByZWFkb25seSBwcmVQcm9jZXNzZWRJbnB1dDogc3RyaW5nO1xuICAgIHJlYWRvbmx5IGFzdDogYW55O1xuICAgIHJlYWRvbmx5IGJpbmRpbmdzOiB7IFtpZDogc3RyaW5nXSA6IE5vZGU7IH07XG4gICAgZW52OiBFbnZpcm9ubWVudDtcblxuICAgIC8qXG4gICAgICAgIFRoZSBjb25zdHJ1Y3RvciBpcyBhIGxlbmd0aHkgb3BlcmF0aW9uLCBzaW5jZSBpdCBoYXMgdG86XG4gICAgICAgICAgICAtIHByZXByb2Nlc3MgdGhlIGdyYW1tYXIgKHN0cmluZyA9PiBzdHJpbmcpXG4gICAgICAgICAgICAtIHBhcnNlIHRoZSBwcmVwcm9jZXNzZWQgZ3JhbW1hciAoc3RyaW5nID0+IGFzdClcbiAgICAgICAgICAgIC0gY29udmVydCB0aGUgYXN0IGluIGRpY3Rpb25hcnkgb2YgbmFtZWQgbm9kZXMgKGJpbmRpbmdzKSAoYXN0ID0+IGJpbmRpbmdzKVxuXG4gICAgICAgIEFsbCB0aGVzZSBpbnRlcm1lZGlhdGUgc3RydWN0dXJlcyBhcmUga2VwdCBpbiB0aGUgb2JqZWN0IGZvciBkZWJ1Z2dpbmcgcHVycG9zZXMuXG4gICAgICAgIFRoaXMgbWV0aG9kIGNhbiB0aHJvdyBhIG51bWJlciBvZiBwYXJzaW5nL2xleGluZyBleGNlcHRpb24sIHNvIGl0IG11c3QgYmUgY2FsbGVkIGluXG4gICAgICAgIGEgdHJ5LWNhdGNoIGJsb2NrLlxuICAgICovXG4gICAgY29uc3RydWN0b3IoaW5wdXQ6IHN0cmluZykge1xuICAgICAgICB0aGlzLnByZVByb2Nlc3NvciA9IG5ldyBQcmVQcm9jZXNzb3IocnVsZXMsIGxleGVyLCAnU1RBUlQnKTtcbiAgICAgICAgdGhpcy5pbnB1dCA9IGlucHV0O1xuICAgICAgICBjb25zdCByZWdleHAgPSAvXFwvXFwqKC58XFxuKSo/XFwqXFwvfFxcL1xcLy4qJC9nbTtcbiAgICAgICAgdGhpcy5jb21tZW50c1ByZVByb2Nlc3NlZElucHV0ID0gdGhpcy5pbnB1dC5yZXBsYWNlKHJlZ2V4cCwgJycpO1xuICAgICAgICB0aGlzLnByZVByb2Nlc3NlZElucHV0ID0gdGhpcy5wcmVQcm9jZXNzb3IucHJvY2Vzcyh0aGlzLmNvbW1lbnRzUHJlUHJvY2Vzc2VkSW5wdXQpO1xuICAgICAgICB0aGlzLmFzdCA9IGluc3RhR3JhbW1hclBhcnNlKHRoaXMucHJlUHJvY2Vzc2VkSW5wdXQpO1xuICAgICAgICBsZXQgYmluZGluZ3M6IHsgW2lkOiBzdHJpbmddIDogTm9kZTsgfSA9IHt9O1xuICAgICAgICBpZiAoT2JqZWN0LmtleXModGhpcy5hc3QpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGZvciAobGV0IGJpbmRpbmcgb2YgdGhpcy5hc3QpIHtcbiAgICAgICAgICAgICAgICBiaW5kaW5nc1tiaW5kaW5nLm5hbWVdID0gTm9kZS5mYWN0b3J5KGJpbmRpbmcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuYmluZGluZ3MgPSBiaW5kaW5ncztcbiAgICB9XG5cbiAgICAvKlxuICAgICAgICBnZW5lcmF0ZSBpcyB0aGUgbWV0aG9kIHRvIGNhbGwgb24gYSBJbnN0YUdyYW1tYXIgb2JqZWN0LlxuICAgICAgICAgICAgLSBzdGFydFN5bWJvbCBpcyB0aGUgZ3JhbW1hciBlbnRydXkgcG9pbnRcbiAgICAgICAgICAgIC0ga2VlcFN0cm9uZ0JpbmRpbmdzIGlzIHVzZWQgd2hlbiB5b3UgYXJlIGdvaW5nIHRvIGNhbGwgZ2VuZXJhdGUgbXVsdGlwbGUgdGltZXMgb24gdGhlXG4gICAgICAgICAgICAgICAgc2FtZSBvYmplY3QgYW5kIHlvdSB3YW50IHRvIHJlLXVzZSBzdHJvbmdCaW5kaW5ncyAoQ0ZSID0+IEluc3RhR3JhbW1hclRlbXBsYXRlKVxuICAgICovXG4gICAgZ2VuZXJhdGUoc3RhcnRTeW1ib2wgPSAnJHN0YXJ0Jywga2VlcFN0cm9uZ0JpbmRpbmdzID0gZmFsc2UpOiBzdHJpbmcge1xuICAgICAgICBsZXQgc3Ryb25nQmluZGluZ3MgPSB7fTtcbiAgICAgICAgaWYgKGtlZXBTdHJvbmdCaW5kaW5ncyAmJiAodGhpcy5lbnYgIT0gbnVsbCkpIHtcbiAgICAgICAgICAgIHN0cm9uZ0JpbmRpbmdzID0gdGhpcy5lbnYuc3Ryb25nQmluZGluZ3M7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5lbnYgPSBuZXcgRW52aXJvbm1lbnQodGhpcy5iaW5kaW5ncywgc3Ryb25nQmluZGluZ3MsIHN0YXJ0U3ltYm9sKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW52LmdlbmVyYXRlKCk7XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgZ2VuZXJhdGUgaXMgYSBzdGF0aWMgY29udmVuaWVuY2UgbWV0aG9kIHVzZWZ1bCB3aGVuIHlvdSBuZWVkIGEgZ2VuZXJhdGlvbiBvbmx5IG9uY2UsXG4gICAgICAgIG9yIHdoZW4geW91IGRvbid0IGNhcmUgYWJvdXQgcGVyZm9ybWFuY2VzLlxuICAgICovXG4gICAgc3RhdGljIGdlbmVyYXRlKGlucHV0OiBzdHJpbmcsIHN0YXJ0U3ltYm9sID0gJyRzdGFydCcpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gKG5ldyBJbnN0YUdyYW1tYXIoaW5wdXQpKS5nZW5lcmF0ZShzdGFydFN5bWJvbCk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCB7IEluc3RhR3JhbW1hciB9O1xuIiwiLypcbiAgICBUaGUgRU5WIG9iamVjdHMgYXJlIGNyZWF0ZWQgZnJlc2ggZm9yIGVhY2ggSW5zdGFHcmFtbWFyIGdlbmVyYXRpb25cbiovXG5cbmltcG9ydCB7IE5vZGUsIEVtcHR5Tm9kZSB9IGZyb20gJy4vbm9kZSc7XG5cbi8qXG4gICAgRHVyaW5nIGdlbmVyYXRpb24sIGFsbCBvdXRwdXQgaXMgbWFuYWdlZCBieSB0b2tlbnNcbiAgICBJbiB0aGlzIHdheSwgXCJmdW5jdGlvblwiIHRva2VucyAoZS5nLiBTTkFQKSBjYW4gYmUgZm9sZGVkXG4qL1xuYWJzdHJhY3QgY2xhc3MgVG9rZW4ge1xuICAgIGFic3RyYWN0IHJlYWRvbmx5IGtpbmQ6IHN0cmluZztcbiAgICByZWFkb25seSBjb250ZW50cz86IHN0cmluZztcbn1cblxuXG5jbGFzcyBUZXh0VG9rZW4gZXh0ZW5kcyBUb2tlbiB7XG4gICAga2luZCA9ICdUZXh0VG9rZW4nO1xuICAgIHJlYWRvbmx5IGNvbnRlbnRzOiBzdHJpbmc7XG4gICAgY29uc3RydWN0b3IoY29udGVudHM6IHN0cmluZykge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICB0aGlzLmNvbnRlbnRzID0gY29udGVudHM7XG4gICAgfVxufVxuXG5cbmNsYXNzIFNuYXBUb2tlbiBleHRlbmRzIFRva2VuIHtcbiAgICBraW5kID0gJ1NuYXBUb2tlbic7XG59XG5cblxuY2xhc3MgVXBwZXJjYXNlVG9rZW4gZXh0ZW5kcyBUb2tlbiB7XG4gICAga2luZCA9ICdVcHBlcmNhc2UnO1xufVxuXG5cbi8qXG4gICAgRW52aXJvbm1lbnQgaXMgdGhlIGNvbXBhbmlvbiBvYmplY3QgZm9yIGVhY2ggSW5zdGFHcmFtbWFyIGdlbmVyYXRpb24uXG4gICAgSXRzIG1haW4gcHVycG9zZXMgYXJlOlxuICAgICAgICAtIGNvbGxlY3RzIG91dHB1dCB0b2tlbnNcbiAgICAgICAgLSBjb2xsZWN0IHN0cm9uZ0JpbmRpbmdzXG4gICAgICAgIC0gcmVjb3JkIHN5bWJvbCB2aXNpdHMgKGluIG9yZGVyIHRvIGF2b2lkIHJlY3Vyc2lvbiwgd2hpY2ggaXMgY2F1Z2h0IGF0IHJ1bnRpbWUpXG4gICAgICAgIC0gdHJhY2sgbm9kZSB3ZWlnaHQgZm9yIGF1dG9tYXRlZCB0ZXN0aW5nIHB1cnBvc2VzXG4qL1xuXG5jbGFzcyBFbnZpcm9ubWVudCB7XG4gICAgc3RhdGljIGtSZWN1cnNpb25UaHJlc2hvbGQ6IG51bWJlciA9IDEwMDtcbiAgICB0b2tlbnM6IEFycmF5PFRva2VuPjtcbiAgICBiaW5kaW5nczogeyBbaWQ6IHN0cmluZ10gOiBOb2RlOyB9O1xuICAgIHN0cm9uZ0JpbmRpbmdzOiB7IFtpZDogc3RyaW5nXSA6IEFycmF5PFRva2VuPjsgfTtcbiAgICBzdGFydFN5bWJvbDogc3RyaW5nO1xuICAgIG91dHB1dDogc3RyaW5nO1xuICAgIHZpc2l0czogeyBbaWQ6IHN0cmluZ10gOiBudW1iZXI7IH07XG4gICAgd2VpZ2h0czogQXJyYXk8bnVtYmVyPjsgLy8gdXNlZCBmb3IgYXV0b21hdGVkIHRlc3RpbmdcblxuICAgIHByZXR0eVdlaWdodHMoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMud2VpZ2h0cy5qb2luKCdfJyk7XG4gICAgfVxuXG4gICAgY29uc3RydWN0b3IoYmluZGluZ3M6IHsgW2lkOiBzdHJpbmddIDogTm9kZTsgfSwgc3Ryb25nQmluZGluZ3M6IHsgW2lkOiBzdHJpbmddIDogQXJyYXk8VG9rZW4+OyB9ID0ge30sIHN0YXJ0U3ltYm9sOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy50b2tlbnMgPSBuZXcgQXJyYXkoKTtcbiAgICAgICAgdGhpcy5iaW5kaW5ncyA9IGJpbmRpbmdzO1xuICAgICAgICB0aGlzLnN0cm9uZ0JpbmRpbmdzID0gc3Ryb25nQmluZGluZ3M7XG4gICAgICAgIHRoaXMuc3RhcnRTeW1ib2wgPSBzdGFydFN5bWJvbDtcbiAgICAgICAgdGhpcy5vdXRwdXQgPSBcIlwiO1xuICAgICAgICB0aGlzLnZpc2l0cyA9IHt9O1xuICAgICAgICB0aGlzLndlaWdodHMgPSBbXTtcbiAgICB9XG5cbiAgICAvKlxuICAgICAgICBsb29rdXBzIGEgYmluZGluZyBnaXZlbiBpdHMgbmFtZS5cbiAgICAgICAgSXQgcmFpc2VzIGFuIGV4Y2VwdGlvbiBpZiBub2RlIGlzIG5vZGUgZm91bmRcbiAgICAqL1xuICAgIGxvb2t1cEJpbmRpbmcobmFtZTogc3RyaW5nKTogTm9kZSB7XG5cbiAgICAgICAgLy8gRW1wdHkgZ3JhbW1hciwgd2UgdXNlIHRoZSBFbXB0eU5vZGUgdG8gaGF2ZSBhIHdvcmtpbmcgZ3JhbW1hciBhbnl3YXlcbiAgICAgICAgaWYgKE9iamVjdC5rZXlzKHRoaXMuYmluZGluZ3MpLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IEVtcHR5Tm9kZSh7fSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5iaW5kaW5nc1tuYW1lXSkge1xuICAgICAgICAgICAgaWYgKHRoaXMudmlzaXRzW25hbWVdKSB7XG4gICAgICAgICAgICAgICAgdGhpcy52aXNpdHNbbmFtZV0gKz0gMTtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy52aXNpdHNbbmFtZV0gPT0gRW52aXJvbm1lbnQua1JlY3Vyc2lvblRocmVzaG9sZCkge1xuICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYFBvc3NpYmlsZSByZWN1cnNpb24gZm91bmQgZm9yIGJpbmRpbmcgJHtuYW1lfWApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMudmlzaXRzW25hbWVdID0gMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmJpbmRpbmdzW25hbWVdO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBCaW5kaW5nIG5vdCBmb3VuZDogJHtuYW1lfWApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgY29udmVydHMgYW4gYXJyYXkgb2YgdG9rZW5zIGludG8gYSBzdHJpbmdcbiAgICAqL1xuICAgIHByZXR0eVRva2VucygpOiBzdHJpbmcge1xuICAgICAgICBsZXQgb3V0cHV0ID0gXCJcIjtcbiAgICAgICAgbGV0IHNuYXAgPSB0cnVlO1xuICAgICAgICBsZXQgdXBwZXJjYXNlID0gZmFsc2U7XG4gICAgICAgIGZvciAobGV0IHRva2VuIG9mIHRoaXMudG9rZW5zKXtcbiAgICAgICAgICAgIHN3aXRjaCAodG9rZW4ua2luZCkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ1RleHRUb2tlbic6IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNuYXApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNuYXAgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG91dHB1dCArPSBcIiBcIjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodXBwZXJjYXNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBvdXRwdXQgKz0gdG9rZW4uY29udGVudHMuY2hhckF0KDApLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBvdXRwdXQgKz0gdG9rZW4uY29udGVudHMuc2xpY2UoMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB1cHBlcmNhc2UgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG91dHB1dCArPSB0b2tlbi5jb250ZW50cztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2FzZSAnU25hcFRva2VuJzoge1xuICAgICAgICAgICAgICAgICAgICBzbmFwID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhc2UgJ1VwcGVyY2FzZSc6IHtcbiAgICAgICAgICAgICAgICAgICAgdXBwZXJjYXNlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBvdXRwdXQ7XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgR2VuZXJhdGUgaXMgdGhlIG1haW4gbWV0aG9kLlxuICAgICovXG4gICAgZ2VuZXJhdGUoKTogc3RyaW5nIHtcbiAgICAgICAgLy8gc3Ryb25nQmluZGluZyBmb3VuZDogbGV0J3MgcmUtdXNlIHRoZSBwcmV2aW91c2x5IGdlbmVyYXRlIHRva2Vuc1xuICAgICAgICBpZiAodGhpcy5zdHJvbmdCaW5kaW5nc1t0aGlzLnN0YXJ0U3ltYm9sXSAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLnRva2VucyA9IHRoaXMuc3Ryb25nQmluZGluZ3NbdGhpcy5zdGFydFN5bWJvbF07XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBsZXQgc3RhcnROb2RlID0gdGhpcy5sb29rdXBCaW5kaW5nKHRoaXMuc3RhcnRTeW1ib2wpO1xuICAgICAgICAgICAgdGhpcy50b2tlbnMgPSBzdGFydE5vZGUuZ2VuZXJhdGUodGhpcywgW10pO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub3V0cHV0ID0gdGhpcy5wcmV0dHlUb2tlbnMoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMub3V0cHV0O1xuICAgIH1cbn1cblxuXG4vKlxuICAgIFBydW5pbmdFbnZpcm9ubWVudCBpcyB1c2VkIHdoZW4gcHJ1bmluZyBncmFwaC5cbiAgICBJdHMgbWFpbiBwdXJwb3NlcyBhcmU6XG4gICAgICAgIC0gbG9va3VwIGJpbmRpbmcgd2l0aG91dCB0YWtpbmcgY2FyZSBvZiByZWN1cnNpb24gZGV0ZWN0aW9uXG4gICAgICAgIC0gY29sbGVjdCB3ZWlnaHQgZm9yIHRlc3RpbmcgcHVycG9zZXNcbiovXG5cbmNsYXNzIFBydW5pbmdFbnZpcm9ubWVudCB7XG4gICAgd2VpZ2h0czogQXJyYXk8bnVtYmVyPjtcbiAgICBiaW5kaW5nczogeyBbaWQ6IHN0cmluZ10gOiBOb2RlOyB9O1xuXG4gICAgY29uc3RydWN0b3IoYmluZGluZ3M6IHsgW2lkOiBzdHJpbmddIDogTm9kZTsgfSkge1xuICAgICAgICB0aGlzLndlaWdodHMgPSBbXTtcbiAgICAgICAgdGhpcy5iaW5kaW5ncyA9IGJpbmRpbmdzO1xuICAgIH1cblxuICAgIGxvb2t1cEJpbmRpbmcobmFtZTogc3RyaW5nKTogTm9kZSB7XG4gICAgICAgIGlmICh0aGlzLmJpbmRpbmdzW25hbWVdKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5iaW5kaW5nc1tuYW1lXTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgQmluZGluZyBub3QgZm91bmQ6ICR7bmFtZX1gKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuZXhwb3J0IHsgRW52aXJvbm1lbnQsIFBydW5pbmdFbnZpcm9ubWVudCwgVG9rZW4sIFVwcGVyY2FzZVRva2VuLCBTbmFwVG9rZW4sIFRleHRUb2tlbiB9O1xuIiwiLypcbiAgICBOb2RlLnRzIGNvbnRhaW5zIHRoZSB3aG9sZSBJbnN0YUdyYW1tYXIgbG9naWMuXG4qL1xuXG5pbXBvcnQgeyBFbnZpcm9ubWVudCwgUHJ1bmluZ0Vudmlyb25tZW50LCBUb2tlbiwgVXBwZXJjYXNlVG9rZW4sIFNuYXBUb2tlbiwgVGV4dFRva2VuIH0gZnJvbSAnLi9lbnYnO1xuXG5cbmZ1bmN0aW9uIHJhbmRvbUludEJldHdlZW4oYTogbnVtYmVyLCBiOiBudW1iZXIpOiBudW1iZXIge1xuICAgIHZhciBkaWZmID0gYiAtIGE7XG4gICAgcmV0dXJuIE1hdGguZmxvb3IgKChNYXRoLnJhbmRvbSgpICogKGRpZmYgKyAxKSkgKyBhKTtcbn1cblxuXG5lbnVtIE5vZGVUeXBlIHtcbiAgICBUZXh0ID0gJ3RleHQnLFxuICAgIFN5bWJvbCA9ICdzeW1ib2wnLFxuICAgIEJyYWNrZXQgPSAnYnJhY2tldCcsXG4gICAgQW5kID0gJ2FuZCcsXG4gICAgT3IgPSAnb3InLFxuICAgIEVtcHR5ID0gJ2VtcHR5J1xufVxuXG5cbi8qXG4gICAgTm9kZSBpcyBhIHN1cGVyIHR5cGUgd2l0aCBzb21lIGFic3RyYWN0IG1ldGhvZCB0aGF0IE1VU1QgYmUgaW1wbGVtZW50ZWQgYnkgc3ViY2xhc3Nlcy5cbiAgICBIeWVyYXJjaHk6XG4gICAgICAgIC0gTm9kZVxuICAgICAgICAgICAgLSBUZXh0XG4gICAgICAgICAgICAtIFN5bWJvbFxuICAgICAgICAgICAgLSBCcmFja2V0XG4gICAgICAgICAgICAtIEVtcHR5XG4gICAgICAgICAgICAtIENoaWxkcmVuXG4gICAgICAgICAgICAgICAgLSBBbmRcbiAgICAgICAgICAgICAgICAtIE9yXG5cbiAgICBBIG5vZGUgaXMgY3JlYXRlZCBmb3IgZWFjaCBBU1Qgb2JqZWN0LCB1c2luZyBjbGFzcyBtZXRob2QgTm9kZS5mYWN0b3J5XG4gICAgQWxsIG1lbWJlcnMgYXJlIHJlYWRvbmx5IChhcyB0byBwcmV2ZW50IGFjY2lkZW50YWwgbW9kaWZpY2F0aW9ucyBkdXJpbmcgdXNlKSxcbiAgICBleGNlcHQgZm9yIHVuZm9sZGVkIGFuZCBjb21wdXRlZFdlaWdodCB3aGljaCBhcmUgb25seSB1c2VkIGZvciBkZWJ1Z2dpbmcgYW5kXG4gICAgYXV0b21hdGVkIHRlc3RpbmcgcHVycG9zZXNcbiovXG5hYnN0cmFjdCBjbGFzcyBOb2RlIHtcbiAgICByZWFkb25seSBtaW5UaW1lcz86IG51bWJlcjtcbiAgICByZWFkb25seSBtYXhUaW1lcz86IG51bWJlcjtcbiAgICByZWFkb25seSBsZWZ0U25hcD86IGJvb2xlYW47XG4gICAgcmVhZG9ubHkgcmlnaHRTbmFwPzogYm9vbGVhbjtcbiAgICByZWFkb25seSBzdHJvbmdCaW5kaW5nPzogc3RyaW5nO1xuICAgIHJlYWRvbmx5IHNodWZmbGU/OiBib29sZWFuO1xuICAgIHJlYWRvbmx5IHVwcGVyY2FzZT86IGJvb2xlYW47XG4gICAgcmVhZG9ubHkgdGFncz86IEFycmF5PHN0cmluZz47XG4gICAgcmVhZG9ubHkgZmlsdGVycz86IEFycmF5PHN0cmluZz47XG4gICAgcmVhZG9ubHkgcmVzZXRGaWx0ZXJzPzogYm9vbGVhbjtcbiAgICByZWFkb25seSBsb2NhbFdlaWdodD86IG51bWJlcjtcbiAgICByZWFkb25seSB1bmZvbGQ/OiBib29sZWFuO1xuXG4gICAgY29tcHV0ZWRXZWlnaHQ/OiBudW1iZXI7XG4gICAgdW5mb2xkZWQ/OiBib29sZWFuO1xuXG4gICAgY29uc3RydWN0b3Iobm9kZUFzdDogYW55KSB7XG4gICAgICAgIGlmIChub2RlQXN0Lm1pblRpbWVzICE9IG51bGwpIHsgdGhpcy5taW5UaW1lcyA9IG5vZGVBc3QubWluVGltZXM7IH1cbiAgICAgICAgaWYgKG5vZGVBc3QubWF4VGltZXMgIT0gbnVsbCkgeyB0aGlzLm1heFRpbWVzID0gbm9kZUFzdC5tYXhUaW1lczsgfVxuICAgICAgICBpZiAobm9kZUFzdC5sZWZ0U25hcCAhPSBudWxsKSB7IHRoaXMubGVmdFNuYXAgPSBub2RlQXN0LmxlZnRTbmFwOyB9XG4gICAgICAgIGlmIChub2RlQXN0LnJpZ2h0U25hcCAhPSBudWxsKSB7IHRoaXMucmlnaHRTbmFwID0gbm9kZUFzdC5yaWdodFNuYXA7IH1cbiAgICAgICAgaWYgKG5vZGVBc3Quc3Ryb25nQmluZGluZyAhPSBudWxsKSB7IHRoaXMuc3Ryb25nQmluZGluZyA9IG5vZGVBc3Quc3Ryb25nQmluZGluZzsgfVxuICAgICAgICBpZiAobm9kZUFzdC5zaHVmZmxlICE9IG51bGwpIHsgdGhpcy5zaHVmZmxlID0gbm9kZUFzdC5zaHVmZmxlOyB9XG4gICAgICAgIGlmIChub2RlQXN0LnVwcGVyY2FzZSAhPSBudWxsKSB7IHRoaXMudXBwZXJjYXNlID0gbm9kZUFzdC51cHBlcmNhc2U7IH1cbiAgICAgICAgaWYgKG5vZGVBc3QudGFncyAhPSBudWxsKSB7IHRoaXMudGFncyA9IG5vZGVBc3QudGFnczsgfVxuICAgICAgICBpZiAobm9kZUFzdC5maWx0ZXJzICE9IG51bGwpIHsgdGhpcy5maWx0ZXJzID0gbm9kZUFzdC5maWx0ZXJzOyB9XG4gICAgICAgIGlmIChub2RlQXN0LnJlc2V0RmlsdGVycyAhPSBudWxsKSB7IHRoaXMucmVzZXRGaWx0ZXJzID0gbm9kZUFzdC5yZXNldEZpbHRlcnM7IH1cbiAgICAgICAgaWYgKG5vZGVBc3QubG9jYWxXZWlnaHQgIT0gbnVsbCkgeyB0aGlzLmxvY2FsV2VpZ2h0ID0gbm9kZUFzdC5sb2NhbFdlaWdodDsgfVxuICAgICAgICBpZiAobm9kZUFzdC51bmZvbGQgIT0gbnVsbCkgeyB0aGlzLnVuZm9sZCA9IG5vZGVBc3QudW5mb2xkOyB9XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgUmV0dXJucyB0aGUgbnVtYmVyIG9mIHRpbWUgdGhpcyBub2RlIG11c3QgYmUgZ2VuZXJhdGVkIChlczogTl4yID0+IDIpXG4gICAgKi9cbiAgICByYW5kb21UaW1lcygpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5taW5UaW1lcyAhPSBudWxsICYmIHRoaXMubWF4VGltZXMgIT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIHJhbmRvbUludEJldHdlZW4odGhpcy5taW5UaW1lcywgdGhpcy5tYXhUaW1lcyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gMTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qXG4gICAgICAgIFJldHVybnMgbm9kZSB3ZWlnaHQsIGNhbGxpbmcgYWJzdHJhY3QgbWV0aG9kIFwiY3VzdG9tV2VpZ2h0XCIgb24gc2VsZi5cbiAgICAgICAgVGhpcyBtZXRob2QgQ0FOTk9UIGJlIG92ZXJyaWRlbi4gSW5zdGVhZCwgb3ZlcnJpZGUgY3VzdG9tV2VpZ2h0LlxuICAgICAgICBRdWl0ZSBhIGNvbXBsaWNhdGVkIGxvZ2ljIGhhcyBiZWVuIGltcGxlbWVudGVkIGZvciB1bmZvbGRpbmcuXG4gICAgKi9cbiAgICB3ZWlnaHQocHJ1bmluZ0VudjogUHJ1bmluZ0Vudmlyb25tZW50LCBmaWx0ZXJzOiBBcnJheTxzdHJpbmc+LCB1bmZvbGQ6IGJvb2xlYW4gPSBmYWxzZSwgdmlzaXRzOiBBcnJheTxzdHJpbmc+KTogbnVtYmVyIHtcbiAgICAgICAgbGV0IHdlaWdodCA9IDA7XG4gICAgICAgIGZpbHRlcnMgPSB0aGlzLnVwZGF0ZUZpbHRlcnMoZmlsdGVycyk7XG4gICAgICAgIHVuZm9sZCA9IHVuZm9sZCB8fCB0aGlzLnVuZm9sZDtcbiAgICAgICAgaWYgKHVuZm9sZCkge1xuICAgICAgICAgICAgdGhpcy51bmZvbGRlZCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLmV4Y2x1ZGVkQnlGaWx0ZXJzKGZpbHRlcnMpKSB7XG4gICAgICAgICAgICB3ZWlnaHQgPSAxO1xuICAgICAgICAgICAgd2VpZ2h0ID0gdGhpcy5jdXN0b21XZWlnaHQocHJ1bmluZ0VudiwgZmlsdGVycywgdW5mb2xkLCB2aXNpdHMsIHdlaWdodCk7XG4gICAgICAgICAgICBpZiAodW5mb2xkID09IHRydWUgJiYgdGhpcy5taW5UaW1lcyAhPSBudWxsICYmIHRoaXMubWF4VGltZXMgIT0gbnVsbCAmJiB3ZWlnaHQgPiAwKSB7XG4gICAgICAgICAgICAgICAgLy8gbmVsIGNhc28gZGkgdW5hIHByb2R1emlvbmUgb3B6aW9uYWxlIHVuZm9sZGF0YSwgZXMgIVthIHwgYiB8IGNdLFxuICAgICAgICAgICAgICAgIC8vIGlsIHBlc28gbm9uIMOoIDMqMiBtYSAzKzEgcGVyY2jDqSBsYSBwcm9kdXppb25lIHZ1b3RhIMOoIHVuaWNhXG4gICAgICAgICAgICAgICAgLy8gaW5kaXBlbmRlbnRlbWVudGUgZGFsIHBlc28gZGVsbCdPUi5cbiAgICAgICAgICAgICAgICAvLyBJbnZlY2UgbmVsIGNhc28gZGkgdW5hIHByb2R1emlvbmUgaXRlcmF0YSB0aXBvIEFeMi00XG4gICAgICAgICAgICAgICAgLy8gaWwgcGVzbyDDqCBuKig0LTIrMSlcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5taW5UaW1lcyA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHdlaWdodCA9ICh3ZWlnaHQgKiAodGhpcy5tYXhUaW1lcyAtIHRoaXMubWluVGltZXMpKSArIDE7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB3ZWlnaHQgPSB3ZWlnaHQgKiAoKHRoaXMubWF4VGltZXMgLSB0aGlzLm1pblRpbWVzKSArIDEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBwcnVuaW5nRW52LndlaWdodHMucHVzaCh3ZWlnaHQpO1xuICAgICAgICB0aGlzLmNvbXB1dGVkV2VpZ2h0ID0gd2VpZ2h0O1xuICAgICAgICByZXR1cm4gd2VpZ2h0O1xuICAgIH1cblxuICAgIC8qXG4gICAgICAgIFRoaXMgbWV0aG9kIE1VU1QgYmUgaW1wbG1lbnRlZCBieSBzdWJjbGFzc2VzLCBhbmQgQ0FOTk9UIGJlIGNhbGxlZCBkaXJlY3RseS5cbiAgICAgICAgSWYgYSBzdWJjbGFzcyBuZWVkIHRvIGtub3cgdGhlIHdlaWdodCBvZiBhIGNoaWxkLCBpdCBoYXMgdG8gY2FsbCB3ZWlnaHQuXG4gICAgICAgIHN1cGVyV2VpZ2h0IGlzIHRoZSB3ZWlnaHQgY29tcHV0ZWQgYnkgbWV0aG9kIHdlaWdodDsgY3VzdG9tV2VpZ2h0IFNIT1VMRCB1c2UgaXRcbiAgICAqL1xuICAgIGFic3RyYWN0IGN1c3RvbVdlaWdodChwcnVuaW5nRW52OiBQcnVuaW5nRW52aXJvbm1lbnQsIGZpbHRlcnM6IEFycmF5PHN0cmluZz4sIHVuZm9sZDogYm9vbGVhbiwgdmlzaXRzOiBBcnJheTxzdHJpbmc+LCBzdXBlcldlaWdodDogbnVtYmVyKTogbnVtYmVyO1xuXG4gICAgLypcbiAgICAgICAgUmV0dXJucyBUUlVFIGlzIHRoZSBub2RlIGlzIGV4Y2x1ZGVkIGJ5IGN1cnJlbnQgZmlsdGVycy5cbiAgICAgICAgVXNlZCBkdXJpbmcgZ2VuZXJhdGlvbiBhbmQgd2hpbGUgcHJ1bmluZyBub2RlIGdyYXBoLlxuICAgICovXG4gICAgZXhjbHVkZWRCeUZpbHRlcnMoZmlsdGVyczogQXJyYXk8c3RyaW5nPik6IGJvb2xlYW4ge1xuICAgICAgICBpZiAodGhpcy50YWdzICE9IG51bGwpIHtcbiAgICAgICAgICAgIC8vIGlmIG5vZGUgaGFzIHRhZ3MsIGNoZWNrIGFsbCBmaWx0ZXJzIChpbiBBTkQpXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGBub2RlIGhhcyB0YWdzICR7dGhpcy50YWdzfWApO1xuICAgICAgICAgICAgZm9yIChsZXQgZmlsdGVyIG9mIGZpbHRlcnMpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YWdzLmluZGV4T2YoZmlsdGVyKSA9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGBmaWx0ZXIgbm90IGZvdW5kICR7ZmlsdGVyfSwgcmV0dXJuIHRydWVgKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhgYWxsIGZpbHRlcnMgZm91bmQsIHJldHVybiBmYWxzZWApO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgLy8gaWYgbm9kZSBoYXMgbm8gdGFncywgaXQgd29uJ3QgYmUgZXhjbHVkZWRcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qXG4gICAgICAgIFJldHVybnMgYSBORVcgYXJyYXkgb2YgZmlsdGVycyB1cGRhdGVkIHdpdGggbm9kZSBmaWx0ZXJzXG4gICAgKi9cbiAgICB1cGRhdGVGaWx0ZXJzKGZpbHRlcnM6IEFycmF5PHN0cmluZz4pOiBBcnJheTxzdHJpbmc+IHtcbiAgICAgICAgaWYgKHRoaXMucmVzZXRGaWx0ZXJzID09PSB0cnVlKSB7IHJldHVybiBbXTsgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmZpbHRlcnMgPT0gbnVsbCkgeyByZXR1cm4gWy4uLmZpbHRlcnNdOyB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbGV0IG5ld0ZpbHRlcnMgPSBbLi4uZmlsdGVyc107XG4gICAgICAgICAgICBmb3IgKGxldCBmaWx0ZXIgb2YgdGhpcy5maWx0ZXJzKSB7XG4gICAgICAgICAgICAgICAgaWYgKG5ld0ZpbHRlcnMuaW5kZXhPZihmaWx0ZXIpID09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIG5ld0ZpbHRlcnMucHVzaChmaWx0ZXIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBuZXdGaWx0ZXJzO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgUmV0dXJucyBhbiBhcnJheSBvZiB0b2tlbiBzdGFydGluZyBmcm9tIHNlbGYuXG4gICAgICAgIFRoaXMgaXMgdGhlIG1vc3QgaW1wb3J0YW50IG1ldGhvZDpcbiAgICAgICAgICAgIC0gaXQgaGFuZGxlcyB0aGUgY29tbW9uZyB0aGluZ3MgZXZlcnkgbm9kZSBoYXNcbiAgICAgICAgICAgIC0gaXQgdXNlcyB0aGUgYWJzdHJhY3QgbWV0aG9kIGN1c3RvbUdlbmVyYXRlIG9uIHN1YmNsYXNzZXNcbiAgICAgICAgICAgIC0gaXQgdXNlcyBhbmQgcHJvZ2F0YXRlIGVudiAodHJhc3ZlcnNhbCkgYW5kIGZpbHRlcnMgKHZlcnRpY2FsKVxuXG4gICAgKi9cbiAgICBnZW5lcmF0ZShlbnY6IEVudmlyb25tZW50LCBmaWx0ZXJzOiBBcnJheTxzdHJpbmc+KTogQXJyYXk8VG9rZW4+IHtcbiAgICAgICAgbGV0IHRva2VuczogQXJyYXk8VG9rZW4+ID0gW107XG4gICAgICAgIGZpbHRlcnMgPSB0aGlzLnVwZGF0ZUZpbHRlcnMoZmlsdGVycyk7XG5cbiAgICAgICAgaWYgKCF0aGlzLmV4Y2x1ZGVkQnlGaWx0ZXJzKGZpbHRlcnMpKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucmFuZG9tVGltZXMoKTsgaSsrKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudXBwZXJjYXNlKSB7IHRva2Vucy5wdXNoKG5ldyBVcHBlcmNhc2VUb2tlbigpKTsgfVxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmxlZnRTbmFwKSB7IHRva2Vucy5wdXNoKG5ldyBTbmFwVG9rZW4oKSk7IH1cbiAgICAgICAgICAgICAgICB0b2tlbnMgPSB0b2tlbnMuY29uY2F0ICh0aGlzLmN1c3RvbUdlbmVyYXRlKGVudiwgZmlsdGVycykpO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnJpZ2h0U25hcCkgeyB0b2tlbnMucHVzaChuZXcgU25hcFRva2VuKCkpOyB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBzYXZpbmcgc3Ryb25nQmluZGluZ1xuICAgICAgICAgICAgaWYgKHRoaXMuc3Ryb25nQmluZGluZyAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgZW52LnN0cm9uZ0JpbmRpbmdzW3RoaXMuc3Ryb25nQmluZGluZ10gPSB0b2tlbnM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRva2VucztcbiAgICB9XG5cbiAgICAvKlxuICAgICAgICBSZXR1cm5zIGFuZCBhcnJheSBvZiB0b2tlbnMuXG4gICAgICAgIFRoaXMgbWV0aG9kIE1VU1QgYmUgaW1wbGVtZW50ZWQgYnkgc3ViY2xhc3NlcywgYW5kIGlzIHdoZXJlIHRoZSBjdXN0b20gbG9naWMgaXMgd3JpdHRlblxuICAgICovXG4gICAgYWJzdHJhY3QgY3VzdG9tR2VuZXJhdGUoZW52OiBFbnZpcm9ubWVudCwgZmlsdGVyczogQXJyYXk8c3RyaW5nPik6IEFycmF5PFRva2VuPjtcblxuICAgIC8qXG4gICAgICAgIFN0YXRpYyBtZXRob2QgdG8gY3JlYXRlIGEgTm9kZSBnaXZlbiBhbiBvYmplY3QgZnJvbSB0aGUgcGFyc2UuXG4gICAgICAgIFNlZSBncmFtbWFyLmppc29uXG4gICAgKi9cbiAgICBzdGF0aWMgZmFjdG9yeShub2RlQXN0OiBhbnkpOiBOb2RlIHtcbiAgICAgICAgbGV0IG5vZGU6IE5vZGU7XG4gICAgICAgIHN3aXRjaCAobm9kZUFzdC50eXBlKSB7XG4gICAgICAgICAgICBjYXNlIE5vZGVUeXBlLlRleHQ6XG4gICAgICAgICAgICAgICAgbm9kZSA9IG5ldyBUZXh0Tm9kZShub2RlQXN0KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgTm9kZVR5cGUuU3ltYm9sOlxuICAgICAgICAgICAgICAgIG5vZGUgPSBuZXcgU3ltYm9sTm9kZShub2RlQXN0KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgTm9kZVR5cGUuQnJhY2tldDpcbiAgICAgICAgICAgICAgICBub2RlID0gbmV3IEJyYWNrZXROb2RlKG5vZGVBc3QpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBOb2RlVHlwZS5BbmQ6XG4gICAgICAgICAgICAgICAgbm9kZSA9IG5ldyBBbmROb2RlKG5vZGVBc3QpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBOb2RlVHlwZS5PcjpcbiAgICAgICAgICAgICAgICBub2RlID0gbmV3IE9yTm9kZShub2RlQXN0KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgTm9kZVR5cGUuRW1wdHk6XG4gICAgICAgICAgICAgICAgbm9kZSA9IG5ldyBFbXB0eU5vZGUobm9kZUFzdCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgQ2Fubm90IGNyZWF0ZSBub2RlIGZvciB0eXBlICR7bm9kZUFzdC50eXBlfWApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBub2RlO1xuICAgIH1cbn1cblxuXG4vKlxuICAgIExlYWYgbm9kZVxuKi9cbmNsYXNzIFRleHROb2RlIGV4dGVuZHMgTm9kZSB7XG4gICAgcmVhZG9ubHkgY29udGVudHM6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKG5vZGVBc3Q6IGFueSkge1xuICAgICAgICBzdXBlcihub2RlQXN0KTtcbiAgICAgICAgdGhpcy5jb250ZW50cyA9IG5vZGVBc3QuY29udGVudHM7XG4gICAgfVxuXG4gICAgY3VzdG9tV2VpZ2h0KHBydW5pbmdFbnY6IFBydW5pbmdFbnZpcm9ubWVudCwgZmlsdGVyczogQXJyYXk8c3RyaW5nPiwgdW5mb2xkOiBib29sZWFuLCB2aXNpdHM6IEFycmF5PHN0cmluZz4sIHN1cGVyV2VpZ2h0OiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gc3VwZXJXZWlnaHQ7XG4gICAgfVxuXG4gICAgY3VzdG9tR2VuZXJhdGUoZW52OiBFbnZpcm9ubWVudCkge1xuICAgICAgICByZXR1cm5bbmV3IFRleHRUb2tlbih0aGlzLmNvbnRlbnRzKV07XG4gICAgfVxufVxuXG5cbi8qXG4gICAgQSBTeW1ib2wgbm9kZSBoYXMgYSB0YXJnZXQgd2hpY2ggaXMgdGhlIG5hbWUgb2YgYSBiaW5kaW5nXG4qL1xuY2xhc3MgU3ltYm9sTm9kZSBleHRlbmRzIE5vZGUge1xuICAgIHJlYWRvbmx5IHRhcmdldDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3Iobm9kZUFzdDogYW55KSB7XG4gICAgICAgIHN1cGVyKG5vZGVBc3QpO1xuICAgICAgICB0aGlzLnRhcmdldCA9IG5vZGVBc3QudGFyZ2V0O1xuICAgIH1cblxuICAgIGN1c3RvbVdlaWdodChwcnVuaW5nRW52OiBQcnVuaW5nRW52aXJvbm1lbnQsIGZpbHRlcnM6IEFycmF5PHN0cmluZz4sIHVuZm9sZDogYm9vbGVhbiwgdmlzaXRzOiBBcnJheTxzdHJpbmc+LCBzdXBlcldlaWdodDogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgbGV0IGNoaWxkV2VpZ2h0ID0gMTtcbiAgICAgICAgLy8gbm9kZSBhbHJlYWR5IHZpc2l0ZWQuIExldCdzIGdldCBiYWNrXG4gICAgICAgIGlmICh2aXNpdHMuaW5kZXhPZih0aGlzLnRhcmdldCkgPiAtMSkge1xuICAgICAgICAgICAgLy8gbGV0J3MgYXZvaWQgcmVjdXJzaW9uXG4gICAgICAgICAgICAvLyBUT0RPOiBhcmUgd2Ugc3VyZSBpdCdzIGEgZ29vZCB0aGluZz8gVWhtLi4uXG4gICAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgfVxuICAgICAgICB2aXNpdHMucHVzaCh0aGlzLnRhcmdldCk7XG4gICAgICAgIC8qXG4gICAgICAgICAgICBjaGlsZC9jaGlsZHJlbiB3ZWlnaHQgaXMgb25seSBpbnRlcmVzdGluZyBpZiB1bmZvbGQsIG9yIGlmIHRoZXJlIGFyZSBmaWx0ZXJzLCBiZWNhdXNlXG4gICAgICAgICAgICB3ZSB3YW50IHRvIGRlZXAtcHJ1bmUgdGhlIGdyYXBoXG4gICAgICAgICovXG4gICAgICAgIGlmICh1bmZvbGQgfHwgZmlsdGVycy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBsZXQgY2hpbGROb2RlID0gcHJ1bmluZ0Vudi5sb29rdXBCaW5kaW5nKHRoaXMudGFyZ2V0KTtcbiAgICAgICAgICAgIGNoaWxkV2VpZ2h0ID0gY2hpbGROb2RlLndlaWdodChwcnVuaW5nRW52LCBmaWx0ZXJzLCB1bmZvbGQsIHZpc2l0cyk7XG4gICAgICAgICAgICBzdXBlcldlaWdodCAqPSBjaGlsZFdlaWdodDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBzdXBlcldlaWdodDtcbiAgICB9XG5cbiAgICBjdXN0b21HZW5lcmF0ZShlbnY6IEVudmlyb25tZW50LCBmaWx0ZXJzOiBBcnJheTxzdHJpbmc+KTogQXJyYXk8VG9rZW4+IHtcbiAgICAgICAgLy8gdG9rZW5zIGFscmVhZHkgZ2VuZXJhdGVkIGFuZCBzYXZlZCBpbiBhIHN0cm9uZ2JpbmRpbmdcbiAgICAgICAgaWYgKGVudi5zdHJvbmdCaW5kaW5nc1t0aGlzLnRhcmdldF0pIHtcbiAgICAgICAgICAgIHJldHVybiBlbnYuc3Ryb25nQmluZGluZ3NbdGhpcy50YXJnZXRdO1xuICAgICAgICB9XG4gICAgICAgIGxldCBub2RlID0gZW52Lmxvb2t1cEJpbmRpbmcodGhpcy50YXJnZXQpO1xuICAgICAgICBsZXQgdG9rZW5zID0gbm9kZS5nZW5lcmF0ZShlbnYsIGZpbHRlcnMpO1xuICAgICAgICByZXR1cm4gdG9rZW5zO1xuICAgIH1cbn1cblxuLypcbiAgICBCcmFja2V0Tm9kZSBoYXMgZXhhY3RseSBvbmUgY2hpbGQuXG4gICAgTm90aWNlOiBpdCBpcyB1c2VkIGZvciBhbnkga2luZCBvZiBicmFja2V0IVxuICAgICAgICAtIChhIHwgYilcbiAgICAgICAgLSB7YSB8IGJ9XG4gICAgICAgIC0gW2EgfCBiXVxuKi9cbmNsYXNzIEJyYWNrZXROb2RlIGV4dGVuZHMgTm9kZSB7XG4gICAgcmVhZG9ubHkgY2hpbGQ6IE5vZGU7XG5cbiAgICBjb25zdHJ1Y3Rvcihub2RlQXN0OiBhbnkpIHtcbiAgICAgICAgc3VwZXIobm9kZUFzdCk7XG4gICAgICAgIHRoaXMuY2hpbGQgPSBOb2RlLmZhY3Rvcnkobm9kZUFzdC5jaGlsZCk7XG4gICAgfVxuXG4gICAgY3VzdG9tV2VpZ2h0KHBydW5pbmdFbnY6IFBydW5pbmdFbnZpcm9ubWVudCwgZmlsdGVyczogQXJyYXk8c3RyaW5nPiwgdW5mb2xkOiBib29sZWFuLCB2aXNpdHM6IEFycmF5PHN0cmluZz4sIHN1cGVyV2VpZ2h0OiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICAvKlxuICAgICAgICAgICAgY2hpbGQvY2hpbGRyZW4gd2VpZ2h0IGlzIG9ubHkgaW50ZXJlc3RpbmcgaWYgdW5mb2xkLCBvciBpZiB0aGVyZSBhcmUgZmlsdGVycywgYmVjYXVzZVxuICAgICAgICAgICAgd2Ugd2FudCB0byBkZWVwLXBydW5lIHRoZSBncmFwaFxuICAgICAgICAqL1xuICAgICAgICBpZiAodW5mb2xkIHx8IGZpbHRlcnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbGV0IGNoaWxkV2VpZ2h0ID0gdGhpcy5jaGlsZC53ZWlnaHQocHJ1bmluZ0VudiwgZmlsdGVycywgdW5mb2xkLCB2aXNpdHMpO1xuICAgICAgICAgICAgc3VwZXJXZWlnaHQgKj0gY2hpbGRXZWlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHN1cGVyV2VpZ2h0O1xuICAgIH1cblxuICAgIGN1c3RvbUdlbmVyYXRlKGVudjogRW52aXJvbm1lbnQsIGZpbHRlcnM6IEFycmF5PHN0cmluZz4pOiBBcnJheTxUb2tlbj4ge1xuICAgICAgICByZXR1cm4gKHRoaXMuY2hpbGQuZ2VuZXJhdGUoZW52LCBmaWx0ZXJzKSk7XG4gICAgfVxufVxuXG5cbi8qXG4gICAgQ2hpbGRyZW4gaGFzIGFuIGFycmF5IG9mIGNoaWxkcmVuIG5vZGUuXG4gICAgVGhpcyBjbGFzcyBpcyBuZXZlciB1c2VkIGRpcmVjdGx5LCBpbnN0ZWFkIHVzZSBzdWJjbGFzc2VzIEFuZCBhbmQgT3JcbiovXG5hYnN0cmFjdCBjbGFzcyBDaGlsZHJlbk5vZGUgZXh0ZW5kcyBOb2RlIHtcbiAgICByZWFkb25seSBjaGlsZHJlbjogQXJyYXk8Tm9kZT47XG5cbiAgICBjb25zdHJ1Y3Rvcihub2RlQXN0OiBhbnkpIHtcbiAgICAgICAgc3VwZXIobm9kZUFzdCk7XG4gICAgICAgIGxldCBjaGlsZHJlbjogQXJyYXk8Tm9kZT4gPSBuZXcgQXJyYXkoKTtcbiAgICAgICAgZm9yIChsZXQgY2hpbGROb2RlQXN0IG9mIG5vZGVBc3QuY2hpbGRyZW4pIHtcbiAgICAgICAgICAgIGNoaWxkcmVuLnB1c2goTm9kZS5mYWN0b3J5KGNoaWxkTm9kZUFzdCkpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2hpbGRyZW4gPSBjaGlsZHJlbjtcbiAgICB9XG59XG5cblxuLypcbiAgICBBbmROb2RlLiBEdXJpbmcgZ2VuZXJhdGlvbiwgYWxsIG9mIHRoZSBjaGlsZHJlbiBhcmUgZ2VuZXJhdGVkXG4qL1xuY2xhc3MgQW5kTm9kZSBleHRlbmRzIENoaWxkcmVuTm9kZSB7XG4gICAgY3VzdG9tV2VpZ2h0KHBydW5pbmdFbnY6IFBydW5pbmdFbnZpcm9ubWVudCwgZmlsdGVyczogQXJyYXk8c3RyaW5nPiwgdW5mb2xkOiBib29sZWFuLCB2aXNpdHM6IEFycmF5PHN0cmluZz4sIHN1cGVyV2VpZ2h0OiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICBsZXQgY2hpbGRyZW5XZWlnaHQgPSAwO1xuICAgICAgICBsZXQgc2h1ZmZsZU5vZGVzTnVtYmVyID0gMDsgLy8gYWxsIG5vZGUgY2FuIGhhdmUgd2VpZ2h0ID0gMFxuICAgICAgICBmb3IgKGxldCBjaGlsZCBvZiB0aGlzLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgICEoYSB7Yn0ge2N9IFtjXSkgID0+XG4gICAgICAgICAgICAgICAxICAxICAgMSAgIDEgID0+ICgxICogMSkgKiAgICgobiAqIChuIC0xKSkgKiAxICogMSlcbiAgICAgICAgICAgICAhKGEge2IgfCBnfSB7Y30gKGEgfCBiKSlcbiAgICAgICAgICAgICAgIDEgICAgMSAgIDEgICAgIDEgID0+ICgxICogMSkgKiAoKG4gKiAobiAtMSkgKiAxICogMSkpXG4gICAgICAgICAgICAgIShhICF7YiB8IGd9IHtjfSAhKGEgfCBiIHwgYykpXG4gICAgICAgICAgICAgICAxICAgMSAgIDEgICAxICAgIDEgICAxICAgMVxuICAgICAgICAgICAgICAgMSAgICAgMiAgICAgMSAgICAgICAgM1xuICAgICAgICAgICAgICAxICogMyAgKiAoKG4gKiAobiAtMSkpICogMiAgKiAxXG4gICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICBsZXQgY2hpbGRXZWlnaHQgPSBjaGlsZC53ZWlnaHQocHJ1bmluZ0VudiwgZmlsdGVycywgZmFsc2UsIHZpc2l0cyk7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGBjaGlsZCB3ZWlnaHQgaXMgJHtjaGlsZFdlaWdodH1gKTtcbiAgICAgICAgICAgIGlmIChjaGlsZFdlaWdodCA+IDApIHtcbiAgICAgICAgICAgICAgICAvLyBpbiBjYXNlIGFsbCBjaGlsZHJlbiB3ZWlnaHQgaXMgMFxuICAgICAgICAgICAgICAgIGlmIChjaGlsZHJlbldlaWdodCA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGNoaWxkcmVuV2VpZ2h0ID0gMTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2hpbGRyZW5XZWlnaHQgKj0gY2hpbGRXZWlnaHQ7XG4gICAgICAgICAgICAgICAgaWYgKHVuZm9sZCAmJiBjaGlsZC5zaHVmZmxlKSB7XG4gICAgICAgICAgICAgICAgICAgIHNodWZmbGVOb2Rlc051bWJlcisrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHdpdGggbW9yZSB0aGFuIDEgc2h1ZmZsZSBub2RlLCBudW1iZXIgb2YgcGVybXV0YXRpb25zIGlzIG4gKiAobiAtIDEpXG4gICAgICAgIGlmIChzaHVmZmxlTm9kZXNOdW1iZXIgPiAxKSB7XG4gICAgICAgICAgICBjaGlsZHJlbldlaWdodCAqPSAoc2h1ZmZsZU5vZGVzTnVtYmVyICogKHNodWZmbGVOb2Rlc051bWJlciAtIDEpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHN1cGVyV2VpZ2h0ICo9IGNoaWxkcmVuV2VpZ2h0O1xuICAgICAgICByZXR1cm4gc3VwZXJXZWlnaHQ7XG4gICAgfVxuXG4gICAgY3VzdG9tR2VuZXJhdGUoZW52OiBFbnZpcm9ubWVudCwgZmlsdGVyczogQXJyYXk8c3RyaW5nPik6IEFycmF5PFRva2VuPiB7XG4gICAgICAgIGxldCB0b2tlbnM6IEFycmF5PFRva2VuPiA9IFtdO1xuICAgICAgICBsZXQgc2h1ZmZsZU5vZGVzOiBBcnJheTxOb2RlPiA9IHRoaXMuY2hpbGRyZW4uZmlsdGVyKGZ1bmN0aW9uKG5vZGUpIHtcbiAgICAgICAgICAgIHJldHVybiAobm9kZS5zaHVmZmxlID09IHRydWUpO1xuICAgICAgICB9KTtcbiAgICAgICAgZm9yIChsZXQgbm9kZSBvZiB0aGlzLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICBpZiAobm9kZS5zaHVmZmxlKSB7XG4gICAgICAgICAgICAgICAgbGV0IGNob29zZW4gPSByYW5kb21JbnRCZXR3ZWVuKDAsIHNodWZmbGVOb2Rlcy5sZW5ndGggLSAxKTtcbiAgICAgICAgICAgICAgICBsZXQgY2hvb3Nlbk5vZGUgPSBzaHVmZmxlTm9kZXMuc3BsaWNlKGNob29zZW4sIDEpWzBdO1xuICAgICAgICAgICAgICAgIHRva2VucyA9IHRva2Vucy5jb25jYXQoY2hvb3Nlbk5vZGUuZ2VuZXJhdGUoZW52LCBmaWx0ZXJzKSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRva2VucyA9IHRva2Vucy5jb25jYXQobm9kZS5nZW5lcmF0ZShlbnYsIGZpbHRlcnMpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdG9rZW5zO1xuICAgIH1cbn1cblxuXG4vKlxuICAgIE9yTm9kZS4gRHVyaW5nIGdlbmVyYXRpb24sIE9ORSBvZiB0aGUgY2hpbGRyZW4gaXMgY2hvb3NlbiBhbmQgZ2VuZXJhdGVkXG4qL1xuY2xhc3MgT3JOb2RlIGV4dGVuZHMgQ2hpbGRyZW5Ob2RlIHtcbiAgICBjdXN0b21XZWlnaHQocHJ1bmluZ0VudjogUHJ1bmluZ0Vudmlyb25tZW50LCBmaWx0ZXJzOiBBcnJheTxzdHJpbmc+LCB1bmZvbGQ6IGJvb2xlYW4sIHZpc2l0czogQXJyYXk8c3RyaW5nPiwgc3VwZXJXZWlnaHQ6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIGxldCBjaGlsZHJlbldlaWdodCA9IDA7XG4gICAgICAgIC8qXG4gICAgICAgICAgICBjaGlsZC9jaGlsZHJlbiB3ZWlnaHQgaXMgb25seSBpbnRlcmVzdGluZyBpZiB1bmZvbGQsIG9yIGlmIHRoZXJlIGFyZSBmaWx0ZXJzLCBiZWNhdXNlXG4gICAgICAgICAgICB3ZSB3YW50IHRvIGRlZXAtcHJ1bmUgdGhlIGdyYXBoXG4gICAgICAgICovXG4gICAgICAgIGlmICh1bmZvbGQgfHwgZmlsdGVycy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBjaGlsZCBvZiB0aGlzLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAgICAgY2hpbGRyZW5XZWlnaHQgKz0gY2hpbGQud2VpZ2h0KHBydW5pbmdFbnYsIGZpbHRlcnMsIGZhbHNlLCB2aXNpdHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc3VwZXJXZWlnaHQgKj0gY2hpbGRyZW5XZWlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHN1cGVyV2VpZ2h0O1xuICAgIH1cblxuICAgIGN1c3RvbUdlbmVyYXRlKGVudjogRW52aXJvbm1lbnQsIGZpbHRlcnM6IEFycmF5PHN0cmluZz4pOiBBcnJheTxUb2tlbj4ge1xuICAgICAgICBsZXQgcHJ1bmluZ0VudiA9IG5ldyBQcnVuaW5nRW52aXJvbm1lbnQoZW52LmJpbmRpbmdzKTtcbiAgICAgICAgbGV0IHdlaWdodHMgPSBuZXcgQXJyYXkoKTtcbiAgICAgICAgLypcbiAgICAgICAgICAgIFRoaXMgaXMgYSBiaXQgdHJpY2t5LiBXZSBuZWVkIHRvIGNvbnNpZGVyIHJlbGF0aXZlIG5vZGUgd2VpZ2h0LlxuICAgICAgICAgICAgRWc6ICRhID0gYSB8IGIgKGMgfCBkIHwgZSk7XG4gICAgICAgICAgICB3ZWlnaHRzOlxuICAgICAgICAgICAgICAgIC0gYTogMVxuICAgICAgICAgICAgICAgIC0gYiAoYyB8IGQgfCBlKTogM1xuICAgICAgICAgICAgc28gdGhlIHdlaWdodHMgYXJyYXkgaXM6XG4gICAgICAgICAgICAgICAgWzA7IDE7IDE7IDFdXG4gICAgICAgICovXG4gICAgICAgIGZvciAobGV0IGNoaWxkSW5kZXggPSAwOyBjaGlsZEluZGV4IDwgdGhpcy5jaGlsZHJlbi5sZW5ndGg7IGNoaWxkSW5kZXgrKykge1xuICAgICAgICAgICAgbGV0IHcgPSB0aGlzLmNoaWxkcmVuW2NoaWxkSW5kZXhdLndlaWdodChwcnVuaW5nRW52LCBmaWx0ZXJzLCBmYWxzZSwgW10pO1xuICAgICAgICAgICAgaWYgKHRoaXMuY2hpbGRyZW5bY2hpbGRJbmRleF0ubG9jYWxXZWlnaHQgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHcgKj0gdGhpcy5jaGlsZHJlbltjaGlsZEluZGV4XS5sb2NhbFdlaWdodDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgdzsgaisrKSB7XG4gICAgICAgICAgICAgICAgd2VpZ2h0cy5wdXNoKGNoaWxkSW5kZXgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gc2F2aW5nIGFsbCB3ZWlnaHRzIGZvciB0ZXN0aW5nIHB1cnBvc2VzXG4gICAgICAgIGVudi53ZWlnaHRzID0gZW52LndlaWdodHMuY29uY2F0KHBydW5pbmdFbnYud2VpZ2h0cyk7XG5cbiAgICAgICAgaWYgKHdlaWdodHMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbGV0IHJhbmRvbUluZGV4ID0gcmFuZG9tSW50QmV0d2VlbigwLCB3ZWlnaHRzLmxlbmd0aCAtIDEpO1xuICAgICAgICAgICAgbGV0IGNob29zZW5Ob2RlID0gdGhpcy5jaGlsZHJlblt3ZWlnaHRzW3JhbmRvbUluZGV4XV07XG4gICAgICAgICAgICByZXR1cm4gKGNob29zZW5Ob2RlLmdlbmVyYXRlKGVudiwgZmlsdGVycykpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgLy8gVE9ETzogdGhpcyBzaG91bGQgbmV2ZXIgaGFwcGVuIHRoYW5rcyB0byBkZWVwIHBydW5pbmc7XG4gICAgICAgICAgICAvLyBpZiBpdCBoYXBwZW5zIHRoZXJlIGlzIGEgYnVnIHNvbWV3aGVyZSwgc28gYmV0dGVyIHRvIGNoZWNrIGl0XG4gICAgICAgICAgICAvLyBFRElUOiBlaG0gaXQgaGFwcGVucyBhIGxvdC4gOilcbiAgICAgICAgICAgIC8vIE11c3QgcmV0aGluayB0aGUgd2F5IHRhZ3MmZmlsdGVycyBhcmUgbWFuYWdlZFxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdDYW5ub3QgY2hvb3NlIGFuIG9yIG5vZGUuIEl0IGxvb2tzIGxpa2UgdGhlIHRhZ3MmZmlsdGVycyBpbiB0aGUgZ3JhbW1hciBleGNsdWRlZCBjb21wbGV0ZWx5IGEgbm9kZScpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5cbi8qXG4gICAgRW1wdHkgTm9kZS4gVXNlZCB3aXRoIGxpdGVyYWwgXCJfXCJcbiovXG5jbGFzcyBFbXB0eU5vZGUgZXh0ZW5kcyBOb2RlIHtcbiAgICBjdXN0b21XZWlnaHQocHJ1bmluZ0VudjogUHJ1bmluZ0Vudmlyb25tZW50LCBmaWx0ZXJzOiBBcnJheTxzdHJpbmc+LCB1bmZvbGQ6IGJvb2xlYW4sIHZpc2l0czogQXJyYXk8c3RyaW5nPiwgc3VwZXJXZWlnaHQ6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBzdXBlcldlaWdodDtcbiAgICB9XG5cbiAgICBjdXN0b21HZW5lcmF0ZShlbnY6IEVudmlyb25tZW50LCBmaWx0ZXJzOiBBcnJheTxzdHJpbmc+KTogQXJyYXk8VG9rZW4+IHtcbiAgICAgICAgcmV0dXJuIFtdO1xuICAgIH1cbn1cblxuZXhwb3J0IHsgTm9kZSwgRW1wdHlOb2RlIH07XG4iLCIvKiBwYXJzZXIgZ2VuZXJhdGVkIGJ5IGppc29uIDAuNC4xOCAqL1xuLypcbiAgUmV0dXJucyBhIFBhcnNlciBvYmplY3Qgb2YgdGhlIGZvbGxvd2luZyBzdHJ1Y3R1cmU6XG5cbiAgUGFyc2VyOiB7XG4gICAgeXk6IHt9XG4gIH1cblxuICBQYXJzZXIucHJvdG90eXBlOiB7XG4gICAgeXk6IHt9LFxuICAgIHRyYWNlOiBmdW5jdGlvbigpLFxuICAgIHN5bWJvbHNfOiB7YXNzb2NpYXRpdmUgbGlzdDogbmFtZSA9PT4gbnVtYmVyfSxcbiAgICB0ZXJtaW5hbHNfOiB7YXNzb2NpYXRpdmUgbGlzdDogbnVtYmVyID09PiBuYW1lfSxcbiAgICBwcm9kdWN0aW9uc186IFsuLi5dLFxuICAgIHBlcmZvcm1BY3Rpb246IGZ1bmN0aW9uIGFub255bW91cyh5eXRleHQsIHl5bGVuZywgeXlsaW5lbm8sIHl5LCB5eXN0YXRlLCAkJCwgXyQpLFxuICAgIHRhYmxlOiBbLi4uXSxcbiAgICBkZWZhdWx0QWN0aW9uczogey4uLn0sXG4gICAgcGFyc2VFcnJvcjogZnVuY3Rpb24oc3RyLCBoYXNoKSxcbiAgICBwYXJzZTogZnVuY3Rpb24oaW5wdXQpLFxuXG4gICAgbGV4ZXI6IHtcbiAgICAgICAgRU9GOiAxLFxuICAgICAgICBwYXJzZUVycm9yOiBmdW5jdGlvbihzdHIsIGhhc2gpLFxuICAgICAgICBzZXRJbnB1dDogZnVuY3Rpb24oaW5wdXQpLFxuICAgICAgICBpbnB1dDogZnVuY3Rpb24oKSxcbiAgICAgICAgdW5wdXQ6IGZ1bmN0aW9uKHN0ciksXG4gICAgICAgIG1vcmU6IGZ1bmN0aW9uKCksXG4gICAgICAgIGxlc3M6IGZ1bmN0aW9uKG4pLFxuICAgICAgICBwYXN0SW5wdXQ6IGZ1bmN0aW9uKCksXG4gICAgICAgIHVwY29taW5nSW5wdXQ6IGZ1bmN0aW9uKCksXG4gICAgICAgIHNob3dQb3NpdGlvbjogZnVuY3Rpb24oKSxcbiAgICAgICAgdGVzdF9tYXRjaDogZnVuY3Rpb24ocmVnZXhfbWF0Y2hfYXJyYXksIHJ1bGVfaW5kZXgpLFxuICAgICAgICBuZXh0OiBmdW5jdGlvbigpLFxuICAgICAgICBsZXg6IGZ1bmN0aW9uKCksXG4gICAgICAgIGJlZ2luOiBmdW5jdGlvbihjb25kaXRpb24pLFxuICAgICAgICBwb3BTdGF0ZTogZnVuY3Rpb24oKSxcbiAgICAgICAgX2N1cnJlbnRSdWxlczogZnVuY3Rpb24oKSxcbiAgICAgICAgdG9wU3RhdGU6IGZ1bmN0aW9uKCksXG4gICAgICAgIHB1c2hTdGF0ZTogZnVuY3Rpb24oY29uZGl0aW9uKSxcblxuICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgICByYW5nZXM6IGJvb2xlYW4gICAgICAgICAgIChvcHRpb25hbDogdHJ1ZSA9PT4gdG9rZW4gbG9jYXRpb24gaW5mbyB3aWxsIGluY2x1ZGUgYSAucmFuZ2VbXSBtZW1iZXIpXG4gICAgICAgICAgICBmbGV4OiBib29sZWFuICAgICAgICAgICAgIChvcHRpb25hbDogdHJ1ZSA9PT4gZmxleC1saWtlIGxleGluZyBiZWhhdmlvdXIgd2hlcmUgdGhlIHJ1bGVzIGFyZSB0ZXN0ZWQgZXhoYXVzdGl2ZWx5IHRvIGZpbmQgdGhlIGxvbmdlc3QgbWF0Y2gpXG4gICAgICAgICAgICBiYWNrdHJhY2tfbGV4ZXI6IGJvb2xlYW4gIChvcHRpb25hbDogdHJ1ZSA9PT4gbGV4ZXIgcmVnZXhlcyBhcmUgdGVzdGVkIGluIG9yZGVyIGFuZCBmb3IgZWFjaCBtYXRjaGluZyByZWdleCB0aGUgYWN0aW9uIGNvZGUgaXMgaW52b2tlZDsgdGhlIGxleGVyIHRlcm1pbmF0ZXMgdGhlIHNjYW4gd2hlbiBhIHRva2VuIGlzIHJldHVybmVkIGJ5IHRoZSBhY3Rpb24gY29kZSlcbiAgICAgICAgfSxcblxuICAgICAgICBwZXJmb3JtQWN0aW9uOiBmdW5jdGlvbih5eSwgeXlfLCAkYXZvaWRpbmdfbmFtZV9jb2xsaXNpb25zLCBZWV9TVEFSVCksXG4gICAgICAgIHJ1bGVzOiBbLi4uXSxcbiAgICAgICAgY29uZGl0aW9uczoge2Fzc29jaWF0aXZlIGxpc3Q6IG5hbWUgPT0+IHNldH0sXG4gICAgfVxuICB9XG5cblxuICB0b2tlbiBsb2NhdGlvbiBpbmZvIChAJCwgXyQsIGV0Yy4pOiB7XG4gICAgZmlyc3RfbGluZTogbixcbiAgICBsYXN0X2xpbmU6IG4sXG4gICAgZmlyc3RfY29sdW1uOiBuLFxuICAgIGxhc3RfY29sdW1uOiBuLFxuICAgIHJhbmdlOiBbc3RhcnRfbnVtYmVyLCBlbmRfbnVtYmVyXSAgICAgICAod2hlcmUgdGhlIG51bWJlcnMgYXJlIGluZGV4ZXMgaW50byB0aGUgaW5wdXQgc3RyaW5nLCByZWd1bGFyIHplcm8tYmFzZWQpXG4gIH1cblxuXG4gIHRoZSBwYXJzZUVycm9yIGZ1bmN0aW9uIHJlY2VpdmVzIGEgJ2hhc2gnIG9iamVjdCB3aXRoIHRoZXNlIG1lbWJlcnMgZm9yIGxleGVyIGFuZCBwYXJzZXIgZXJyb3JzOiB7XG4gICAgdGV4dDogICAgICAgIChtYXRjaGVkIHRleHQpXG4gICAgdG9rZW46ICAgICAgICh0aGUgcHJvZHVjZWQgdGVybWluYWwgdG9rZW4sIGlmIGFueSlcbiAgICBsaW5lOiAgICAgICAgKHl5bGluZW5vKVxuICB9XG4gIHdoaWxlIHBhcnNlciAoZ3JhbW1hcikgZXJyb3JzIHdpbGwgYWxzbyBwcm92aWRlIHRoZXNlIG1lbWJlcnMsIGkuZS4gcGFyc2VyIGVycm9ycyBkZWxpdmVyIGEgc3VwZXJzZXQgb2YgYXR0cmlidXRlczoge1xuICAgIGxvYzogICAgICAgICAoeXlsbG9jKVxuICAgIGV4cGVjdGVkOiAgICAoc3RyaW5nIGRlc2NyaWJpbmcgdGhlIHNldCBvZiBleHBlY3RlZCB0b2tlbnMpXG4gICAgcmVjb3ZlcmFibGU6IChib29sZWFuOiBUUlVFIHdoZW4gdGhlIHBhcnNlciBoYXMgYSBlcnJvciByZWNvdmVyeSBydWxlIGF2YWlsYWJsZSBmb3IgdGhpcyBwYXJ0aWN1bGFyIGVycm9yKVxuICB9XG4qL1xudmFyIHBhcnNlciA9IChmdW5jdGlvbigpe1xudmFyIG89ZnVuY3Rpb24oayx2LG8sbCl7Zm9yKG89b3x8e30sbD1rLmxlbmd0aDtsLS07b1trW2xdXT12KTtyZXR1cm4gb30sJFYwPVsxLDVdLCRWMT1bNSw3XSwkVjI9WzEsMThdLCRWMz1bMSwxMV0sJFY0PVsxLDEyXSwkVjU9WzEsMTNdLCRWNj1bMSwxNF0sJFY3PVsxLDE1XSwkVjg9WzEsMTZdLCRWOT1bMSwxN10sJFZhPVsxLDE5XSwkVmI9WzEsMjJdLCRWYz1bMSwyM10sJFZkPVsxLDI0XSwkVmU9WzEsMjVdLCRWZj1bMSwyNl0sJFZnPVsxLDI3XSwkVmg9WzEsMjhdLCRWaT1bMSwyOV0sJFZqPVsxLDMwXSwkVms9WzEwLDEzLDE0LDE2LDE4LDIwLDIxLDI0LDI1LDI3LDI4LDMyLDMzXSwkVmw9WzEwLDEzLDE0LDE2LDE4LDIwXTtcbnZhciBwYXJzZXIgPSB7dHJhY2U6IGZ1bmN0aW9uIHRyYWNlICgpIHsgfSxcbnl5OiB7fSxcbnN5bWJvbHNfOiB7XCJlcnJvclwiOjIsXCJTVEFSVFwiOjMsXCJCSU5ESU5HU1wiOjQsXCJFT0ZcIjo1LFwiQklORElOR1wiOjYsXCJTWU1CT0xcIjo3LFwiQklORFwiOjgsXCJOT0RFXCI6OSxcIlNFTUlDT0xPTlwiOjEwLFwiU1RST05HQklORFwiOjExLFwiVEFHXCI6MTIsXCJBTkRcIjoxMyxcIk9SXCI6MTQsXCJCUkFcIjoxNSxcIktFVFwiOjE2LFwiU1FVQVJFQlJBXCI6MTcsXCJTUVVBUkVLRVRcIjoxOCxcIkNVUkxZQlJBXCI6MTksXCJDVVJMWUtFVFwiOjIwLFwiTE9DQUxXRUlHSFRcIjoyMSxcIk5VTUJFUlwiOjIyLFwiTEVGVFNOQVBcIjoyMyxcIlJJR0hUU05BUFwiOjI0LFwiVVBQRVJDQVNFXCI6MjUsXCJVTkZPTERcIjoyNixcIklOTkVSU1RST05HQklORFwiOjI3LFwiVElNRVNcIjoyOCxcIk1JTlVTXCI6MjksXCJURVhUXCI6MzAsXCJFTVBUWVwiOjMxLFwiRklMVEVSXCI6MzIsXCJSRVNFVEZJTFRFUlNcIjozMyxcIiRhY2NlcHRcIjowLFwiJGVuZFwiOjF9LFxudGVybWluYWxzXzogezI6XCJlcnJvclwiLDU6XCJFT0ZcIiw3OlwiU1lNQk9MXCIsODpcIkJJTkRcIiwxMDpcIlNFTUlDT0xPTlwiLDExOlwiU1RST05HQklORFwiLDEyOlwiVEFHXCIsMTM6XCJBTkRcIiwxNDpcIk9SXCIsMTU6XCJCUkFcIiwxNjpcIktFVFwiLDE3OlwiU1FVQVJFQlJBXCIsMTg6XCJTUVVBUkVLRVRcIiwxOTpcIkNVUkxZQlJBXCIsMjA6XCJDVVJMWUtFVFwiLDIxOlwiTE9DQUxXRUlHSFRcIiwyMjpcIk5VTUJFUlwiLDIzOlwiTEVGVFNOQVBcIiwyNDpcIlJJR0hUU05BUFwiLDI1OlwiVVBQRVJDQVNFXCIsMjY6XCJVTkZPTERcIiwyNzpcIklOTkVSU1RST05HQklORFwiLDI4OlwiVElNRVNcIiwyOTpcIk1JTlVTXCIsMzA6XCJURVhUXCIsMzE6XCJFTVBUWVwiLDMyOlwiRklMVEVSXCIsMzM6XCJSRVNFVEZJTFRFUlNcIn0sXG5wcm9kdWN0aW9uc186IFswLFszLDJdLFszLDFdLFs0LDFdLFs0LDJdLFs2LDRdLFs2LDRdLFs5LDJdLFs5LDNdLFs5LDNdLFs5LDNdLFs5LDNdLFs5LDNdLFs5LDNdLFs5LDJdLFs5LDJdLFs5LDJdLFs5LDJdLFs5LDNdLFs5LDNdLFs5LDVdLFs5LDFdLFs5LDFdLFs5LDFdLFs5LDJdLFs5LDJdXSxcbnBlcmZvcm1BY3Rpb246IGZ1bmN0aW9uIGFub255bW91cyh5eXRleHQsIHl5bGVuZywgeXlsaW5lbm8sIHl5LCB5eXN0YXRlIC8qIGFjdGlvblsxXSAqLywgJCQgLyogdnN0YWNrICovLCBfJCAvKiBsc3RhY2sgKi8pIHtcbi8qIHRoaXMgPT0geXl2YWwgKi9cblxudmFyICQwID0gJCQubGVuZ3RoIC0gMTtcbnN3aXRjaCAoeXlzdGF0ZSkge1xuY2FzZSAxOlxuIHRoaXMuJCA9ICQkWyQwLTFdOyByZXR1cm4odGhpcy4kKTsgXG5icmVhaztcbmNhc2UgMjpcbiByZXR1cm4ge307IFxuYnJlYWs7XG5jYXNlIDM6XG4gdGhpcy4kID0gWyQkWyQwXV07IFxuYnJlYWs7XG5jYXNlIDQ6XG4gdGhpcy4kID0gJCRbJDAtMV07IHRoaXMuJC5wdXNoKCQkWyQwXSk7IFxuYnJlYWs7XG5jYXNlIDU6XG4gJCRbJDAtMV1bJ25hbWUnXSA9ICQkWyQwLTNdOyB0aGlzLiQgPSAkJFskMC0xXSBcbmJyZWFrO1xuY2FzZSA2OlxuXG4gICAgICAgICQkWyQwLTFdWyduYW1lJ10gPSAkJFskMC0xXVsnc3Ryb25nQmluZGluZyddID0gJCRbJDAtM107XG4gICAgICAgIHRoaXMuJCA9ICQkWyQwLTFdXG4gICAgXG5icmVhaztcbmNhc2UgNzpcblxuICAgICAgaWYgKCEkJFskMF1bJ3RhZ3MnXSkgeyAkJFskMF1bJ3RhZ3MnXSA9IFtdOyB9XG4gICAgICAkJFskMF1bJ3RhZ3MnXS5wdXNoKCQkWyQwLTFdKTtcbiAgICAgIHRoaXMuJCA9ICQkWyQwXTtcbiAgICBcbmJyZWFrO1xuY2FzZSA4OlxuXG4gICAgICB2YXIgbCA9IG5ldyBBcnJheSgpO1xuICAgICAgKCQkWyQwLTJdLnR5cGUgPT0gXCJhbmRcIikgPyBsID0gbC5jb25jYXQoJCRbJDAtMl0uY2hpbGRyZW4pIDogbC5wdXNoKCQkWyQwLTJdKTtcbiAgICAgICgkJFskMF0udHlwZSA9PSBcImFuZFwiKSA/IGwgPSBsLmNvbmNhdCgkJFskMF0uY2hpbGRyZW4pIDogbC5wdXNoKCQkWyQwXSk7XG4gICAgICB0aGlzLiQgPSB7IHR5cGU6IFwiYW5kXCIsIFwiY2hpbGRyZW5cIjogbCB9O1xuICAgIFxuYnJlYWs7XG5jYXNlIDk6XG5cbiAgICAgIHZhciBsID0gbmV3IEFycmF5KCk7XG4gICAgICAoJCRbJDAtMl0udHlwZSA9PSBcIm9yXCIpID8gbCA9IGwuY29uY2F0KCQkWyQwLTJdLmNoaWxkcmVuKSA6IGwucHVzaCgkJFskMC0yXSk7XG4gICAgICAoJCRbJDBdLnR5cGUgPT0gXCJvclwiKSA/IGwgPSBsLmNvbmNhdCgkJFskMF0uY2hpbGRyZW4pIDogbC5wdXNoKCQkWyQwXSk7XG4gICAgICB0aGlzLiQgPSB7IHR5cGU6IFwib3JcIiwgXCJjaGlsZHJlblwiOiBsIH07XG4gICAgXG5icmVhaztcbmNhc2UgMTA6XG4gdGhpcy4kID0geyB0eXBlOiBcImJyYWNrZXRcIiwgY2hpbGQ6ICQkWyQwLTFdIH0gXG5icmVhaztcbmNhc2UgMTE6XG5cbiAgICAgIHRoaXMuJCA9IHsgdHlwZTogXCJicmFja2V0XCIsIGNoaWxkOiAkJFskMC0xXSwgbWluVGltZXM6IDAsIG1heFRpbWVzOiAxIH1cbiAgICBcbmJyZWFrO1xuY2FzZSAxMjpcblxuICAgICAgdGhpcy4kID0geyB0eXBlOiBcImJyYWNrZXRcIiwgY2hpbGQ6ICQkWyQwLTFdLCBzaHVmZmxlOiB0cnVlIH1cbiAgICBcbmJyZWFrO1xuY2FzZSAxMzpcblxuICAgICAgdmFyIHcgPSBOdW1iZXIoJCRbJDBdKTtcbiAgICAgIGlmICh3ID4gMSkge1xuICAgICAgICAkJFskMC0yXVsnbG9jYWxXZWlnaHQnXSA9IHc7XG4gICAgICB9XG4gICAgICB0aGlzLiQgPSAkJFskMC0yXTtcbiAgICBcbmJyZWFrO1xuY2FzZSAxNDpcbiAkJFskMF1bJ2xlZnRTbmFwJ10gPSB0cnVlOyB0aGlzLiQgPSAkJFskMF0gXG5icmVhaztcbmNhc2UgMTU6XG4gJCRbJDAtMV1bJ3JpZ2h0U25hcCddID0gdHJ1ZTsgdGhpcy4kID0gJCRbJDAtMV0gXG5icmVhaztcbmNhc2UgMTY6XG4gJCRbJDAtMV1bJ3VwcGVyY2FzZSddID0gdHJ1ZTsgdGhpcy4kID0gJCRbJDAtMV0gXG5icmVhaztcbmNhc2UgMTc6XG4gJCRbJDBdWyd1bmZvbGQnXSA9IHRydWU7IHRoaXMuJCA9ICQkWyQwXSBcbmJyZWFrO1xuY2FzZSAxODpcblxuICAgICAgJCRbJDAtMl1bJ3N0cm9uZ0JpbmRpbmcnXSA9ICQkWyQwXTtcbiAgICAgIHRoaXMuJCA9ICQkWyQwLTJdO1xuICAgIFxuYnJlYWs7XG5jYXNlIDE5OlxuXG4gICAgICAkJFskMC0yXVsnbWluVGltZXMnXSA9IE51bWJlcigkJFskMF0pO1xuICAgICAgJCRbJDAtMl1bJ21heFRpbWVzJ10gPSBOdW1iZXIoJCRbJDBdKTtcbiAgICAgIHRoaXMuJCA9ICQkWyQwLTJdO1xuICAgIFxuYnJlYWs7XG5jYXNlIDIwOlxuXG4gICAgICAkJFskMC00XVsnbWluVGltZXMnXSA9IE51bWJlcigkJFskMC0yXSk7XG4gICAgICAkJFskMC00XVsnbWF4VGltZXMnXSA9IE51bWJlcigkJFskMF0pO1xuICAgICAgdGhpcy4kID0gJCRbJDAtNF07XG4gICAgXG5icmVhaztcbmNhc2UgMjE6XG4gdGhpcy4kID0geyB0eXBlOiBcInRleHRcIiwgY29udGVudHM6ICQkWyQwXSB9IFxuYnJlYWs7XG5jYXNlIDIyOlxuIHRoaXMuJCA9IHsgdHlwZTogXCJzeW1ib2xcIiwgdGFyZ2V0OiAkJFskMF0gfSBcbmJyZWFrO1xuY2FzZSAyMzpcbiB0aGlzLiQgPSB7IHR5cGU6IFwiZW1wdHlcIiB9IFxuYnJlYWs7XG5jYXNlIDI0OlxuXG4gICAgICBpZiAoISQkWyQwLTFdWydmaWx0ZXJzJ10pIHsgJCRbJDAtMV1bJ2ZpbHRlcnMnXSA9IFtdOyB9XG4gICAgICAkJFskMC0xXVsnZmlsdGVycyddLnB1c2goJCRbJDBdKTtcbiAgICAgIHRoaXMuJCA9ICQkWyQwLTFdO1xuICAgIFxuYnJlYWs7XG5jYXNlIDI1OlxuICQkWyQwLTFdWydyZXNldEZpbHRlcnMnXSA9IHRydWU7IHRoaXMuJCA9ICQkWyQwLTFdIFxuYnJlYWs7XG59XG59LFxudGFibGU6IFt7MzoxLDQ6Miw1OlsxLDNdLDY6NCw3OiRWMH0sezE6WzNdfSx7NTpbMSw2XSw2OjcsNzokVjB9LHsxOlsyLDJdfSxvKCRWMSxbMiwzXSksezg6WzEsOF0sMTE6WzEsOV19LHsxOlsyLDFdfSxvKCRWMSxbMiw0XSksezc6JFYyLDk6MTAsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezc6JFYyLDk6MjAsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezEwOlsxLDIxXSwxMzokVmIsMTQ6JFZjLDIxOiRWZCwyNDokVmUsMjU6JFZmLDI3OiRWZywyODokVmgsMzI6JFZpLDMzOiRWan0sezc6JFYyLDk6MzEsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezc6JFYyLDk6MzIsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezc6JFYyLDk6MzMsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezc6JFYyLDk6MzQsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezc6JFYyLDk6MzUsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sezc6JFYyLDk6MzYsMTI6JFYzLDE1OiRWNCwxNzokVjUsMTk6JFY2LDIzOiRWNywyNjokVjgsMzA6JFY5LDMxOiRWYX0sbygkVmssWzIsMjFdKSxvKCRWayxbMiwyMl0pLG8oJFZrLFsyLDIzXSksezEwOlsxLDM3XSwxMzokVmIsMTQ6JFZjLDIxOiRWZCwyNDokVmUsMjU6JFZmLDI3OiRWZywyODokVmgsMzI6JFZpLDMzOiRWan0sbygkVjEsWzIsNV0pLHs3OiRWMiw5OjM4LDEyOiRWMywxNTokVjQsMTc6JFY1LDE5OiRWNiwyMzokVjcsMjY6JFY4LDMwOiRWOSwzMTokVmF9LHs3OiRWMiw5OjM5LDEyOiRWMywxNTokVjQsMTc6JFY1LDE5OiRWNiwyMzokVjcsMjY6JFY4LDMwOiRWOSwzMTokVmF9LHsyMjpbMSw0MF19LG8oJFZrLFsyLDE1XSksbygkVmssWzIsMTZdKSx7NzpbMSw0MV19LHsyMjpbMSw0Ml19LG8oJFZrLFsyLDI0XSksbygkVmssWzIsMjVdKSxvKCRWbCxbMiw3XSx7MjE6JFZkLDI0OiRWZSwyNTokVmYsMjc6JFZnLDI4OiRWaCwzMjokVmksMzM6JFZqfSksezEzOiRWYiwxNDokVmMsMTY6WzEsNDNdLDIxOiRWZCwyNDokVmUsMjU6JFZmLDI3OiRWZywyODokVmgsMzI6JFZpLDMzOiRWan0sezEzOiRWYiwxNDokVmMsMTg6WzEsNDRdLDIxOiRWZCwyNDokVmUsMjU6JFZmLDI3OiRWZywyODokVmgsMzI6JFZpLDMzOiRWan0sezEzOiRWYiwxNDokVmMsMjA6WzEsNDVdLDIxOiRWZCwyNDokVmUsMjU6JFZmLDI3OiRWZywyODokVmgsMzI6JFZpLDMzOiRWan0sbygkVmwsWzIsMTRdLHsyMTokVmQsMjQ6JFZlLDI1OiRWZiwyNzokVmcsMjg6JFZoLDMyOiRWaSwzMzokVmp9KSxvKCRWbCxbMiwxN10sezIxOiRWZCwyNDokVmUsMjU6JFZmLDI3OiRWZywyODokVmgsMzI6JFZpLDMzOiRWan0pLG8oJFYxLFsyLDZdKSxvKCRWbCxbMiw4XSx7MjE6JFZkLDI0OiRWZSwyNTokVmYsMjc6JFZnLDI4OiRWaCwzMjokVmksMzM6JFZqfSksbyhbMTAsMTQsMTYsMTgsMjBdLFsyLDldLHsxMzokVmIsMjE6JFZkLDI0OiRWZSwyNTokVmYsMjc6JFZnLDI4OiRWaCwzMjokVmksMzM6JFZqfSksbygkVmssWzIsMTNdKSxvKCRWayxbMiwxOF0pLG8oJFZrLFsyLDE5XSx7Mjk6WzEsNDZdfSksbygkVmssWzIsMTBdKSxvKCRWayxbMiwxMV0pLG8oJFZrLFsyLDEyXSksezIyOlsxLDQ3XX0sbygkVmssWzIsMjBdKV0sXG5kZWZhdWx0QWN0aW9uczogezM6WzIsMl0sNjpbMiwxXX0sXG5wYXJzZUVycm9yOiBmdW5jdGlvbiBwYXJzZUVycm9yIChzdHIsIGhhc2gpIHtcbiAgICBpZiAoaGFzaC5yZWNvdmVyYWJsZSkge1xuICAgICAgICB0aGlzLnRyYWNlKHN0cik7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGVycm9yID0gbmV3IEVycm9yKHN0cik7XG4gICAgICAgIGVycm9yLmhhc2ggPSBoYXNoO1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICB9XG59LFxucGFyc2U6IGZ1bmN0aW9uIHBhcnNlKGlucHV0KSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzLCBzdGFjayA9IFswXSwgdHN0YWNrID0gW10sIHZzdGFjayA9IFtudWxsXSwgbHN0YWNrID0gW10sIHRhYmxlID0gdGhpcy50YWJsZSwgeXl0ZXh0ID0gJycsIHl5bGluZW5vID0gMCwgeXlsZW5nID0gMCwgcmVjb3ZlcmluZyA9IDAsIFRFUlJPUiA9IDIsIEVPRiA9IDE7XG4gICAgdmFyIGFyZ3MgPSBsc3RhY2suc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuICAgIHZhciBsZXhlciA9IE9iamVjdC5jcmVhdGUodGhpcy5sZXhlcik7XG4gICAgdmFyIHNoYXJlZFN0YXRlID0geyB5eToge30gfTtcbiAgICBmb3IgKHZhciBrIGluIHRoaXMueXkpIHtcbiAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbCh0aGlzLnl5LCBrKSkge1xuICAgICAgICAgICAgc2hhcmVkU3RhdGUueXlba10gPSB0aGlzLnl5W2tdO1xuICAgICAgICB9XG4gICAgfVxuICAgIGxleGVyLnNldElucHV0KGlucHV0LCBzaGFyZWRTdGF0ZS55eSk7XG4gICAgc2hhcmVkU3RhdGUueXkubGV4ZXIgPSBsZXhlcjtcbiAgICBzaGFyZWRTdGF0ZS55eS5wYXJzZXIgPSB0aGlzO1xuICAgIGlmICh0eXBlb2YgbGV4ZXIueXlsbG9jID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIGxleGVyLnl5bGxvYyA9IHt9O1xuICAgIH1cbiAgICB2YXIgeXlsb2MgPSBsZXhlci55eWxsb2M7XG4gICAgbHN0YWNrLnB1c2goeXlsb2MpO1xuICAgIHZhciByYW5nZXMgPSBsZXhlci5vcHRpb25zICYmIGxleGVyLm9wdGlvbnMucmFuZ2VzO1xuICAgIGlmICh0eXBlb2Ygc2hhcmVkU3RhdGUueXkucGFyc2VFcnJvciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLnBhcnNlRXJyb3IgPSBzaGFyZWRTdGF0ZS55eS5wYXJzZUVycm9yO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMucGFyc2VFcnJvciA9IE9iamVjdC5nZXRQcm90b3R5cGVPZih0aGlzKS5wYXJzZUVycm9yO1xuICAgIH1cbiAgICBmdW5jdGlvbiBwb3BTdGFjayhuKSB7XG4gICAgICAgIHN0YWNrLmxlbmd0aCA9IHN0YWNrLmxlbmd0aCAtIDIgKiBuO1xuICAgICAgICB2c3RhY2subGVuZ3RoID0gdnN0YWNrLmxlbmd0aCAtIG47XG4gICAgICAgIGxzdGFjay5sZW5ndGggPSBsc3RhY2subGVuZ3RoIC0gbjtcbiAgICB9XG4gICAgX3Rva2VuX3N0YWNrOlxuICAgICAgICB2YXIgbGV4ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHRva2VuO1xuICAgICAgICAgICAgdG9rZW4gPSBsZXhlci5sZXgoKSB8fCBFT0Y7XG4gICAgICAgICAgICBpZiAodHlwZW9mIHRva2VuICE9PSAnbnVtYmVyJykge1xuICAgICAgICAgICAgICAgIHRva2VuID0gc2VsZi5zeW1ib2xzX1t0b2tlbl0gfHwgdG9rZW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgIH07XG4gICAgdmFyIHN5bWJvbCwgcHJlRXJyb3JTeW1ib2wsIHN0YXRlLCBhY3Rpb24sIGEsIHIsIHl5dmFsID0ge30sIHAsIGxlbiwgbmV3U3RhdGUsIGV4cGVjdGVkO1xuICAgIHdoaWxlICh0cnVlKSB7XG4gICAgICAgIHN0YXRlID0gc3RhY2tbc3RhY2subGVuZ3RoIC0gMV07XG4gICAgICAgIGlmICh0aGlzLmRlZmF1bHRBY3Rpb25zW3N0YXRlXSkge1xuICAgICAgICAgICAgYWN0aW9uID0gdGhpcy5kZWZhdWx0QWN0aW9uc1tzdGF0ZV07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoc3ltYm9sID09PSBudWxsIHx8IHR5cGVvZiBzeW1ib2wgPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICBzeW1ib2wgPSBsZXgoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGFjdGlvbiA9IHRhYmxlW3N0YXRlXSAmJiB0YWJsZVtzdGF0ZV1bc3ltYm9sXTtcbiAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGFjdGlvbiA9PT0gJ3VuZGVmaW5lZCcgfHwgIWFjdGlvbi5sZW5ndGggfHwgIWFjdGlvblswXSkge1xuICAgICAgICAgICAgICAgIHZhciBlcnJTdHIgPSAnJztcbiAgICAgICAgICAgICAgICBleHBlY3RlZCA9IFtdO1xuICAgICAgICAgICAgICAgIGZvciAocCBpbiB0YWJsZVtzdGF0ZV0pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGVybWluYWxzX1twXSAmJiBwID4gVEVSUk9SKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBleHBlY3RlZC5wdXNoKCdcXCcnICsgdGhpcy50ZXJtaW5hbHNfW3BdICsgJ1xcJycpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChsZXhlci5zaG93UG9zaXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyU3RyID0gJ1BhcnNlIGVycm9yIG9uIGxpbmUgJyArICh5eWxpbmVubyArIDEpICsgJzpcXG4nICsgbGV4ZXIuc2hvd1Bvc2l0aW9uKCkgKyAnXFxuRXhwZWN0aW5nICcgKyBleHBlY3RlZC5qb2luKCcsICcpICsgJywgZ290IFxcJycgKyAodGhpcy50ZXJtaW5hbHNfW3N5bWJvbF0gfHwgc3ltYm9sKSArICdcXCcnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVyclN0ciA9ICdQYXJzZSBlcnJvciBvbiBsaW5lICcgKyAoeXlsaW5lbm8gKyAxKSArICc6IFVuZXhwZWN0ZWQgJyArIChzeW1ib2wgPT0gRU9GID8gJ2VuZCBvZiBpbnB1dCcgOiAnXFwnJyArICh0aGlzLnRlcm1pbmFsc19bc3ltYm9sXSB8fCBzeW1ib2wpICsgJ1xcJycpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLnBhcnNlRXJyb3IoZXJyU3RyLCB7XG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IGxleGVyLm1hdGNoLFxuICAgICAgICAgICAgICAgICAgICB0b2tlbjogdGhpcy50ZXJtaW5hbHNfW3N5bWJvbF0gfHwgc3ltYm9sLFxuICAgICAgICAgICAgICAgICAgICBsaW5lOiBsZXhlci55eWxpbmVubyxcbiAgICAgICAgICAgICAgICAgICAgbG9jOiB5eWxvYyxcbiAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWQ6IGV4cGVjdGVkXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIGlmIChhY3Rpb25bMF0gaW5zdGFuY2VvZiBBcnJheSAmJiBhY3Rpb24ubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdQYXJzZSBFcnJvcjogbXVsdGlwbGUgYWN0aW9ucyBwb3NzaWJsZSBhdCBzdGF0ZTogJyArIHN0YXRlICsgJywgdG9rZW46ICcgKyBzeW1ib2wpO1xuICAgICAgICB9XG4gICAgICAgIHN3aXRjaCAoYWN0aW9uWzBdKSB7XG4gICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgIHN0YWNrLnB1c2goc3ltYm9sKTtcbiAgICAgICAgICAgIHZzdGFjay5wdXNoKGxleGVyLnl5dGV4dCk7XG4gICAgICAgICAgICBsc3RhY2sucHVzaChsZXhlci55eWxsb2MpO1xuICAgICAgICAgICAgc3RhY2sucHVzaChhY3Rpb25bMV0pO1xuICAgICAgICAgICAgc3ltYm9sID0gbnVsbDtcbiAgICAgICAgICAgIGlmICghcHJlRXJyb3JTeW1ib2wpIHtcbiAgICAgICAgICAgICAgICB5eWxlbmcgPSBsZXhlci55eWxlbmc7XG4gICAgICAgICAgICAgICAgeXl0ZXh0ID0gbGV4ZXIueXl0ZXh0O1xuICAgICAgICAgICAgICAgIHl5bGluZW5vID0gbGV4ZXIueXlsaW5lbm87XG4gICAgICAgICAgICAgICAgeXlsb2MgPSBsZXhlci55eWxsb2M7XG4gICAgICAgICAgICAgICAgaWYgKHJlY292ZXJpbmcgPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlY292ZXJpbmctLTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHN5bWJvbCA9IHByZUVycm9yU3ltYm9sO1xuICAgICAgICAgICAgICAgIHByZUVycm9yU3ltYm9sID0gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICBsZW4gPSB0aGlzLnByb2R1Y3Rpb25zX1thY3Rpb25bMV1dWzFdO1xuICAgICAgICAgICAgeXl2YWwuJCA9IHZzdGFja1t2c3RhY2subGVuZ3RoIC0gbGVuXTtcbiAgICAgICAgICAgIHl5dmFsLl8kID0ge1xuICAgICAgICAgICAgICAgIGZpcnN0X2xpbmU6IGxzdGFja1tsc3RhY2subGVuZ3RoIC0gKGxlbiB8fCAxKV0uZmlyc3RfbGluZSxcbiAgICAgICAgICAgICAgICBsYXN0X2xpbmU6IGxzdGFja1tsc3RhY2subGVuZ3RoIC0gMV0ubGFzdF9saW5lLFxuICAgICAgICAgICAgICAgIGZpcnN0X2NvbHVtbjogbHN0YWNrW2xzdGFjay5sZW5ndGggLSAobGVuIHx8IDEpXS5maXJzdF9jb2x1bW4sXG4gICAgICAgICAgICAgICAgbGFzdF9jb2x1bW46IGxzdGFja1tsc3RhY2subGVuZ3RoIC0gMV0ubGFzdF9jb2x1bW5cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBpZiAocmFuZ2VzKSB7XG4gICAgICAgICAgICAgICAgeXl2YWwuXyQucmFuZ2UgPSBbXG4gICAgICAgICAgICAgICAgICAgIGxzdGFja1tsc3RhY2subGVuZ3RoIC0gKGxlbiB8fCAxKV0ucmFuZ2VbMF0sXG4gICAgICAgICAgICAgICAgICAgIGxzdGFja1tsc3RhY2subGVuZ3RoIC0gMV0ucmFuZ2VbMV1cbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgciA9IHRoaXMucGVyZm9ybUFjdGlvbi5hcHBseSh5eXZhbCwgW1xuICAgICAgICAgICAgICAgIHl5dGV4dCxcbiAgICAgICAgICAgICAgICB5eWxlbmcsXG4gICAgICAgICAgICAgICAgeXlsaW5lbm8sXG4gICAgICAgICAgICAgICAgc2hhcmVkU3RhdGUueXksXG4gICAgICAgICAgICAgICAgYWN0aW9uWzFdLFxuICAgICAgICAgICAgICAgIHZzdGFjayxcbiAgICAgICAgICAgICAgICBsc3RhY2tcbiAgICAgICAgICAgIF0uY29uY2F0KGFyZ3MpKTtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgciAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChsZW4pIHtcbiAgICAgICAgICAgICAgICBzdGFjayA9IHN0YWNrLnNsaWNlKDAsIC0xICogbGVuICogMik7XG4gICAgICAgICAgICAgICAgdnN0YWNrID0gdnN0YWNrLnNsaWNlKDAsIC0xICogbGVuKTtcbiAgICAgICAgICAgICAgICBsc3RhY2sgPSBsc3RhY2suc2xpY2UoMCwgLTEgKiBsZW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc3RhY2sucHVzaCh0aGlzLnByb2R1Y3Rpb25zX1thY3Rpb25bMV1dWzBdKTtcbiAgICAgICAgICAgIHZzdGFjay5wdXNoKHl5dmFsLiQpO1xuICAgICAgICAgICAgbHN0YWNrLnB1c2goeXl2YWwuXyQpO1xuICAgICAgICAgICAgbmV3U3RhdGUgPSB0YWJsZVtzdGFja1tzdGFjay5sZW5ndGggLSAyXV1bc3RhY2tbc3RhY2subGVuZ3RoIC0gMV1dO1xuICAgICAgICAgICAgc3RhY2sucHVzaChuZXdTdGF0ZSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG59fTtcbi8qIGdlbmVyYXRlZCBieSBqaXNvbi1sZXggMC4zLjQgKi9cbnZhciBsZXhlciA9IChmdW5jdGlvbigpe1xudmFyIGxleGVyID0gKHtcblxuRU9GOjEsXG5cbnBhcnNlRXJyb3I6ZnVuY3Rpb24gcGFyc2VFcnJvcihzdHIsIGhhc2gpIHtcbiAgICAgICAgaWYgKHRoaXMueXkucGFyc2VyKSB7XG4gICAgICAgICAgICB0aGlzLnl5LnBhcnNlci5wYXJzZUVycm9yKHN0ciwgaGFzaCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3Ioc3RyKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbi8vIHJlc2V0cyB0aGUgbGV4ZXIsIHNldHMgbmV3IGlucHV0XG5zZXRJbnB1dDpmdW5jdGlvbiAoaW5wdXQsIHl5KSB7XG4gICAgICAgIHRoaXMueXkgPSB5eSB8fCB0aGlzLnl5IHx8IHt9O1xuICAgICAgICB0aGlzLl9pbnB1dCA9IGlucHV0O1xuICAgICAgICB0aGlzLl9tb3JlID0gdGhpcy5fYmFja3RyYWNrID0gdGhpcy5kb25lID0gZmFsc2U7XG4gICAgICAgIHRoaXMueXlsaW5lbm8gPSB0aGlzLnl5bGVuZyA9IDA7XG4gICAgICAgIHRoaXMueXl0ZXh0ID0gdGhpcy5tYXRjaGVkID0gdGhpcy5tYXRjaCA9ICcnO1xuICAgICAgICB0aGlzLmNvbmRpdGlvblN0YWNrID0gWydJTklUSUFMJ107XG4gICAgICAgIHRoaXMueXlsbG9jID0ge1xuICAgICAgICAgICAgZmlyc3RfbGluZTogMSxcbiAgICAgICAgICAgIGZpcnN0X2NvbHVtbjogMCxcbiAgICAgICAgICAgIGxhc3RfbGluZTogMSxcbiAgICAgICAgICAgIGxhc3RfY29sdW1uOiAwXG4gICAgICAgIH07XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMucmFuZ2VzKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGxvYy5yYW5nZSA9IFswLDBdO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2Zmc2V0ID0gMDtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfSxcblxuLy8gY29uc3VtZXMgYW5kIHJldHVybnMgb25lIGNoYXIgZnJvbSB0aGUgaW5wdXRcbmlucHV0OmZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGNoID0gdGhpcy5faW5wdXRbMF07XG4gICAgICAgIHRoaXMueXl0ZXh0ICs9IGNoO1xuICAgICAgICB0aGlzLnl5bGVuZysrO1xuICAgICAgICB0aGlzLm9mZnNldCsrO1xuICAgICAgICB0aGlzLm1hdGNoICs9IGNoO1xuICAgICAgICB0aGlzLm1hdGNoZWQgKz0gY2g7XG4gICAgICAgIHZhciBsaW5lcyA9IGNoLm1hdGNoKC8oPzpcXHJcXG4/fFxcbikuKi9nKTtcbiAgICAgICAgaWYgKGxpbmVzKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGluZW5vKys7XG4gICAgICAgICAgICB0aGlzLnl5bGxvYy5sYXN0X2xpbmUrKztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLmxhc3RfY29sdW1uKys7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5yYW5nZXMpIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLnJhbmdlWzFdKys7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9pbnB1dCA9IHRoaXMuX2lucHV0LnNsaWNlKDEpO1xuICAgICAgICByZXR1cm4gY2g7XG4gICAgfSxcblxuLy8gdW5zaGlmdHMgb25lIGNoYXIgKG9yIGEgc3RyaW5nKSBpbnRvIHRoZSBpbnB1dFxudW5wdXQ6ZnVuY3Rpb24gKGNoKSB7XG4gICAgICAgIHZhciBsZW4gPSBjaC5sZW5ndGg7XG4gICAgICAgIHZhciBsaW5lcyA9IGNoLnNwbGl0KC8oPzpcXHJcXG4/fFxcbikvZyk7XG5cbiAgICAgICAgdGhpcy5faW5wdXQgPSBjaCArIHRoaXMuX2lucHV0O1xuICAgICAgICB0aGlzLnl5dGV4dCA9IHRoaXMueXl0ZXh0LnN1YnN0cigwLCB0aGlzLnl5dGV4dC5sZW5ndGggLSBsZW4pO1xuICAgICAgICAvL3RoaXMueXlsZW5nIC09IGxlbjtcbiAgICAgICAgdGhpcy5vZmZzZXQgLT0gbGVuO1xuICAgICAgICB2YXIgb2xkTGluZXMgPSB0aGlzLm1hdGNoLnNwbGl0KC8oPzpcXHJcXG4/fFxcbikvZyk7XG4gICAgICAgIHRoaXMubWF0Y2ggPSB0aGlzLm1hdGNoLnN1YnN0cigwLCB0aGlzLm1hdGNoLmxlbmd0aCAtIDEpO1xuICAgICAgICB0aGlzLm1hdGNoZWQgPSB0aGlzLm1hdGNoZWQuc3Vic3RyKDAsIHRoaXMubWF0Y2hlZC5sZW5ndGggLSAxKTtcblxuICAgICAgICBpZiAobGluZXMubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgdGhpcy55eWxpbmVubyAtPSBsaW5lcy5sZW5ndGggLSAxO1xuICAgICAgICB9XG4gICAgICAgIHZhciByID0gdGhpcy55eWxsb2MucmFuZ2U7XG5cbiAgICAgICAgdGhpcy55eWxsb2MgPSB7XG4gICAgICAgICAgICBmaXJzdF9saW5lOiB0aGlzLnl5bGxvYy5maXJzdF9saW5lLFxuICAgICAgICAgICAgbGFzdF9saW5lOiB0aGlzLnl5bGluZW5vICsgMSxcbiAgICAgICAgICAgIGZpcnN0X2NvbHVtbjogdGhpcy55eWxsb2MuZmlyc3RfY29sdW1uLFxuICAgICAgICAgICAgbGFzdF9jb2x1bW46IGxpbmVzID9cbiAgICAgICAgICAgICAgICAobGluZXMubGVuZ3RoID09PSBvbGRMaW5lcy5sZW5ndGggPyB0aGlzLnl5bGxvYy5maXJzdF9jb2x1bW4gOiAwKVxuICAgICAgICAgICAgICAgICArIG9sZExpbmVzW29sZExpbmVzLmxlbmd0aCAtIGxpbmVzLmxlbmd0aF0ubGVuZ3RoIC0gbGluZXNbMF0ubGVuZ3RoIDpcbiAgICAgICAgICAgICAgdGhpcy55eWxsb2MuZmlyc3RfY29sdW1uIC0gbGVuXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5yYW5nZXMpIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLnJhbmdlID0gW3JbMF0sIHJbMF0gKyB0aGlzLnl5bGVuZyAtIGxlbl07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy55eWxlbmcgPSB0aGlzLnl5dGV4dC5sZW5ndGg7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sXG5cbi8vIFdoZW4gY2FsbGVkIGZyb20gYWN0aW9uLCBjYWNoZXMgbWF0Y2hlZCB0ZXh0IGFuZCBhcHBlbmRzIGl0IG9uIG5leHQgYWN0aW9uXG5tb3JlOmZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5fbW9yZSA9IHRydWU7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sXG5cbi8vIFdoZW4gY2FsbGVkIGZyb20gYWN0aW9uLCBzaWduYWxzIHRoZSBsZXhlciB0aGF0IHRoaXMgcnVsZSBmYWlscyB0byBtYXRjaCB0aGUgaW5wdXQsIHNvIHRoZSBuZXh0IG1hdGNoaW5nIHJ1bGUgKHJlZ2V4KSBzaG91bGQgYmUgdGVzdGVkIGluc3RlYWQuXG5yZWplY3Q6ZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmJhY2t0cmFja19sZXhlcikge1xuICAgICAgICAgICAgdGhpcy5fYmFja3RyYWNrID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlRXJyb3IoJ0xleGljYWwgZXJyb3Igb24gbGluZSAnICsgKHRoaXMueXlsaW5lbm8gKyAxKSArICcuIFlvdSBjYW4gb25seSBpbnZva2UgcmVqZWN0KCkgaW4gdGhlIGxleGVyIHdoZW4gdGhlIGxleGVyIGlzIG9mIHRoZSBiYWNrdHJhY2tpbmcgcGVyc3Vhc2lvbiAob3B0aW9ucy5iYWNrdHJhY2tfbGV4ZXIgPSB0cnVlKS5cXG4nICsgdGhpcy5zaG93UG9zaXRpb24oKSwge1xuICAgICAgICAgICAgICAgIHRleHQ6IFwiXCIsXG4gICAgICAgICAgICAgICAgdG9rZW46IG51bGwsXG4gICAgICAgICAgICAgICAgbGluZTogdGhpcy55eWxpbmVub1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9LFxuXG4vLyByZXRhaW4gZmlyc3QgbiBjaGFyYWN0ZXJzIG9mIHRoZSBtYXRjaFxubGVzczpmdW5jdGlvbiAobikge1xuICAgICAgICB0aGlzLnVucHV0KHRoaXMubWF0Y2guc2xpY2UobikpO1xuICAgIH0sXG5cbi8vIGRpc3BsYXlzIGFscmVhZHkgbWF0Y2hlZCBpbnB1dCwgaS5lLiBmb3IgZXJyb3IgbWVzc2FnZXNcbnBhc3RJbnB1dDpmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwYXN0ID0gdGhpcy5tYXRjaGVkLnN1YnN0cigwLCB0aGlzLm1hdGNoZWQubGVuZ3RoIC0gdGhpcy5tYXRjaC5sZW5ndGgpO1xuICAgICAgICByZXR1cm4gKHBhc3QubGVuZ3RoID4gMjAgPyAnLi4uJzonJykgKyBwYXN0LnN1YnN0cigtMjApLnJlcGxhY2UoL1xcbi9nLCBcIlwiKTtcbiAgICB9LFxuXG4vLyBkaXNwbGF5cyB1cGNvbWluZyBpbnB1dCwgaS5lLiBmb3IgZXJyb3IgbWVzc2FnZXNcbnVwY29taW5nSW5wdXQ6ZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgbmV4dCA9IHRoaXMubWF0Y2g7XG4gICAgICAgIGlmIChuZXh0Lmxlbmd0aCA8IDIwKSB7XG4gICAgICAgICAgICBuZXh0ICs9IHRoaXMuX2lucHV0LnN1YnN0cigwLCAyMC1uZXh0Lmxlbmd0aCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIChuZXh0LnN1YnN0cigwLDIwKSArIChuZXh0Lmxlbmd0aCA+IDIwID8gJy4uLicgOiAnJykpLnJlcGxhY2UoL1xcbi9nLCBcIlwiKTtcbiAgICB9LFxuXG4vLyBkaXNwbGF5cyB0aGUgY2hhcmFjdGVyIHBvc2l0aW9uIHdoZXJlIHRoZSBsZXhpbmcgZXJyb3Igb2NjdXJyZWQsIGkuZS4gZm9yIGVycm9yIG1lc3NhZ2VzXG5zaG93UG9zaXRpb246ZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcHJlID0gdGhpcy5wYXN0SW5wdXQoKTtcbiAgICAgICAgdmFyIGMgPSBuZXcgQXJyYXkocHJlLmxlbmd0aCArIDEpLmpvaW4oXCItXCIpO1xuICAgICAgICByZXR1cm4gcHJlICsgdGhpcy51cGNvbWluZ0lucHV0KCkgKyBcIlxcblwiICsgYyArIFwiXlwiO1xuICAgIH0sXG5cbi8vIHRlc3QgdGhlIGxleGVkIHRva2VuOiByZXR1cm4gRkFMU0Ugd2hlbiBub3QgYSBtYXRjaCwgb3RoZXJ3aXNlIHJldHVybiB0b2tlblxudGVzdF9tYXRjaDpmdW5jdGlvbihtYXRjaCwgaW5kZXhlZF9ydWxlKSB7XG4gICAgICAgIHZhciB0b2tlbixcbiAgICAgICAgICAgIGxpbmVzLFxuICAgICAgICAgICAgYmFja3VwO1xuXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYmFja3RyYWNrX2xleGVyKSB7XG4gICAgICAgICAgICAvLyBzYXZlIGNvbnRleHRcbiAgICAgICAgICAgIGJhY2t1cCA9IHtcbiAgICAgICAgICAgICAgICB5eWxpbmVubzogdGhpcy55eWxpbmVubyxcbiAgICAgICAgICAgICAgICB5eWxsb2M6IHtcbiAgICAgICAgICAgICAgICAgICAgZmlyc3RfbGluZTogdGhpcy55eWxsb2MuZmlyc3RfbGluZSxcbiAgICAgICAgICAgICAgICAgICAgbGFzdF9saW5lOiB0aGlzLmxhc3RfbGluZSxcbiAgICAgICAgICAgICAgICAgICAgZmlyc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5maXJzdF9jb2x1bW4sXG4gICAgICAgICAgICAgICAgICAgIGxhc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5sYXN0X2NvbHVtblxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgeXl0ZXh0OiB0aGlzLnl5dGV4dCxcbiAgICAgICAgICAgICAgICBtYXRjaDogdGhpcy5tYXRjaCxcbiAgICAgICAgICAgICAgICBtYXRjaGVzOiB0aGlzLm1hdGNoZXMsXG4gICAgICAgICAgICAgICAgbWF0Y2hlZDogdGhpcy5tYXRjaGVkLFxuICAgICAgICAgICAgICAgIHl5bGVuZzogdGhpcy55eWxlbmcsXG4gICAgICAgICAgICAgICAgb2Zmc2V0OiB0aGlzLm9mZnNldCxcbiAgICAgICAgICAgICAgICBfbW9yZTogdGhpcy5fbW9yZSxcbiAgICAgICAgICAgICAgICBfaW5wdXQ6IHRoaXMuX2lucHV0LFxuICAgICAgICAgICAgICAgIHl5OiB0aGlzLnl5LFxuICAgICAgICAgICAgICAgIGNvbmRpdGlvblN0YWNrOiB0aGlzLmNvbmRpdGlvblN0YWNrLnNsaWNlKDApLFxuICAgICAgICAgICAgICAgIGRvbmU6IHRoaXMuZG9uZVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMucmFuZ2VzKSB7XG4gICAgICAgICAgICAgICAgYmFja3VwLnl5bGxvYy5yYW5nZSA9IHRoaXMueXlsbG9jLnJhbmdlLnNsaWNlKDApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgbGluZXMgPSBtYXRjaFswXS5tYXRjaCgvKD86XFxyXFxuP3xcXG4pLiovZyk7XG4gICAgICAgIGlmIChsaW5lcykge1xuICAgICAgICAgICAgdGhpcy55eWxpbmVubyArPSBsaW5lcy5sZW5ndGg7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy55eWxsb2MgPSB7XG4gICAgICAgICAgICBmaXJzdF9saW5lOiB0aGlzLnl5bGxvYy5sYXN0X2xpbmUsXG4gICAgICAgICAgICBsYXN0X2xpbmU6IHRoaXMueXlsaW5lbm8gKyAxLFxuICAgICAgICAgICAgZmlyc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5sYXN0X2NvbHVtbixcbiAgICAgICAgICAgIGxhc3RfY29sdW1uOiBsaW5lcyA/XG4gICAgICAgICAgICAgICAgICAgICAgICAgbGluZXNbbGluZXMubGVuZ3RoIC0gMV0ubGVuZ3RoIC0gbGluZXNbbGluZXMubGVuZ3RoIC0gMV0ubWF0Y2goL1xccj9cXG4/LylbMF0ubGVuZ3RoIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnl5bGxvYy5sYXN0X2NvbHVtbiArIG1hdGNoWzBdLmxlbmd0aFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnl5dGV4dCArPSBtYXRjaFswXTtcbiAgICAgICAgdGhpcy5tYXRjaCArPSBtYXRjaFswXTtcbiAgICAgICAgdGhpcy5tYXRjaGVzID0gbWF0Y2g7XG4gICAgICAgIHRoaXMueXlsZW5nID0gdGhpcy55eXRleHQubGVuZ3RoO1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zLnJhbmdlcykge1xuICAgICAgICAgICAgdGhpcy55eWxsb2MucmFuZ2UgPSBbdGhpcy5vZmZzZXQsIHRoaXMub2Zmc2V0ICs9IHRoaXMueXlsZW5nXTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9tb3JlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2JhY2t0cmFjayA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9pbnB1dCA9IHRoaXMuX2lucHV0LnNsaWNlKG1hdGNoWzBdLmxlbmd0aCk7XG4gICAgICAgIHRoaXMubWF0Y2hlZCArPSBtYXRjaFswXTtcbiAgICAgICAgdG9rZW4gPSB0aGlzLnBlcmZvcm1BY3Rpb24uY2FsbCh0aGlzLCB0aGlzLnl5LCB0aGlzLCBpbmRleGVkX3J1bGUsIHRoaXMuY29uZGl0aW9uU3RhY2tbdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGggLSAxXSk7XG4gICAgICAgIGlmICh0aGlzLmRvbmUgJiYgdGhpcy5faW5wdXQpIHtcbiAgICAgICAgICAgIHRoaXMuZG9uZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0b2tlbikge1xuICAgICAgICAgICAgcmV0dXJuIHRva2VuO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuX2JhY2t0cmFjaykge1xuICAgICAgICAgICAgLy8gcmVjb3ZlciBjb250ZXh0XG4gICAgICAgICAgICBmb3IgKHZhciBrIGluIGJhY2t1cCkge1xuICAgICAgICAgICAgICAgIHRoaXNba10gPSBiYWNrdXBba107XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7IC8vIHJ1bGUgYWN0aW9uIGNhbGxlZCByZWplY3QoKSBpbXBseWluZyB0aGUgbmV4dCBydWxlIHNob3VsZCBiZSB0ZXN0ZWQgaW5zdGVhZC5cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSxcblxuLy8gcmV0dXJuIG5leHQgbWF0Y2ggaW4gaW5wdXRcbm5leHQ6ZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5kb25lKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5FT0Y7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLl9pbnB1dCkge1xuICAgICAgICAgICAgdGhpcy5kb25lID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciB0b2tlbixcbiAgICAgICAgICAgIG1hdGNoLFxuICAgICAgICAgICAgdGVtcE1hdGNoLFxuICAgICAgICAgICAgaW5kZXg7XG4gICAgICAgIGlmICghdGhpcy5fbW9yZSkge1xuICAgICAgICAgICAgdGhpcy55eXRleHQgPSAnJztcbiAgICAgICAgICAgIHRoaXMubWF0Y2ggPSAnJztcbiAgICAgICAgfVxuICAgICAgICB2YXIgcnVsZXMgPSB0aGlzLl9jdXJyZW50UnVsZXMoKTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBydWxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdGVtcE1hdGNoID0gdGhpcy5faW5wdXQubWF0Y2godGhpcy5ydWxlc1tydWxlc1tpXV0pO1xuICAgICAgICAgICAgaWYgKHRlbXBNYXRjaCAmJiAoIW1hdGNoIHx8IHRlbXBNYXRjaFswXS5sZW5ndGggPiBtYXRjaFswXS5sZW5ndGgpKSB7XG4gICAgICAgICAgICAgICAgbWF0Y2ggPSB0ZW1wTWF0Y2g7XG4gICAgICAgICAgICAgICAgaW5kZXggPSBpO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYmFja3RyYWNrX2xleGVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHRva2VuID0gdGhpcy50ZXN0X21hdGNoKHRlbXBNYXRjaCwgcnVsZXNbaV0pO1xuICAgICAgICAgICAgICAgICAgICBpZiAodG9rZW4gIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5fYmFja3RyYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXRjaCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWU7IC8vIHJ1bGUgYWN0aW9uIGNhbGxlZCByZWplY3QoKSBpbXBseWluZyBhIHJ1bGUgTUlTbWF0Y2guXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBlbHNlOiB0aGlzIGlzIGEgbGV4ZXIgcnVsZSB3aGljaCBjb25zdW1lcyBpbnB1dCB3aXRob3V0IHByb2R1Y2luZyBhIHRva2VuIChlLmcuIHdoaXRlc3BhY2UpXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLm9wdGlvbnMuZmxleCkge1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgICAgICB0b2tlbiA9IHRoaXMudGVzdF9tYXRjaChtYXRjaCwgcnVsZXNbaW5kZXhdKTtcbiAgICAgICAgICAgIGlmICh0b2tlbiAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBlbHNlOiB0aGlzIGlzIGEgbGV4ZXIgcnVsZSB3aGljaCBjb25zdW1lcyBpbnB1dCB3aXRob3V0IHByb2R1Y2luZyBhIHRva2VuIChlLmcuIHdoaXRlc3BhY2UpXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX2lucHV0ID09PSBcIlwiKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5FT0Y7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wYXJzZUVycm9yKCdMZXhpY2FsIGVycm9yIG9uIGxpbmUgJyArICh0aGlzLnl5bGluZW5vICsgMSkgKyAnLiBVbnJlY29nbml6ZWQgdGV4dC5cXG4nICsgdGhpcy5zaG93UG9zaXRpb24oKSwge1xuICAgICAgICAgICAgICAgIHRleHQ6IFwiXCIsXG4gICAgICAgICAgICAgICAgdG9rZW46IG51bGwsXG4gICAgICAgICAgICAgICAgbGluZTogdGhpcy55eWxpbmVub1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyByZXR1cm4gbmV4dCBtYXRjaCB0aGF0IGhhcyBhIHRva2VuXG5sZXg6ZnVuY3Rpb24gbGV4ICgpIHtcbiAgICAgICAgdmFyIHIgPSB0aGlzLm5leHQoKTtcbiAgICAgICAgaWYgKHIpIHtcbiAgICAgICAgICAgIHJldHVybiByO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMubGV4KCk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyBhY3RpdmF0ZXMgYSBuZXcgbGV4ZXIgY29uZGl0aW9uIHN0YXRlIChwdXNoZXMgdGhlIG5ldyBsZXhlciBjb25kaXRpb24gc3RhdGUgb250byB0aGUgY29uZGl0aW9uIHN0YWNrKVxuYmVnaW46ZnVuY3Rpb24gYmVnaW4gKGNvbmRpdGlvbikge1xuICAgICAgICB0aGlzLmNvbmRpdGlvblN0YWNrLnB1c2goY29uZGl0aW9uKTtcbiAgICB9LFxuXG4vLyBwb3AgdGhlIHByZXZpb3VzbHkgYWN0aXZlIGxleGVyIGNvbmRpdGlvbiBzdGF0ZSBvZmYgdGhlIGNvbmRpdGlvbiBzdGFja1xucG9wU3RhdGU6ZnVuY3Rpb24gcG9wU3RhdGUgKCkge1xuICAgICAgICB2YXIgbiA9IHRoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMTtcbiAgICAgICAgaWYgKG4gPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jb25kaXRpb25TdGFjay5wb3AoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvblN0YWNrWzBdO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gcHJvZHVjZSB0aGUgbGV4ZXIgcnVsZSBzZXQgd2hpY2ggaXMgYWN0aXZlIGZvciB0aGUgY3VycmVudGx5IGFjdGl2ZSBsZXhlciBjb25kaXRpb24gc3RhdGVcbl9jdXJyZW50UnVsZXM6ZnVuY3Rpb24gX2N1cnJlbnRSdWxlcyAoKSB7XG4gICAgICAgIGlmICh0aGlzLmNvbmRpdGlvblN0YWNrLmxlbmd0aCAmJiB0aGlzLmNvbmRpdGlvblN0YWNrW3RoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMV0pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvbnNbdGhpcy5jb25kaXRpb25TdGFja1t0aGlzLmNvbmRpdGlvblN0YWNrLmxlbmd0aCAtIDFdXS5ydWxlcztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvbnNbXCJJTklUSUFMXCJdLnJ1bGVzO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gcmV0dXJuIHRoZSBjdXJyZW50bHkgYWN0aXZlIGxleGVyIGNvbmRpdGlvbiBzdGF0ZTsgd2hlbiBhbiBpbmRleCBhcmd1bWVudCBpcyBwcm92aWRlZCBpdCBwcm9kdWNlcyB0aGUgTi10aCBwcmV2aW91cyBjb25kaXRpb24gc3RhdGUsIGlmIGF2YWlsYWJsZVxudG9wU3RhdGU6ZnVuY3Rpb24gdG9wU3RhdGUgKG4pIHtcbiAgICAgICAgbiA9IHRoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMSAtIE1hdGguYWJzKG4gfHwgMCk7XG4gICAgICAgIGlmIChuID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvblN0YWNrW25dO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIFwiSU5JVElBTFwiO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gYWxpYXMgZm9yIGJlZ2luKGNvbmRpdGlvbilcbnB1c2hTdGF0ZTpmdW5jdGlvbiBwdXNoU3RhdGUgKGNvbmRpdGlvbikge1xuICAgICAgICB0aGlzLmJlZ2luKGNvbmRpdGlvbik7XG4gICAgfSxcblxuLy8gcmV0dXJuIHRoZSBudW1iZXIgb2Ygc3RhdGVzIGN1cnJlbnRseSBvbiB0aGUgc3RhY2tcbnN0YXRlU3RhY2tTaXplOmZ1bmN0aW9uIHN0YXRlU3RhY2tTaXplKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGg7XG4gICAgfSxcbm9wdGlvbnM6IHt9LFxucGVyZm9ybUFjdGlvbjogZnVuY3Rpb24gYW5vbnltb3VzKHl5LHl5XywkYXZvaWRpbmdfbmFtZV9jb2xsaXNpb25zLFlZX1NUQVJUKSB7XG52YXIgWVlTVEFURT1ZWV9TVEFSVDtcbnN3aXRjaCgkYXZvaWRpbmdfbmFtZV9jb2xsaXNpb25zKSB7XG5jYXNlIDA6Lyogc2tpcCB3aGl0ZXNwYWNlICovXG5icmVhaztcbmNhc2UgMTp5eV8ueXl0ZXh0ID0geXlfLnl5dGV4dC5zdWJzdHIoMSx5eV8ueXlsZW5nLTIpOyByZXR1cm4gMzBcbmJyZWFrO1xuY2FzZSAyOnJldHVybiA3XG5icmVhaztcbmNhc2UgMzpyZXR1cm4gMjJcbmJyZWFrO1xuY2FzZSA0OnJldHVybiAxMlxuYnJlYWs7XG5jYXNlIDU6eXlfLnl5dGV4dCA9IHl5Xy55eXRleHQuc3Vic3RyKDEseXlfLnl5bGVuZy0xKTsgcmV0dXJuIDMyXG5icmVhaztcbmNhc2UgNjpyZXR1cm4gMTFcbmJyZWFrO1xuY2FzZSA3OnJldHVybiAyN1xuYnJlYWs7XG5jYXNlIDg6cmV0dXJuIDhcbmJyZWFrO1xuY2FzZSA5OnJldHVybiAxNVxuYnJlYWs7XG5jYXNlIDEwOnJldHVybiAxNlxuYnJlYWs7XG5jYXNlIDExOnJldHVybiAxN1xuYnJlYWs7XG5jYXNlIDEyOnJldHVybiAxOFxuYnJlYWs7XG5jYXNlIDEzOnJldHVybiAxOVxuYnJlYWs7XG5jYXNlIDE0OnJldHVybiAyMFxuYnJlYWs7XG5jYXNlIDE1OnJldHVybiAyNVxuYnJlYWs7XG5jYXNlIDE2OnJldHVybiAzM1xuYnJlYWs7XG5jYXNlIDE3OnJldHVybiAyOFxuYnJlYWs7XG5jYXNlIDE4OnJldHVybiAyM1xuYnJlYWs7XG5jYXNlIDE5OnJldHVybiAyNFxuYnJlYWs7XG5jYXNlIDIwOnJldHVybiAxNFxuYnJlYWs7XG5jYXNlIDIxOnJldHVybiAzMVxuYnJlYWs7XG5jYXNlIDIyOnJldHVybiAxMFxuYnJlYWs7XG5jYXNlIDIzOnJldHVybiAxM1xuYnJlYWs7XG5jYXNlIDI0OnJldHVybiAyOVxuYnJlYWs7XG5jYXNlIDI1OnJldHVybiAyNlxuYnJlYWs7XG5jYXNlIDI2OnJldHVybiAyMVxuYnJlYWs7XG5jYXNlIDI3OnJldHVybiAnSU5WQUxJRCdcbmJyZWFrO1xuY2FzZSAyODpyZXR1cm4gNVxuYnJlYWs7XG59XG59LFxucnVsZXM6IFsvXig/OlxccyspLywvXig/OlwiKD86XFxcXFtcXFxcXCJiZm5ydFxcL118XFxcXHVbYS1mQS1GMC05XXs0fXxbXlxcXFxcXDAtXFx4MDlcXHgwYS1cXHgxZlwiXXxcXG4pKlwiKS8sL14oPzpcXCRbYS16QS1aMC05XSspLywvXig/OlswLTldKykvLC9eKD86I1thLXpBLVowLTldKykvLC9eKD86XFxeI1thLXpBLVowLTldKykvLC9eKD86PD0pLywvXig/Oj0+KS8sL14oPzo9KS8sL14oPzpcXCgpLywvXig/OlxcKSkvLC9eKD86XFxbKS8sL14oPzpcXF0pLywvXig/OlxceykvLC9eKD86XFx9KS8sL14oPzpcXF5VXFxiKS8sL14oPzpcXF4jIykvLC9eKD86XFxeKS8sL14oPzo8KS8sL14oPzo+KS8sL14oPzpcXHwpLywvXig/Ol9cXGIpLywvXig/OjspLywvXig/OlxcKykvLC9eKD86LSkvLC9eKD86ISkvLC9eKD86XFwqKS8sL14oPzouKS8sL14oPzokKS9dLFxuY29uZGl0aW9uczoge1wiSU5JVElBTFwiOntcInJ1bGVzXCI6WzAsMSwyLDMsNCw1LDYsNyw4LDksMTAsMTEsMTIsMTMsMTQsMTUsMTYsMTcsMTgsMTksMjAsMjEsMjIsMjMsMjQsMjUsMjYsMjcsMjhdLFwiaW5jbHVzaXZlXCI6dHJ1ZX19XG59KTtcbnJldHVybiBsZXhlcjtcbn0pKCk7XG5wYXJzZXIubGV4ZXIgPSBsZXhlcjtcbmZ1bmN0aW9uIFBhcnNlciAoKSB7XG4gIHRoaXMueXkgPSB7fTtcbn1cblBhcnNlci5wcm90b3R5cGUgPSBwYXJzZXI7cGFyc2VyLlBhcnNlciA9IFBhcnNlcjtcbnJldHVybiBuZXcgUGFyc2VyO1xufSkoKTtcblxuXG5pZiAodHlwZW9mIHJlcXVpcmUgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBleHBvcnRzICE9PSAndW5kZWZpbmVkJykge1xuZXhwb3J0cy5wYXJzZXIgPSBwYXJzZXI7XG5leHBvcnRzLlBhcnNlciA9IHBhcnNlci5QYXJzZXI7XG5leHBvcnRzLnBhcnNlID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gcGFyc2VyLnBhcnNlLmFwcGx5KHBhcnNlciwgYXJndW1lbnRzKTsgfTtcbmV4cG9ydHMubWFpbiA9IGZ1bmN0aW9uIGNvbW1vbmpzTWFpbiAoYXJncykge1xuICAgIGlmICghYXJnc1sxXSkge1xuICAgICAgICBjb25zb2xlLmxvZygnVXNhZ2U6ICcrYXJnc1swXSsnIEZJTEUnKTtcbiAgICAgICAgcHJvY2Vzcy5leGl0KDEpO1xuICAgIH1cbiAgICB2YXIgc291cmNlID0gcmVxdWlyZSgnZnMnKS5yZWFkRmlsZVN5bmMocmVxdWlyZSgncGF0aCcpLm5vcm1hbGl6ZShhcmdzWzFdKSwgXCJ1dGY4XCIpO1xuICAgIHJldHVybiBleHBvcnRzLnBhcnNlci5wYXJzZShzb3VyY2UpO1xufTtcbmlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiByZXF1aXJlLm1haW4gPT09IG1vZHVsZSkge1xuICBleHBvcnRzLm1haW4ocHJvY2Vzcy5hcmd2LnNsaWNlKDEpKTtcbn1cbn0iLCIvKlxuICAgIEluc3RhR3JhbW1hclRlbXBsYXRlXG4qL1xuXG5pbXBvcnQgeyBJbnN0YUdyYW1tYXIgfSBmcm9tICcuLi9pZy9lbmdpbmUnO1xuXG4vKlxuICAgIEluc3RhR3JhbW1hclRlbXBsYXRlIGlzIGEgY29udmVuaWVuY2UgY2xhc3MgdG8gYmUgdXNlZCB3aGVuIHlvdSBoYXZlIGEgZ2VuZXJhbCBwdXJwb3Nlc1xuICAgIHRleHQgKHRlbXBsYXRlKSB3aXRoIG9jY2FzaW9uYWwgJHZhcmlhYmxlcyBpbnNpZGUuXG4qL1xuY2xhc3MgSW5zdGFHcmFtbWFyVGVtcGxhdGUge1xuICAgIHJlYWRvbmx5IHRlbXBsYXRlOiBzdHJpbmc7XG4gICAgcmVhZG9ubHkgZ3JhbW1hcjogc3RyaW5nO1xuICAgIHJlYWRvbmx5IGluc3RhR3JhbW1hcjogSW5zdGFHcmFtbWFyO1xuICAgIG91dHB1dDogc3RyaW5nO1xuXG4gICAgLypcbiAgICAgICAgY29udHJ1Y3RvciB3YW50cyBhIHRlbXBsYXRlIGFuZCBhIGdyYW1tYXIuXG4gICAgICAgIHRoaXMgbWV0aG9kIGNhbiB0aHJvdyBhbGwgdGhlIGV4ZWN0aW9uIG9mIEluc3RhR3JhbW1hciBjb250cnVjdG9yXG4gICAgKi9cbiAgICBjb25zdHJ1Y3Rvcih0ZW1wbGF0ZTogc3RyaW5nLCBncmFtbWFyOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5ncmFtbWFyID0gZ3JhbW1hcjtcbiAgICAgICAgdGhpcy50ZW1wbGF0ZSA9IHRlbXBsYXRlO1xuICAgICAgICB0aGlzLmluc3RhR3JhbW1hciA9IG5ldyBJbnN0YUdyYW1tYXIoZ3JhbW1hcik7XG4gICAgICAgIHRoaXMub3V0cHV0ID0gXCJcIjtcbiAgICB9XG5cbiAgICAvKlxuICAgICAgICBnZW5lcmF0ZSBpcyB0aGUgbWV0aG9kIHRvIGNhbGwgb24gYSBJbnN0YUdyYW1tYXJUZW1wbGF0ZSBvYmplY3QuXG4gICAgICAgIEl0IHVzZXMgc2ltcGxlIFJlZ0V4cCB0byBmaW5kICR2YXJpYWJsZXMgaW4gdGhlIHRlbXBsYXRlLCBhbmQgY29udmVydGluZyB0aGVtXG4gICAgICAgIGluIGEgZ2VuZXJhdGUgdGV4dCwgcmV1c2luZyB0aGUgc3Ryb25nQmluZGluZ3MuXG4gICAgKi9cbiAgICBnZW5lcmF0ZSgpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy50ZW1wbGF0ZSA9PSAnJykge1xuICAgICAgICAgICAgdGhpcy5vdXRwdXQgPSB0aGlzLmluc3RhR3JhbW1hci5nZW5lcmF0ZSgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuZ3JhbW1hciA9PSAnJykge1xuICAgICAgICAgICAgdGhpcy5vdXRwdXQgPSB0aGlzLnRlbXBsYXRlO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgLy8gcmVzZXR0aW5nIHN0cm9uZ0JpbmRpbmdzIGJlZm9yZSBldmVyeXRoaW5nIGVsc2VcbiAgICAgICAgICAgIGlmICh0aGlzLmluc3RhR3JhbW1hci5lbnYgJiYgdGhpcy5pbnN0YUdyYW1tYXIuZW52LnN0cm9uZ0JpbmRpbmdzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbnN0YUdyYW1tYXIuZW52LnN0cm9uZ0JpbmRpbmdzID0ge307XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBsZXQgaW5zdGFHcmFtbWFyID0gdGhpcy5pbnN0YUdyYW1tYXI7XG4gICAgICAgICAgICBsZXQgZ2VuZXJhdGVTeW1ib2wgPSBmdW5jdGlvbihuYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgICAgICAgICAvLyB0cnVlID0ga2VlcFN0cm9uZ0JpbmRpbmdzXG4gICAgICAgICAgICAgICAgcmV0dXJuIGluc3RhR3JhbW1hci5nZW5lcmF0ZShuYW1lLCB0cnVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBzeW1ib2xzID0gbmV3IFJlZ0V4cCgnXFxcXCRbYS16QS1aMC05XSsnLCAnZycpO1xuICAgICAgICAgICAgdGhpcy5vdXRwdXQgPSB0aGlzLnRlbXBsYXRlLnJlcGxhY2Uoc3ltYm9scywgZ2VuZXJhdGVTeW1ib2wpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMub3V0cHV0O1xuICAgIH1cblxuICAgIC8qXG4gICAgICAgIENvbnZlbmllbmNlIG1ldGhvZFxuICAgICovXG4gICAgc3RhdGljIGdlbmVyYXRlKHRlbXBsYXRlOiBzdHJpbmcsIGdyYW1tYXI6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiBuZXcgSW5zdGFHcmFtbWFyVGVtcGxhdGUodGVtcGxhdGUsIGdyYW1tYXIpLmdlbmVyYXRlKCk7XG4gICAgfVxufVxuXG5leHBvcnQgeyBJbnN0YUdyYW1tYXJUZW1wbGF0ZSB9O1xuIiwiLypcbiAgICBQcmVQcm9jZXNzb3IgZW5naW5lXG5cbiAgICBHaXZlbiBhIGxleGVyIGFuZCBhIHNldCBvZiBydWxlcywgaXQgY29udmVydHMgYSBzdHJpbmcgaW50byBhIHByZXByb2Nlc3NlZCBzdHJpbmcuXG4gICAgVGhpcyBtb2R1bGUgaXMgYnVuZGxlZCBpbnRvIEluc3RhR3JhbW1hciwgYnV0IGl0IGNvdWxkIGJlIHN0YW5kYWxvbmUuXG5cblxuKi9cblxuaW1wb3J0IHsgcGFyc2UgYXMgcnVsZXNQYXJzZSB9IGZyb20gXCIuL3J1bGVzUGFyc2VyLmpzXCI7XG5cbmNsYXNzIFJ1bGUge1xuICAgIGZyb21TdGF0ZTogc3RyaW5nO1xuICAgIHRva2VuczogQXJyYXk8c3RyaW5nPjtcbiAgICB0b1N0YXRlPzogc3RyaW5nO1xuICAgIHRleHQ/OiBzdHJpbmc7XG4gICAgY29uc3VtZT86IG51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKGZyb21TdGF0ZTogc3RyaW5nLCBydWxlOmFueSkge1xuICAgICAgICB0aGlzLmZyb21TdGF0ZSA9IGZyb21TdGF0ZTtcbiAgICAgICAgdGhpcy50b2tlbnMgPSBydWxlLnRva2VucztcbiAgICAgICAgIGlmIChydWxlLnRoZW4pIHtcbiAgICAgICAgICAgIGlmIChydWxlLnRoZW4udGV4dCkge1xuICAgICAgICAgICAgICAgIHRoaXMudGV4dCA9IHJ1bGUudGhlbi50ZXh0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHJ1bGUudGhlbi5jb25zdW1lKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb25zdW1lID0gcnVsZS50aGVuLmNvbnN1bWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAocnVsZS50aGVuLnN0YXRlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy50b1N0YXRlID0gcnVsZS50aGVuLnN0YXRlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgUmV0dXJucyB0cnVlIGlmIHRva2VucyBtYXRjaGVzIHRoZSBoZWFkIG9mIHRoZSBzdGFja1xuICAgICovXG4gICAgbWF0Y2hlc1N0YWNrKHN0YWNrOiBBcnJheTxUb2tlbj4pOiBCb29sZWFuIHtcbiAgICAgICAgaWYgKHN0YWNrLmxlbmd0aCA+PSB0aGlzLnRva2Vucy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgaW4gdGhpcy50b2tlbnMpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50b2tlbnNbaV0gIT0gc3RhY2tbaV0ubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAvLyB0aGUgdG9rZW4gXCIqXCIgbWF0Y2hlcyB3aXRoIGFueXRoaW5nXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRva2Vuc1tpXSAhPSAnKicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG59XG5cbmNsYXNzIFRva2VuIHtcbiAgICByZWFkb25seSBuYW1lOiBzdHJpbmc7XG4gICAgcmVhZG9ubHkgdGV4dDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IobmFtZTpzdHJpbmcsIHRleHQ6c3RyaW5nKSB7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMudGV4dCA9IHRleHQ7XG4gICAgfVxufVxuXG5jbGFzcyBQcmVQcm9jZXNzb3Ige1xuICAgIHJlYWRvbmx5IGxleGVyOiBhbnk7XG4gICAgcmVhZG9ubHkgcnVsZXM6IEFycmF5PFJ1bGU+O1xuICAgIHJlYWRvbmx5IHN0YXJ0U3RhdGU6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKHJ1bGVzOnN0cmluZywgbGV4ZXI6IGFueSwgc3RhcnRTdGF0ZTpzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5ydWxlcyA9IHRoaXMucGFyc2VSdWxlcyhydWxlcyk7XG4gICAgICAgIHRoaXMubGV4ZXIgPSBsZXhlcjtcbiAgICAgICAgdGhpcy5zdGFydFN0YXRlID0gc3RhcnRTdGF0ZTtcbiAgICB9XG5cbiAgICAvKlxuICAgICAgICBDb252ZXJ0cyB0aGUgcGFyc2VyIEFTVCBpbnRvIGEgZmxhdHRlbiBhcnJheSBvZiBQUFJ1bGVzXG4gICAgKi9cbiAgICBwYXJzZVJ1bGVzKHJ1bGVzOiBzdHJpbmcpOiBBcnJheTxSdWxlPiB7XG4gICAgICAgIGxldCBsb2NhbFJ1bGVzID0gbmV3IEFycmF5KCk7XG4gICAgICAgIGxldCBydWxlc2V0cyA9IG51bGw7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBydWxlc2V0cyA9IHJ1bGVzUGFyc2UocnVsZXMpO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZS5tZXNzYWdlKTtcbiAgICAgICAgfVxuICAgICAgICBmb3IgKGxldCBydWxlc2V0IG9mIHJ1bGVzZXRzKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBmcm9tU3RhdGUgb2YgcnVsZXNldC5zdGF0ZXMpIHtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBydWxlIG9mIHJ1bGVzZXQucnVsZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgbG9jYWxSdWxlcy5wdXNoKG5ldyBSdWxlKGZyb21TdGF0ZSwgcnVsZSkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbG9jYWxSdWxlcztcbiAgICB9XG5cblxuICAgIC8qXG4gICAgICAgIENvdmVydHMgYSBzdGFjayBhbmQgYSB0ZW1wbGF0ZSBzdHJpbmcgaW50byBhIHN0cmluZ1xuICAgICAgICBFZy46IC0gVDIgVDMgVDQgVDEgVDQgPT4gXCJvayAkMSAkMiAkM1wiXG5cbiAgICAqL1xuICAgIHByaXZhdGUgcHJvY2Vzc1RleHQodG9rZW5zOkFycmF5PFRva2VuPiwgdGVtcGxhdGU6c3RyaW5nKTogc3RyaW5nIHtcbiAgICAgICAgbGV0IG91dHB1dCA9IHRlbXBsYXRlO1xuICAgICAgICBmb3IgKGxldCBpIGluIHRva2Vucykge1xuICAgICAgICAgICAgb3V0cHV0ID0gb3V0cHV0LnJlcGxhY2UoXCIkXCIraSwgdG9rZW5zW2ldLnRleHQpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBvdXRwdXQ7XG4gICAgfVxuXG4gICAgLypcbiAgICAgICAgQ29udmVydHMgdGhlIGlucHV0IHN0cmluZyBpbnRvIGFuIGFycmF5IG9mIHRva2Vuc1xuICAgICovXG4gICAgcHJpdmF0ZSBwYXJzZVRva2VucyhpbnB1dDpzdHJpbmcpOiBBcnJheTxUb2tlbj4ge1xuICAgICAgICBsZXQgdG9rZW5zID0gbmV3IEFycmF5KCk7XG4gICAgICAgIHRoaXMubGV4ZXIuc2V0SW5wdXQoaW5wdXQpO1xuICAgICAgICBsZXQgbmFtZSA9IFwiXCI7XG4gICAgICAgIHdoaWxlKChuYW1lID0gdGhpcy5sZXhlci5sZXgoKSkgIT0gMSkge1xuICAgICAgICAgICAgdG9rZW5zLnB1c2gobmV3IFRva2VuKG5hbWUsIHRoaXMubGV4ZXIueXl0ZXh0KSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRva2VucztcbiAgICB9XG5cbiAgICAvKlxuICAgICAgICBQcmVwcm9jZXNzIGFuIGlucHV0IHN0cmluZ1xuICAgICovXG4gICAgcHJvY2VzcyhpbnB1dDpzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBsZXQgdG9rZW5zID0gdGhpcy5wYXJzZVRva2VucyhpbnB1dCk7XG4gICAgICAgIGxldCBzdGFjayA9IFsuLi50b2tlbnNdO1xuICAgICAgICBsZXQgY3VycmVudFN0YXRlID0gdGhpcy5zdGFydFN0YXRlO1xuICAgICAgICBsZXQgb3V0cHV0ID0gXCJcIjtcbiAgICAgICAgd2hpbGUgKHN0YWNrLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIC8vIGZpbHRlcmVkIHJ1bGVzIGFjY29yZGluZyB0byBzdGF0ZVxuICAgICAgICAgICAgbGV0IGZpbHRlcmVkUnVsZXMgPSB0aGlzLnJ1bGVzLmZpbHRlcihmdW5jdGlvbihydWxlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChydWxlLmZyb21TdGF0ZSA9PSBjdXJyZW50U3RhdGUpIHx8IChydWxlLmZyb21TdGF0ZSA9PSAnKicpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBsZXQgZm91bmRSdWxlID0gZmFsc2U7XG5cbiAgICAgICAgICAgIGZvciAobGV0IGZpbHRlcmVkUnVsZSBvZiBmaWx0ZXJlZFJ1bGVzKSB7XG4gICAgICAgICAgICAgICAgaWYgKGZpbHRlcmVkUnVsZS5tYXRjaGVzU3RhY2soc3RhY2spKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHByb2Nlc3MgcnVsZVxuICAgICAgICAgICAgICAgICAgICBsZXQgb2xkU3RhdGUgPSBjdXJyZW50U3RhdGU7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gdGV4dCBuZWVkIHRvIGJlIHByb2Nlc3NlZFxuICAgICAgICAgICAgICAgICAgICBpZiAoZmlsdGVyZWRSdWxlLnRleHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG91dHB1dCArPSB0aGlzLnByb2Nlc3NUZXh0KHN0YWNrLnNsaWNlKDAsIGZpbHRlcmVkUnVsZS50b2tlbnMubGVuZ3RoKSwgZmlsdGVyZWRSdWxlLnRleHQpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gbW92aW5nIEZTTSB0byBuZXcgc3RhdGVcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpbHRlcmVkUnVsZS50b1N0YXRlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGUgPSBmaWx0ZXJlZFJ1bGUudG9TdGF0ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnN1bWluZyBpbnB1dCB0b2tlbnNcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRva2Vuc1RvQ29uc3VtZSA9IGZpbHRlcmVkUnVsZS50b2tlbnMubGVuZ3RoO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZmlsdGVyZWRSdWxlLmNvbnN1bWUgJiYgZmlsdGVyZWRSdWxlLmNvbnN1bWUgPCB0b2tlbnNUb0NvbnN1bWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRva2Vuc1RvQ29uc3VtZSA9IGZpbHRlcmVkUnVsZS5jb25zdW1lO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gcG9wcGluZyBjb25zdW1lZCB0b2tlbnMgb3V0IG9mIHN0YWNrXG4gICAgICAgICAgICAgICAgICAgIHN0YWNrID0gc3RhY2suc2xpY2UodG9rZW5zVG9Db25zdW1lLCBzdGFjay5sZW5ndGgpO1xuICAgICAgICAgICAgICAgICAgICBmb3VuZFJ1bGUgPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghZm91bmRSdWxlKSB7XG4gICAgICAgICAgICAgICAgbGV0IHByZXR0eVN0YWNrID0gXCJcIjtcbiAgICAgICAgICAgICAgICBsZXQgcHJldHR5U3RhY2tTaXplID0gc3RhY2subGVuZ3RoIDwgNSA/IHN0YWNrLmxlbmd0aCA6IDU7XG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGk8cHJldHR5U3RhY2tTaXplOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgcHJldHR5U3RhY2sgKz0gYCR7c3RhY2tbaV0ubmFtZX0gYDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbGV0IHMgPSBgUnVsZSBub3QgZm91bmQuIFN0YXRlOiAke2N1cnJlbnRTdGF0ZX0sIHN0YWNrOiAke3ByZXR0eVN0YWNrfWA7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBvdXRwdXQ7XG4gICAgfVxufVxuXG5leHBvcnQgeyBQcmVQcm9jZXNzb3IgfTtcbiIsIi8qIGdlbmVyYXRlZCBieSBqaXNvbi1sZXggMC4zLjQgKi9cbnZhciBsZXhlciA9IChmdW5jdGlvbigpe1xudmFyIGxleGVyID0gKHtcblxuRU9GOjEsXG5cbnBhcnNlRXJyb3I6ZnVuY3Rpb24gcGFyc2VFcnJvcihzdHIsIGhhc2gpIHtcbiAgICAgICAgaWYgKHRoaXMueXkucGFyc2VyKSB7XG4gICAgICAgICAgICB0aGlzLnl5LnBhcnNlci5wYXJzZUVycm9yKHN0ciwgaGFzaCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3Ioc3RyKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbi8vIHJlc2V0cyB0aGUgbGV4ZXIsIHNldHMgbmV3IGlucHV0XG5zZXRJbnB1dDpmdW5jdGlvbiAoaW5wdXQsIHl5KSB7XG4gICAgICAgIHRoaXMueXkgPSB5eSB8fCB0aGlzLnl5IHx8IHt9O1xuICAgICAgICB0aGlzLl9pbnB1dCA9IGlucHV0O1xuICAgICAgICB0aGlzLl9tb3JlID0gdGhpcy5fYmFja3RyYWNrID0gdGhpcy5kb25lID0gZmFsc2U7XG4gICAgICAgIHRoaXMueXlsaW5lbm8gPSB0aGlzLnl5bGVuZyA9IDA7XG4gICAgICAgIHRoaXMueXl0ZXh0ID0gdGhpcy5tYXRjaGVkID0gdGhpcy5tYXRjaCA9ICcnO1xuICAgICAgICB0aGlzLmNvbmRpdGlvblN0YWNrID0gWydJTklUSUFMJ107XG4gICAgICAgIHRoaXMueXlsbG9jID0ge1xuICAgICAgICAgICAgZmlyc3RfbGluZTogMSxcbiAgICAgICAgICAgIGZpcnN0X2NvbHVtbjogMCxcbiAgICAgICAgICAgIGxhc3RfbGluZTogMSxcbiAgICAgICAgICAgIGxhc3RfY29sdW1uOiAwXG4gICAgICAgIH07XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMucmFuZ2VzKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGxvYy5yYW5nZSA9IFswLDBdO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub2Zmc2V0ID0gMDtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfSxcblxuLy8gY29uc3VtZXMgYW5kIHJldHVybnMgb25lIGNoYXIgZnJvbSB0aGUgaW5wdXRcbmlucHV0OmZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGNoID0gdGhpcy5faW5wdXRbMF07XG4gICAgICAgIHRoaXMueXl0ZXh0ICs9IGNoO1xuICAgICAgICB0aGlzLnl5bGVuZysrO1xuICAgICAgICB0aGlzLm9mZnNldCsrO1xuICAgICAgICB0aGlzLm1hdGNoICs9IGNoO1xuICAgICAgICB0aGlzLm1hdGNoZWQgKz0gY2g7XG4gICAgICAgIHZhciBsaW5lcyA9IGNoLm1hdGNoKC8oPzpcXHJcXG4/fFxcbikuKi9nKTtcbiAgICAgICAgaWYgKGxpbmVzKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGluZW5vKys7XG4gICAgICAgICAgICB0aGlzLnl5bGxvYy5sYXN0X2xpbmUrKztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLmxhc3RfY29sdW1uKys7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5yYW5nZXMpIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLnJhbmdlWzFdKys7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9pbnB1dCA9IHRoaXMuX2lucHV0LnNsaWNlKDEpO1xuICAgICAgICByZXR1cm4gY2g7XG4gICAgfSxcblxuLy8gdW5zaGlmdHMgb25lIGNoYXIgKG9yIGEgc3RyaW5nKSBpbnRvIHRoZSBpbnB1dFxudW5wdXQ6ZnVuY3Rpb24gKGNoKSB7XG4gICAgICAgIHZhciBsZW4gPSBjaC5sZW5ndGg7XG4gICAgICAgIHZhciBsaW5lcyA9IGNoLnNwbGl0KC8oPzpcXHJcXG4/fFxcbikvZyk7XG5cbiAgICAgICAgdGhpcy5faW5wdXQgPSBjaCArIHRoaXMuX2lucHV0O1xuICAgICAgICB0aGlzLnl5dGV4dCA9IHRoaXMueXl0ZXh0LnN1YnN0cigwLCB0aGlzLnl5dGV4dC5sZW5ndGggLSBsZW4pO1xuICAgICAgICAvL3RoaXMueXlsZW5nIC09IGxlbjtcbiAgICAgICAgdGhpcy5vZmZzZXQgLT0gbGVuO1xuICAgICAgICB2YXIgb2xkTGluZXMgPSB0aGlzLm1hdGNoLnNwbGl0KC8oPzpcXHJcXG4/fFxcbikvZyk7XG4gICAgICAgIHRoaXMubWF0Y2ggPSB0aGlzLm1hdGNoLnN1YnN0cigwLCB0aGlzLm1hdGNoLmxlbmd0aCAtIDEpO1xuICAgICAgICB0aGlzLm1hdGNoZWQgPSB0aGlzLm1hdGNoZWQuc3Vic3RyKDAsIHRoaXMubWF0Y2hlZC5sZW5ndGggLSAxKTtcblxuICAgICAgICBpZiAobGluZXMubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgdGhpcy55eWxpbmVubyAtPSBsaW5lcy5sZW5ndGggLSAxO1xuICAgICAgICB9XG4gICAgICAgIHZhciByID0gdGhpcy55eWxsb2MucmFuZ2U7XG5cbiAgICAgICAgdGhpcy55eWxsb2MgPSB7XG4gICAgICAgICAgICBmaXJzdF9saW5lOiB0aGlzLnl5bGxvYy5maXJzdF9saW5lLFxuICAgICAgICAgICAgbGFzdF9saW5lOiB0aGlzLnl5bGluZW5vICsgMSxcbiAgICAgICAgICAgIGZpcnN0X2NvbHVtbjogdGhpcy55eWxsb2MuZmlyc3RfY29sdW1uLFxuICAgICAgICAgICAgbGFzdF9jb2x1bW46IGxpbmVzID9cbiAgICAgICAgICAgICAgICAobGluZXMubGVuZ3RoID09PSBvbGRMaW5lcy5sZW5ndGggPyB0aGlzLnl5bGxvYy5maXJzdF9jb2x1bW4gOiAwKVxuICAgICAgICAgICAgICAgICArIG9sZExpbmVzW29sZExpbmVzLmxlbmd0aCAtIGxpbmVzLmxlbmd0aF0ubGVuZ3RoIC0gbGluZXNbMF0ubGVuZ3RoIDpcbiAgICAgICAgICAgICAgdGhpcy55eWxsb2MuZmlyc3RfY29sdW1uIC0gbGVuXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5yYW5nZXMpIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLnJhbmdlID0gW3JbMF0sIHJbMF0gKyB0aGlzLnl5bGVuZyAtIGxlbl07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy55eWxlbmcgPSB0aGlzLnl5dGV4dC5sZW5ndGg7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sXG5cbi8vIFdoZW4gY2FsbGVkIGZyb20gYWN0aW9uLCBjYWNoZXMgbWF0Y2hlZCB0ZXh0IGFuZCBhcHBlbmRzIGl0IG9uIG5leHQgYWN0aW9uXG5tb3JlOmZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5fbW9yZSA9IHRydWU7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sXG5cbi8vIFdoZW4gY2FsbGVkIGZyb20gYWN0aW9uLCBzaWduYWxzIHRoZSBsZXhlciB0aGF0IHRoaXMgcnVsZSBmYWlscyB0byBtYXRjaCB0aGUgaW5wdXQsIHNvIHRoZSBuZXh0IG1hdGNoaW5nIHJ1bGUgKHJlZ2V4KSBzaG91bGQgYmUgdGVzdGVkIGluc3RlYWQuXG5yZWplY3Q6ZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmJhY2t0cmFja19sZXhlcikge1xuICAgICAgICAgICAgdGhpcy5fYmFja3RyYWNrID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlRXJyb3IoJ0xleGljYWwgZXJyb3Igb24gbGluZSAnICsgKHRoaXMueXlsaW5lbm8gKyAxKSArICcuIFlvdSBjYW4gb25seSBpbnZva2UgcmVqZWN0KCkgaW4gdGhlIGxleGVyIHdoZW4gdGhlIGxleGVyIGlzIG9mIHRoZSBiYWNrdHJhY2tpbmcgcGVyc3Vhc2lvbiAob3B0aW9ucy5iYWNrdHJhY2tfbGV4ZXIgPSB0cnVlKS5cXG4nICsgdGhpcy5zaG93UG9zaXRpb24oKSwge1xuICAgICAgICAgICAgICAgIHRleHQ6IFwiXCIsXG4gICAgICAgICAgICAgICAgdG9rZW46IG51bGwsXG4gICAgICAgICAgICAgICAgbGluZTogdGhpcy55eWxpbmVub1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9LFxuXG4vLyByZXRhaW4gZmlyc3QgbiBjaGFyYWN0ZXJzIG9mIHRoZSBtYXRjaFxubGVzczpmdW5jdGlvbiAobikge1xuICAgICAgICB0aGlzLnVucHV0KHRoaXMubWF0Y2guc2xpY2UobikpO1xuICAgIH0sXG5cbi8vIGRpc3BsYXlzIGFscmVhZHkgbWF0Y2hlZCBpbnB1dCwgaS5lLiBmb3IgZXJyb3IgbWVzc2FnZXNcbnBhc3RJbnB1dDpmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwYXN0ID0gdGhpcy5tYXRjaGVkLnN1YnN0cigwLCB0aGlzLm1hdGNoZWQubGVuZ3RoIC0gdGhpcy5tYXRjaC5sZW5ndGgpO1xuICAgICAgICByZXR1cm4gKHBhc3QubGVuZ3RoID4gMjAgPyAnLi4uJzonJykgKyBwYXN0LnN1YnN0cigtMjApLnJlcGxhY2UoL1xcbi9nLCBcIlwiKTtcbiAgICB9LFxuXG4vLyBkaXNwbGF5cyB1cGNvbWluZyBpbnB1dCwgaS5lLiBmb3IgZXJyb3IgbWVzc2FnZXNcbnVwY29taW5nSW5wdXQ6ZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgbmV4dCA9IHRoaXMubWF0Y2g7XG4gICAgICAgIGlmIChuZXh0Lmxlbmd0aCA8IDIwKSB7XG4gICAgICAgICAgICBuZXh0ICs9IHRoaXMuX2lucHV0LnN1YnN0cigwLCAyMC1uZXh0Lmxlbmd0aCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIChuZXh0LnN1YnN0cigwLDIwKSArIChuZXh0Lmxlbmd0aCA+IDIwID8gJy4uLicgOiAnJykpLnJlcGxhY2UoL1xcbi9nLCBcIlwiKTtcbiAgICB9LFxuXG4vLyBkaXNwbGF5cyB0aGUgY2hhcmFjdGVyIHBvc2l0aW9uIHdoZXJlIHRoZSBsZXhpbmcgZXJyb3Igb2NjdXJyZWQsIGkuZS4gZm9yIGVycm9yIG1lc3NhZ2VzXG5zaG93UG9zaXRpb246ZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcHJlID0gdGhpcy5wYXN0SW5wdXQoKTtcbiAgICAgICAgdmFyIGMgPSBuZXcgQXJyYXkocHJlLmxlbmd0aCArIDEpLmpvaW4oXCItXCIpO1xuICAgICAgICByZXR1cm4gcHJlICsgdGhpcy51cGNvbWluZ0lucHV0KCkgKyBcIlxcblwiICsgYyArIFwiXlwiO1xuICAgIH0sXG5cbi8vIHRlc3QgdGhlIGxleGVkIHRva2VuOiByZXR1cm4gRkFMU0Ugd2hlbiBub3QgYSBtYXRjaCwgb3RoZXJ3aXNlIHJldHVybiB0b2tlblxudGVzdF9tYXRjaDpmdW5jdGlvbihtYXRjaCwgaW5kZXhlZF9ydWxlKSB7XG4gICAgICAgIHZhciB0b2tlbixcbiAgICAgICAgICAgIGxpbmVzLFxuICAgICAgICAgICAgYmFja3VwO1xuXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYmFja3RyYWNrX2xleGVyKSB7XG4gICAgICAgICAgICAvLyBzYXZlIGNvbnRleHRcbiAgICAgICAgICAgIGJhY2t1cCA9IHtcbiAgICAgICAgICAgICAgICB5eWxpbmVubzogdGhpcy55eWxpbmVubyxcbiAgICAgICAgICAgICAgICB5eWxsb2M6IHtcbiAgICAgICAgICAgICAgICAgICAgZmlyc3RfbGluZTogdGhpcy55eWxsb2MuZmlyc3RfbGluZSxcbiAgICAgICAgICAgICAgICAgICAgbGFzdF9saW5lOiB0aGlzLmxhc3RfbGluZSxcbiAgICAgICAgICAgICAgICAgICAgZmlyc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5maXJzdF9jb2x1bW4sXG4gICAgICAgICAgICAgICAgICAgIGxhc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5sYXN0X2NvbHVtblxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgeXl0ZXh0OiB0aGlzLnl5dGV4dCxcbiAgICAgICAgICAgICAgICBtYXRjaDogdGhpcy5tYXRjaCxcbiAgICAgICAgICAgICAgICBtYXRjaGVzOiB0aGlzLm1hdGNoZXMsXG4gICAgICAgICAgICAgICAgbWF0Y2hlZDogdGhpcy5tYXRjaGVkLFxuICAgICAgICAgICAgICAgIHl5bGVuZzogdGhpcy55eWxlbmcsXG4gICAgICAgICAgICAgICAgb2Zmc2V0OiB0aGlzLm9mZnNldCxcbiAgICAgICAgICAgICAgICBfbW9yZTogdGhpcy5fbW9yZSxcbiAgICAgICAgICAgICAgICBfaW5wdXQ6IHRoaXMuX2lucHV0LFxuICAgICAgICAgICAgICAgIHl5OiB0aGlzLnl5LFxuICAgICAgICAgICAgICAgIGNvbmRpdGlvblN0YWNrOiB0aGlzLmNvbmRpdGlvblN0YWNrLnNsaWNlKDApLFxuICAgICAgICAgICAgICAgIGRvbmU6IHRoaXMuZG9uZVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMucmFuZ2VzKSB7XG4gICAgICAgICAgICAgICAgYmFja3VwLnl5bGxvYy5yYW5nZSA9IHRoaXMueXlsbG9jLnJhbmdlLnNsaWNlKDApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgbGluZXMgPSBtYXRjaFswXS5tYXRjaCgvKD86XFxyXFxuP3xcXG4pLiovZyk7XG4gICAgICAgIGlmIChsaW5lcykge1xuICAgICAgICAgICAgdGhpcy55eWxpbmVubyArPSBsaW5lcy5sZW5ndGg7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy55eWxsb2MgPSB7XG4gICAgICAgICAgICBmaXJzdF9saW5lOiB0aGlzLnl5bGxvYy5sYXN0X2xpbmUsXG4gICAgICAgICAgICBsYXN0X2xpbmU6IHRoaXMueXlsaW5lbm8gKyAxLFxuICAgICAgICAgICAgZmlyc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5sYXN0X2NvbHVtbixcbiAgICAgICAgICAgIGxhc3RfY29sdW1uOiBsaW5lcyA/XG4gICAgICAgICAgICAgICAgICAgICAgICAgbGluZXNbbGluZXMubGVuZ3RoIC0gMV0ubGVuZ3RoIC0gbGluZXNbbGluZXMubGVuZ3RoIC0gMV0ubWF0Y2goL1xccj9cXG4/LylbMF0ubGVuZ3RoIDpcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnl5bGxvYy5sYXN0X2NvbHVtbiArIG1hdGNoWzBdLmxlbmd0aFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnl5dGV4dCArPSBtYXRjaFswXTtcbiAgICAgICAgdGhpcy5tYXRjaCArPSBtYXRjaFswXTtcbiAgICAgICAgdGhpcy5tYXRjaGVzID0gbWF0Y2g7XG4gICAgICAgIHRoaXMueXlsZW5nID0gdGhpcy55eXRleHQubGVuZ3RoO1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zLnJhbmdlcykge1xuICAgICAgICAgICAgdGhpcy55eWxsb2MucmFuZ2UgPSBbdGhpcy5vZmZzZXQsIHRoaXMub2Zmc2V0ICs9IHRoaXMueXlsZW5nXTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9tb3JlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2JhY2t0cmFjayA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9pbnB1dCA9IHRoaXMuX2lucHV0LnNsaWNlKG1hdGNoWzBdLmxlbmd0aCk7XG4gICAgICAgIHRoaXMubWF0Y2hlZCArPSBtYXRjaFswXTtcbiAgICAgICAgdG9rZW4gPSB0aGlzLnBlcmZvcm1BY3Rpb24uY2FsbCh0aGlzLCB0aGlzLnl5LCB0aGlzLCBpbmRleGVkX3J1bGUsIHRoaXMuY29uZGl0aW9uU3RhY2tbdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGggLSAxXSk7XG4gICAgICAgIGlmICh0aGlzLmRvbmUgJiYgdGhpcy5faW5wdXQpIHtcbiAgICAgICAgICAgIHRoaXMuZG9uZSA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0b2tlbikge1xuICAgICAgICAgICAgcmV0dXJuIHRva2VuO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuX2JhY2t0cmFjaykge1xuICAgICAgICAgICAgLy8gcmVjb3ZlciBjb250ZXh0XG4gICAgICAgICAgICBmb3IgKHZhciBrIGluIGJhY2t1cCkge1xuICAgICAgICAgICAgICAgIHRoaXNba10gPSBiYWNrdXBba107XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7IC8vIHJ1bGUgYWN0aW9uIGNhbGxlZCByZWplY3QoKSBpbXBseWluZyB0aGUgbmV4dCBydWxlIHNob3VsZCBiZSB0ZXN0ZWQgaW5zdGVhZC5cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSxcblxuLy8gcmV0dXJuIG5leHQgbWF0Y2ggaW4gaW5wdXRcbm5leHQ6ZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5kb25lKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5FT0Y7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLl9pbnB1dCkge1xuICAgICAgICAgICAgdGhpcy5kb25lID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciB0b2tlbixcbiAgICAgICAgICAgIG1hdGNoLFxuICAgICAgICAgICAgdGVtcE1hdGNoLFxuICAgICAgICAgICAgaW5kZXg7XG4gICAgICAgIGlmICghdGhpcy5fbW9yZSkge1xuICAgICAgICAgICAgdGhpcy55eXRleHQgPSAnJztcbiAgICAgICAgICAgIHRoaXMubWF0Y2ggPSAnJztcbiAgICAgICAgfVxuICAgICAgICB2YXIgcnVsZXMgPSB0aGlzLl9jdXJyZW50UnVsZXMoKTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBydWxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdGVtcE1hdGNoID0gdGhpcy5faW5wdXQubWF0Y2godGhpcy5ydWxlc1tydWxlc1tpXV0pO1xuICAgICAgICAgICAgaWYgKHRlbXBNYXRjaCAmJiAoIW1hdGNoIHx8IHRlbXBNYXRjaFswXS5sZW5ndGggPiBtYXRjaFswXS5sZW5ndGgpKSB7XG4gICAgICAgICAgICAgICAgbWF0Y2ggPSB0ZW1wTWF0Y2g7XG4gICAgICAgICAgICAgICAgaW5kZXggPSBpO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYmFja3RyYWNrX2xleGVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHRva2VuID0gdGhpcy50ZXN0X21hdGNoKHRlbXBNYXRjaCwgcnVsZXNbaV0pO1xuICAgICAgICAgICAgICAgICAgICBpZiAodG9rZW4gIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5fYmFja3RyYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXRjaCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWU7IC8vIHJ1bGUgYWN0aW9uIGNhbGxlZCByZWplY3QoKSBpbXBseWluZyBhIHJ1bGUgTUlTbWF0Y2guXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBlbHNlOiB0aGlzIGlzIGEgbGV4ZXIgcnVsZSB3aGljaCBjb25zdW1lcyBpbnB1dCB3aXRob3V0IHByb2R1Y2luZyBhIHRva2VuIChlLmcuIHdoaXRlc3BhY2UpXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLm9wdGlvbnMuZmxleCkge1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgICAgICB0b2tlbiA9IHRoaXMudGVzdF9tYXRjaChtYXRjaCwgcnVsZXNbaW5kZXhdKTtcbiAgICAgICAgICAgIGlmICh0b2tlbiAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBlbHNlOiB0aGlzIGlzIGEgbGV4ZXIgcnVsZSB3aGljaCBjb25zdW1lcyBpbnB1dCB3aXRob3V0IHByb2R1Y2luZyBhIHRva2VuIChlLmcuIHdoaXRlc3BhY2UpXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX2lucHV0ID09PSBcIlwiKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5FT0Y7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wYXJzZUVycm9yKCdMZXhpY2FsIGVycm9yIG9uIGxpbmUgJyArICh0aGlzLnl5bGluZW5vICsgMSkgKyAnLiBVbnJlY29nbml6ZWQgdGV4dC5cXG4nICsgdGhpcy5zaG93UG9zaXRpb24oKSwge1xuICAgICAgICAgICAgICAgIHRleHQ6IFwiXCIsXG4gICAgICAgICAgICAgICAgdG9rZW46IG51bGwsXG4gICAgICAgICAgICAgICAgbGluZTogdGhpcy55eWxpbmVub1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyByZXR1cm4gbmV4dCBtYXRjaCB0aGF0IGhhcyBhIHRva2VuXG5sZXg6ZnVuY3Rpb24gbGV4ICgpIHtcbiAgICAgICAgdmFyIHIgPSB0aGlzLm5leHQoKTtcbiAgICAgICAgaWYgKHIpIHtcbiAgICAgICAgICAgIHJldHVybiByO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMubGV4KCk7XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyBhY3RpdmF0ZXMgYSBuZXcgbGV4ZXIgY29uZGl0aW9uIHN0YXRlIChwdXNoZXMgdGhlIG5ldyBsZXhlciBjb25kaXRpb24gc3RhdGUgb250byB0aGUgY29uZGl0aW9uIHN0YWNrKVxuYmVnaW46ZnVuY3Rpb24gYmVnaW4gKGNvbmRpdGlvbikge1xuICAgICAgICB0aGlzLmNvbmRpdGlvblN0YWNrLnB1c2goY29uZGl0aW9uKTtcbiAgICB9LFxuXG4vLyBwb3AgdGhlIHByZXZpb3VzbHkgYWN0aXZlIGxleGVyIGNvbmRpdGlvbiBzdGF0ZSBvZmYgdGhlIGNvbmRpdGlvbiBzdGFja1xucG9wU3RhdGU6ZnVuY3Rpb24gcG9wU3RhdGUgKCkge1xuICAgICAgICB2YXIgbiA9IHRoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMTtcbiAgICAgICAgaWYgKG4gPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jb25kaXRpb25TdGFjay5wb3AoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvblN0YWNrWzBdO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gcHJvZHVjZSB0aGUgbGV4ZXIgcnVsZSBzZXQgd2hpY2ggaXMgYWN0aXZlIGZvciB0aGUgY3VycmVudGx5IGFjdGl2ZSBsZXhlciBjb25kaXRpb24gc3RhdGVcbl9jdXJyZW50UnVsZXM6ZnVuY3Rpb24gX2N1cnJlbnRSdWxlcyAoKSB7XG4gICAgICAgIGlmICh0aGlzLmNvbmRpdGlvblN0YWNrLmxlbmd0aCAmJiB0aGlzLmNvbmRpdGlvblN0YWNrW3RoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMV0pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvbnNbdGhpcy5jb25kaXRpb25TdGFja1t0aGlzLmNvbmRpdGlvblN0YWNrLmxlbmd0aCAtIDFdXS5ydWxlcztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvbnNbXCJJTklUSUFMXCJdLnJ1bGVzO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gcmV0dXJuIHRoZSBjdXJyZW50bHkgYWN0aXZlIGxleGVyIGNvbmRpdGlvbiBzdGF0ZTsgd2hlbiBhbiBpbmRleCBhcmd1bWVudCBpcyBwcm92aWRlZCBpdCBwcm9kdWNlcyB0aGUgTi10aCBwcmV2aW91cyBjb25kaXRpb24gc3RhdGUsIGlmIGF2YWlsYWJsZVxudG9wU3RhdGU6ZnVuY3Rpb24gdG9wU3RhdGUgKG4pIHtcbiAgICAgICAgbiA9IHRoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMSAtIE1hdGguYWJzKG4gfHwgMCk7XG4gICAgICAgIGlmIChuID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvblN0YWNrW25dO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIFwiSU5JVElBTFwiO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gYWxpYXMgZm9yIGJlZ2luKGNvbmRpdGlvbilcbnB1c2hTdGF0ZTpmdW5jdGlvbiBwdXNoU3RhdGUgKGNvbmRpdGlvbikge1xuICAgICAgICB0aGlzLmJlZ2luKGNvbmRpdGlvbik7XG4gICAgfSxcblxuLy8gcmV0dXJuIHRoZSBudW1iZXIgb2Ygc3RhdGVzIGN1cnJlbnRseSBvbiB0aGUgc3RhY2tcbnN0YXRlU3RhY2tTaXplOmZ1bmN0aW9uIHN0YXRlU3RhY2tTaXplKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGg7XG4gICAgfSxcbm9wdGlvbnM6IHtcIm1vZHVsZU5hbWVcIjpcImxleGVyXCJ9LFxucGVyZm9ybUFjdGlvbjogZnVuY3Rpb24gYW5vbnltb3VzKHl5LHl5XywkYXZvaWRpbmdfbmFtZV9jb2xsaXNpb25zLFlZX1NUQVJUKSB7XG52YXIgWVlTVEFURT1ZWV9TVEFSVDtcbnN3aXRjaCgkYXZvaWRpbmdfbmFtZV9jb2xsaXNpb25zKSB7XG5jYXNlIDA6cmV0dXJuICdXSElURVNQQUNFJ1xuYnJlYWs7XG5jYXNlIDE6eXlfLnl5dGV4dCA9IHl5Xy55eXRleHQuc3Vic3RyKDEseXlfLnl5bGVuZy0xKTsgcmV0dXJuICdURVhUJ1xuYnJlYWs7XG5jYXNlIDI6eXlfLnl5dGV4dCA9ICdcXFxcXCInOyByZXR1cm4gJ1RFWFQnXG5icmVhaztcbmNhc2UgMzpyZXR1cm4gJ1JFU0VUJ1xuYnJlYWs7XG5jYXNlIDQ6cmV0dXJuICdMRUZUX0tFWVdPUkQnXG5icmVhaztcbmNhc2UgNTpyZXR1cm4gJ1JJR0hUX0tFWVdPUkQnXG5icmVhaztcbmNhc2UgNjpyZXR1cm4gJ0xFRlRfS0VZV09SRCdcbmJyZWFrO1xuY2FzZSA3OnJldHVybiAnUklHSFRfS0VZV09SRCdcbmJyZWFrO1xuY2FzZSA4OnJldHVybiAnTEVGVF9LRVlXT1JEJ1xuYnJlYWs7XG5jYXNlIDk6cmV0dXJuICdSSUdIVF9LRVlXT1JEJ1xuYnJlYWs7XG5jYXNlIDEwOnJldHVybiAnTk9ERV9TRVBBUkFUT1InXG5icmVhaztcbmNhc2UgMTE6cmV0dXJuICdOT0RFJ1xuYnJlYWs7XG5jYXNlIDEyOnJldHVybiAnTEVGVF9LRVlXT1JEJ1xuYnJlYWs7XG5jYXNlIDEzOnJldHVybiAnUklHSFRfS0VZV09SRCdcbmJyZWFrO1xuY2FzZSAxNDpyZXR1cm4gJ0xFRlRfS0VZV09SRCdcbmJyZWFrO1xuY2FzZSAxNTpyZXR1cm4gJ1JJR0hUX0tFWVdPUkQnXG5icmVhaztcbmNhc2UgMTY6cmV0dXJuICdSSUdIVF9LRVlXT1JEJ1xuYnJlYWs7XG5jYXNlIDE3OnJldHVybiAnQklORF9PUl9TVFJPTkdCSU5EJ1xuYnJlYWs7XG5jYXNlIDE4OnJldHVybiAnQklORF9PUl9TVFJPTkdCSU5EJ1xuYnJlYWs7XG5jYXNlIDE5OnJldHVybiAnUklHSFRfS0VZV09SRCdcbmJyZWFrO1xuY2FzZSAyMDpyZXR1cm4gJ1JJR0hUX0tFWVdPUkQnXG5icmVhaztcbmNhc2UgMjE6cmV0dXJuICdMRUZUX0tFWVdPUkQnXG5icmVhaztcbmNhc2UgMjI6cmV0dXJuICdSSUdIVF9LRVlXT1JEJ1xuYnJlYWs7XG5jYXNlIDIzOnJldHVybiAnUklHSFRfS0VZV09SRCdcbmJyZWFrO1xuY2FzZSAyNDpyZXR1cm4gJ1JJR0hUX0tFWVdPUkQnXG5icmVhaztcbmNhc2UgMjU6cmV0dXJuICdSSUdIVF9LRVlXT1JEJ1xuYnJlYWs7XG5jYXNlIDI2OnJldHVybiAnTk9ERSdcbmJyZWFrO1xuY2FzZSAyNzpyZXR1cm4gJ1RFWFQnXG5icmVhaztcbmNhc2UgMjg6cmV0dXJuICdFT0YnXG5icmVhaztcbn1cbn0sXG5ydWxlczogWy9eKD86XFxzKykvLC9eKD86XFxcXFs7ITw+XyNeJHt9KClcXFtcXF1cXCpdKS8sL14oPzpcIikvLC9eKD86OykvLC9eKD86XFxbKS8sL14oPzpcXF0pLywvXig/OlxcKCkvLC9eKD86XFwpKS8sL14oPzpcXHspLywvXig/OlxcfSkvLC9eKD86XFx8KS8sL14oPzpfXFxiKS8sL14oPzo8KS8sL14oPzo+KS8sL14oPzohKS8sL14oPzpcXCpbMC05XSspLywvXig/Oig9PlxccypcXCRbYS16QS1aMC05XSspKS8sL14oPzo8PSkvLC9eKD86PSkvLC9eKD86XFxeI1thLXpBLVowLTldKykvLC9eKD86XFxeIyMpLywvXig/OiNbYS16QS1aMC05XSspLywvXig/OlxcXlswLTldK1tcXC1dWzAtOV0rKS8sL14oPzpcXF5bMC05XSspLywvXig/OlxcXiNbYS16QS1aMC05XSspLywvXig/OlxcXlVcXGIpLywvXig/OlxcJFthLXpBLVowLTldKykvLC9eKD86W147XFxbXFxdXFwoXFwpXFx8XFxcXFwiXFx7XFx9PSM8PiFcXHNcXG5cXHRcXHJcXF5cXCpdKykvLC9eKD86JCkvXSxcbmNvbmRpdGlvbnM6IHtcIklOSVRJQUxcIjp7XCJydWxlc1wiOlswLDEsMiwzLDQsNSw2LDcsOCw5LDEwLDExLDEyLDEzLDE0LDE1LDE2LDE3LDE4LDE5LDIwLDIxLDIyLDIzLDI0LDI1LDI2LDI3LDI4XSxcImluY2x1c2l2ZVwiOnRydWV9fVxufSk7XG5yZXR1cm4gbGV4ZXI7XG59KSgpO1xuZXhwb3J0cy5sZXhlciA9IGxleGVyOyIsImxldCBydWxlcyA9IGBcblxuU1RBUlQ6XG4tICogPT4gTEVGVF9CSU5ESU5HIDBcbjtcblxuTEVGVF9CSU5ESU5HOlxuLSBCSU5EX09SX1NUUk9OR0JJTkQgPT4gJyQwJyBXQUlUSU5HX0ZPUl9OT0RFXG47XG5cbldBSVRJTkdfRk9SX05PREU6XG4tIFRFWFQgPT4gJ1wiJyBURVhUIDBcbi0gTEVGVF9LRVlXT1JEID0+IExFRlRfS0VZV09SRCAwXG4tIE5PREUgPT4gJyQwJyBSSUdIVF9LRVlXT1JEXG47XG5cblRFWFQ6XG4tIE5PREVfU0VQQVJBVE9SID0+ICdcIiAnIFdBSVRJTkdfRk9SX05PREUgMFxuLSBXSElURVNQQUNFIE5PREVfU0VQQVJBVE9SID0+ICdcIiAnIFdBSVRJTkdfRk9SX05PREUgMVxuLSBMRUZUX0tFWVdPUkQgPT4gJ1wiICsgJyBMRUZUX0tFWVdPUkQgMFxuLSBXSElURVNQQUNFIExFRlRfS0VZV09SRCA9PiAnXCIgKyAnIExFRlRfS0VZV09SRCAxXG4tIFJJR0hUX0tFWVdPUkQgPT4gJ1wiJyBSSUdIVF9LRVlXT1JEIDBcbi0gV0hJVEVTUEFDRSBSSUdIVF9LRVlXT1JEID0+ICdcIiAnIFJJR0hUX0tFWVdPUkQgMVxuLSBOT0RFID0+ICdcIiArICQwJyBSSUdIVF9LRVlXT1JEXG4tIFdISVRFU1BBQ0UgTk9ERSA9PiAnXCIgKyAkMScgUklHSFRfS0VZV09SRFxuLSBSRVNFVCA9PiAnXCIkMCcgU1RBUlRcbi0gV0hJVEVTUEFDRSBSRVNFVCA9PiAnXCIkMScgU1RBUlRcbjtcblxuTEVGVF9LRVlXT1JEOlxuLSBURVhUID0+ICdcIicgVEVYVCAwXG4tIE5PREUgPT4gJyQwJyBSSUdIVF9LRVlXT1JEXG47XG5cblJJR0hUX0tFWVdPUkQ6XG4tIFRFWFQgPT4gJysgXCInIFRFWFQgMFxuLSBMRUZUX0tFWVdPUkQgPT4gJysgJyBMRUZUX0tFWVdPUkQgMFxuLSBOT0RFID0+ICcrICQwJ1xuLSBOT0RFX1NFUEFSQVRPUiA9PiBXQUlUSU5HX0ZPUl9OT0RFIDBcbjtcblxuKjpcbi0gUkVTRVQgPT4gJyQwJyBTVEFSVFxuLSAqID0+ICckMCdcbjtcblxuYDtcblxuZXhwb3J0IHsgcnVsZXMgfTtcbiIsIi8qIHBhcnNlciBnZW5lcmF0ZWQgYnkgamlzb24gMC40LjE4ICovXG4vKlxuICBSZXR1cm5zIGEgUGFyc2VyIG9iamVjdCBvZiB0aGUgZm9sbG93aW5nIHN0cnVjdHVyZTpcblxuICBQYXJzZXI6IHtcbiAgICB5eToge31cbiAgfVxuXG4gIFBhcnNlci5wcm90b3R5cGU6IHtcbiAgICB5eToge30sXG4gICAgdHJhY2U6IGZ1bmN0aW9uKCksXG4gICAgc3ltYm9sc186IHthc3NvY2lhdGl2ZSBsaXN0OiBuYW1lID09PiBudW1iZXJ9LFxuICAgIHRlcm1pbmFsc186IHthc3NvY2lhdGl2ZSBsaXN0OiBudW1iZXIgPT0+IG5hbWV9LFxuICAgIHByb2R1Y3Rpb25zXzogWy4uLl0sXG4gICAgcGVyZm9ybUFjdGlvbjogZnVuY3Rpb24gYW5vbnltb3VzKHl5dGV4dCwgeXlsZW5nLCB5eWxpbmVubywgeXksIHl5c3RhdGUsICQkLCBfJCksXG4gICAgdGFibGU6IFsuLi5dLFxuICAgIGRlZmF1bHRBY3Rpb25zOiB7Li4ufSxcbiAgICBwYXJzZUVycm9yOiBmdW5jdGlvbihzdHIsIGhhc2gpLFxuICAgIHBhcnNlOiBmdW5jdGlvbihpbnB1dCksXG5cbiAgICBsZXhlcjoge1xuICAgICAgICBFT0Y6IDEsXG4gICAgICAgIHBhcnNlRXJyb3I6IGZ1bmN0aW9uKHN0ciwgaGFzaCksXG4gICAgICAgIHNldElucHV0OiBmdW5jdGlvbihpbnB1dCksXG4gICAgICAgIGlucHV0OiBmdW5jdGlvbigpLFxuICAgICAgICB1bnB1dDogZnVuY3Rpb24oc3RyKSxcbiAgICAgICAgbW9yZTogZnVuY3Rpb24oKSxcbiAgICAgICAgbGVzczogZnVuY3Rpb24obiksXG4gICAgICAgIHBhc3RJbnB1dDogZnVuY3Rpb24oKSxcbiAgICAgICAgdXBjb21pbmdJbnB1dDogZnVuY3Rpb24oKSxcbiAgICAgICAgc2hvd1Bvc2l0aW9uOiBmdW5jdGlvbigpLFxuICAgICAgICB0ZXN0X21hdGNoOiBmdW5jdGlvbihyZWdleF9tYXRjaF9hcnJheSwgcnVsZV9pbmRleCksXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uKCksXG4gICAgICAgIGxleDogZnVuY3Rpb24oKSxcbiAgICAgICAgYmVnaW46IGZ1bmN0aW9uKGNvbmRpdGlvbiksXG4gICAgICAgIHBvcFN0YXRlOiBmdW5jdGlvbigpLFxuICAgICAgICBfY3VycmVudFJ1bGVzOiBmdW5jdGlvbigpLFxuICAgICAgICB0b3BTdGF0ZTogZnVuY3Rpb24oKSxcbiAgICAgICAgcHVzaFN0YXRlOiBmdW5jdGlvbihjb25kaXRpb24pLFxuXG4gICAgICAgIG9wdGlvbnM6IHtcbiAgICAgICAgICAgIHJhbmdlczogYm9vbGVhbiAgICAgICAgICAgKG9wdGlvbmFsOiB0cnVlID09PiB0b2tlbiBsb2NhdGlvbiBpbmZvIHdpbGwgaW5jbHVkZSBhIC5yYW5nZVtdIG1lbWJlcilcbiAgICAgICAgICAgIGZsZXg6IGJvb2xlYW4gICAgICAgICAgICAgKG9wdGlvbmFsOiB0cnVlID09PiBmbGV4LWxpa2UgbGV4aW5nIGJlaGF2aW91ciB3aGVyZSB0aGUgcnVsZXMgYXJlIHRlc3RlZCBleGhhdXN0aXZlbHkgdG8gZmluZCB0aGUgbG9uZ2VzdCBtYXRjaClcbiAgICAgICAgICAgIGJhY2t0cmFja19sZXhlcjogYm9vbGVhbiAgKG9wdGlvbmFsOiB0cnVlID09PiBsZXhlciByZWdleGVzIGFyZSB0ZXN0ZWQgaW4gb3JkZXIgYW5kIGZvciBlYWNoIG1hdGNoaW5nIHJlZ2V4IHRoZSBhY3Rpb24gY29kZSBpcyBpbnZva2VkOyB0aGUgbGV4ZXIgdGVybWluYXRlcyB0aGUgc2NhbiB3aGVuIGEgdG9rZW4gaXMgcmV0dXJuZWQgYnkgdGhlIGFjdGlvbiBjb2RlKVxuICAgICAgICB9LFxuXG4gICAgICAgIHBlcmZvcm1BY3Rpb246IGZ1bmN0aW9uKHl5LCB5eV8sICRhdm9pZGluZ19uYW1lX2NvbGxpc2lvbnMsIFlZX1NUQVJUKSxcbiAgICAgICAgcnVsZXM6IFsuLi5dLFxuICAgICAgICBjb25kaXRpb25zOiB7YXNzb2NpYXRpdmUgbGlzdDogbmFtZSA9PT4gc2V0fSxcbiAgICB9XG4gIH1cblxuXG4gIHRva2VuIGxvY2F0aW9uIGluZm8gKEAkLCBfJCwgZXRjLik6IHtcbiAgICBmaXJzdF9saW5lOiBuLFxuICAgIGxhc3RfbGluZTogbixcbiAgICBmaXJzdF9jb2x1bW46IG4sXG4gICAgbGFzdF9jb2x1bW46IG4sXG4gICAgcmFuZ2U6IFtzdGFydF9udW1iZXIsIGVuZF9udW1iZXJdICAgICAgICh3aGVyZSB0aGUgbnVtYmVycyBhcmUgaW5kZXhlcyBpbnRvIHRoZSBpbnB1dCBzdHJpbmcsIHJlZ3VsYXIgemVyby1iYXNlZClcbiAgfVxuXG5cbiAgdGhlIHBhcnNlRXJyb3IgZnVuY3Rpb24gcmVjZWl2ZXMgYSAnaGFzaCcgb2JqZWN0IHdpdGggdGhlc2UgbWVtYmVycyBmb3IgbGV4ZXIgYW5kIHBhcnNlciBlcnJvcnM6IHtcbiAgICB0ZXh0OiAgICAgICAgKG1hdGNoZWQgdGV4dClcbiAgICB0b2tlbjogICAgICAgKHRoZSBwcm9kdWNlZCB0ZXJtaW5hbCB0b2tlbiwgaWYgYW55KVxuICAgIGxpbmU6ICAgICAgICAoeXlsaW5lbm8pXG4gIH1cbiAgd2hpbGUgcGFyc2VyIChncmFtbWFyKSBlcnJvcnMgd2lsbCBhbHNvIHByb3ZpZGUgdGhlc2UgbWVtYmVycywgaS5lLiBwYXJzZXIgZXJyb3JzIGRlbGl2ZXIgYSBzdXBlcnNldCBvZiBhdHRyaWJ1dGVzOiB7XG4gICAgbG9jOiAgICAgICAgICh5eWxsb2MpXG4gICAgZXhwZWN0ZWQ6ICAgIChzdHJpbmcgZGVzY3JpYmluZyB0aGUgc2V0IG9mIGV4cGVjdGVkIHRva2VucylcbiAgICByZWNvdmVyYWJsZTogKGJvb2xlYW46IFRSVUUgd2hlbiB0aGUgcGFyc2VyIGhhcyBhIGVycm9yIHJlY292ZXJ5IHJ1bGUgYXZhaWxhYmxlIGZvciB0aGlzIHBhcnRpY3VsYXIgZXJyb3IpXG4gIH1cbiovXG52YXIgcnVsZXNQYXJzZXIgPSAoZnVuY3Rpb24oKXtcbnZhciBvPWZ1bmN0aW9uKGssdixvLGwpe2ZvcihvPW98fHt9LGw9ay5sZW5ndGg7bC0tO29ba1tsXV09dik7cmV0dXJuIG99LCRWMD1bMSw1XSwkVjE9WzUsMTddLCRWMj1bOCwxOV0sJFYzPVsxLDEyXSwkVjQ9WzEwLDEyXSwkVjU9WzEwLDEyLDE0LDE3XSwkVjY9WzEwLDEyLDE2LDE3LDE4XTtcbnZhciBwYXJzZXIgPSB7dHJhY2U6IGZ1bmN0aW9uIHRyYWNlICgpIHsgfSxcbnl5OiB7fSxcbnN5bWJvbHNfOiB7XCJlcnJvclwiOjIsXCJTVEFSVFwiOjMsXCJSVUxFU0VUU1wiOjQsXCJFT0ZcIjo1LFwiUlVMRVNFVFwiOjYsXCJTVEFURVNcIjo3LFwiQ09MT05cIjo4LFwiUlVMRVNcIjo5LFwiU0VNSUNPTE9OXCI6MTAsXCJSVUxFXCI6MTEsXCJNSU5VU1wiOjEyLFwiVE9LRU5TXCI6MTMsXCJJTVBMSUVTXCI6MTQsXCJUSEVOXCI6MTUsXCJURVhUXCI6MTYsXCJTWU1CT0xcIjoxNyxcIk5VTUJFUlwiOjE4LFwiQ09NTUFcIjoxOSxcIiRhY2NlcHRcIjowLFwiJGVuZFwiOjF9LFxudGVybWluYWxzXzogezI6XCJlcnJvclwiLDU6XCJFT0ZcIiw4OlwiQ09MT05cIiwxMDpcIlNFTUlDT0xPTlwiLDEyOlwiTUlOVVNcIiwxNDpcIklNUExJRVNcIiwxNjpcIlRFWFRcIiwxNzpcIlNZTUJPTFwiLDE4OlwiTlVNQkVSXCIsMTk6XCJDT01NQVwifSxcbnByb2R1Y3Rpb25zXzogWzAsWzMsMl0sWzQsMV0sWzQsMl0sWzYsNF0sWzksMV0sWzksMl0sWzExLDRdLFsxMSwyXSxbMTUsMl0sWzE1LDJdLFsxNSwyXSxbMTUsMV0sWzE1LDFdLFsxNSwxXSxbMTMsMV0sWzEzLDJdLFs3LDFdLFs3LDNdXSxcbnBlcmZvcm1BY3Rpb246IGZ1bmN0aW9uIGFub255bW91cyh5eXRleHQsIHl5bGVuZywgeXlsaW5lbm8sIHl5LCB5eXN0YXRlIC8qIGFjdGlvblsxXSAqLywgJCQgLyogdnN0YWNrICovLCBfJCAvKiBsc3RhY2sgKi8pIHtcbi8qIHRoaXMgPT0geXl2YWwgKi9cblxudmFyICQwID0gJCQubGVuZ3RoIC0gMTtcbnN3aXRjaCAoeXlzdGF0ZSkge1xuY2FzZSAxOlxuIHRoaXMuJCA9ICQkWyQwLTFdOyByZXR1cm4odGhpcy4kKSBcbmJyZWFrO1xuY2FzZSAyOiBjYXNlIDU6IGNhc2UgMTU6IGNhc2UgMTc6XG4gdGhpcy4kID0gWyQkWyQwXV07IFxuYnJlYWs7XG5jYXNlIDM6IGNhc2UgNjogY2FzZSAxNjpcbiB0aGlzLiQgPSAkJFskMC0xXTsgdGhpcy4kLnB1c2goJCRbJDBdKTsgXG5icmVhaztcbmNhc2UgNDpcbiB0aGlzLiQgPSB7IHN0YXRlczogJCRbJDAtM10sIHJ1bGVzOiAkJFskMC0xXSB9ICBcbmJyZWFrO1xuY2FzZSA3OlxuIHRoaXMuJCA9IHsgdG9rZW5zOiAkJFskMC0yXSwgdGhlbjogJCRbJDBdIH07IFxuYnJlYWs7XG5jYXNlIDg6XG4gdGhpcy4kID0geyB0b2tlbnM6ICQkWyQwXSB9OyBcbmJyZWFrO1xuY2FzZSA5OlxuIHRoaXMuJCA9ICQkWyQwLTFdOyB0aGlzLiRbJ3RleHQnXSA9ICQkWyQwXTsgXG5icmVhaztcbmNhc2UgMTA6XG4gdGhpcy4kID0gJCRbJDAtMV07IHRoaXMuJFsnc3RhdGUnXSA9ICQkWyQwXTsgXG5icmVhaztcbmNhc2UgMTE6XG4gdGhpcy4kID0gJCRbJDAtMV07IHRoaXMuJFsnY29uc3VtZSddID0gJCRbJDBdOyBcbmJyZWFrO1xuY2FzZSAxMjpcbiB0aGlzLiQgPSB7IHRleHQ6ICQkWyQwXX07IFxuYnJlYWs7XG5jYXNlIDEzOlxuIHRoaXMuJCA9IHsgc3RhdGU6ICQkWyQwXX07IFxuYnJlYWs7XG5jYXNlIDE0OlxuIHRoaXMuJCA9IHsgY29uc3VtZTogJCRbJDBdfTsgXG5icmVhaztcbmNhc2UgMTg6XG4gdGhpcy4kID0gJCRbJDAtMl07IHRoaXMuJC5wdXNoKCQkWyQwXSk7IFxuYnJlYWs7XG59XG59LFxudGFibGU6IFt7MzoxLDQ6Miw2OjMsNzo0LDE3OiRWMH0sezE6WzNdfSx7NTpbMSw2XSw2OjcsNzo0LDE3OiRWMH0sbygkVjEsWzIsMl0pLHs4OlsxLDhdLDE5OlsxLDldfSxvKCRWMixbMiwxN10pLHsxOlsyLDFdfSxvKCRWMSxbMiwzXSksezk6MTAsMTE6MTEsMTI6JFYzfSx7MTc6WzEsMTNdfSx7MTA6WzEsMTRdLDExOjE1LDEyOiRWM30sbygkVjQsWzIsNV0pLHsxMzoxNiwxNzpbMSwxN119LG8oJFYyLFsyLDE4XSksbygkVjEsWzIsNF0pLG8oJFY0LFsyLDZdKSxvKCRWNCxbMiw4XSx7MTQ6WzEsMThdLDE3OlsxLDE5XX0pLG8oJFY1LFsyLDE1XSksezE1OjIwLDE2OlsxLDIxXSwxNzpbMSwyMl0sMTg6WzEsMjNdfSxvKCRWNSxbMiwxNl0pLG8oJFY0LFsyLDddLHsxNjpbMSwyNF0sMTc6WzEsMjVdLDE4OlsxLDI2XX0pLG8oJFY2LFsyLDEyXSksbygkVjYsWzIsMTNdKSxvKCRWNixbMiwxNF0pLG8oJFY2LFsyLDldKSxvKCRWNixbMiwxMF0pLG8oJFY2LFsyLDExXSldLFxuZGVmYXVsdEFjdGlvbnM6IHs2OlsyLDFdfSxcbnBhcnNlRXJyb3I6IGZ1bmN0aW9uIHBhcnNlRXJyb3IgKHN0ciwgaGFzaCkge1xuICAgIGlmIChoYXNoLnJlY292ZXJhYmxlKSB7XG4gICAgICAgIHRoaXMudHJhY2Uoc3RyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgZXJyb3IgPSBuZXcgRXJyb3Ioc3RyKTtcbiAgICAgICAgZXJyb3IuaGFzaCA9IGhhc2g7XG4gICAgICAgIHRocm93IGVycm9yO1xuICAgIH1cbn0sXG5wYXJzZTogZnVuY3Rpb24gcGFyc2UoaW5wdXQpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXMsIHN0YWNrID0gWzBdLCB0c3RhY2sgPSBbXSwgdnN0YWNrID0gW251bGxdLCBsc3RhY2sgPSBbXSwgdGFibGUgPSB0aGlzLnRhYmxlLCB5eXRleHQgPSAnJywgeXlsaW5lbm8gPSAwLCB5eWxlbmcgPSAwLCByZWNvdmVyaW5nID0gMCwgVEVSUk9SID0gMiwgRU9GID0gMTtcbiAgICB2YXIgYXJncyA9IGxzdGFjay5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG4gICAgdmFyIGxleGVyID0gT2JqZWN0LmNyZWF0ZSh0aGlzLmxleGVyKTtcbiAgICB2YXIgc2hhcmVkU3RhdGUgPSB7IHl5OiB7fSB9O1xuICAgIGZvciAodmFyIGsgaW4gdGhpcy55eSkge1xuICAgICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHRoaXMueXksIGspKSB7XG4gICAgICAgICAgICBzaGFyZWRTdGF0ZS55eVtrXSA9IHRoaXMueXlba107XG4gICAgICAgIH1cbiAgICB9XG4gICAgbGV4ZXIuc2V0SW5wdXQoaW5wdXQsIHNoYXJlZFN0YXRlLnl5KTtcbiAgICBzaGFyZWRTdGF0ZS55eS5sZXhlciA9IGxleGVyO1xuICAgIHNoYXJlZFN0YXRlLnl5LnBhcnNlciA9IHRoaXM7XG4gICAgaWYgKHR5cGVvZiBsZXhlci55eWxsb2MgPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgbGV4ZXIueXlsbG9jID0ge307XG4gICAgfVxuICAgIHZhciB5eWxvYyA9IGxleGVyLnl5bGxvYztcbiAgICBsc3RhY2sucHVzaCh5eWxvYyk7XG4gICAgdmFyIHJhbmdlcyA9IGxleGVyLm9wdGlvbnMgJiYgbGV4ZXIub3B0aW9ucy5yYW5nZXM7XG4gICAgaWYgKHR5cGVvZiBzaGFyZWRTdGF0ZS55eS5wYXJzZUVycm9yID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHRoaXMucGFyc2VFcnJvciA9IHNoYXJlZFN0YXRlLnl5LnBhcnNlRXJyb3I7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5wYXJzZUVycm9yID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKHRoaXMpLnBhcnNlRXJyb3I7XG4gICAgfVxuICAgIGZ1bmN0aW9uIHBvcFN0YWNrKG4pIHtcbiAgICAgICAgc3RhY2subGVuZ3RoID0gc3RhY2subGVuZ3RoIC0gMiAqIG47XG4gICAgICAgIHZzdGFjay5sZW5ndGggPSB2c3RhY2subGVuZ3RoIC0gbjtcbiAgICAgICAgbHN0YWNrLmxlbmd0aCA9IGxzdGFjay5sZW5ndGggLSBuO1xuICAgIH1cbiAgICBfdG9rZW5fc3RhY2s6XG4gICAgICAgIHZhciBsZXggPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgdG9rZW47XG4gICAgICAgICAgICB0b2tlbiA9IGxleGVyLmxleCgpIHx8IEVPRjtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdG9rZW4gIT09ICdudW1iZXInKSB7XG4gICAgICAgICAgICAgICAgdG9rZW4gPSBzZWxmLnN5bWJvbHNfW3Rva2VuXSB8fCB0b2tlbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0b2tlbjtcbiAgICAgICAgfTtcbiAgICB2YXIgc3ltYm9sLCBwcmVFcnJvclN5bWJvbCwgc3RhdGUsIGFjdGlvbiwgYSwgciwgeXl2YWwgPSB7fSwgcCwgbGVuLCBuZXdTdGF0ZSwgZXhwZWN0ZWQ7XG4gICAgd2hpbGUgKHRydWUpIHtcbiAgICAgICAgc3RhdGUgPSBzdGFja1tzdGFjay5sZW5ndGggLSAxXTtcbiAgICAgICAgaWYgKHRoaXMuZGVmYXVsdEFjdGlvbnNbc3RhdGVdKSB7XG4gICAgICAgICAgICBhY3Rpb24gPSB0aGlzLmRlZmF1bHRBY3Rpb25zW3N0YXRlXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmIChzeW1ib2wgPT09IG51bGwgfHwgdHlwZW9mIHN5bWJvbCA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgIHN5bWJvbCA9IGxleCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYWN0aW9uID0gdGFibGVbc3RhdGVdICYmIHRhYmxlW3N0YXRlXVtzeW1ib2xdO1xuICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgYWN0aW9uID09PSAndW5kZWZpbmVkJyB8fCAhYWN0aW9uLmxlbmd0aCB8fCAhYWN0aW9uWzBdKSB7XG4gICAgICAgICAgICAgICAgdmFyIGVyclN0ciA9ICcnO1xuICAgICAgICAgICAgICAgIGV4cGVjdGVkID0gW107XG4gICAgICAgICAgICAgICAgZm9yIChwIGluIHRhYmxlW3N0YXRlXSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50ZXJtaW5hbHNfW3BdICYmIHAgPiBURVJST1IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cGVjdGVkLnB1c2goJ1xcJycgKyB0aGlzLnRlcm1pbmFsc19bcF0gKyAnXFwnJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGxleGVyLnNob3dQb3NpdGlvbikge1xuICAgICAgICAgICAgICAgICAgICBlcnJTdHIgPSAnUGFyc2UgZXJyb3Igb24gbGluZSAnICsgKHl5bGluZW5vICsgMSkgKyAnOlxcbicgKyBsZXhlci5zaG93UG9zaXRpb24oKSArICdcXG5FeHBlY3RpbmcgJyArIGV4cGVjdGVkLmpvaW4oJywgJykgKyAnLCBnb3QgXFwnJyArICh0aGlzLnRlcm1pbmFsc19bc3ltYm9sXSB8fCBzeW1ib2wpICsgJ1xcJyc7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyU3RyID0gJ1BhcnNlIGVycm9yIG9uIGxpbmUgJyArICh5eWxpbmVubyArIDEpICsgJzogVW5leHBlY3RlZCAnICsgKHN5bWJvbCA9PSBFT0YgPyAnZW5kIG9mIGlucHV0JyA6ICdcXCcnICsgKHRoaXMudGVybWluYWxzX1tzeW1ib2xdIHx8IHN5bWJvbCkgKyAnXFwnJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMucGFyc2VFcnJvcihlcnJTdHIsIHtcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogbGV4ZXIubWF0Y2gsXG4gICAgICAgICAgICAgICAgICAgIHRva2VuOiB0aGlzLnRlcm1pbmFsc19bc3ltYm9sXSB8fCBzeW1ib2wsXG4gICAgICAgICAgICAgICAgICAgIGxpbmU6IGxleGVyLnl5bGluZW5vLFxuICAgICAgICAgICAgICAgICAgICBsb2M6IHl5bG9jLFxuICAgICAgICAgICAgICAgICAgICBleHBlY3RlZDogZXhwZWN0ZWRcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgaWYgKGFjdGlvblswXSBpbnN0YW5jZW9mIEFycmF5ICYmIGFjdGlvbi5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1BhcnNlIEVycm9yOiBtdWx0aXBsZSBhY3Rpb25zIHBvc3NpYmxlIGF0IHN0YXRlOiAnICsgc3RhdGUgKyAnLCB0b2tlbjogJyArIHN5bWJvbCk7XG4gICAgICAgIH1cbiAgICAgICAgc3dpdGNoIChhY3Rpb25bMF0pIHtcbiAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgc3RhY2sucHVzaChzeW1ib2wpO1xuICAgICAgICAgICAgdnN0YWNrLnB1c2gobGV4ZXIueXl0ZXh0KTtcbiAgICAgICAgICAgIGxzdGFjay5wdXNoKGxleGVyLnl5bGxvYyk7XG4gICAgICAgICAgICBzdGFjay5wdXNoKGFjdGlvblsxXSk7XG4gICAgICAgICAgICBzeW1ib2wgPSBudWxsO1xuICAgICAgICAgICAgaWYgKCFwcmVFcnJvclN5bWJvbCkge1xuICAgICAgICAgICAgICAgIHl5bGVuZyA9IGxleGVyLnl5bGVuZztcbiAgICAgICAgICAgICAgICB5eXRleHQgPSBsZXhlci55eXRleHQ7XG4gICAgICAgICAgICAgICAgeXlsaW5lbm8gPSBsZXhlci55eWxpbmVubztcbiAgICAgICAgICAgICAgICB5eWxvYyA9IGxleGVyLnl5bGxvYztcbiAgICAgICAgICAgICAgICBpZiAocmVjb3ZlcmluZyA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgcmVjb3ZlcmluZy0tO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgc3ltYm9sID0gcHJlRXJyb3JTeW1ib2w7XG4gICAgICAgICAgICAgICAgcHJlRXJyb3JTeW1ib2wgPSBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMjpcbiAgICAgICAgICAgIGxlbiA9IHRoaXMucHJvZHVjdGlvbnNfW2FjdGlvblsxXV1bMV07XG4gICAgICAgICAgICB5eXZhbC4kID0gdnN0YWNrW3ZzdGFjay5sZW5ndGggLSBsZW5dO1xuICAgICAgICAgICAgeXl2YWwuXyQgPSB7XG4gICAgICAgICAgICAgICAgZmlyc3RfbGluZTogbHN0YWNrW2xzdGFjay5sZW5ndGggLSAobGVuIHx8IDEpXS5maXJzdF9saW5lLFxuICAgICAgICAgICAgICAgIGxhc3RfbGluZTogbHN0YWNrW2xzdGFjay5sZW5ndGggLSAxXS5sYXN0X2xpbmUsXG4gICAgICAgICAgICAgICAgZmlyc3RfY29sdW1uOiBsc3RhY2tbbHN0YWNrLmxlbmd0aCAtIChsZW4gfHwgMSldLmZpcnN0X2NvbHVtbixcbiAgICAgICAgICAgICAgICBsYXN0X2NvbHVtbjogbHN0YWNrW2xzdGFjay5sZW5ndGggLSAxXS5sYXN0X2NvbHVtblxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGlmIChyYW5nZXMpIHtcbiAgICAgICAgICAgICAgICB5eXZhbC5fJC5yYW5nZSA9IFtcbiAgICAgICAgICAgICAgICAgICAgbHN0YWNrW2xzdGFjay5sZW5ndGggLSAobGVuIHx8IDEpXS5yYW5nZVswXSxcbiAgICAgICAgICAgICAgICAgICAgbHN0YWNrW2xzdGFjay5sZW5ndGggLSAxXS5yYW5nZVsxXVxuICAgICAgICAgICAgICAgIF07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByID0gdGhpcy5wZXJmb3JtQWN0aW9uLmFwcGx5KHl5dmFsLCBbXG4gICAgICAgICAgICAgICAgeXl0ZXh0LFxuICAgICAgICAgICAgICAgIHl5bGVuZyxcbiAgICAgICAgICAgICAgICB5eWxpbmVubyxcbiAgICAgICAgICAgICAgICBzaGFyZWRTdGF0ZS55eSxcbiAgICAgICAgICAgICAgICBhY3Rpb25bMV0sXG4gICAgICAgICAgICAgICAgdnN0YWNrLFxuICAgICAgICAgICAgICAgIGxzdGFja1xuICAgICAgICAgICAgXS5jb25jYXQoYXJncykpO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiByICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgIHJldHVybiByO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGxlbikge1xuICAgICAgICAgICAgICAgIHN0YWNrID0gc3RhY2suc2xpY2UoMCwgLTEgKiBsZW4gKiAyKTtcbiAgICAgICAgICAgICAgICB2c3RhY2sgPSB2c3RhY2suc2xpY2UoMCwgLTEgKiBsZW4pO1xuICAgICAgICAgICAgICAgIGxzdGFjayA9IGxzdGFjay5zbGljZSgwLCAtMSAqIGxlbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzdGFjay5wdXNoKHRoaXMucHJvZHVjdGlvbnNfW2FjdGlvblsxXV1bMF0pO1xuICAgICAgICAgICAgdnN0YWNrLnB1c2goeXl2YWwuJCk7XG4gICAgICAgICAgICBsc3RhY2sucHVzaCh5eXZhbC5fJCk7XG4gICAgICAgICAgICBuZXdTdGF0ZSA9IHRhYmxlW3N0YWNrW3N0YWNrLmxlbmd0aCAtIDJdXVtzdGFja1tzdGFjay5sZW5ndGggLSAxXV07XG4gICAgICAgICAgICBzdGFjay5wdXNoKG5ld1N0YXRlKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbn19O1xuLyogZ2VuZXJhdGVkIGJ5IGppc29uLWxleCAwLjMuNCAqL1xudmFyIGxleGVyID0gKGZ1bmN0aW9uKCl7XG52YXIgbGV4ZXIgPSAoe1xuXG5FT0Y6MSxcblxucGFyc2VFcnJvcjpmdW5jdGlvbiBwYXJzZUVycm9yKHN0ciwgaGFzaCkge1xuICAgICAgICBpZiAodGhpcy55eS5wYXJzZXIpIHtcbiAgICAgICAgICAgIHRoaXMueXkucGFyc2VyLnBhcnNlRXJyb3Ioc3RyLCBoYXNoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihzdHIpO1xuICAgICAgICB9XG4gICAgfSxcblxuLy8gcmVzZXRzIHRoZSBsZXhlciwgc2V0cyBuZXcgaW5wdXRcbnNldElucHV0OmZ1bmN0aW9uIChpbnB1dCwgeXkpIHtcbiAgICAgICAgdGhpcy55eSA9IHl5IHx8IHRoaXMueXkgfHwge307XG4gICAgICAgIHRoaXMuX2lucHV0ID0gaW5wdXQ7XG4gICAgICAgIHRoaXMuX21vcmUgPSB0aGlzLl9iYWNrdHJhY2sgPSB0aGlzLmRvbmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy55eWxpbmVubyA9IHRoaXMueXlsZW5nID0gMDtcbiAgICAgICAgdGhpcy55eXRleHQgPSB0aGlzLm1hdGNoZWQgPSB0aGlzLm1hdGNoID0gJyc7XG4gICAgICAgIHRoaXMuY29uZGl0aW9uU3RhY2sgPSBbJ0lOSVRJQUwnXTtcbiAgICAgICAgdGhpcy55eWxsb2MgPSB7XG4gICAgICAgICAgICBmaXJzdF9saW5lOiAxLFxuICAgICAgICAgICAgZmlyc3RfY29sdW1uOiAwLFxuICAgICAgICAgICAgbGFzdF9saW5lOiAxLFxuICAgICAgICAgICAgbGFzdF9jb2x1bW46IDBcbiAgICAgICAgfTtcbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5yYW5nZXMpIHtcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLnJhbmdlID0gWzAsMF07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5vZmZzZXQgPSAwO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9LFxuXG4vLyBjb25zdW1lcyBhbmQgcmV0dXJucyBvbmUgY2hhciBmcm9tIHRoZSBpbnB1dFxuaW5wdXQ6ZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgY2ggPSB0aGlzLl9pbnB1dFswXTtcbiAgICAgICAgdGhpcy55eXRleHQgKz0gY2g7XG4gICAgICAgIHRoaXMueXlsZW5nKys7XG4gICAgICAgIHRoaXMub2Zmc2V0Kys7XG4gICAgICAgIHRoaXMubWF0Y2ggKz0gY2g7XG4gICAgICAgIHRoaXMubWF0Y2hlZCArPSBjaDtcbiAgICAgICAgdmFyIGxpbmVzID0gY2gubWF0Y2goLyg/Olxcclxcbj98XFxuKS4qL2cpO1xuICAgICAgICBpZiAobGluZXMpIHtcbiAgICAgICAgICAgIHRoaXMueXlsaW5lbm8rKztcbiAgICAgICAgICAgIHRoaXMueXlsbG9jLmxhc3RfbGluZSsrO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy55eWxsb2MubGFzdF9jb2x1bW4rKztcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLnJhbmdlcykge1xuICAgICAgICAgICAgdGhpcy55eWxsb2MucmFuZ2VbMV0rKztcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2lucHV0ID0gdGhpcy5faW5wdXQuc2xpY2UoMSk7XG4gICAgICAgIHJldHVybiBjaDtcbiAgICB9LFxuXG4vLyB1bnNoaWZ0cyBvbmUgY2hhciAob3IgYSBzdHJpbmcpIGludG8gdGhlIGlucHV0XG51bnB1dDpmdW5jdGlvbiAoY2gpIHtcbiAgICAgICAgdmFyIGxlbiA9IGNoLmxlbmd0aDtcbiAgICAgICAgdmFyIGxpbmVzID0gY2guc3BsaXQoLyg/Olxcclxcbj98XFxuKS9nKTtcblxuICAgICAgICB0aGlzLl9pbnB1dCA9IGNoICsgdGhpcy5faW5wdXQ7XG4gICAgICAgIHRoaXMueXl0ZXh0ID0gdGhpcy55eXRleHQuc3Vic3RyKDAsIHRoaXMueXl0ZXh0Lmxlbmd0aCAtIGxlbik7XG4gICAgICAgIC8vdGhpcy55eWxlbmcgLT0gbGVuO1xuICAgICAgICB0aGlzLm9mZnNldCAtPSBsZW47XG4gICAgICAgIHZhciBvbGRMaW5lcyA9IHRoaXMubWF0Y2guc3BsaXQoLyg/Olxcclxcbj98XFxuKS9nKTtcbiAgICAgICAgdGhpcy5tYXRjaCA9IHRoaXMubWF0Y2guc3Vic3RyKDAsIHRoaXMubWF0Y2gubGVuZ3RoIC0gMSk7XG4gICAgICAgIHRoaXMubWF0Y2hlZCA9IHRoaXMubWF0Y2hlZC5zdWJzdHIoMCwgdGhpcy5tYXRjaGVkLmxlbmd0aCAtIDEpO1xuXG4gICAgICAgIGlmIChsaW5lcy5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGluZW5vIC09IGxpbmVzLmxlbmd0aCAtIDE7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHIgPSB0aGlzLnl5bGxvYy5yYW5nZTtcblxuICAgICAgICB0aGlzLnl5bGxvYyA9IHtcbiAgICAgICAgICAgIGZpcnN0X2xpbmU6IHRoaXMueXlsbG9jLmZpcnN0X2xpbmUsXG4gICAgICAgICAgICBsYXN0X2xpbmU6IHRoaXMueXlsaW5lbm8gKyAxLFxuICAgICAgICAgICAgZmlyc3RfY29sdW1uOiB0aGlzLnl5bGxvYy5maXJzdF9jb2x1bW4sXG4gICAgICAgICAgICBsYXN0X2NvbHVtbjogbGluZXMgP1xuICAgICAgICAgICAgICAgIChsaW5lcy5sZW5ndGggPT09IG9sZExpbmVzLmxlbmd0aCA/IHRoaXMueXlsbG9jLmZpcnN0X2NvbHVtbiA6IDApXG4gICAgICAgICAgICAgICAgICsgb2xkTGluZXNbb2xkTGluZXMubGVuZ3RoIC0gbGluZXMubGVuZ3RoXS5sZW5ndGggLSBsaW5lc1swXS5sZW5ndGggOlxuICAgICAgICAgICAgICB0aGlzLnl5bGxvYy5maXJzdF9jb2x1bW4gLSBsZW5cbiAgICAgICAgfTtcblxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLnJhbmdlcykge1xuICAgICAgICAgICAgdGhpcy55eWxsb2MucmFuZ2UgPSBbclswXSwgclswXSArIHRoaXMueXlsZW5nIC0gbGVuXTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnl5bGVuZyA9IHRoaXMueXl0ZXh0Lmxlbmd0aDtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfSxcblxuLy8gV2hlbiBjYWxsZWQgZnJvbSBhY3Rpb24sIGNhY2hlcyBtYXRjaGVkIHRleHQgYW5kIGFwcGVuZHMgaXQgb24gbmV4dCBhY3Rpb25cbm1vcmU6ZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLl9tb3JlID0gdHJ1ZTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfSxcblxuLy8gV2hlbiBjYWxsZWQgZnJvbSBhY3Rpb24sIHNpZ25hbHMgdGhlIGxleGVyIHRoYXQgdGhpcyBydWxlIGZhaWxzIHRvIG1hdGNoIHRoZSBpbnB1dCwgc28gdGhlIG5leHQgbWF0Y2hpbmcgcnVsZSAocmVnZXgpIHNob3VsZCBiZSB0ZXN0ZWQgaW5zdGVhZC5cbnJlamVjdDpmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYmFja3RyYWNrX2xleGVyKSB7XG4gICAgICAgICAgICB0aGlzLl9iYWNrdHJhY2sgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMucGFyc2VFcnJvcignTGV4aWNhbCBlcnJvciBvbiBsaW5lICcgKyAodGhpcy55eWxpbmVubyArIDEpICsgJy4gWW91IGNhbiBvbmx5IGludm9rZSByZWplY3QoKSBpbiB0aGUgbGV4ZXIgd2hlbiB0aGUgbGV4ZXIgaXMgb2YgdGhlIGJhY2t0cmFja2luZyBwZXJzdWFzaW9uIChvcHRpb25zLmJhY2t0cmFja19sZXhlciA9IHRydWUpLlxcbicgKyB0aGlzLnNob3dQb3NpdGlvbigpLCB7XG4gICAgICAgICAgICAgICAgdGV4dDogXCJcIixcbiAgICAgICAgICAgICAgICB0b2tlbjogbnVsbCxcbiAgICAgICAgICAgICAgICBsaW5lOiB0aGlzLnl5bGluZW5vXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sXG5cbi8vIHJldGFpbiBmaXJzdCBuIGNoYXJhY3RlcnMgb2YgdGhlIG1hdGNoXG5sZXNzOmZ1bmN0aW9uIChuKSB7XG4gICAgICAgIHRoaXMudW5wdXQodGhpcy5tYXRjaC5zbGljZShuKSk7XG4gICAgfSxcblxuLy8gZGlzcGxheXMgYWxyZWFkeSBtYXRjaGVkIGlucHV0LCBpLmUuIGZvciBlcnJvciBtZXNzYWdlc1xucGFzdElucHV0OmZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHBhc3QgPSB0aGlzLm1hdGNoZWQuc3Vic3RyKDAsIHRoaXMubWF0Y2hlZC5sZW5ndGggLSB0aGlzLm1hdGNoLmxlbmd0aCk7XG4gICAgICAgIHJldHVybiAocGFzdC5sZW5ndGggPiAyMCA/ICcuLi4nOicnKSArIHBhc3Quc3Vic3RyKC0yMCkucmVwbGFjZSgvXFxuL2csIFwiXCIpO1xuICAgIH0sXG5cbi8vIGRpc3BsYXlzIHVwY29taW5nIGlucHV0LCBpLmUuIGZvciBlcnJvciBtZXNzYWdlc1xudXBjb21pbmdJbnB1dDpmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBuZXh0ID0gdGhpcy5tYXRjaDtcbiAgICAgICAgaWYgKG5leHQubGVuZ3RoIDwgMjApIHtcbiAgICAgICAgICAgIG5leHQgKz0gdGhpcy5faW5wdXQuc3Vic3RyKDAsIDIwLW5leHQubGVuZ3RoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gKG5leHQuc3Vic3RyKDAsMjApICsgKG5leHQubGVuZ3RoID4gMjAgPyAnLi4uJyA6ICcnKSkucmVwbGFjZSgvXFxuL2csIFwiXCIpO1xuICAgIH0sXG5cbi8vIGRpc3BsYXlzIHRoZSBjaGFyYWN0ZXIgcG9zaXRpb24gd2hlcmUgdGhlIGxleGluZyBlcnJvciBvY2N1cnJlZCwgaS5lLiBmb3IgZXJyb3IgbWVzc2FnZXNcbnNob3dQb3NpdGlvbjpmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwcmUgPSB0aGlzLnBhc3RJbnB1dCgpO1xuICAgICAgICB2YXIgYyA9IG5ldyBBcnJheShwcmUubGVuZ3RoICsgMSkuam9pbihcIi1cIik7XG4gICAgICAgIHJldHVybiBwcmUgKyB0aGlzLnVwY29taW5nSW5wdXQoKSArIFwiXFxuXCIgKyBjICsgXCJeXCI7XG4gICAgfSxcblxuLy8gdGVzdCB0aGUgbGV4ZWQgdG9rZW46IHJldHVybiBGQUxTRSB3aGVuIG5vdCBhIG1hdGNoLCBvdGhlcndpc2UgcmV0dXJuIHRva2VuXG50ZXN0X21hdGNoOmZ1bmN0aW9uKG1hdGNoLCBpbmRleGVkX3J1bGUpIHtcbiAgICAgICAgdmFyIHRva2VuLFxuICAgICAgICAgICAgbGluZXMsXG4gICAgICAgICAgICBiYWNrdXA7XG5cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5iYWNrdHJhY2tfbGV4ZXIpIHtcbiAgICAgICAgICAgIC8vIHNhdmUgY29udGV4dFxuICAgICAgICAgICAgYmFja3VwID0ge1xuICAgICAgICAgICAgICAgIHl5bGluZW5vOiB0aGlzLnl5bGluZW5vLFxuICAgICAgICAgICAgICAgIHl5bGxvYzoge1xuICAgICAgICAgICAgICAgICAgICBmaXJzdF9saW5lOiB0aGlzLnl5bGxvYy5maXJzdF9saW5lLFxuICAgICAgICAgICAgICAgICAgICBsYXN0X2xpbmU6IHRoaXMubGFzdF9saW5lLFxuICAgICAgICAgICAgICAgICAgICBmaXJzdF9jb2x1bW46IHRoaXMueXlsbG9jLmZpcnN0X2NvbHVtbixcbiAgICAgICAgICAgICAgICAgICAgbGFzdF9jb2x1bW46IHRoaXMueXlsbG9jLmxhc3RfY29sdW1uXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB5eXRleHQ6IHRoaXMueXl0ZXh0LFxuICAgICAgICAgICAgICAgIG1hdGNoOiB0aGlzLm1hdGNoLFxuICAgICAgICAgICAgICAgIG1hdGNoZXM6IHRoaXMubWF0Y2hlcyxcbiAgICAgICAgICAgICAgICBtYXRjaGVkOiB0aGlzLm1hdGNoZWQsXG4gICAgICAgICAgICAgICAgeXlsZW5nOiB0aGlzLnl5bGVuZyxcbiAgICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMub2Zmc2V0LFxuICAgICAgICAgICAgICAgIF9tb3JlOiB0aGlzLl9tb3JlLFxuICAgICAgICAgICAgICAgIF9pbnB1dDogdGhpcy5faW5wdXQsXG4gICAgICAgICAgICAgICAgeXk6IHRoaXMueXksXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uU3RhY2s6IHRoaXMuY29uZGl0aW9uU3RhY2suc2xpY2UoMCksXG4gICAgICAgICAgICAgICAgZG9uZTogdGhpcy5kb25lXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5yYW5nZXMpIHtcbiAgICAgICAgICAgICAgICBiYWNrdXAueXlsbG9jLnJhbmdlID0gdGhpcy55eWxsb2MucmFuZ2Uuc2xpY2UoMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBsaW5lcyA9IG1hdGNoWzBdLm1hdGNoKC8oPzpcXHJcXG4/fFxcbikuKi9nKTtcbiAgICAgICAgaWYgKGxpbmVzKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGluZW5vICs9IGxpbmVzLmxlbmd0aDtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnl5bGxvYyA9IHtcbiAgICAgICAgICAgIGZpcnN0X2xpbmU6IHRoaXMueXlsbG9jLmxhc3RfbGluZSxcbiAgICAgICAgICAgIGxhc3RfbGluZTogdGhpcy55eWxpbmVubyArIDEsXG4gICAgICAgICAgICBmaXJzdF9jb2x1bW46IHRoaXMueXlsbG9jLmxhc3RfY29sdW1uLFxuICAgICAgICAgICAgbGFzdF9jb2x1bW46IGxpbmVzID9cbiAgICAgICAgICAgICAgICAgICAgICAgICBsaW5lc1tsaW5lcy5sZW5ndGggLSAxXS5sZW5ndGggLSBsaW5lc1tsaW5lcy5sZW5ndGggLSAxXS5tYXRjaCgvXFxyP1xcbj8vKVswXS5sZW5ndGggOlxuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMueXlsbG9jLmxhc3RfY29sdW1uICsgbWF0Y2hbMF0ubGVuZ3RoXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMueXl0ZXh0ICs9IG1hdGNoWzBdO1xuICAgICAgICB0aGlzLm1hdGNoICs9IG1hdGNoWzBdO1xuICAgICAgICB0aGlzLm1hdGNoZXMgPSBtYXRjaDtcbiAgICAgICAgdGhpcy55eWxlbmcgPSB0aGlzLnl5dGV4dC5sZW5ndGg7XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMucmFuZ2VzKSB7XG4gICAgICAgICAgICB0aGlzLnl5bGxvYy5yYW5nZSA9IFt0aGlzLm9mZnNldCwgdGhpcy5vZmZzZXQgKz0gdGhpcy55eWxlbmddO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX21vcmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fYmFja3RyYWNrID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2lucHV0ID0gdGhpcy5faW5wdXQuc2xpY2UobWF0Y2hbMF0ubGVuZ3RoKTtcbiAgICAgICAgdGhpcy5tYXRjaGVkICs9IG1hdGNoWzBdO1xuICAgICAgICB0b2tlbiA9IHRoaXMucGVyZm9ybUFjdGlvbi5jYWxsKHRoaXMsIHRoaXMueXksIHRoaXMsIGluZGV4ZWRfcnVsZSwgdGhpcy5jb25kaXRpb25TdGFja1t0aGlzLmNvbmRpdGlvblN0YWNrLmxlbmd0aCAtIDFdKTtcbiAgICAgICAgaWYgKHRoaXMuZG9uZSAmJiB0aGlzLl9pbnB1dCkge1xuICAgICAgICAgICAgdGhpcy5kb25lID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRva2VuKSB7XG4gICAgICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5fYmFja3RyYWNrKSB7XG4gICAgICAgICAgICAvLyByZWNvdmVyIGNvbnRleHRcbiAgICAgICAgICAgIGZvciAodmFyIGsgaW4gYmFja3VwKSB7XG4gICAgICAgICAgICAgICAgdGhpc1trXSA9IGJhY2t1cFtrXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBmYWxzZTsgLy8gcnVsZSBhY3Rpb24gY2FsbGVkIHJlamVjdCgpIGltcGx5aW5nIHRoZSBuZXh0IHJ1bGUgc2hvdWxkIGJlIHRlc3RlZCBpbnN0ZWFkLlxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9LFxuXG4vLyByZXR1cm4gbmV4dCBtYXRjaCBpbiBpbnB1dFxubmV4dDpmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLmRvbmUpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLkVPRjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIXRoaXMuX2lucHV0KSB7XG4gICAgICAgICAgICB0aGlzLmRvbmUgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHRva2VuLFxuICAgICAgICAgICAgbWF0Y2gsXG4gICAgICAgICAgICB0ZW1wTWF0Y2gsXG4gICAgICAgICAgICBpbmRleDtcbiAgICAgICAgaWYgKCF0aGlzLl9tb3JlKSB7XG4gICAgICAgICAgICB0aGlzLnl5dGV4dCA9ICcnO1xuICAgICAgICAgICAgdGhpcy5tYXRjaCA9ICcnO1xuICAgICAgICB9XG4gICAgICAgIHZhciBydWxlcyA9IHRoaXMuX2N1cnJlbnRSdWxlcygpO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJ1bGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB0ZW1wTWF0Y2ggPSB0aGlzLl9pbnB1dC5tYXRjaCh0aGlzLnJ1bGVzW3J1bGVzW2ldXSk7XG4gICAgICAgICAgICBpZiAodGVtcE1hdGNoICYmICghbWF0Y2ggfHwgdGVtcE1hdGNoWzBdLmxlbmd0aCA+IG1hdGNoWzBdLmxlbmd0aCkpIHtcbiAgICAgICAgICAgICAgICBtYXRjaCA9IHRlbXBNYXRjaDtcbiAgICAgICAgICAgICAgICBpbmRleCA9IGk7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5iYWNrdHJhY2tfbGV4ZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgdG9rZW4gPSB0aGlzLnRlc3RfbWF0Y2godGVtcE1hdGNoLCBydWxlc1tpXSk7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0b2tlbiAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0b2tlbjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9iYWNrdHJhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hdGNoID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTsgLy8gcnVsZSBhY3Rpb24gY2FsbGVkIHJlamVjdCgpIGltcGx5aW5nIGEgcnVsZSBNSVNtYXRjaC5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGVsc2U6IHRoaXMgaXMgYSBsZXhlciBydWxlIHdoaWNoIGNvbnN1bWVzIGlucHV0IHdpdGhvdXQgcHJvZHVjaW5nIGEgdG9rZW4gKGUuZy4gd2hpdGVzcGFjZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXRoaXMub3B0aW9ucy5mbGV4KSB7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAobWF0Y2gpIHtcbiAgICAgICAgICAgIHRva2VuID0gdGhpcy50ZXN0X21hdGNoKG1hdGNoLCBydWxlc1tpbmRleF0pO1xuICAgICAgICAgICAgaWYgKHRva2VuICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0b2tlbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIGVsc2U6IHRoaXMgaXMgYSBsZXhlciBydWxlIHdoaWNoIGNvbnN1bWVzIGlucHV0IHdpdGhvdXQgcHJvZHVjaW5nIGEgdG9rZW4gKGUuZy4gd2hpdGVzcGFjZSlcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5faW5wdXQgPT09IFwiXCIpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLkVPRjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlRXJyb3IoJ0xleGljYWwgZXJyb3Igb24gbGluZSAnICsgKHRoaXMueXlsaW5lbm8gKyAxKSArICcuIFVucmVjb2duaXplZCB0ZXh0LlxcbicgKyB0aGlzLnNob3dQb3NpdGlvbigpLCB7XG4gICAgICAgICAgICAgICAgdGV4dDogXCJcIixcbiAgICAgICAgICAgICAgICB0b2tlbjogbnVsbCxcbiAgICAgICAgICAgICAgICBsaW5lOiB0aGlzLnl5bGluZW5vXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH0sXG5cbi8vIHJldHVybiBuZXh0IG1hdGNoIHRoYXQgaGFzIGEgdG9rZW5cbmxleDpmdW5jdGlvbiBsZXggKCkge1xuICAgICAgICB2YXIgciA9IHRoaXMubmV4dCgpO1xuICAgICAgICBpZiAocikge1xuICAgICAgICAgICAgcmV0dXJuIHI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sZXgoKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbi8vIGFjdGl2YXRlcyBhIG5ldyBsZXhlciBjb25kaXRpb24gc3RhdGUgKHB1c2hlcyB0aGUgbmV3IGxleGVyIGNvbmRpdGlvbiBzdGF0ZSBvbnRvIHRoZSBjb25kaXRpb24gc3RhY2spXG5iZWdpbjpmdW5jdGlvbiBiZWdpbiAoY29uZGl0aW9uKSB7XG4gICAgICAgIHRoaXMuY29uZGl0aW9uU3RhY2sucHVzaChjb25kaXRpb24pO1xuICAgIH0sXG5cbi8vIHBvcCB0aGUgcHJldmlvdXNseSBhY3RpdmUgbGV4ZXIgY29uZGl0aW9uIHN0YXRlIG9mZiB0aGUgY29uZGl0aW9uIHN0YWNrXG5wb3BTdGF0ZTpmdW5jdGlvbiBwb3BTdGF0ZSAoKSB7XG4gICAgICAgIHZhciBuID0gdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGggLSAxO1xuICAgICAgICBpZiAobiA+IDApIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvblN0YWNrLnBvcCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29uZGl0aW9uU3RhY2tbMF07XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyBwcm9kdWNlIHRoZSBsZXhlciBydWxlIHNldCB3aGljaCBpcyBhY3RpdmUgZm9yIHRoZSBjdXJyZW50bHkgYWN0aXZlIGxleGVyIGNvbmRpdGlvbiBzdGF0ZVxuX2N1cnJlbnRSdWxlczpmdW5jdGlvbiBfY3VycmVudFJ1bGVzICgpIHtcbiAgICAgICAgaWYgKHRoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoICYmIHRoaXMuY29uZGl0aW9uU3RhY2tbdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGggLSAxXSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29uZGl0aW9uc1t0aGlzLmNvbmRpdGlvblN0YWNrW3RoaXMuY29uZGl0aW9uU3RhY2subGVuZ3RoIC0gMV1dLnJ1bGVzO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29uZGl0aW9uc1tcIklOSVRJQUxcIl0ucnVsZXM7XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyByZXR1cm4gdGhlIGN1cnJlbnRseSBhY3RpdmUgbGV4ZXIgY29uZGl0aW9uIHN0YXRlOyB3aGVuIGFuIGluZGV4IGFyZ3VtZW50IGlzIHByb3ZpZGVkIGl0IHByb2R1Y2VzIHRoZSBOLXRoIHByZXZpb3VzIGNvbmRpdGlvbiBzdGF0ZSwgaWYgYXZhaWxhYmxlXG50b3BTdGF0ZTpmdW5jdGlvbiB0b3BTdGF0ZSAobikge1xuICAgICAgICBuID0gdGhpcy5jb25kaXRpb25TdGFjay5sZW5ndGggLSAxIC0gTWF0aC5hYnMobiB8fCAwKTtcbiAgICAgICAgaWYgKG4gPj0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29uZGl0aW9uU3RhY2tbbl07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gXCJJTklUSUFMXCI7XG4gICAgICAgIH1cbiAgICB9LFxuXG4vLyBhbGlhcyBmb3IgYmVnaW4oY29uZGl0aW9uKVxucHVzaFN0YXRlOmZ1bmN0aW9uIHB1c2hTdGF0ZSAoY29uZGl0aW9uKSB7XG4gICAgICAgIHRoaXMuYmVnaW4oY29uZGl0aW9uKTtcbiAgICB9LFxuXG4vLyByZXR1cm4gdGhlIG51bWJlciBvZiBzdGF0ZXMgY3VycmVudGx5IG9uIHRoZSBzdGFja1xuc3RhdGVTdGFja1NpemU6ZnVuY3Rpb24gc3RhdGVTdGFja1NpemUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbmRpdGlvblN0YWNrLmxlbmd0aDtcbiAgICB9LFxub3B0aW9uczoge30sXG5wZXJmb3JtQWN0aW9uOiBmdW5jdGlvbiBhbm9ueW1vdXMoeXkseXlfLCRhdm9pZGluZ19uYW1lX2NvbGxpc2lvbnMsWVlfU1RBUlQpIHtcbnZhciBZWVNUQVRFPVlZX1NUQVJUO1xuc3dpdGNoKCRhdm9pZGluZ19uYW1lX2NvbGxpc2lvbnMpIHtcbmNhc2UgMDovKiBza2lwIHdoaXRlc3BhY2UgKi9cbmJyZWFrO1xuY2FzZSAxOnl5Xy55eXRleHQgPSB5eV8ueXl0ZXh0LnN1YnN0cigxLHl5Xy55eWxlbmctMik7IHJldHVybiAxNlxuYnJlYWs7XG5jYXNlIDI6cmV0dXJuIDE3XG5icmVhaztcbmNhc2UgMzpyZXR1cm4gOFxuYnJlYWs7XG5jYXNlIDQ6cmV0dXJuIDE5XG5icmVhaztcbmNhc2UgNTpyZXR1cm4gMTBcbmJyZWFrO1xuY2FzZSA2OnJldHVybiAxOFxuYnJlYWs7XG5jYXNlIDc6cmV0dXJuIDEyXG5icmVhaztcbmNhc2UgODpyZXR1cm4gMTRcbmJyZWFrO1xuY2FzZSA5OnJldHVybiAnQU5ZJ1xuYnJlYWs7XG5jYXNlIDEwOnJldHVybiAnSU5WQUxJRCdcbmJyZWFrO1xuY2FzZSAxMTpyZXR1cm4gNVxuYnJlYWs7XG59XG59LFxucnVsZXM6IFsvXig/OlxccyspLywvXig/OicoLikqJykvLC9eKD86W2EtekEtWl1bYS16QS1aMC05XFwtX10qfFxcKikvLC9eKD86OikvLC9eKD86LCkvLC9eKD86OykvLC9eKD86WzAtOV0rKS8sL14oPzotKS8sL14oPzo9PikvLC9eKD86XFwqKS8sL14oPzouKS8sL14oPzokKS9dLFxuY29uZGl0aW9uczoge1wiSU5JVElBTFwiOntcInJ1bGVzXCI6WzAsMSwyLDMsNCw1LDYsNyw4LDksMTAsMTFdLFwiaW5jbHVzaXZlXCI6dHJ1ZX19XG59KTtcbnJldHVybiBsZXhlcjtcbn0pKCk7XG5wYXJzZXIubGV4ZXIgPSBsZXhlcjtcbmZ1bmN0aW9uIFBhcnNlciAoKSB7XG4gIHRoaXMueXkgPSB7fTtcbn1cblBhcnNlci5wcm90b3R5cGUgPSBwYXJzZXI7cGFyc2VyLlBhcnNlciA9IFBhcnNlcjtcbnJldHVybiBuZXcgUGFyc2VyO1xufSkoKTtcblxuXG5pZiAodHlwZW9mIHJlcXVpcmUgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBleHBvcnRzICE9PSAndW5kZWZpbmVkJykge1xuZXhwb3J0cy5wYXJzZXIgPSBydWxlc1BhcnNlcjtcbmV4cG9ydHMuUGFyc2VyID0gcnVsZXNQYXJzZXIuUGFyc2VyO1xuZXhwb3J0cy5wYXJzZSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHJ1bGVzUGFyc2VyLnBhcnNlLmFwcGx5KHJ1bGVzUGFyc2VyLCBhcmd1bWVudHMpOyB9O1xuZXhwb3J0cy5tYWluID0gZnVuY3Rpb24gY29tbW9uanNNYWluIChhcmdzKSB7XG4gICAgaWYgKCFhcmdzWzFdKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdVc2FnZTogJythcmdzWzBdKycgRklMRScpO1xuICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgfVxuICAgIHZhciBzb3VyY2UgPSByZXF1aXJlKCdmcycpLnJlYWRGaWxlU3luYyhyZXF1aXJlKCdwYXRoJykubm9ybWFsaXplKGFyZ3NbMV0pLCBcInV0ZjhcIik7XG4gICAgcmV0dXJuIGV4cG9ydHMucGFyc2VyLnBhcnNlKHNvdXJjZSk7XG59O1xuaWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnICYmIHJlcXVpcmUubWFpbiA9PT0gbW9kdWxlKSB7XG4gIGV4cG9ydHMubWFpbihwcm9jZXNzLmFyZ3Yuc2xpY2UoMSkpO1xufVxufSJdfQ==
